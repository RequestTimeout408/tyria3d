var gulp = require('gulp');

var args = require('yargs').argv;

var compass = require('gulp-compass');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');

var browserify = require('browserify');

var source = require('vinyl-source-stream');
var CombinedStream = require('combined-stream');
var watch = require('gulp-watch');

var streamify = require('gulp-streamify');
var uglify = require('gulp-uglify');

var errorHandler = function (msg) {
    return function (error) {
        console.error(msg);
        console.error(error.stack);
    };
};

var buildJS = function(module) {
	
	var b = browserify(
		{
			'opts.basedir' : './',
			entries : module.entries,
			debug : true,
		}
	);

	//b.external('T3D');

	var bundleFile = CombinedStream
		.create()

		/// Bundle browsified scripts
		.append(b.bundle({external: 'T3D'}))

		/// Error handler
		.on('error', errorHandler("Javascript build failed"))

		/// Complete handler
        .on('end', function () { console.log("JS Build finished"); })

		/// Write to file
		.pipe( source(module.bundle) )

		/// Minify
		//.pipe(streamify(uglify()))
		
		/// Move to build directory
		.pipe(gulp.dest('js/build'));
};

var buildCompass = function(release) {
	//TODO: Only min if release is true
	return gulp.src('./css/sass/main.scss').pipe(compass({
		config_file : 'config.rb',
		sass : './css/sass',
		css : './css'
	})).on('error', function(err) {
		console.log("SASS ERROR " + err);
	}).pipe(rename({
		suffix : '.min'
	})).pipe(minifyCSS()).pipe(gulp.dest('./css/'));
};

gulp.task('compass', function() {
	return buildCompass(true);
});

gulp.task('API', function() {
	var b = browserify('./js/tyria3d/area/API/Tyria3DAPI.js');


	var bundleFile = CombinedStream.create()

	/// Bundle browsified scripts
	.append(b.bundle({standalone: 'T3D'}))

	/// Error handler
	.on('error', errorHandler("API build failed"))

	/// Complete handler
    .on('end', function () { console.log("API finished"); })

	/// Write to file
	.pipe( source("API.js") )

	/// Minify
	//.pipe(streamify(uglify()))
	
	/// Move to build directory
	.pipe(gulp.dest('js/build'));
});

gulp.task('watch', function() {
	
	var modules ={
		APP:{
			entries:['./js/tyria3d/area/APP/Tyria3DApp.js'],
			glob:"./js/tyria3d/area/APP/**/*.js",
			bundle:'t3d-app-1.0.3.min.js',
		},
		//planet:{
		//	entries:['./js/tyria3d/planet/main.js'],
		//	glob:"./js/tyria3d/planet/**/*.js",
		//	bundle:'bundle-planet.js'
		//}
	};
	
	/// Build Bundle when APP JS Changes
	watch({
		glob : modules.APP.glob
	}, function(files) {
		return buildJS(modules.APP);
	});

	/// Build CSS bundle when SASS changes
	watch({
		glob : "./css/**/*.scss"
	}, function(files) {
		return buildCompass(false);
	});
});

gulp.task('default', ['watch']);

