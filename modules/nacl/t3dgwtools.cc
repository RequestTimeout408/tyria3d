


// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/// @file file_io.cc
/// This example demonstrates the use of persistent file I/O

#define __STDC_LIMIT_MACROS
#include <stdio.h>

#include <sstream>
#include <string>
//#include <vector>

#include "ppapi/c/pp_stdint.h"
#include "ppapi/c/ppb_file_io.h"
#include "ppapi/cpp/directory_entry.h"
#include "ppapi/cpp/file_io.h"
#include "ppapi/cpp/file_ref.h"
#include "ppapi/cpp/file_system.h"
#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/message_loop.h"
#include "ppapi/cpp/module.h"
#include "ppapi/cpp/var.h"
#include "ppapi/cpp/var_array.h"
#include "ppapi/cpp/var_array_buffer.h"
#include "ppapi/utility/completion_callback_factory.h"
#include "ppapi/utility/threading/simple_thread.h"


#include "gw2DatTools\compression\inflateDatFileBuffer.cpp"
#include "gw2DatTools\compression\inflateTextureFileBuffer.cpp"

#include "gw2DatTools\exception\Exception.h"

#include "gw2formats\TextureFile.cpp"


#include "squish/squish.cpp"

#ifndef INT32_MAX
#define INT32_MAX (0x7FFFFFFF)
#endif

#ifdef WIN32
#undef min
#undef max
#undef PostMessage

// Allow 'this' in initializer list
#pragma warning(disable : 4355)
#endif


//namespace {
//typedef std::vector<std::string> StringVector;
//}

/// The Instance class.  One of these exists for each instance of your NaCl
/// module on the web page.  The browser will ask the Module object to create
/// a new Instance for each occurrence of the <embed> tag that has these
/// attributes:
///     type="application/x-nacl"
///     src="file_io.nmf"





////
//// BEGIN 3DCX
////

typedef unsigned int            uint;       /**< Short for 'unsigned int'. */
typedef uint8_t                 uint8;      /**< Unsigned 8-bit integer. */
typedef uint32_t                uint32;     /**< Unsigned 32-bit integer. */
typedef uint64_t                uint64;     /**< Unsigned 64-bit integer. */
typedef uint16_t                uint16;     /**< Unsigned 16-bit integer. */

union RGBA
{
  struct {
    uint8 r;
    uint8 g;
    uint8 b;
    uint8 a;
  };
  uint8 parts[4];
  uint32 color;
};

struct BGR
{
  uint8   b;
  uint8   g;
  uint8   r;
};

struct RGB
{
  uint8   r;
  uint8   g;
  uint8   b;
};

struct DCXBlock     // Should be 3DCXBlock, but names can't start with a number D:
{
  uint64  green;
  uint64  red;
};

template <typename T>
T* allocate(uint p_count)
{
  return static_cast<T*>(::malloc(p_count * sizeof(T)));
}

void process3DCXBlock(RGB* p_colors, const DCXBlock& p_block, uint p_blockX, uint p_blockY, uint p_width)
{
  const float floatToByte = 127.5f;
  const float byteToFloat = (1.0f / floatToByte);

  uint64 red = p_block.red;
  uint64 green = p_block.green;
  uint8 reds[8];
  uint8 greens[8];

  // Reds 1 and 2
  reds[0] = (red & 0xff);
  reds[1] = (red & 0xff00) >> 8;
  red >>= 16;
  // Reds 3 to 8
  if (reds[0] > reds[1]) {
    for (uint i = 2; i < 8; i++) {
      reds[i] = ((8 - i) * reds[0] + (i - 1) * reds[1]) / 7;
    }
  }
  else {
    for (uint i = 2; i < 6; i++) {
      reds[i] = ((6 - i) * reds[0] + (i - 1) * reds[1]) / 5;
    }
    reds[6] = 0x00;
    reds[7] = 0xff;
  }
  // Greens 1 and 2
  greens[0] = (green & 0xff);
  greens[1] = (green & 0xff00) >> 8;
  green >>= 16;
  //Greens 3 to 8
  if (greens[0] > greens[1]) {
    for (uint i = 2; i < 8; i++) {
      greens[i] = ((8 - i) * greens[0] + (i - 1) * greens[1]) / 7;
    }
  }
  else {
    for (uint i = 2; i < 6; i++) {
      greens[i] = ((6 - i) * greens[0] + (i - 1) * greens[1]) / 5;
    }
    greens[6] = 0x00;
    greens[7] = 0xff;
  }

  struct { float r; float g; float b; } normal;
  for (uint y = 0; y < 4; y++) {
    uint curPixel = (p_blockY + y) * p_width + p_blockX;

    for (uint x = 0; x < 4; x++) {
      RGB& color = p_colors[curPixel];

      // Get normal
      normal.r = ((float)reds[red & 7] * byteToFloat) - 1.0f;
      normal.g = ((float)greens[green & 7] * byteToFloat) - 1.0f;

      // Compute blue, based on red/green
      normal.b = ::sqrt(1.0f - normal.r * normal.r - normal.g * normal.g);

      // Store normal
      color.r = ((normal.r + 1.0f) * floatToByte);
      color.g = ((normal.g + 1.0f) * floatToByte);
      color.b = ((normal.b + 1.0f) * floatToByte);

      // Invert green as that seems to be the more common format
      color.g = 0xff - color.g;

      curPixel++;
      red >>= 3;
      green >>= 3;
    }
  }
}

void process3DCX(const RGBA* p_data, uint p_width, uint p_height, BGR*& po_colors, uint8*& po_alphas)
{
  uint numPixels = (p_width * p_height);
  uint numBlocks = numPixels >> 4;
  const DCXBlock* blocks = reinterpret_cast<const DCXBlock*>(p_data);

  po_colors = allocate<BGR>(numPixels);
  po_alphas = nullptr; // 3DCX does not use alpha

  const uint numHorizBlocks = p_width >> 2;
  const uint numVertBlocks = p_height >> 2;

#pragma omp parallel for
  for (int y = 0; y < static_cast<int>(numVertBlocks); y++) {
    for (uint x = 0; x < numHorizBlocks; x++)
    {
      const DCXBlock& block = blocks[(y * numHorizBlocks) + x];
      // 3DCX actually uses RGB and not BGR, so *pretend* that's what the output is
      process3DCXBlock(reinterpret_cast<RGB*>(po_colors), block, x * 4, y * 4, p_width);
    }
  }
}

////
//// END 3DCX
////


enum FormatFlags
{
  FF_COLOR = 0x10,
  FF_ALPHA = 0x20,
  FF_DEDUCEDALPHACOMP = 0x40,
  FF_PLAINCOMP = 0x80,
  FF_BICOLORCOMP = 0x200
};

enum CompressionFlags
{
  CF_DECODE_WHITE_COLOR = 0x01,
  CF_DECODE_CONSTANT_ALPHA_FROM4BITS = 0x02,
  CF_DECODE_CONSTANT_ALPHA_FROM8BITS = 0x04,
  CF_DECODE_PLAIN_COLOR = 0x08
};


pp::VarArray decompressGW1Tex(uint8_t* buffer, pp::Var handle, uint32_t size)
{
  int width = 64;//32;
  int height = width;

  /// Step 1 
  /// Get raw data and raw data size
  uint32_t inputSize = size;//handle.AsInt();
  // inputTab is buffer
  uint32_t outputSize  = ((width + 3) >> 2) * ((height + 3) >> 2);
  outputSize *= 8;///8 for dxt1 16 else

  



   // Initialize state
    gw2dt::compression::State aState;
    aState.input = reinterpret_cast<const uint32_t*>(buffer);
    aState.inputSize = inputSize /  4;
    aState.inputPos = 0;
    aState.head = 0;
    aState.bits = 0;
    aState.buffer = 0;
    aState.isEmpty = false;

    // Skipping header
    //needBits(aState, 32);
    //dropBits(aState, 32);
    
    // Format
    //needBits(aState, 32);
    //uint32_t aFormatFourCc = readBits(aState, 32);
    //dropBits(aState, 32);

    gw2dt::compression::texture::FullFormat aFullFormat;

    //aFullFormat.format = texture::deduceFormat(aFormatFourCc);
    gw2dt::compression::texture::Format aDxt1Format;
    aDxt1Format.flags = FF_COLOR | FF_ALPHA | FF_DEDUCEDALPHACOMP;
    aDxt1Format.pixelSizeInBits = 4;

    gw2dt::compression::texture::Format aDxt2Format;
    aDxt2Format.flags = FF_COLOR | FF_ALPHA | FF_PLAINCOMP;
    aDxt2Format.pixelSizeInBits = 8;

    
    //aFullFormat.format = texture::deduceFormat(aFormatFourCc);
    //aFullFormat.format = aDxt2Format;
    aFullFormat.format = aDxt1Format;


    aFullFormat.width = width;
    aFullFormat.height = height;


    aFullFormat.nbObPixelBlocks = ((aFullFormat.width + 3) / 4) * ((aFullFormat.height + 3) / 4);
    aFullFormat.bytesPerPixelBlock = (aFullFormat.format.pixelSizeInBits * 4 * 4) / 8;
    aFullFormat.hasTwoComponents = 
      ((aFullFormat.format.flags &
        (gw2dt::compression::texture::FF_PLAINCOMP | gw2dt::compression::texture::FF_COLOR | gw2dt::compression::texture::FF_ALPHA))
        ==
        (gw2dt::compression::texture::FF_PLAINCOMP | gw2dt::compression::texture::FF_COLOR | gw2dt::compression::texture::FF_ALPHA))
        ||
        (aFullFormat.format.flags & gw2dt::compression::texture::FF_BICOLORCOMP);

    aFullFormat.bytesPerComponent = aFullFormat.bytesPerPixelBlock / (aFullFormat.hasTwoComponents ? 2 : 1);
    
    uint32_t anOutputSize = outputSize*2;// aFullFormat.bytesPerPixelBlock * aFullFormat.nbObPixelBlocks;
    uint8_t* anOutputTab(nullptr);
    anOutputTab = static_cast<uint8_t*>(malloc(sizeof(uint8_t) * anOutputSize));




  /// Step 2 decompress
  /// GW2DATTOOLS_API uint8_t* GW2DATTOOLS_APIENTRY inflateTextureFileBuffer(
  /// texture::inflateData(aState, aFullFormat, ioOutputSize, anOutputTab);
  /// uint32_t iInputSize, const uint8_t* iInputTab,  uint32_t& ioOutputSize, uint8_t* ioOutputTab)
  /// 
  gw2dt::compression::texture::inflateDataGW1(aState, aFullFormat, anOutputSize, anOutputTab, inputSize);

  /// 
  /// Step 3 translate from DXT format to bitmap (quish)
  /// 

  int bm_size = width*height*4;
  squish::u8 pixels[bm_size];  // uncompressed pixles
  int dxtFormat = squish::kDxt1;
  

  //squish::DecompressImage( pixels, width, height, buffer, dxtFormat);
  squish::DecompressImage( pixels, width, height, anOutputTab, dxtFormat);


  ///
  /// Step 4 Populate outputArray
  ///
  auto outputBuffer = pp::VarArrayBuffer::VarArrayBuffer(bm_size);
  uint8_t* outputData = static_cast<uint8_t*>(outputBuffer.Map());
  
  memcpy(outputData,pixels,bm_size);
  
  pp::VarArray outArray = pp::VarArray::VarArray();
  outArray.SetLength(2);
  outArray.Set(0,outputBuffer);
  outArray.Set(1,handle);
  
  return outArray;

}




class FileIoInstance : public pp::Instance {
 public:
  /// The constructor creates the plugin-side instance.
  /// @param[in] instance the handle to the browser-side plugin instance.
  explicit FileIoInstance(PP_Instance instance)
      : pp::Instance(instance)
       {}

  virtual ~FileIoInstance() { }

  virtual bool Init(uint32_t /*argc*/,
                    const char * /*argn*/ [],
                    const char * /*argv*/ []) {
   
    return true;
  }

 private:
  
  

  /// Handler for messages coming in from the browser via postMessage().  The
  /// @a var_message can contain anything: a JSON string; a string that encodes
  /// method names and arguments; etc.
  ///
  /// Here we use messages to communicate with the user interface
  ///
  /// @param[in] var_message The message posted by the browser.
  virtual void HandleMessage(const pp::Var& var_message) {


    if (!var_message.is_array())
      return;    

    bool hasError = false;
    int dxtType=0;
    int imgW=0;
    int imgH=0;
    pp::VarArray args;
    pp::Var handle;
    pp::VarArrayBuffer array_buffer_var;
    bool isImage = false;
    int32_t capLength = 0;
    uint8_t* inData;
    uint32_t byte_length;
    uint8_t* pmapInfBuffer;
    uint32_t mapInfSize;

    try{
      args = pp::VarArray::VarArray(var_message);

      /// First entry is handle Var just passtrough
      handle =  pp::Var(args.Get(0));


      

      /// Second entry is data buffer
      //pp::VarArrayBuffer array_buffer_var = pp::VarArrayBuffer::VarArrayBuffer(var_message);
      array_buffer_var = pp::VarArrayBuffer::VarArrayBuffer(args.Get(1));

      /// Third entry is boolean indicating if the file is an image
      isImage =  pp::Var(args.Get(2)).AsBool();
      capLength =  pp::Var(args.Get(3)).AsInt();

      if(args.Get(4).AsBool()){
        /// Try to read texture
        inData = static_cast<uint8_t*>(array_buffer_var.Map());
        
        PostMessage( decompressGW1Tex(inData, handle, args.Get(5).AsInt()) );
        return;
      }
      

      inData = static_cast<uint8_t*>(array_buffer_var.Map());
      byte_length = array_buffer_var.ByteLength();

      

      //mapInfSize =  ( (uint32_t * )pmapInfBuffer )[1];
      if(capLength>1){
        mapInfSize = capLength;
      }
      else{
        memcpy( &mapInfSize, inData + 4, sizeof( mapInfSize ) ); 
        //mapInfSize = 1024*1024*100;
      }

      //byte_length=mapInfSize;
      

      pmapInfBuffer = new uint8_t[mapInfSize];


    }
    catch (gw2dt::exception::Exception& caught)
    {
      PostMessage(pp::Var::Var(caught.what()));
      return;
    }
    catch (...)
    {
      PostMessage(pp::Var::Var(408));
      return;
    }

    try{

      /// Always inflate the dat entry
      gw2dt::compression::inflateDatFileBuffer(byte_length, inData, mapInfSize, pmapInfBuffer);

      /// Images need much more work...
      if(isImage){
       
        /// Read image file
        gw2f::TextureFile file(pmapInfBuffer, mapInfSize);

        /// Get mipmap info
        uint32_t mipCount = file.mipMapCount();

        if(mipCount==0){
          throw gw2dt::exception::Exception("No mipmap levels found!");
        }    
        auto& mipmap = file.mipMapLevel(0); //std::min(2/*0*/, mipCount)


        short width = mipmap.width();
        short height = mipmap.height();    

        // Allocate output buffer, if needed
        std::vector<uint8_t> buffer;
        if (buffer.size() < mipmap.uncompressedSize()) {
          buffer.resize(mipmap.uncompressedSize());
        }

        // Decompress mipmap
        uint32_t size = buffer.size();
    
        uint32_t inflatedImageSize = buffer.size();
        std::vector<uint8_t> imageBuffer;

        uint8_t* textureBuffer = nullptr;


        /// Translate DXT to bitmap
        uint32_t mipmapFormat = mipmap.format();

        //mipmapFormat = 0x35545844;

        ///Inflate mipmap 0 (highest quality)
        gw2dt::compression::inflateTextureBlockBuffer(
          width, height,
          mipmapFormat == 0x4c545844 ? 0x35545844 : mipmapFormat,
          mipmap.size(),
          mipmap.data(),
          size,
          buffer.data()
        );

        

        int bitmapSize=0;

        int dxtFormat=0;

        switch(mipmapFormat)
        {
          case 0x31545844: // DXT1
            dxtType=1;
            //bitmapSize = width*height*3;
            bitmapSize = width*height*4;
            dxtFormat = squish::kDxt1;
            break;

          case 0x33545844: // DXT3
            dxtType=3;
            bitmapSize = width*height*4;
            dxtFormat = squish::kDxt3;
            break;
        
          case 0x4c545844: // DXTL
          case 0x35545844: // DXT5
            dxtType=5;
            bitmapSize = width*height*4;
            dxtFormat = squish::kDxt5;
            break;

          case 0x58434433: // 3DCX
            bitmapSize = width*height*4;
            break;

          default:{
            std::stringstream message;
            message << "Unknown mipmap format " << mipmapFormat << " extracted size " << mipmap.uncompressedSize();
            throw gw2dt::exception::Exception(message.str());
          }
        }

        ///DXT
        squish::u8 pixels[bitmapSize];  // uncompressed pixles
        
        /// NOT 3DCX
        if(mipmapFormat != 0x58434433){          

          /// DXTL
          if( mipmapFormat == 0x4c545844 ){
          }
          
          squish::DecompressImage( pixels, width, height, buffer.data(), dxtFormat);

          /// DXTL
          if( mipmapFormat == 0x4c545844 ){
            for (uint i = 0; i < width * height * 4; i+=4) {
              pixels[i + 0] =  ( pixels[i + 0] * pixels[i + 3] )  / 0xff;
              pixels[i + 1] =  ( pixels[i + 1] * pixels[i + 3] )  / 0xff;
              pixels[i + 2] =  ( pixels[i + 2] * pixels[i + 3] )  / 0xff;
                         
            }
          }
        }

        /// 3DCX
        else{
          BGR* po_colors = nullptr;
          uint8* po_alphas = nullptr;
          process3DCX(
            reinterpret_cast<RGBA*>(buffer.data()),
            width,
            height,
            po_colors,
            po_alphas);

          //Loop through image and set all pixels
          for (int y = height - 1; y >= 0 ; y--) {
            for (int x = 0; x<width; x++)
            {
            
              pixels[ (x+y*width)*4 + 0] = (*po_colors).b;
              pixels[ (x+y*width)*4 + 1] = (*po_colors).g;
              pixels[ (x+y*width)*4 + 2] = (*po_colors).r;
              pixels[ (x+y*width)*4 + 3] = 255;
              po_colors++;


            }
          }

        }

        /// Pass bitmap back
        delete [] pmapInfBuffer;
        pmapInfBuffer = new uint8_t[bitmapSize];
        //pmapInfBuffer = pixels;
        memcpy(pmapInfBuffer,pixels,bitmapSize);
        

        /// Set width height and mem size for image
        mapInfSize = bitmapSize;
        imgH = height;
        imgW = width;    

        

      }/// ENd if is image and not too large


    }
    catch (gw2dt::exception::Exception& caught)
    {
      hasError = true;
      PostMessage(pp::Var::Var(caught.what()));
      //return;
    }
    catch (std::exception)
    {
      hasError = true;
      PostMessage(666);
      //return;
    }


    try{

      if(!hasError){
        auto outputBuffer = pp::VarArrayBuffer::VarArrayBuffer(mapInfSize);
        uint8_t* outputData = static_cast<uint8_t*>(outputBuffer.Map());
        
        memcpy(outputData,pmapInfBuffer,mapInfSize);
        
        pp::VarArray outArray = pp::VarArray::VarArray();
        outArray.SetLength(5);
        outArray.Set(0,handle);
        outArray.Set(1,outputBuffer);
        outArray.Set(2,dxtType);
        outArray.Set(3,imgW);
        outArray.Set(4,imgH);

        //PostMessage(array_buffer_var);
        PostMessage(outArray);
      }

      //delete[] inData;
      delete[] pmapInfBuffer;
    }
    catch(...){
      PostMessage(pp::Var::Var(10000));
    }

  }

};

/// The Module class.  The browser calls the CreateInstance() method to create
/// an instance of your NaCl module on the web page.  The browser creates a new
/// instance for each <embed> tag with type="application/x-nacl".
class FileIoModule : public pp::Module {
 public:
  FileIoModule() : pp::Module() {}
  virtual ~FileIoModule() {}

  /// Create and return a FileIoInstance object.
  /// @param[in] instance The browser-side instance.
  /// @return the plugin-side instance.
  virtual pp::Instance* CreateInstance(PP_Instance instance) {
    return new FileIoInstance(instance);
  }
};

namespace pp {
/// Factory function called by the browser when the module is first loaded.
/// The browser keeps a singleton of this module.  It calls the
/// CreateInstance() method on the object you return to make instances.  There
/// is one instance per <embed> tag on the page.  This is the main binding
/// point for your NaCl module with the browser.
Module* CreateModule() { return new FileIoModule(); }
}  // namespace pp
