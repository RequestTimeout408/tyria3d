#ifndef GW2DATTOOLS_INTERFACE_ANDATINTERFACE_H
#define GW2DATTOOLS_INTERFACE_ANDATINTERFACE_H

#include <cstdint>
#include <vector>
#include <memory>

//#include "../dllMacros.h"

namespace gw2dt
{
namespace _interface
{

class ANDatInterface
{
    public:
        struct FileRecord
        {
            uint64_t offset;
            uint32_t size;
            
            uint32_t baseId;
            uint32_t fileId;
            
            bool isCompressed;
        };
        
        virtual ~ANDatInterface() {};
        
        virtual void getBuffer(const ANDatInterface::FileRecord& iFileRecord, uint32_t& ioOutputSize, uint8_t* ioBuffer) = 0;
        
        virtual const FileRecord& getFileRecordForFileId(const uint32_t& iFileId) const = 0;
        virtual const FileRecord& getFileRecordForBaseId(const uint32_t& iBaseId) const = 0;
        
        virtual const std::vector<FileRecord>& getFileRecordVect() const = 0;
};

std::unique_ptr<ANDatInterface> createANDatInterface(const char* iDatPath);

}//end _interface ns

}//end gw2dt ns

#endif // GW2DATTOOLS_INTERFACE_ANDATINTERFACE_H
