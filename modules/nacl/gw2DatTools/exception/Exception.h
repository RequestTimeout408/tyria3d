#ifndef GW2DATTOOLS_EXCEPTION_EXCEPTION_H
#define GW2DATTOOLS_EXCEPTION_EXCEPTION_H

#include <exception>

#include "../dllMacros.h"

namespace gw2dt
{
namespace exception
{

struct Exception : public std::exception
{
   std::string s;
   Exception(std::string ss) : s(ss) {}
   ~Exception() throw () {} // Updated
   const char* what() const throw() { return s.c_str(); }
};

}
}

#endif // GW2DATTOOLS_EXCEPTION_EXCEPTION_H
