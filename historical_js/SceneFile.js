var GW2File = require('./GW2File');

function SceneFile(ds, addr){
	GW2File.call(this, ds, addr);
};
SceneFile.prototype = Object.create(GW2File.prototype);
SceneFile.prototype.constructor = SceneFile;

SceneFile.prototype.getChunkStructs = function(){

	return {
		//"cscn":require('../definition/cinp/SceneData'),
		"cscn":(new T3D.Formats.CSCN() ).__root
	};
};

module.exports = SceneFile;