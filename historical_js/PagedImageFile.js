var GW2File = require('./GW2File');

function PagedImageFile(ds, addr){
	GW2File.call(this, ds, addr);
};
PagedImageFile.prototype = Object.create(GW2File.prototype);
PagedImageFile.prototype.constructor = PagedImageFile;

PagedImageFile.prototype.getChunkStructs = function(){
	return {
		"pgtb":require('../definition/pimg/PagedImageTableDataDefinition')
	};
};

module.exports = PagedImageFile;