var GW2File = require('./GW2File');

/**
 * "mapc" File
 * @class MapFile
 * @constructor
 * @extends GW2File
 * @param {DataStream} ds A DataStream containing deflated map file binary data.
 * @param {Number} addr Offset of file start within the DataStream
 */
function MapFile(ds, addr){
	GW2File.call(this, ds, addr);
};
MapFile.prototype = Object.create(GW2File.prototype);
MapFile.prototype.constructor = MapFile;


MapFile.prototype.getChunkStructs = function(){
	return {
		"havk":require(	'../definition/mapc/HavokDefinition'),
		"trn":require(	'../definition/mapc/TerrainDefinition'),
		"parm":require(	'../definition/mapc/ParameterDefinition'),
		"shor":require(	'../definition/mapc/ShoreDefinition'),
		"zon2":require(	'../definition/mapc/ZoneDefinition'),
		"prp2":require(	'../definition/mapc/PropertiesDefinition'),
		"cube":require(	'../definition/mapc/CubeMapDefinition'),
		"env":require(	'../definition/mapc/EnvironmentDefinition'),
	};
};

module.exports = MapFile;