var Utils = require('../../../util/ParserUtils');
/**
    dword flags;
 
 */
module.exports = [

	//dword flags;
    "flags", "uint32",

    //TSTRUCT_ARRAY_PTR_START PackContentTypeInfo typeInfos TSTRUCT_ARRAY_PTR_END;
    "typeInfos", Utils.getArrayReader([

    	//PackContentTypeInfo

    	//dword guidOffset;
    	"guidOffset", "uint32",

	    //dword uidOffset;
	    "uidOffset", "uint32",

	    //dword dataIdOffset;
	    "dataIdOffset", "uint32",

	    //dword nameOffset;
	    "nameOffset", "uint32",

	    //byte trackReferences;
	    "trackReferences", "uint8"
	]),

    //TSTRUCT_ARRAY_PTR_START PackContentNamespace namespaces TSTRUCT_ARRAY_PTR_END;
    "namespaces", Utils.getArrayReader([

    	//PackContentNamespace
    	
    	//wchar_ptr name;
    	"name", Utils.getString16Reader(),

	    //dword domain;
	    "domain", "uint32",

	    //dword parentIndex;
	    "parentIndex", "uint32",
	
	]),

    //TSTRUCT_ARRAY_PTR_START fileref fileRefs TSTRUCT_ARRAY_PTR_END;
    "fileRefs", Utils.getArrayReader(Utils.getFileNameReader()),

    //TSTRUCT_ARRAY_PTR_START PackContentIndexEntry indexEntries TSTRUCT_ARRAY_PTR_END;
    "indexEntries", Utils.getArrayReader([

    	//PackContentIndexEntr
    	//dword type;
    	"relocOffset", "uint32",

	    //dword offset;
	    "offset", "uint32",

	    //dword namespaceIndex;
	    "namespaceIndex", "uint32",

	    //dword rootIndex;
	    "rootIndex", "uint32"
	]),

    //TSTRUCT_ARRAY_PTR_START PackContentLocalOffsetFixup localOffsets TSTRUCT_ARRAY_PTR_END;
    "localOffsets", Utils.getArrayReader([

    	//PackContentLocalOffsetFixup
    	
    	//dword relocOffset;
    	"relocOffset", "uint32"

	]),

    //TSTRUCT_ARRAY_PTR_START PackContentExternalOffsetFixup externalOffsets TSTRUCT_ARRAY_PTR_END;
    "externalOffsets", Utils.getArrayReader([
    	
    	//PackContentExternalOffsetFixup

    	//dword relocOffset;
    	"relocOffset", "uint32",

    	//dword targetFileIndex;
    	"targetFileIndex", "uint32"

	]),

    //TSTRUCT_ARRAY_PTR_START PackContentFileIndexFixup fileIndices TSTRUCT_ARRAY_PTR_END;
    "fileIndices", Utils.getArrayReader([
			//PackContentFileIndexFixup

			//dword relocOffset;
			"relocOffset", "uint32"
	]),

    //TSTRUCT_ARRAY_PTR_START PackContentStringIndexFixup stringIndices TSTRUCT_ARRAY_PTR_END;
    "stringIndices", Utils.getArrayReader([
  		//PackContentStringIndexFixup  	

    	//dword relocOffset;
    	"relocOffset", "uint32"
	]),

    //TSTRUCT_ARRAY_PTR_START PackContentTrackedReference trackedReferences TSTRUCT_ARRAY_PTR_END;
    "trackedReferences", Utils.getArrayReader([
    	//PackContentTrackedReference
		
		//dword sourceOffset;
		"sourceOffset", "uint32",

		//dword targetFileIndex;
		"targetFileIndex", "uint32",

		//dword targetOffset;
		"targetOffset", "uint32"
	], 1000),

    //TSTRUCT_ARRAY_PTR_START wchar_ptr strings TSTRUCT_ARRAY_PTR_END;
    "strings", Utils.getArrayReader(Utils.getString16Reader()),

    //TSTRUCT_ARRAY_PTR_START byte content TSTRUCT_ARRAY_PTR_END;
    "content", Utils.getArrayReader("uint8"),
    
];
