var Utils = require('../../../util/ParserUtils');

module.exports = [
    //helpers::Array<ModelPermutationDataV65> permutations;
    "permutations",Utils.getArrayReader([
    		//qword token;
    		"token_pt1","uint32","token_pt2","uint32",

			//helpers::RefList<ModelMaterialDataV65> materials;
			"materials", Utils.getRefArrayReader([
				    //qword token;
				    //"token_pt1","uint32","token_pt2","uint32",
				    "token", Utils.getQWordReader(),

				    //dword materialId;
				    "materialId","uint32",

				    //helpers::FileName filename;
				    "filename",/*"uint32",//*/Utils.getFileNameReader(),

				    //dword materialFlags;
				    "materialFlags","uint32",

				    //dword sortOrder;
				    "sortOrder","uint32",

				    //helpers::Array<ModelTextureDataV65> textures;
				    "textures", Utils.getArrayReader([
				    		//helpers::FileName filename;
				    		"filename",Utils.getFileNameReader(),

						    //dword textureFlags;
						    "textureFlags","uint32",

						    //qword token;
						    //"token","uint32","token_pt2","uint32",
						    "token",Utils.getQWordReader(),
						    

						    //qword blitId;
						    //"blitId_pt1","uint32","blitId_pt2","uint32",
						    "blitId",Utils.getQWordReader(),
						    

						    //dword uvAnimId;
						    "uvAnimId","uint32",

						    //byte uvPSInputIndex;
						    "uvPSInputIndex","uint8",
				    	],100),


				    //helpers::Array<ModelConstantDataV65> constants;
				    "constants",Utils.getArrayReader([
				    		//dword name;
				    		"name", "uint32",
						    //float4 value;
						    "value", ["[]","float32",4],
						    //dword constantFlags;
						    "constantFlags", "uint32"
				    	]),
				    
				    //helpers::Array<ModelMatConstLinkV65> matConstLinks;
				    "matConstLinks",["[]","uint32",2],

				    //helpers::Array<ModelUVTransLinkV65> uvTransLinks;
				    "uvTransLinks",["[]","uint32",2],

				    //helpers::Array<ModelMaterialTexTransformV65> texTransforms;
				    "texTransforms",["[]","uint32",2],

				    //byte texCoordCount;
				    "texCoordCount", "uint8"
				])

    	]),

    //helpers::Ptr<ModelCloudDataV65> cloudData;
    "cloudData_p","uint32",
    //helpers::Array<ModelObstacleDataV65> obstacles;
    "obstacles_n","uint32","obstacles_p","uint32",
    //helpers::Ptr<ModelStreakDataV65> streakData;
    "streakData_p","uint32",
    //helpers::Ptr<ModelLightDataV65> lightData;
    "lightData_p","uint32",
    //helpers::Array<ModelClothDataV65> clothData;
    "clothData_n","uint32","clothData_p","uint32",
    //helpers::Ptr<ModelWindDataV65> windData;
    "windData_p","uint32",
    //helpers::Array<qword> actionOffsetNames;
    "actionOffsetNames_n","uint32","actionOffsetNames_p","uint32",
    //helpers::Array<float3> actionOffsets;
    "actionOffsets_n","uint32","actionOffsets_p","uint32",
    //float lodOverride[2];
    "lodOverride",["[]","float32",2],
    //helpers::FileName soundScript;
    "soundScript_p","uint32",
    //helpers::Ptr<ModelLightningDataV65> lightningData;
    "lightningData_p","uint32",
    //helpers::Array<ModelSoftBodyDataV65> softBodyData;
    "softBodyData_n","uint32",
    "softBodyData_p","uint32",
    //helpers::Array<ModelBoneOffsetDataV65> boneOffsetData;
    "boneOffsetData_n","uint32",
    "boneOffsetData_p","uint32",
    //helpers::Ptr<ModelBoundingSphereV65> boundingSphere;
    "boundingSphere",Utils.getPointerReader([
    		//float3 center;
    		"center", ["x","float32","z","float32","y","float32"],
			//float radius;
			"radius","float32"
    	]),
]