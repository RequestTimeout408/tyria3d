var GW2File = require('./GW2File');

function PackContentFile(ds, addr){
	GW2File.call(this, ds, addr);
};
PackContentFile.prototype = Object.create(GW2File.prototype);
PackContentFile.prototype.constructor = PackContentFile;

PackContentFile.prototype.getChunkStructs = function(){
	return {
		//"main":require('../definition/cntc/PackContentMain'),
		"main":(new T3D.Formats.Main() ).__root
	};
};

module.exports = PackContentFile;