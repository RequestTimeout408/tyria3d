var Utils = require('../../../util/ParserUtils');


var ColorDefDataV36Struct = [
	//float intensity;
	"intensity", "float32",
	//byte3 color;
	"color", ["[]","uint8",3]
];
var TrackGroupDataV36Struct = [
	//TrackGroupDataV36
	//qword name;
	"name", Utils.getQWordReader(),
    //dword flags;
    "flags", "uint32",
    //TSTRUCT_ARRAY_PTR_START PropertyDataV36 prop TSTRUCT_ARRAY_PTR_END;
    "prop", Utils.getArrayReader([
    	//PropertyDataV36
	    //qword value;
	    "value", Utils.getQWordReader(),
	    //filename pathVal;

	    /// PROBABLY SCRIPT FILES! (AMSP)
	    "pathVal", Utils.getFileNameReader(), 

	    
	    //byte type;
	    "type", "uint8"
	]),
    //TSTRUCT_ARRAY_PTR_START TrackDataV36 track TSTRUCT_ARRAY_PTR_END;
    "track", Utils.getArrayReader([
    	//TrackDataV36
    	//qword name;
    	"name", Utils.getQWordReader(),
	    //TSTRUCT_ARRAY_PTR_START CurveKeyDataV36 curveKey TSTRUCT_ARRAY_PTR_END;
	    "curveKey", Utils.getArrayReader([
	    	//CurveKeyDataV36
		    //float time;
		    "time", "float32",
		    //float value;
		    "value", "float32",
		    //float inTangent;
		    "inTangent", "float32",
		    //float outTangent;
		    "outTangent", "float32"
    	]),
	    //TSTRUCT_ARRAY_PTR_START FlagKeyDataV36 flagKey TSTRUCT_ARRAY_PTR_END;
	    "flagKey", Utils.getArrayReader([
	    	//FlagKeyDataV36
    		//float time;
    		"time", "float32",
    		//float value;
    		"value", "float32"
    	]),
	    //TSTRUCT_ARRAY_PTR_START TriggerKeyDataV36 triggerKey TSTRUCT_ARRAY_PTR_END;
	    "triggerKey", Utils.getArrayReader([
	    	//TriggerKeyDataV36
	    	//float time;
	    	"time", "float32",
		    //byte flags1;
		    "flags1", "uint8",
		    //byte flags2;
		    "flags2", "uint8",
		    //byte flags3;
		    "flags3", "uint8",
		    //byte flags4;
		    "flags4", "uint8",
		    //qword token1;
		    "token1", Utils.getQWordReader(),
		    //qword token2;
		    "token2", Utils.getQWordReader(),
		    //float value1;
		    "value1", "float32",
		    //float value2;
		    "value2", "float32",
		    //float value3;
		    "value3", "float32",
		    //float value4;
		    "value4", "float32"
    	]),
	    //byte type;
	    "type", "uint8"
	]),
    //byte type;
    "type", "uint8"
];

module.exports = [
	//qword startingSequence;
	"startingSequence", Utils.getQWordReader(),

    //TSTRUCT_ARRAY_PTR_START SequenceDataV36 sequence TSTRUCT_ARRAY_PTR_END;
    "sequences", Utils.getArrayReader([
    	//qword name;
    	"name", Utils.getQWordReader(),
	    //qword playScript;
	    "playScript", Utils.getQWordReader(),
	    //qword updateScript;
	    "updateScript", Utils.getQWordReader(),
	    //filename environmentMap;
	    "environmentMap", Utils.getFileNameReader(),
	    //wchar_ptr map;
	    "map", Utils.getString16Reader(),
	    //wchar_ptr clientMap;
	    "clientMap", Utils.getString16Reader(),
	    //float length;
	    "length", "float32",
	    //dword flags;
	    "flags", "uint32",
	    //TSTRUCT_ARRAY_PTR_START TrackGroupDataV36 trackGroup TSTRUCT_ARRAY_PTR_END;
	    "trackGroup", Utils.getArrayReader(TrackGroupDataV36Struct)
	]),

    //ResourceDataV36 resources;
    "resources", [
    	//ResourceDataV36
    	//TSTRUCT_ARRAY_PTR_START AmbientLightDataV36 ambientLightResource TSTRUCT_ARRAY_PTR_END;
    	"ambientLightResource", Utils.getArrayReader([
    		
    			//AmbientLightDataV36
			    //ColorDefDataV36 ambientGroundColor;
			    "ambientGroundColor", ColorDefDataV36Struct,
			    //ColorDefDataV36 ambientSkyColor;
			    "ambientSkyColor", ColorDefDataV36Struct,
			    //ColorDefDataV36 fillColor;
			    "fillColor", ColorDefDataV36Struct,
			    //ColorDefDataV36 hemisphericalColor;
			    "hemisphericalColor", ColorDefDataV36Struct,
			    //qword name;
			    "name", Utils.getQWordReader()

		]),
	    //TSTRUCT_ARRAY_PTR_START FileNameRefDataV36 fileNameRef TSTRUCT_ARRAY_PTR_END;
	    "fileNameRef", Utils.getArrayReader([
	    	//FileNameRefDataV36
		    //qword name;
		    "name", Utils.getQWordReader(),
    		//filename fileName;
    		"fileName", Utils.getFileNameReader()
    	]),
	    //TSTRUCT_ARRAY_PTR_START ScriptDataV36 script TSTRUCT_ARRAY_PTR_END;
	    "script", Utils.getArrayReader([
	    	//ScriptDataV36
    		//qword name;
    		"name", Utils.getQWordReader(),
    		//TSTRUCT_ARRAY_PTR_START byte byteCode TSTRUCT_ARRAY_PTR_END;
    		"byteCode", Utils.getArrayReader("uint8")
    	]),
	    //TSTRUCT_ARRAY_PTR_START TextResourceDataV36 textResource TSTRUCT_ARRAY_PTR_END;
	    "textResource", Utils.getArrayReader([
	    	//TextResourceDataV36
		    //qword name;
		    "name", Utils.getQWordReader(),
		    //dword index;
		    "index", "uint32",
		    //dword voiceId;
		    "voiceId", "uint32",
		    //TSTRUCT_ARRAY_PTR_START TextEntryDataV36 textEntry TSTRUCT_ARRAY_PTR_END;
		    "textEntry", Utils.getArrayReader([
		    	//TextEntryDataV36
			    //wchar_ptr text;
			    "text", Utils.getString16Reader(),
			    //byte language;
			    "text", "uint8"
	    	])
	    ]),
	    //TSTRUCT_ARRAY_PTR_START SpeciesResourceDataV36 speciesResource TSTRUCT_ARRAY_PTR_END;
	    "speciesResource", Utils.getArrayReader([
	    	//SpeciesResourceDataV36
		    //byte16 speciesId;
		    "speciesId", ["[]","uint8", 16],
		    //qword name;
		    "name", Utils.getQWordReader(),
		    //qword modelId;
		    "modelId", Utils.getQWordReader(),
		    //qword modelVariant;
		    "modelVariant", Utils.getQWordReader(),
    	]),
    ],

    //TPTR_START TrackGroupDataV36 trackGroup TPTR_END;
    //"trackGroup", Utils.getPointerReader(TrackGroupDataV36Struct)
];


