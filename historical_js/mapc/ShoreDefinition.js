var Utils = require('../../../util/ParserUtils');

module.exports = [
	//  helpers::Array<MapShoreChainV0> chains;
	"chains", Utils.getArrayReader([
		//float offset;
		"offset", "float32",

	    //float opacity;
	    "opacity", "float32",

	    //float animationSpeed;
	    "animationSpeed", "float32",

	    //float2 edgeSize;
	    "edgeSize", ["sizeX", "float32", "sizeZ","float32"],

	    //dword flags;
	    "flags", "uint32",

	    //helpers::Array<float2> points;
	    "points", Utils.getArrayReader(["x", "float32", "z", "float32"]),

	    //helpers::FileName materialFilename;
	    "fileName", "uint32",

	    //helpers::Array<helpers::FileName> textureFilenames;
	    "points", Utils.getArrayReader("uint32"),

	    //float restTime;
	    "restTime", "float32",

	    //float2 fadeRanges[4];
	    "fadeRanges",  ["[]", ["min", "float32", "max","float32"], 4],

	    //float simplifyDistMin;
	    "simplifyDistMin", "float32",

	    //float simplifyDistMax;
	    "simplifyDistMax", "float32",

	    //float simplifyDot;
	    "simplifyDot", "float32"

	])
];