var GW2File = require('./GW2File');

function ModelFile(ds, addr){
	GW2File.call(this, ds, addr);
};
ModelFile.prototype = Object.create(GW2File.prototype);
ModelFile.prototype.constructor = ModelFile;

ModelFile.prototype.getChunkStructs = function(){
	return {
		"modl":require('../definition/modl/ModelDataDefinition'),
		"geom":require('../definition/modl/GeometryDefinition')
	};
};

module.exports = ModelFile;