!function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var f;"undefined"!=typeof window?f=window:"undefined"!=typeof global?f=global:"undefined"!=typeof self&&(f=self),f.T3D=e()}}(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
/// Generic ArenaNet File with basic header functionality
var File = _dereq_('../format/file/GW2File.js');

/// Definition of main dat file header
var defANDAT = _dereq_('../format/definition/ANDAT');

/// Definition of the MFT index
var defMFT = _dereq_('../format/definition/MFT');

/// Small collection of Math and Sort algorithms
var MathUtils = _dereq_('../util/MathUtils.js');

/// List of known GW2 maps used to make lookup faster
var MapFileList =  _dereq_('../MapFileList');


/**
 * A statefull class that handles reading and inflating data from a local GW2 dat file.
 * @class LocalReader
 * @constructor
 * @param {File}	datFile A core JS File instance, must refer to the GW2 .dat
 * @param {String}	version T3D version.
 * @param {element}	output  jQuery element reference for some output.
 */
var LocalReader = function(datFile, version, output){

	/// Initiate list of file infaltion listeners
	this.fileListeners = [];

	/// Add reference to file object to DAT and jQuery output element
	this.dat = datFile;
	this.version = version;
	this.output = output;
};

/**
 * TODO
 * @method parseHeaderAsync 
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
LocalReader.prototype.parseHeaderAsync = function(callback){
	var self = this;

	///Read dat file header, 40 bytes should always be the length
	this.loadFilePart(self.dat, 0, 40, self.readANDatHeader);

	this.onFullyLoaded = callback;
}

/**
 * TODO
 * @param  {[type]} inflater        [description]
 * @param  {[type]} inflaterWrapper [description]
 * @return {[type]}                 [description]
 */
LocalReader.prototype.connectInflater = function(inflater, inflaterWrapper){
	var self = this;

	/// HTML pNaCl emed elements
	this.NaClInflater = inflater;	

    /// Set up a listener for any messages passed by the pNaCl component
    inflaterWrapper[0].addEventListener(
    	'message',
    	function(message_event){
    		self.NaClListener.call(self, message_event);
    	},
    	true
	);

}

/**
 * TODO
 * @param {[type]} message_event [description]
 */
LocalReader.prototype.NaClListener = function(message_event){
	if( typeof message_event.data === 'string' ) {
		console.warn("NaCl threw an error",message_event.data);
		return;
	}

	
	//console.log("Got back a DS from NaCl RAW", message_event.data);
	//console.log("Got back a DS from NaCl Uint32Array", new Uint32Array(message_event.data));
	var handle = message_event.data[0];

	if(this.fileListeners[handle]){
		this.fileListeners[handle].forEach(function(callback){
			var data = message_event.data;
			/// Array buffer, dxtType, imageWidth, imageHeigh			
			callback(data[1], data[2], data[3], data[4]);	
		});

		// Remove triggered listeners
		this.fileListeners[handle] = null;
	}
	
}

/**
 * TODO
 * @param  {[type]} ds [description]
 * @return {[type]}    [description]
 */
LocalReader.prototype.readANDatHeader = function(ds){


	/// Read file header data struct
	this.fileHeader = ds.readStruct(defANDAT);

	console.log("Loaded Main .dat header", this.fileHeader);

	/// Get pointer to MFT chunk header
	this.fileHeader.mftOffset =  MathUtils.arr32To64(this.fileHeader.mftOffset);

	/// Load MFT
	this.loadFilePart(
		this.dat,
		this.fileHeader.mftOffset,
		this.fileHeader.mftSize,
		this.readMFTHeader );

};

/**
 * TODO
 * @param  {[type]} ds [description]
 * @return {[type]}    [description]
 */
LocalReader.prototype.readMFTHeader = function(ds){

	/// Read MFT header data struct
	/// Global variable mft
	this.mft = ds.readStruct(defMFT);

	var entryStartPtr = ds.position;
	var numEntires = this.mft.nbOfEntries;

	/// MFT has entries with offset, size and compression flag
	/// for all files in the .dat

	/// Read all entry offsets and sizes into uint32 arrays
	this.mft.entryDict = {
		offset_0:new Uint32Array(numEntires),
		offset_1:new Uint32Array(numEntires),
		offset:new Float64Array(numEntires),
		size:new Uint32Array(numEntires),
		compressed:new Uint16Array(numEntires),
	}


	/// Read offset, size and compressed flag of the entries.
	//console.log("reading MFT entries ");	
	for(var i=0; i<numEntires-1; i++){
		
		/// Read first 14 bytes
		this.mft.entryDict.offset_0[i] = ds.readUint32();
		this.mft.entryDict.offset_1[i] = ds.readUint32();
		this.mft.entryDict.size[i] = ds.readUint32();
		this.mft.entryDict.compressed[i] = ds.readUint16();


		this.mft.entryDict.offset[i] =  MathUtils.arr32To64(
			[ this.mft.entryDict.offset_0[i],
			  this.mft.entryDict.offset_1[i] ]
		);

		/// Skip 10
		ds.seek(ds.position + 10);

	}
	
	console.log( "Finished indexing MFT ", this.mft );


	/// Read data pointed to by 2nd mft entry
	/// This entry maps file ID to MFT index	
	var offset = MathUtils.arr32To64(
		[ this.mft.entryDict.offset_0[1],
		  this.mft.entryDict.offset_1[1] ]
	);
	var size = this.mft.entryDict.size[1];

	this.loadFilePart(this.dat, offset, size, this.readMFTIndexFile);

};

/**
 * TODO
 * @param  {[type]} ds   [description]
 * @param  {[type]} size [description]
 * @return {[type]}      [description]
 */
LocalReader.prototype.readMFTIndexFile = function(ds, size){
	
	var length = size / 8;

	/// fileIdTable
	var ids = new Uint32Array(length);
	var mftIndices = new Uint32Array(length);

	/// m_entryToId
	var m_entryToId_baseId = new Uint32Array(length);
	var m_entryToId_fileFileId = new Uint32Array(length);

	
	for(var i=0; i<length; i++){
		ids[i] = ds.readUint32();
		mftIndices[i] = ds.readUint32();
	}
	
	/// Raw map of "ID" to mft index
	this.mft.id2index = {
		ids:ids,
		mftIndices:mftIndices
	}

	/// m_entryToId has both base and filed id
	for(var i=0; i<length; i++){


		if (ids[i] == 0 && mftIndices[i] == 0) {
            continue;
        }
 		
 		var entryIndex = mftIndices[i];
        var entry     = {
        	fileId : m_entryToId_fileFileId[entryIndex],
        	baseId : m_entryToId_baseId[entryIndex]
        }

        if (entry.baseId == 0) {
            entry.baseId = ids[i];            
        } else if (entry.fileId == 0) {
            entry.fileId = ids[i];
        }

        if (entry.baseId > 0 && entry.fileId > 0) {
            if (entry.baseId > entry.fileId) {
            	//std::swap(entry.baseId, entry.fileId);
            	var temp = entry.baseId;
            	entry.baseId = entry.fileId;
            	entry.fileId = temp;                
            }
        }

        //Write back
        m_entryToId_fileFileId[entryIndex] = entry.fileId;
        m_entryToId_baseId[entryIndex] = entry.baseId;
	}

	this.mft.m_entryToId = {
		baseId:m_entryToId_baseId,
		fileId:m_entryToId_fileFileId
	}

	if(this.onFullyLoaded)
		this.onFullyLoaded();
};


/**
 * Reads the cached list of maps corresponding to the reader's .dat from the localStorage.
 * @return {Array} Grouped List of maps
 */
LocalReader.prototype.loadMapList = function(){
	var datFile = this.dat;
	var mapName = "mapList_" + this.version + "." + datFile.name + "_" + datFile.lastModified + "_" + datFile.size;
	var str = localStorage.getItem(mapName);
	if(!str)
		return null;
	try{
		return JSON.parse(str);	
	}
	catch(e){
		
	}
	return null;
	
}

/**
 * Stores a map list array into the localStorage.
 * @param  {[type]} datFile [description]
 * @param  {[type]} mapList [description]
 * @return {[type]}         [description]
 */
LocalReader.prototype.storeMapList = function(datFile, mapList){
	var mapName = "mapList_" + this.version + "." + datFile.name + "_" + datFile.lastModified + "_" + datFile.size;
	localStorage.setItem(mapName, JSON.stringify(mapList) );
}

/**
 * TODO
 * @param  {[type]}   searchAll [description]
 * @param  {Function} callback  [description]
 * @return {[type]}             [description]
 */
LocalReader.prototype.readMapListAsync = function(searchAll, callback){
	var self = this;
	var datFile = this.dat;
	var mftIndices = [];

	/// Number of threads for map lookup
	var N = 8; 

	/// Time spent looking up maps
	var t = new Date().getTime();

	/// Only look for known maps
	if(!searchAll){

		MapFileList.maps.forEach(function(mapCol){
			mapCol.maps.forEach(function(mapEntry){
				var entryBaseId = mapEntry.fileName.split(".")[0];
				var mftIndex = self.getFileIndex(entryBaseId)
				mftIndices.push(mftIndex);
			});
		});
	}

	/// Look for all maps
	else{
		mftIndices = this.mft.id2index.mftIndices;
	}

	/// Get a list of unique indices based on file addresses
	var uniqueIdxs = MathUtils.sort_unique(mftIndices, function(a, b) {
		return self.mft.entryDict.offset[a] - self.mft.entryDict.offset[b];
	});

	/// Callback for map lookup
	var cb = function(result){
		var dt = new Date().getTime() - t;
		console.log("Time elapsed ", Math.round(0.001*dt)," seconds");	

		/// Clear listeners used during indexing
		self.fileListeners = [];

		var localMapList = {maps:[]};

		/// Arrange each found map into a grouped list
		result.forEach(function(mftIndex){

			/// Base Id is used by gw2browser and is therefore the de facto identifier
			/// in the community. Let's use it here too!
			var baseId = self.mft.m_entryToId.baseId[mftIndex+1];

			/// Hack to avoid releasing maps newer than VB
			if(baseId > /*969663*/ 11542000 * 10000){
				return;
			}

			var name = "";
			var group = "";

			MapFileList.maps.forEach(function(mapCol){
				mapCol.maps.forEach(function(mapEntry){
					if(name.length == 0 && mapEntry.fileName.indexOf(baseId+".")>=0){
						name = mapEntry.name;
						group = mapCol.name;
					}
				});
			});

			if(name.length == 0){
				name = "Unknown map "+baseId;
			}

			if(group.length == 0){
				group = "Unknown maps";
			}

			var localGroup = null;
			localMapList.maps.forEach(function(mapCol){
				if(mapCol.name == group){
					localGroup = mapCol;
				}
			});

			if(!localGroup){
				localGroup = {name:group,maps:[]};
				localMapList.maps.push(localGroup);
			}

			localGroup.maps.push({fileName:baseId, name:name});
			
		});
		
		
		/// Store map list into local storage.
		self.storeMapList(datFile, localMapList);

		/// Fire callback and pass the map list
		callback(localMapList);

	}/// End of map lookup callback.


	/// Start map lookup:
	var maxLength = uniqueIdxs.length;
	console.log("Scanning ",maxLength, " files for maps...");	
	this.findType(uniqueIdxs, "mapc", 0, maxLength, N, cb);
}

/**
 * TODO
 * @param  {[type]}   uniqueIdxs [description]
 * @param  {[type]}   type       [description]
 * @param  {[type]}   start      [description]
 * @param  {[type]}   length     [description]
 * @param  {[type]}   N          [description]
 * @param  {Function} callback   [description]
 * @return {[type]}              [description]
 */
LocalReader.prototype.findType = function(uniqueIdxs, type, start, length, N ,callback){
	var self = this;
	var maxlen = start+length;
	var lastPct = 0;
	var pctM = 100.0 / Math.min(length, uniqueIdxs.length - start);
	var threadsDone = 0;

	var result = [];

	var peekCallback=function(inflatedData, i, mftIndex){
		if(!inflatedData){
			console.warn("No infalted data for entry");
			readUniqueId(i+N);
			return;
		}

		var ds = new DataStream(inflatedData);
		var file = new File(ds, 0);

		if(file.header.type == type){
			result.push(mftIndex);
		}

		readUniqueId(i+N);
	}

	
	var readUniqueId = function(i){

		if(i%N==0){
			var pct = Math.min(100, Math.floor( (i-start) * pctM) );
			if(lastPct != pct){
				self.output.find(".title").html("Finding maps (first visit only)");
				self.output.find(".progress").html(pct+"%");
				lastPct = pct;
			}
		}

		if( i>=uniqueIdxs.length || i>=maxlen ){
			console.log("Thread ",i%N+" done");
			threadsDone++;
			if(threadsDone == N){
				callback(result);
			}
			return;
		}

		var mftIndex = uniqueIdxs[i];

		var compressed = self.mft.entryDict.compressed[mftIndex];
		if(!compressed){
			//console.log("File is NOT compressed");
			readUniqueId(i+N);
			return;
		}

		var handle = mftIndex;
		var offset = self.mft.entryDict.offset[mftIndex];
		var size = self.mft.entryDict.size[mftIndex];

		if(size<0x20){
			//console.log("File is too small");
			readUniqueId(i+N);
			return;
		}

		/// Read .dat file part
		self.loadFilePart(self.dat, offset, size,
			function(ds, _size){
				
				///Infalte
				self.inflate(
					ds,
					_size,
					handle,
					///Infaltion  callback
					function(inflatedData){
						peekCallback(inflatedData, i, mftIndex);
					}, 
					false, 0x20
				);


		});

	}

	for(var n=0; n<N; n++){
		readUniqueId(start + n);	
	}
};

/**
 * TODO
 * @param  {[type]} baseOrFileId [description]
 * @return {[type]}              [description]
 */
LocalReader.prototype.getFileIndex = function(baseOrFileId){
	var index = -1;

	/// Get by base id
	
	for(var i = 0; i<this.mft.m_entryToId.baseId.length; i++){
		if( this.mft.m_entryToId.baseId[i+1] == baseOrFileId ){	
			index = i;
			//console.log("Found BASE ID "+baseId +" at INDEX "+i);
		}
	}

	/// Get by file id
	if(index==-1){
		for(var i = 0; i<this.mft.m_entryToId.fileId.length; i++){
			if( this.mft.m_entryToId.fileId[i+1] == baseOrFileId ){	
				index = i;
				//console.log("Found FILE ID "+baseId +" at INDEX "+i);
			}
		}	
	}

	return index;
}

/**
 * TODO
 * @param  {[type]}   baseId   [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
LocalReader.prototype.loadTextureFile = function(baseId, callback){
	this.loadFile(baseId, callback, true);
}

/**
 * TODO
 * @param  {[type]}   baseId   [description]
 * @param  {Function} callback [description]
 * @param  {Boolean}  isImage  [description]
 * @param  {[type]}   raw      [description]
 * @return {[type]}            [description]
 */
LocalReader.prototype.loadFile = function(baseId, callback, isImage, raw){
	
	var self = this;

	var index = this.getFileIndex(baseId);
	var mftIndex = index;

	//If the file doesn't exist still make sure to fire the callback
	if(mftIndex <= 0 ){
		console.log("Could not find file with baseId ",baseId);
		callback(null);
		return;
	}

	//console.log("fetchign  baseId ",baseId);
	
	var offset = MathUtils.arr32To64(
		[ this.mft.entryDict.offset_0[mftIndex],
		  this.mft.entryDict.offset_1[mftIndex] ]
	);

	var size = this.mft.entryDict.size[mftIndex];
	//console.log("File at found index is "+ size +" bytes");
	
	var compressed = this.mft.entryDict.compressed[mftIndex];
	if(!compressed){
		console.log("File is NOT compressed");
		
		callback(null);
		return;
	}

	/// Read map and pass the ds to our pNaCL inflate function
	//TODO: will this work?? Shared base id's and all that...
	var handle = index;

	this.loadFilePart(this.dat, offset, size, function(ds, size){
		
		/// If the raw param was passed return the un-inflated data
		if(raw){
			callback(ds);
		}

		/// If raw parameter was not passed or false, inflate first
		else{
			self.inflate(ds, size, handle, callback, isImage);	
		}
		
	});
};


/**
 * TODO
 * @param  {[type]}   ds        [description]
 * @param  {[type]}   size      [description]
 * @param  {[type]}   handle    [description]
 * @param  {Function} callback  [description]
 * @param  {Boolean}  isImage   [description]
 * @param  {[type]}   capLength [description]
 * @return {[type]}             [description]
 */
LocalReader.prototype.inflate = function(ds, size, handle, callback, isImage, capLength){
	
	
	var arrayBuffer = ds.buffer;	

	if(!capLength){
		//capLength = arrayBuffer.byteLength;
		capLength = 1;
	}
	//console.log("Uncompressing file size ",size, "handle", handle, "cap", capLength, "isImage", isImage);
	
	if(arrayBuffer.byteLength < 12){
		console.log("not inflating, length is too short ("+arrayBuffer.byteLength+")", handle);
		callback(null);
		return;
	}
	if(handle == 123350 || handle == 123409 || handle == 123408 ){
		callback(null);
		return;
	}

	/// Register listener for file load callback
	if(this.fileListeners[handle]){
		this.fileListeners[handle].push(callback);

		///No need to make another call, just wait for callback event to fire.
		return;
	}
	else{
		this.fileListeners[handle] = [callback];	
	}
	
    
    /// Call pNaCl component with the handle and arrayBuffer as an arguments

    //console.log("posting",[handle,arrayBuffer,isImage===true])
	this.NaClInflater[0].postMessage([handle,arrayBuffer,isImage===true, capLength]);
};

/**
 * Reads bytes from a big file. Uses offset and length (in bytes) in order to only load
 * parts of the file that are used.
 * @param  {[type]}   file     [description]
 * @param  {[type]}   offset   [description]
 * @param  {[type]}   length   [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
LocalReader.prototype.loadFilePart = function(file, offset, length, callback){
	var self = this;

	var reader = new FileReader();
		
	reader.onerror = function(fileEvent){
		debugger;
	}
	reader.onload  = function(fileEvent){

		var buffer = fileEvent.target.result;
		var ds = new DataStream(buffer);
	  	ds.endianness = DataStream.LITTLE_ENDIAN;

	  	/// Pass data stream and data length to callback function, keeping "this" scope
	  	callback.call(self, ds, length);
	}
	
  	var start = offset;
	var end = start + length;
	reader.readAsArrayBuffer(file.slice(start, end));
};

module.exports = LocalReader;
},{"../MapFileList":2,"../format/definition/ANDAT":10,"../format/definition/MFT":11,"../format/file/GW2File.js":26,"../util/MathUtils.js":32}],2:[function(_dereq_,module,exports){
/*

module.exports = {
	maps : [
		{
			"name":"Sample Maps","maps":
			[
				{"fileName":"Maps_2013_01_28\/191000.parm","name":"Lion's Arch Pre Scarlet"},
				{ fileName :"197402.data", name:"The Battle of Khylo" },
				{ fileName :"193081.data", name:"North of Divinity's Reach" },
				{ fileName :"969663.data", name:"Verdant Brink" }
			]
		}
	]
};

Oh wow, there's even part of Verdant Brink, SAB worlds 1 and 2, the old LS1 dungeon instances, tower of nightmares.. and even more.
This is amazing, great job!
EDIT- If you're looking to be a bit more accurate with the map names:
Dry Top -> Dry Top (Past)
Dry Top Again -> Dry Top (Present)
Silver wastes -> The Silverwastes
Stronghold -> Champion's Dusk
Gendarran Fields and Timberline Falls are similar to Dry Top, the second version has the added mordrem vines.

Both Lion's Arch maps appear to be the same or otherwise very similar, I can't spot the differences.

As for the Newly Added map, it looks like the original design of the Stronghold/Champion's Dusk map that was
datamined ages ago... I don't think it was given a name.
*/


/*
Also, noticed you're missing original Southsun Cove, one of Lion's Arch instances (pre-Battle for Lion's Arch),
and two of Kessex Hills instances (pre-Tower of Nightmares, and during Tower of Nightmares). I think these four are important as well!
*/




/*
Shamans old uns
*/

module.exports = {

	maps : [

/*	{"name":"BWE2","maps":
[
{"fileName":"BWE2\/184799.parm","name":"Plane"}, 
{"fileName":"BWE2\/186397.parm","name":"Snowden Drifts"},
{"fileName":"BWE2\/187611.parm","name":"Wayfarer Foothills"},
{"fileName":"BWE2\/188591.parm","name":"Plains of Ashford"},
{"fileName":"BWE2\/189364.parm","name":"Ascalonian Catacombs"},
{"fileName":"BWE2\/190490.parm","name":"Diessa Plateau"},
{"fileName":"BWE2\/191000.parm","name":"Lion's Arch Pre Scarlet"},
{"fileName":"BWE2\/191265.parm","name":"Divinity's Reach"},
{"fileName":"BWE2\/192711.parm","name":"Queensdale"},
{"fileName":"BWE2\/193081.parm","name":"North of Divinity's Reach"},
{"fileName":"BWE2\/194288.parm","name":"Kessex Hills"},
{"fileName":"BWE2\/195149.parm","name":"Caledon Forest"},
{"fileName":"BWE2\/195493.parm","name":"Metrica Province"},
{"fileName":"BWE2\/195806.parm","name":"Eternal Battlegrounds"},
{"fileName":"BWE2\/196212.parm","name":"Borderlands"},
{"fileName":"BWE2\/196585.parm","name":"Black Citadel"},
{"fileName":"BWE2\/197122.parm","name":"Hoelbrak"},
{"fileName":"BWE2\/197249.parm","name":"Heart of the Mists"},
{"fileName":"BWE2\/197402.parm","name":"The Battle of Khylo"},
{"fileName":"BWE2\/197545.parm","name":"Forest of Niflhel"},
{"fileName":"BWE2\/197562.parm","name":"Empty Box"},
{"fileName":"BWE2\/198076.parm","name":"The Grove"},
{"fileName":"BWE2\/198272.parm","name":"Rata Sum"},
{"fileName":"BWE2\/275155.parm","name":"Dredgehaunt Cliffs"},
{"fileName":"BWE2\/275474.parm","name":"Sorrow's Embrace"},
{"fileName":"BWE2\/276252.parm","name":"Frostgorge Sound"},
{"fileName":"BWE2\/276520.parm","name":"Honor of the Waves"},
{"fileName":"BWE2\/277587.parm","name":"Lornar's Pass"},
{"fileName":"BWE2\/278717.parm","name":"Timberline Falls"},
{"fileName":"BWE2\/280025.parm","name":"Blazeridge Steppes"},
{"fileName":"BWE2\/281313.parm","name":"Fireheart Rise"},
{"fileName":"BWE2\/282668.parm","name":"Iron Marches"},
{"fileName":"BWE2\/283574.parm","name":"Fields of Ruin"},///LEss detailed than release
{"fileName":"BWE2\/284039.parm","name":"Citadel of Flame"},
{"fileName":"BWE2\/284829.parm","name":"Straits of Devastation"},
{"fileName":"BWE2\/285089.parm","name":"Malchor's Leap"},
{"fileName":"BWE2\/285634.parm","name":"Cursed Shore"},
{"fileName":"BWE2\/286945.parm","name":"Bloodtide Coast"},
{"fileName":"BWE2\/287214.parm","name":"Caudecus's Manor"},
{"fileName":"BWE2\/287870.parm","name":"Harathi Hinterlands"},
{"fileName":"BWE2\/289176.parm","name":"Gendarran Fields"}, /// LA entrance original
{"fileName":"BWE2\/291064.parm","name":"Mount Maelstrom"},
{"fileName":"BWE2\/291284.parm","name":"Twilight Arbor"}, /// Less detail
{"fileName":"BWE2\/292254.parm","name":"Sparkfly Fen"}, /// Teq's bone wall is there since BWE2 ???
{"fileName":"BWE2\/293307.parm","name":"Brisban Wildlands"},
{"fileName":"BWE2\/293606.parm","name":"Crucible of Eternity"},
{"fileName":"BWE2\/294938.parm","name":"Claw Island"},
{"fileName":"BWE2\/295005.parm","name":"Chantry of Secrets"},
{"fileName":"BWE2\/295179.parm","name":"Cathedral of Hidden Depths"},
{"fileName":"BWE2\/295282.parm","name":"Eye of the North"},
{"fileName":"BWE2\/295962.parm","name":"A Light in the Darkness"}
]}
,
{"name":"BWE3","maps":
[
{"fileName":"BWE3\/184799.parm","name":"Plane"}, 
{"fileName":"BWE3\/186397.parm","name":"Snowden Drifts"},
{"fileName":"BWE3\/187611.parm","name":"Wayfarer Foothills"},
{"fileName":"BWE3\/188591.parm","name":"Plains of Ashford"},
{"fileName":"BWE3\/189364.parm","name":"Ascalonian Catacombs"},
{"fileName":"BWE3\/190490.parm","name":"Diessa Plateau"},
{"fileName":"BWE3\/191000.parm","name":"Lion's Arch Pre Scarlet"},
{"fileName":"BWE3\/191265.parm","name":"Divinity's Reach"},
{"fileName":"BWE3\/192711.parm","name":"Queensdale"},
{"fileName":"BWE3\/193081.parm","name":"North of Divinity's Reach"},
{"fileName":"BWE3\/194288.parm","name":"Kessex Hills"},
{"fileName":"BWE3\/195149.parm","name":"Caledon Forest"},
{"fileName":"BWE3\/195493.parm","name":"Metrica Province"},
{"fileName":"BWE3\/195806.parm","name":"Eternal Battlegrounds"},
{"fileName":"BWE3\/196212.parm","name":"Borderlands"},
{"fileName":"BWE3\/196585.parm","name":"Black Citadel"},
{"fileName":"BWE3\/197122.parm","name":"Hoelbrak"},
{"fileName":"BWE3\/197249.parm","name":"Heart of the Mists"},
{"fileName":"BWE3\/197402.parm","name":"The Battle of Khylo"},
{"fileName":"BWE3\/197545.parm","name":"Forest of Niflhel"},
{"fileName":"BWE3\/197562.parm","name":"Empty Box"},
{"fileName":"BWE3\/198076.parm","name":"The Grove"},
{"fileName":"BWE3\/198272.parm","name":"Rata Sum"},
{"fileName":"BWE3\/275155.parm","name":"Dredgehaunt Cliffs"},
{"fileName":"BWE3\/275474.parm","name":"Sorrow's Embrace"},
{"fileName":"BWE3\/276252.parm","name":"Frostgorge Sound"},
{"fileName":"BWE3\/276520.parm","name":"Honor of the Waves"},
{"fileName":"BWE3\/277587.parm","name":"Lornar's Pass"},
{"fileName":"BWE3\/278717.parm","name":"Timberline Falls"},
{"fileName":"BWE3\/280025.parm","name":"Blazeridge Steppes"},
{"fileName":"BWE3\/281313.parm","name":"Fireheart Rise"},
{"fileName":"BWE3\/282668.parm","name":"Iron Marches"},
{"fileName":"BWE3\/283574.parm","name":"Fields of Ruin"},///LEss detailed than release
{"fileName":"BWE3\/284039.parm","name":"Citadel of Flame"},
{"fileName":"BWE3\/284829.parm","name":"Straits of Devastation"},
{"fileName":"BWE3\/285089.parm","name":"Malchor's Leap"},
{"fileName":"BWE3\/285634.parm","name":"Cursed Shore"},
{"fileName":"BWE3\/286945.parm","name":"Bloodtide Coast"},
{"fileName":"BWE3\/287214.parm","name":"Caudecus's Manor"},
{"fileName":"BWE3\/287870.parm","name":"Harathi Hinterlands"},
{"fileName":"BWE3\/289176.parm","name":"Gendarran Fields"}, /// LA entrance original
{"fileName":"BWE3\/291064.parm","name":"Mount Maelstrom"},
{"fileName":"BWE3\/291284.parm","name":"Twilight Arbor"}, /// Less detail
{"fileName":"BWE3\/292254.parm","name":"Sparkfly Fen"}, /// Teq's bone wall is there since BWE2 ???
{"fileName":"BWE3\/293307.parm","name":"Brisban Wildlands"},
{"fileName":"BWE3\/293606.parm","name":"Crucible of Eternity"},
{"fileName":"BWE3\/294938.parm","name":"Claw Island"},
{"fileName":"BWE3\/295005.parm","name":"Chantry of Secrets"},
{"fileName":"BWE3\/295179.parm","name":"Cathedral of Hidden Depths"},
{"fileName":"BWE3\/295282.parm","name":"Eye of the North"},
{"fileName":"BWE3\/295962.parm","name":"A Light in the Darkness"},
{"fileName":"BWE3\/376916.parm","name":"Legacy of the Foefire"}
]}
,

*/
/*
{"name":"Historical","maps":
[
{"fileName":"191000.parm","name":"Lion's Arch (Older)"}
{"fileName":"Maps_2013_01_28\/184799.parm","name":"Plane"}, 
{"fileName":"Maps_2013_01_28\/186397.parm","name":"Snowden Drifts"},
{"fileName":"Maps_2013_01_28\/187611.parm","name":"Wayfarer Foothills"},
{"fileName":"Maps_2013_01_28\/188591.parm","name":"Plains of Ashford"},
{"fileName":"Maps_2013_01_28\/189364.parm","name":"Ascalonian Catacombs"},
{"fileName":"Maps_2013_01_28\/190490.parm","name":"Diessa Plateau"},
{"fileName":"Maps_2013_01_28\/191000.parm","name":"Lion's Arch Pre Scarlet"},  *//*
{"fileName":"Maps_2013_01_28\/191265.parm","name":"Divinity's Reach"},
{"fileName":"Maps_2013_01_28\/192711.parm","name":"Queensdale"},
{"fileName":"Maps_2013_01_28\/193081.parm","name":"North of Divinity's Reach"},
{"fileName":"Maps_2013_01_28\/194288.parm","name":"Kessex Hills"},
{"fileName":"Maps_2013_01_28\/195149.parm","name":"Caledon Forest"},
{"fileName":"Maps_2013_01_28\/195493.parm","name":"Metrica Province"},
{"fileName":"Maps_2013_01_28\/195806.parm","name":"Eternal Battlegrounds"},
{"fileName":"Maps_2013_01_28\/196212.parm","name":"Borderlands"},
{"fileName":"Maps_2013_01_28\/196585.parm","name":"Black Citadel"},
{"fileName":"Maps_2013_01_28\/197122.parm","name":"Hoelbrak"},
{"fileName":"Maps_2013_01_28\/197249.parm","name":"Heart of the Mists"},
{"fileName":"Maps_2013_01_28\/197402.parm","name":"The Battle of Khylo"},
{"fileName":"Maps_2013_01_28\/197545.parm","name":"Forest of Niflhel"},
{"fileName":"Maps_2013_01_28\/197562.parm","name":"Empty Box"},
{"fileName":"Maps_2013_01_28\/198076.parm","name":"The Grove"},
{"fileName":"Maps_2013_01_28\/198272.parm","name":"Rata Sum"},
{"fileName":"Maps_2013_01_28\/275155.parm","name":"Dredgehaunt Cliffs"},
{"fileName":"Maps_2013_01_28\/275474.parm","name":"Sorrow's Embrace"},
{"fileName":"Maps_2013_01_28\/276252.parm","name":"Frostgorge Sound"},
{"fileName":"Maps_2013_01_28\/276520.parm","name":"Honor of the Waves"},
{"fileName":"Maps_2013_01_28\/277587.parm","name":"Lornar's Pass"},
{"fileName":"Maps_2013_01_28\/278717.parm","name":"Timberline Falls"},
{"fileName":"Maps_2013_01_28\/280025.parm","name":"Blazeridge Steppes"},
{"fileName":"Maps_2013_01_28\/281313.parm","name":"Fireheart Rise"},
{"fileName":"Maps_2013_01_28\/282668.parm","name":"Iron Marches"},
{"fileName":"Maps_2013_01_28\/283574.parm","name":"Fields of Ruin"},///LEss detailed than release
{"fileName":"Maps_2013_01_28\/284039.parm","name":"Citadel of Flame"},
{"fileName":"Maps_2013_01_28\/284829.parm","name":"Straits of Devastation"},
{"fileName":"Maps_2013_01_28\/285089.parm","name":"Malchor's Leap"},
{"fileName":"Maps_2013_01_28\/285634.parm","name":"Cursed Shore"},
{"fileName":"Maps_2013_01_28\/286945.parm","name":"Bloodtide Coast"},
{"fileName":"Maps_2013_01_28\/287214.parm","name":"Caudecus's Manor"},
{"fileName":"Maps_2013_01_28\/287870.parm","name":"Harathi Hinterlands"},
{"fileName":"Maps_2013_01_28\/289176.parm","name":"Gendarran Fields"}, /// LA entrance original
{"fileName":"Maps_2013_01_28\/291064.parm","name":"Mount Maelstrom"},
{"fileName":"Maps_2013_01_28\/291284.parm","name":"Twilight Arbor"}, /// Less detail
{"fileName":"Maps_2013_01_28\/292254.parm","name":"Sparkfly Fen"}, /// Teq's bone wall is there since Maps_2013_01_28 ???
{"fileName":"Maps_2013_01_28\/293307.parm","name":"Brisban Wildlands"},
{"fileName":"Maps_2013_01_28\/293606.parm","name":"Crucible of Eternity"},
{"fileName":"Maps_2013_01_28\/294938.parm","name":"Claw Island"},
{"fileName":"Maps_2013_01_28\/295005.parm","name":"Chantry of Secrets"},
{"fileName":"Maps_2013_01_28\/295179.parm","name":"Cathedral of Hidden Depths"},
{"fileName":"Maps_2013_01_28\/295282.parm","name":"Eye of the North"},
{"fileName":"Maps_2013_01_28\/295962.parm","name":"A Light in the Darkness"},

{"fileName":"Maps_2013_01_28\/467374.parm","name":"Raid on the Capricorn"},
{"fileName":"Maps_2013_01_28\/473765.parm","name":"Arah - Story"},
{"fileName":"Maps_2013_01_28\/473930.parm","name":"The Ruined City of Arah"},
{"fileName":"Maps_2013_01_28\/506539.parm","name":"Reaper's Rumble"},
{"fileName":"Maps_2013_01_28\/506592.parm","name":"Ascent to Madness"},
{"fileName":"Maps_2013_01_28\/506670.parm","name":"Mad King's Labyrinth"},
{"fileName":"Maps_2013_01_28\/506739.parm","name":"Mad King's Clock Tower"},
{"fileName":"Maps_2013_01_28\/519839.parm","name":"Fractals of the Mists"},
{"fileName":"Maps_2013_01_28\/520479.parm","name":"Southsun Cove"},
{"fileName":"Maps_2013_01_28\/520609.parm","name":"Temple of the Silent Storm"},
{"fileName":"Maps_2013_01_28\/529718.parm","name":"Snowball Mayhem"},
{"fileName":"Maps_2013_01_28\/529896.parm","name":"Tixx's Infinirarium"},
{"fileName":"Maps_2013_01_28\/529945.parm","name":"Winter Wonderland"},
]}
,
*/

		{
			name:"Heart of Maguuma",
		 	maps:[
		 		{ fileName :"1151420.data", name:"HoT BWE3 Raid" },
				{ fileName :"969663.data", name:"Verdant Brink" },				
			]
		},

		{
			name:"Maguuma Wastes",
		 	maps:[

		 		{ fileName :"836211.data", name:"Dry top (Past)" },
		 		{ fileName :"861770.data", name:"Dry top (Present)" },

		 		{ fileName :"909361.data", name:"The Silverwastes v1" },
		 		{ fileName :"996202.data", name:"The Silverwastes v2" },
		 		
			]
		},

		{
			name:"Other",
		 	maps:[

		 		{ fileName :"184799.data", name:"Empty Plane v1" },
		 		{ fileName :"197562.data", name:"Empty Plane v2" },
		 		
		 		{ fileName :"875614.data", name:"Unknown Mists Platforms" },
		 		{ fileName :"908730.data", name:"Glint's Lair" },		 		
		 		{ fileName :"969964.data", name:"Unknown Airship in tree" },

		 		{ fileName :"wizard.data.zip", name:"Wizard's tower ZIP" },
		 		{ fileName :"WizardsTowerSanctumCayLAKrytaMap.data", name:"Wizard's tower PARM" },
		 		
		 		{ fileName :"vale.data.zip", name:"Fortunes Vale ZIP" },
		 		{ fileName :"FortunesVale.data", name:"Fortunes Vale PARM" },
			]
		},
		

		{
			name:"PvP",
		 	maps:[
		 		{ fileName :"871093.data", name:"Original Stronghold" },
		 		{ fileName :"870987.data", name:"Champion's Dusk" },
		 		{ fileName :"197249.data", name:"Heart of the Mists" },
				{ fileName :"197402.data", name:"The Battle of Khylo" },
				{ fileName :"197545.data", name:"Forest of Niflhel" },
				{ fileName :"376916.data", name:"Legacy of the Foefire" },
				{ fileName :"467374.data", name:"Raid on the Capricorn" },
				{ fileName :"520609.data", name:"Temple of the Silent Storm" },
				{ fileName :"677968.data", name:"Skyhammer" },
				{ fileName :"791564.data", name:"Courtyard" },
				{ fileName :"556199.data", name:"Spirit Watch" },


				{ fileName :"506539.data", name:"Reaper's Rumble" },
				{ fileName :"529718.data", name:"Snowball Mayhem" },
				{ fileName :"595582.data", name:"Dragon Ball Arena" },
				{ fileName :"617120.data", name:"Aspect Arena" },

			]
		},

		{
			name:"WvW",
		 	maps:[
		 		{ fileName :"195806.data", name:"Eternal Battlegrounds" },
		 		{ fileName :"641501.data", name:"Borderlands" },
		 		{ fileName :"736241.data", name:"Edge of the Mists" },
			]
		},

		{
			name:"Dungeons",
		 	maps:[
	 			{ fileName :"189364.data", name:"Ascalonian Catacombs" },
				{ fileName :"275474.data", name:"Sorrow's Embrace" },
				{ fileName :"276520.data", name:"Honor of the Waves" },
				{ fileName :"284039.data", name:"Citadel of Flame" },
				{ fileName :"287214.data", name:"Caudecus's Manor" },

				{ fileName :"291284.data", name:"Twilight Arbor (Past)" },
				{ fileName :"645968.data", name:"Twilight Arbor (Present)" },

				{ fileName :"293606.data", name:"Crucible of Eternity" },
				{ fileName :"473930.data", name:"The Ruined City of Arah" },
				{ fileName :"473765.data", name:"Arah - Story" },

				{ fileName :"580061.data", name:"Molten Facility" },
				{ fileName :"595722.data", name:"Aetherblade Retreat" },
			]

		},

		{
			name:"The Mists (PvE)",
		 	maps:[
	 			{ fileName :"519839.data", name:"Fractals of the Mists" },
	 			{ fileName :"697450.data", name:"Thaumanova Reactor" },
	 			{ fileName :"506592.data", name:"Ascent to Madness" },
				{ fileName :"506670.data", name:"Mad King's Labyrinth (Past)" },
				{ fileName :"662436.data", name:"Mad King's Labyrinth (Present)" },
				{ fileName :"506739.data", name:"Mad King's Clock Tower" },
			]

		},


		{
			name:"Shiverpeak Mountains",
		 	maps:[
	 			{ fileName :"187611.data", name:"Wayfarer Foothills" },
	 			{ fileName :"568778.data", name:"Cragstead" },
	 			{ fileName :"197122.data", name:"Hoelbrak" },
	 			{ fileName :"186397.data", name:"Snowden Drifts" },
	 			{ fileName :"275155.data", name:"Dredgehaunt Cliffs" },
	 			{ fileName :"276252.data", name:"Frostgorge Sound" },
	 			{ fileName :"277587.data", name:"Lornar's Pass" },

				{ fileName :"278717.data", name:"Timberline Falls (Past)"},
				{ fileName :"846866.data", name:"Timberline Falls (Present)" },
			]

		},

		{
			name:"Far Shiverpeaks",
		 	maps:[
				{ fileName :"295282.data", name:"Eye of the North" },
			]

		},

		{
			name:"Ascalon",
		 	maps:[
	 			{ fileName :"188591.data", name:"Plains of Ashford" },
	 			{ fileName :"190490.data", name:"Diessa Plateau" },
	 			{ fileName :"196585.data", name:"Black Citadel" },
	 			{ fileName :"280025.data", name:"Blazeridge Steppes" },
				{ fileName :"281313.data", name:"Fireheart Rise" },
				{ fileName :"282668.data", name:"Iron Marches" },
				{ fileName :"283574.data", name:"Fields of Ruin" },
			]

		},

		{
			name:"Kryta",
		 	maps:[
	 			{ fileName :"191000.data", name:"Lion's Arch"},
	 			
	 			{ fileName :"191265.data", name:"Divinity's Reach" },
	 			{ fileName :"705746.data", name:"Divinity's Reach" },
	 			{ fileName :"193081.data", name:"North of Divinity's Reach" },

	 			{ fileName :"192711.data", name:"Queensdale" },
	 			
	 			{ fileName :"194288.data", name:"Kessex Hills v1" },
	 			{ fileName :"672138.data", name:"Kessex Hills v2" },
	 			{ fileName :"861815.data", name:"Kessex Hills v3" },


	 			{ fileName :"286945.data", name:"Bloodtide Coast" },
	 			{ fileName :"287870.data", name:"Harathi Hinterlands" },
				{ fileName :"289176.data", name:"Gendarran Fields" },
				{ fileName :"295005.data", name:"Chantry of Secrets" },
				{ fileName :"294938.data", name:"Claw Island" },
				{ fileName :"520479.data", name:"Southsun Cove" },
				{ fileName :"622681.data", name:"The Crown Pavilion" },
				

				{ fileName :"679089.data", name:"Tower of Nightmares" },
			]

		},

		{
			name:"Maguuma Jungle",
		 	maps:[
	 			{ fileName :"195149.data", name:"Caledon Forest" },
				{ fileName :"195493.data", name:"Metrica Province" },
				{ fileName :"922320.data", name:"Metrica Province Instance" },
				
				

				{ fileName :"198076.data", name:"The Grove" },
				{ fileName :"198272.data", name:"Rata Sum" },
				{ fileName :"291064.data", name:"Mount Maelstrom" },
				{ fileName :"292254.data", name:"Sparkfly Fen" },
				{ fileName :"293307.data", name:"Brisban Wildlands" },

				{ fileName :"636133.data", name:"SAB Hub" },
				{ fileName :"635555.data", name:"SAB World 1" },
				{ fileName :"635960.data", name:"SAB World 2" },

				{ fileName :"606255.data", name:"Zephyr Sanctum" },
		 		{ fileName :"605983.data", name:"Sanctum Sprint" },
				{ fileName :"606030.data", name:"Basket Brawl" },
			]

		},

		{
			name:"Ruins of Orr",
		 	maps:[
		 		{ fileName :"284829.data", name:"Straits of Devastation" },
		 		{ fileName :"285089.data", name:"Malchor's Leap" },
				{ fileName :"285634.data", name:"Cursed Shore" },
				{ fileName :"295179.data", name:"Cathedral of Hidden Depths" },
				{ fileName :"295962.data", name:"A Light in the Darkness" },
			]

		},

		{
			name:"Enchanted Snow Globe",
		 	maps:[
		 		{ fileName :"529896.data", name:"Tixx's Infinirarium" },
				{ fileName :"529945.data", name:"Winter Wonderland" },
			]

		},

		{
			name:"Historical Maps",
		 	maps:[

		 		{ fileName :"814803.data", name:"Lion's Arch" },
		 		
				{ fileName :"579383.data", name:"Skyhammer" },		///		
				{ fileName :"569756.data", name:"SAB" },		///	

				{ fileName :"122695.data", name:"Empty Plane" },		///	
				{ fileName :"127888.data", name:"Diessa Plateau" },		///	

				{ fileName :"128151.data", name:"Divinity's Reach"  },		///	 DR with collapse!

				{ fileName :"129524.data", name:"Queensdale" },		///	
				{ fileName :"129834.data", name:"North of Divinity's Reach" },		///	

				{ fileName :"130970.data", name:"Kessex Hills" },		///	Pre ToN!
				{ fileName :"131235.data", name:"Eternal Battlegrounds" },		///	No JP?
				{ fileName :"131574.data", name:"Borderlands" },		///	Pre ruins

				{ fileName :"131944.data", name:"Black Citadel" },		///	
				{ fileName :"132434.data", name:"Hoelbrak" },		///	
				{ fileName :"132570.data", name:"Heart of the Mists" },		///	

				{ fileName :"132710.data", name:"The Battle of Khylo" },		///	
				{ fileName :"132837.data", name:"Forest of Niflhel" },		///	
				{ fileName :"132853.data", name:"Empty Box" },		///	
				{ fileName :"124093.data", name:"Snowden Drifts" },		///	

				{ fileName :"125199.data", name:"Wayfarer Foothills" },		///	
				{ fileName :"126118.data", name:"Plains of Ashford" },		///	
				{ fileName :"126840.data", name:"Ascalonian Catacombs" },		///	
				
			]
		},
		
	]
}
},{}],3:[function(_dereq_,module,exports){
/**
 * Provides the static Tyria 3D API Class.
 * @module T3D
 */

/* INCLUDES */
LocalReader = _dereq_('./LocalReader/LocalReader.js');

/**
 * Tyria 3D Main API
 * 
 * Use this static class to access file parsers- and data renderer classes.
 * 
 * This class also works as a factory for creating
 * LocalReader instances that looks up and inflates files from the Guild Wars 2 .dat.
 *
 * @main T3D
 * @class T3D
 * @static 
 */
module.exports = T3D;
function T3D() {}

/* PRIVATE VARS */
var _version = "1.0.3";
var _settings = {
	inflaterURL : "modules/nacl/t3dgwtools.nmf"
};

/* PUBLIC PROPERTIES */

/**
 * The current API version. Used to make sure local storage caches are not
 * shared between different releases.
 *
 * @property version
 * @type String
 */
T3D.version = _version;


/* FILES */

/**
 * @property GW2File
 * @type Class
 */
T3D.GW2File =			_dereq_("./format/file/GW2File");

/**
 * @property GW2Chunk
 * @type Class
 */
T3D.GW2Chunk = 			_dereq_("./format/file/GW2Chunk");

/**
 * @property MapFile
 * @type Class
 */
T3D.MapFile = 			_dereq_("./format/file/MapFile");

/**
 * @property MaterialFile
 * @type Class
 */
T3D.MaterialFile = 		_dereq_("./format/file/MaterialFile");

/**
 * @property ModelFile
 * @type Class
 */
T3D.ModelFile = 		_dereq_("./format/file/ModelFile");

/**
 * @property PagedImageFile
 * @type Class
 */
T3D.PagedImageFile = 	_dereq_("./format/file/PagedImageFile");


/* RENDERERS */

/**
 * @property EnvironmentRenderer
 * @type Class
 */
T3D.EnvironmentRenderer = 	_dereq_("./dataRenderer/EnvironmentRenderer");

/**
 * @property HavokRenderer
 * @type Class
 */
T3D.HavokRenderer = 		_dereq_("./dataRenderer/HavokRenderer");

/**
 * @property PropertiesRenderer
 * @type Class
 */
T3D.PropertiesRenderer = 	_dereq_("./dataRenderer/PropertiesRenderer");

/**
 * @property TerrainRenderer
 * @type Class
 */
T3D.TerrainRenderer = 		_dereq_("./dataRenderer/TerrainRenderer");

/**
 * @property ZoneRenderer
 * @type Class
 */
T3D.ZoneRenderer = 			_dereq_("./dataRenderer/ZoneRenderer");



/* SETTINGS */

/**
 * Contains a list of known map id:s and their names. Used in order to quickly
 * look up what maps are in a .dat file.
 * @property MapFileList
 * @type Array
 */
T3D.MapFileList = 	_dereq_("./MapFileList");


/* PRIVATE METHODS */


/* PUBLIC METHODS */


/**
 * Creates a new instance of LocalReader, complete with inflater.
 * @method getLocalReader
 * @async
 * @param  {File}   	file		Core JS File instance, must refeer to a GW2 .dat file
 * @param  {element}	output		jQuery element, must have .title and .progress
 * @param  {Function}	callback	Callback function, fired when the file index is fully constructed
 * takes no parameters.
 * @return {LocalReader}				
 */
T3D.getLocalReader = function(file, output, callback){

	/// Create Inflater for this file reader.
	/// We use a wrapper to catch the events.
	/// We use the embed tag itself for posing messages.
	var pNaClWrapper = $("<div id='pNaClListener'/>");
	
	var pNaClEmbed = $("<embed type='application/x-pnacl'/>");
	pNaClEmbed.css({position:"absolute", width:0, height:0});
	pNaClEmbed.attr("src", _settings.inflaterURL)

	/// Add the objects to the DOM
	pNaClWrapper.append(pNaClEmbed);
	$("body").append(pNaClWrapper);

	/// Connect the provided file reference to a new LocalReader.
	var lrInstance = new LocalReader(file, _version, output);

	/// Give the LocalReader access to the inflater.
	lrInstance.connectInflater(pNaClEmbed, pNaClWrapper);

	/// Parse the DAT file MFT header. This must be done oncein order to access
	/// any files in the DAT.
	lrInstance.parseHeaderAsync(callback);

	/// Return file reader object
	return lrInstance;	
}

/**
 * Reads the map list from localStorage or the dat file. The resulting list is
 * passed via the callback function.
 * @method getMapListAsync
 * @async
 * @param {LocalReader} localReader
 * @param {Function} callback callback function
 * @param {Object} callback.mapList A list of maps grouped by area, for example
 * 
 * 		{	
 * 			maps:[
 * 				{
 * 					name: 'Heart of Maguuma',
 * 					maps: [
 * 						{fileName:1151420, name:'HoT BWE3 Raid'},
 * 						{fileName:969663, name:'Verdant Brink}
 * 					]
 * 				},
 * 				{
 * 					name: 'Unknown maps',
 * 					maps: [
 * 						{fileName:12345678, name:'Unknown map 12345678'}
 * 					]
 * 				}
 * 			]
 
 *	    };
 */
T3D.getMapListAsync = function(localReader, callback){

	/// Check local storage for an existing map list
	var mapList = localReader.loadMapList();

	/// If there is no cached list, read it.
	if(!mapList){
		localReader.readMapListAsync(false, callback);
	}

	/// Otherwise, just fire the callback with the cached list
	else{
		callback(mapList);
	}
	
}

/**
 * @method  renderMapContentsAsync
 * @param  {[type]} fileName [description]
 * @return {[type]}          [description]
 */
T3D.renderMapContentsAsync = function(localReader, fileName, renderers, callback){

	/// Value Object holding all generated objects
    var output = {
    	hasLight : 		false,
    	boundingBox : 	null,
    	ambientLight : 	null,
    	
    	skyElements :	[],
    	visibles : 		[],
    	terrainTiles : 	[],
    	collisions : 	[],
    	nonCollisions: 	[],
    	lights : 		[],
    };

	/// Make sure we got an actuall ID number		
	if(parseInt(fileName)){

		/// File name is baseId, load using local reader.
		localReader.loadFile(
			fileName,
			function(arrayBuffer){

				/// Set up datastream
				var ds = new DataStream(arrayBuffer, 0, DataStream.LITTLE_ENDIAN);

				/// Initiate Map file object. Connect callback
				var mapFile = new T3D.MapFile(ds, 0);

				/// Populate VO by running the renderers
			    var runAllRenderers = function(i){
					
					/// Run each renderer
					if(i < renderers.length ){
						T3D.runRenderer(
							renderers[i].renderClass,
							localReader,
							mapFile,
							renderers[i].settings,
							output,
							runAllRenderers.bind(this,i+1)
						);
					}

					/// Fire callback with VO when done
					else{
						callback(output);
					}
				};

				/// Starting point for running each renderer
				runAllRenderers(0);
			}
		);
	}

	/// Primitive error message...
	else{
		console.error("Map id must be an integer!, was:",fileName);
	}	
}

/**
 * method runRenderer description
 * @param  {[type]}   renderClass [description]
 * @param  {[type]}   localReader [description]
 * @param  {[type]}   mapFile     [description]
 * @param  {[type]}   settings    [description]
 * @param  {[type]}   output      [description]
 * @param  {Function} cb          [description]
 * @return {[type]}               [description]
 */
T3D.runRenderer = function(renderClass, localReader, mapFile , settings, output, cb){

	var r = new renderClass(
		localReader,
		mapFile,
		settings,
		output
	);

	r.renderAsync(cb);
}
},{"./LocalReader/LocalReader.js":1,"./MapFileList":2,"./dataRenderer/EnvironmentRenderer":5,"./dataRenderer/HavokRenderer":6,"./dataRenderer/PropertiesRenderer":7,"./dataRenderer/TerrainRenderer":8,"./dataRenderer/ZoneRenderer":9,"./format/file/GW2Chunk":25,"./format/file/GW2File":26,"./format/file/MapFile":27,"./format/file/MaterialFile":28,"./format/file/ModelFile":29,"./format/file/PagedImageFile":30}],4:[function(_dereq_,module,exports){
/**
 * Base class for data interpretors
 * @class DataRenderer
 * @param  {[type]} localReader  [description]
 * @param  {[type]} mapFile      [description]
 * @param  {[type]} settings     [description]
 * @param  {[type]} output       [description]
 */
var DataRenderer = module.exports = function(localReader, mapFile, settings, output) {
	this.localReader = localReader;
	this.mapFile = mapFile;
	this.settings = settings;
	this.output = output;
	
	/**
	 * TODO
	 * @method log
	 * @param  {[type]} title    [description]
	 * @param  {[type]} message  [description]
	 * @param  {[type]} severity [description]
	 * @return {[type]}          [description]
	 */
	this.log = function(title, message, severity){
		logElem = $("#progressPanel");
		logElem.find(".title").html(title);
		logElem.find(".progress").html(message);
	}
}

/**
 * ABOUT RENDER! TODO
 * @method renderAsync
 * @async
 */
DataRenderer.prototype.renderAsync = function(callback){}
},{}],5:[function(_dereq_,module,exports){
var Utils = _dereq_("../util/RenderUtils");
var DataRenderer = _dereq_('./DataRenderer');

/**
 * @class EnvironmentRenderer
 * @constructor
 * @extends DataRenderer
 * @param  {[type]} localReader  [description]
 * @param  {[type]} mapFile  [description]
 * @param  {[type]} settings [description]
 * @param  {[type]} output   [description]
 */
function EnvironmentRenderer(localReader, mapFile, settings, output){
	DataRenderer.call(this, localReader, mapFile, settings, output);

	this.getMat = function(tex){
		return new THREE.MeshBasicMaterial({
			map: tex,
			side: THREE.BackSide,
			fog: false,
			depthWrite: false
		});
	};

	this.loadTextureWithFallback = function(targetMatIndices, materialArray, filename, fallbackFilename, hazeColorAsInt){
		var self = this;
		
		function writeMat(mat){
			targetMatIndices.forEach(function(i){
				materialArray[i] = mat; 
			});
		}

		function loadFallback(){
			var mat = self.getMat(
				THREE.ImageUtils.loadTexture(fallbackFilename)
			);

			writeMat(mat);
		}

		function errorCallback(){
			setTimeout(loadFallback, 1);
		}

		var mat = self.getMat(
			Utils.loadLocalTexture(
				localReader,
				filename,
				null, hazeColorAsInt,
				errorCallback )
		);

		writeMat(mat);			
	}

	this.getHazeColor = function(environmentChunkData){
		var hazes = environmentChunkData && environmentChunkData.dataGlobal.haze;

		if(!hazes || hazes.length<=0){
			return [190, 160, 60];
		}
		else{
			return hazes[0].farColor;
		}
	};

	this.parseLights = function(environmentChunkData){
		var self = this;

		var lights = environmentChunkData ? environmentChunkData.dataGlobal.lighting : [{
			lights:[],
			backlightIntensity:1.0,
			backlightColor:[255,255,255]
		}];

		var ambientLight;

		//var light = lights[0];
		var hasLight = false;
		lights.forEach(function(light, idx){

			if(hasLight)
				return;

			/// Directional lights
			var sumDirLightIntensity = 0;

			light.lights.forEach(function(dirLightData,idx){

				hasLight = true;
				
				var color = new THREE.Color(
					dirLightData.color[2]/255.0,
					dirLightData.color[1]/255.0,
					dirLightData.color[0]/255.0
				);

				var directionalLight = new THREE.DirectionalLight( color.getHex(), dirLightData.intensity );
				
				directionalLight.position.set(
					-dirLightData.direction[0],
					dirLightData.direction[2],
					dirLightData.direction[1]
				).normalize();
				
				sumDirLightIntensity += dirLightData.intensity;

				self.output.lights.push(directionalLight);

			});// END for each directional light in light


			/// Ambient light
			//light.backlightIntensity /= sumDirLightIntensity +light.backlightIntensity; 
			light.backlightIntensity =  light.backlightIntensity; 
			var color = new THREE.Color(
				/*light.backlightIntensity * light.backlightColor[2]/255.0,
				light.backlightIntensity * light.backlightColor[1]/255.0,
				light.backlightIntensity * light.backlightColor[0]/255.0*/
				light.backlightIntensity * (255.0-light.backlightColor[2])/255.0,
				light.backlightIntensity * (255.0-light.backlightColor[1])/255.0,
				light.backlightIntensity * (255.0-light.backlightColor[0])/255.0
			);

			ambientLight = new THREE.AmbientLight(color);

		})// END for each light in lighting

		var ambientTotal = 0;
		if(ambientLight){
			ambientTotal = ambientLight.color.r + ambientLight.color.g + ambientLight.color.b;
			this.output.lights.push(ambientLight);
		}

		/// Parsing done, set hasLight flag and return
		this.output.hasLight = hasLight || ambientTotal>0;		
	};

	this.parseSkybox = function(environmentChunkData, parameterChunkData, hazeColorAsInt){
		
		/// Grab sky texture.
		/// index 0 and 1 day
		/// index 2 and 3 evening
		var skyModeTex = this.environmentChunkData && this.environmentChunkData.dataGlobal.skyModeTex[0];

		/// Fallback skyboxfrom dat.
		if(!skyModeTex){
			skyModeTex = {
				texPathNE:1930687,
				texPathSW:193069,
				texPathT:193071
			}
		}

		/// Calculate bounds
		var bounds = parameterChunkData.rect;
		var mapW = Math.abs( bounds.x1 -bounds.x2 );
		var mapD = Math.abs( bounds.y1 -bounds.y2 );
		var boundSide = Math.max( mapW, mapD );

		var materialArray = [];

		/// Load skybox textures, fallback to hosted png files.
		this.loadTextureWithFallback([1,4], materialArray, skyModeTex.texPathNE + 1, "img/193068.png", hazeColorAsInt);
		this.loadTextureWithFallback([0,5], materialArray, skyModeTex.texPathSW + 1, "img/193070.png", hazeColorAsInt);
		this.loadTextureWithFallback([2], materialArray, skyModeTex.texPathT + 1, "img/193072.png", hazeColorAsInt);
		materialArray[3] = new THREE.MeshBasicMaterial({visible:false});


		/// Create skybox geometry
		var boxSize = 1024;		
		var skyGeometry = new THREE.BoxGeometry( boxSize, boxSize/2 , boxSize ); //Width Height Depth

		/// Ugly way of fixing UV maps for the skybox (I think)
		skyGeometry.faceVertexUvs[0].forEach(function(vecs, idx){

			var face = Math.floor(idx/2);

			// PX NX
			// PY NY
			// PZ NZ

			/// PX - WEST 	NX - EAST
			if(face == 0 || face == 1){
				vecs.forEach(function(vec2){
					vec2.x = 1 - vec2.x;	
					vec2.y /= 2.0;	
					vec2.y += .5;	
				});
			}

			/// NZ - SOUTH 	PZ - NORTH
			else if(face == 5 || face == 4){
				vecs.forEach(function(vec2){
					vec2.y /= -2.0;	
					vec2.y += .5;	
				});
			}

			else{
				vecs.forEach(function(vec2){
					vec2.x = 1 - vec2.x;	
				});
			}

		});

		skyGeometry.uvsNeedUpdate = true;
		
		/// Generate final skybox
		var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
		var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );

		/// Put horizon in camera center
		skyBox.translateY(boxSize/4);
		//skyBox.translateY( -environmentChunk.data.dataGlobal.sky.verticalOffset );
		
		/// Write to output
		output.skyElements.push(skyBox);
	};
}


/// DataRenderer inherrintance:
EnvironmentRenderer.prototype = Object.create(DataRenderer.prototype);
EnvironmentRenderer.prototype.constructor = EnvironmentRenderer;

/// DataRenderer overwrite:
EnvironmentRenderer.prototype.renderAsync = function(callback){

	var environmentChunkData = this.mapFile.getChunk("env").data;
	var parameterChunkData = this.mapFile.getChunk("parm").data;

	/// Set renderer clear color from environment haze
	var hazeColor = this.getHazeColor(environmentChunkData);
	var hazeColorAsInt =  hazeColor[2]*256*256+hazeColor[1]*256+hazeColor[0];
	this.output.hazeColor = hazeColor;

	/// Add directional lights to output. Also write hasLight flag
	this.parseLights(environmentChunkData);

	/// Generate skybox
	this.parseSkybox(environmentChunkData, parameterChunkData, hazeColorAsInt);

	/// All parsing is synchronous, just fire callback
	callback();
};
	

module.exports = EnvironmentRenderer;
},{"../util/RenderUtils":34,"./DataRenderer":4}],6:[function(_dereq_,module,exports){
var DataRenderer = _dereq_('./DataRenderer');

/**
 * @class HavokRenderer
 * @constructor
 * @extends DataRenderer
 * @param  {[type]} localReader  [description]
 * @param  {[type]} mapFile  [description]
 * @param  {[type]} settings [description]
 * @param  {[type]} output   [description]
 */
function HavokRenderer(localReader, mapFile, settings, output){
	DataRenderer.call(this, localReader, mapFile, settings, output);

	this.lastP = -1;
	this.seed = 1;
	this.meshes = [];

	/**
	 * @method renderModels
	 * @param  {Function} callback         [description]
	 * @async
	 */
	this.renderModels = function(models, title, callback){
		var mat;
		if(this.settings && this.settings.visible){
			mat = new THREE.MeshNormalMaterial( { side: THREE.DoubleSide } ); 
		}
		else{
			mat = new THREE.MeshBasicMaterial( { visible: false } );			
		}

		this.parseAllModels(models, mat, title, 200, 0, callback);
	}


	/**
	 * @method  getCollisionsForAnimation
	 * @param  {[type]} animation  [description]
	 * @param  {[type]} collisions [description]
	 * @return {[type]}            [description]
	 */
	this.getCollisionsForAnimation = function(animation, collisions){
		var ret = [];
		
		for (var i = 0; i < animation.collisionIndices.length; i++) {
			var index = animation.collisionIndices[i];
			var collision = collisions[ index ];
			collision.index = index;
			ret.push( collision );
		}
		
		return ret;
	};

	/**
	 * @method  parseAllModels description
	 * @param  {[type]} models       [description]
	 * @param  {[type]} mat       [description]
	 * @param  {[type]} title     [description]
	 * @param  {[type]} chunkSize [description]
	 * @param  {[type]} offset    [description]
	 * @return {[type]} callback          [description]
	 * @async
	 */
	this.parseAllModels = function(models, mat, title, chunkSize, offset, callback){
		var i = offset;		

		for(; i < offset+chunkSize && i < models.length; i++){
			
			var p = Math.round(i*100/ models.length );
			if( p != this.lastP){

				this.log(
					"Loading Collision Models ("+title+")",
					p+"%"
				);
				this.lastP = p;
			}	
		
			/// Get animation object
			var animation =  this.animationFromGeomIndex(
				models[i].geometryIndex,
				this.geometries,
				this.animations
			);
			
			var collisions = this.getCollisionsForAnimation( animation, this.havokChunkData.collisions);
			
			for(var j=0; j< collisions.length; j++){
				var collision = collisions[j];			
		 		this.renderMesh( collision, models[i], mat );
			}
		}

		if(i<models.length){
			window.setTimeout(
				this.parseAllModels.bind(this, models, mat, title, chunkSize, offset+chunkSize, callback),
				10 /*time in ms to next call*/
			);
		}
		else{
			callback();
		}
	}

	/**
	 * [animationFromGeomIndex description]
	 * @param  {[type]} propGeomIndex [description]
	 * @param  {[type]} geometries    [description]
	 * @param  {[type]} animations    [description]
	 * @return {[type]}               [description]
	 */
	this.animationFromGeomIndex = function(propGeomIndex, geometries, animations){
		
		// geometries is just list of all geometries.animations[end] for now
		var l = geometries[propGeomIndex].animations.length;
		
		return animations[ geometries[propGeomIndex].animations[l-1] ];
		//return animations[ geometries[propGeomIndex].animations[0] ];
	};

	/**
	 * @method renderMesh
	 * @param  {[type]} collision [description]
	 * @param  {[type]} model     [description]
	 * @param  {[type]} mat       [description]
	 * @return {[type]}           [description]
	 */
	this.renderMesh = function( collision, model, mat ){
	    
	    var pos = model.translate;
	    var rot = model.rotation;
	    var scale = 32 * model.scale;    
	    
	    /// Generate mesh
	    var mesh = this.parseHavokMesh(collision, mat);
	    
	    /// Position mesh
	    mesh.position.set(pos.x, -pos.y, -pos.z);    
	    
	    /// Scale mesh
	    if(scale)
	    	mesh.scale.set( scale, scale, scale );

	    /// Rotate mesh
	    if(rot){
	    	mesh.rotation.order = "ZXY";
	    	mesh.rotation.set(rot.x, -rot.y, -rot.z);
	    }
	    	
		/// Add mesh to scene and collisions
		this.output.visibles.push(mesh);
		this.output.collisions.push(mesh);
	};


	/**
	 * @method  seedRandom
	 * @return {[type]} [description]
	 */
	this.seedRandom = function(){
	    var x = Math.sin(this.seed++) * 10000;
	    return x - Math.floor(x);
	};

	/**
	 * @method  parseHavokMesh
	 * @param  {[type]} collision [description]
	 * @param  {[type]} mat       [description]
	 * @return {[type]}           [description]
	 */
	this.parseHavokMesh = function(collision, mat){
		var index = collision.index;

		if(!this.meshes[index]){

			var geom = new THREE.Geometry();
			
			/// Pass vertices	    		
			for(var i=0; i<collision.vertices.length; i++){
				var v=collision.vertices[i];
				geom.vertices.push( new THREE.Vector3(v.x , v.y , -v.z ) );
			}	    		
				
			/// Pass faces
			for(var i=0; i<collision.indices.length; i+=3){

				var f1=collision.indices[i];
				var f2=collision.indices[i+1];
				var f3=collision.indices[i+2];

				if( f1<=collision.vertices.length &&
					f2<=collision.vertices.length &&
					f3<=collision.vertices.length)
	   				geom.faces.push( new THREE.Face3( f1, f2, f3 ) );
	   			else
	   				console.warn("Errorus index!");
			}

			/// Prepare geometry and pass new mesh
			geom.computeFaceNormals();
			//geom.computeVertexNormals();
			
			
			this.meshes[index]= new THREE.Mesh( geom, mat ); 

			
			return this.meshes[index];
		}
		else{
			return this.meshes[index].clone();
		}
	};

};


/// DataRenderer inherrintance:
HavokRenderer.prototype = Object.create(DataRenderer.prototype);
HavokRenderer.prototype.constructor = HavokRenderer;

/// DataRenderer overwrites:

/**
 * HavokRenderer render implementation. TODO
 * @method renderAsync
 * @param  {Function} callback [description]
 * @async
 */
HavokRenderer.prototype.renderAsync = function(callback){
	var self = this;

	// TODO:The design of this method pretty much requires one instance
	// of the class per parallel async render. Should probably fix this
	// at some point...
	
	/// Get required chunks
	this.havokChunkData = this.mapFile.getChunk("havk").data;

    /// Set static bounds to the bounds of the havk models
    output.boundingBox = this.havokChunkData.boundsMax;
	
	/// Clear old meshes
	this.meshes = [];

	/// Grab model raw data from the chunk.
	/// Add missing scale value to obs models.
	var propModels = this.havokChunkData.propModels;
	var zoneModels = this.havokChunkData.zoneModels;
	var obsModels = this.havokChunkData.obsModels;
	obsModels.forEach(function(mdl){
		mdl.scale = 1;
	});

	/// Store geoms and animations from the file in hte instance so we don't
	/// have to pass them arround too much. (fix this later)
	this.geometries = this.havokChunkData.geometries;
	this.animations = this.havokChunkData.animations;		
	
	/// Render "prop", "zone" and "obs" models in that order.
	var renderPropModelsCB = function(){
		self.renderModels(zoneModels, "zone", renderZoneModelsCB);
	};
	var renderZoneModelsCB = function(){
		self.renderModels(obsModels, "obs", callback);
	};
	self.renderModels(propModels, "prop", renderPropModelsCB);

	
}

module.exports = HavokRenderer;
},{"./DataRenderer":4}],7:[function(_dereq_,module,exports){
var Utils = _dereq_("../util/RenderUtils");
var DataRenderer = _dereq_('./DataRenderer');

/**
 * @class PropertiesRenderer
 * @constructor
 * @extends DataRenderer
 * @param  {[type]} localReader  [description]
 * @param  {[type]} mapFile  [description]
 * @param  {[type]} settings [description]
 * @param  {[type]} output   [description]
 */
function PropertiesRenderer(localReader, mapFile, settings, output){
	DataRenderer.call(this, localReader, mapFile, settings, output);
}

/// DataRenderer inherrintance:
PropertiesRenderer.prototype = Object.create(DataRenderer.prototype);
PropertiesRenderer.prototype.constructor = PropertiesRenderer;

/// DataRenderer overwrite:

/**
* Renders all property meshes in a GW2 map described by the map's PROP chunk.
*/
PropertiesRenderer.prototype.renderAsync = function(callback){
	var self = this;

	var propertiesChunkData =  this.mapFile.getChunk("prp2").data;

	if(!propertiesChunkData){
		renderCallback();
		return;
	}

	var props = propertiesChunkData.propArray;
	var animProps =propertiesChunkData.propAnimArray;
	var instanceProps = propertiesChunkData.propInstanceArray;
	var metaProps = propertiesChunkData.propMetaArray;

	/// Concat all prop types
	props = props
		.concat(animProps)
		.concat(instanceProps)
		.concat(metaProps);

	// For now, we'll do all load in serial
	// TODO: load unique meshes and textures in parallell (asynch), then render!
	var lastPct = -1;

	var renderIndex = function(idx){

		if(idx>= props.length){
			callback();
			return;
		}

		var pct = Math.round(1000.0*idx / props.length);
		pct/=10.0;
		
		/// Log progress
		if(lastPct!=pct){
			var pctStr = pct +
				( pct.toString().indexOf(".")<0 ? ".0":"" ) +
				"%";
			self.log("Loading 3D Models (Props)", pctStr);
			lastPct = pct;
		}

		/// Read prop at index.
		var prop = props[idx];								

	    /// Adds a single mesh to a group.
		var addMeshToLOD = function(mesh, groups, lod, prop, needsClone){

			/// Read lod distance before overwriting mesh variable
		    var lodDist = prop.lod2 != 0 ? prop.lod2 : mesh.lodOverride[1];

		    /// Read flags before overwriting mesh variable
	    	var flags = mesh.flags;

	    	/// Mesh flags are 0 1 4
	    	/// For now, use flag 0 as the default level of detail
	    	if(flags==0)
	    		lodDist=0;
	    	
	    	/// Create new empty mesh if needed
	    	if(needsClone){
	    		mesh = new THREE.Mesh( mesh.geometry, mesh.material );
	    	}

	    	mesh.updateMatrix();
			mesh.matrixAutoUpdate = false;

	    	// Find group for this LOD distance
	    	if(groups[lodDist]){
	    		groups[lodDist].add(mesh);
	    	}
	    	// Or create LOD group and add to a level of detail
	    	// WIP, needs some testing!
	    	else{
	    		var group = new THREE.Group();
	    		group.updateMatrix();
				group.matrixAutoUpdate = false;
	    		group.add(mesh);
	    		groups[lodDist] = group;
	    		lod.addLevel(group,lodDist);
	    	}

	    	return lodDist;
	    }

	    /// Adds array of meshes to the scene, also adds transform clones
		var addMeshesToScene = function(meshArray, needsClone, boundingSphere){
			
		    ///Add original 

		    /// Make LOD object and an array of groups for each LOD level
		    var groups = {};
		    var lod = new THREE.LOD();

		    /// Each mesh is added to a group corresponding to its LOD distane
		    var maxDist = 0;
		    meshArray.forEach(function(mesh){
		    	maxDist = Math.max( maxDist, addMeshToLOD(mesh,groups,lod,prop,needsClone) );
	    	});

	    	/// Add invisible level
	    	//lod.addLevel(new THREE.Group(),10000);

		    /// Set position, scale and rotation of the LOD object
			if(prop.rotation){
		    	lod.rotation.order = "ZXY";
		    	lod.rotation.set(prop.rotation.x, -prop.rotation.y, -prop.rotation.z);
		    }
		    lod.scale.set( prop.scale, prop.scale, prop.scale );
		    lod.position.set(prop.position.x, -prop.position.y, -prop.position.z);
		   	

		   	lod.boundingSphereRadius = boundingSphere.radius * prop.scale;

		    lod.updateMatrix();
		    lod.matrixAutoUpdate = false;

		    /// Show highest level always
		    //lod.update(lod);

	    	//Add LOD containing mesh instances to scene
	    	self.output.visibles.push(lod);
	    	self.output.nonCollisions.push(lod);
	    				    
		    // Add one copy per transform, needs to be within it's own LOD
		    if(prop.transforms){

		    	prop.transforms.forEach(function(transform){
		    		
		    		/// Make LOD object and an array of groups for each LOD level
		    		var groups = {};
		    		var lod = new THREE.LOD();

		    		/// Each mesh is added to a group corresponding to its LOD distane
		    		var maxDist = 0;
			    	meshArray.forEach(function(mesh){
			    		maxDist = Math.max( maxDist, addMeshToLOD(mesh,groups,lod,prop,true) );
			    	});

			    	/// Add invisible level
	    			//lod.addLevel(new THREE.Group(),10000);

			    	/// Set position, scale and rotation of the LOD object
					if(transform.rotation){
				    	lod.rotation.order = "ZXY";
				    	lod.rotation.set(transform.rotation.x, -transform.rotation.y, -transform.rotation.z);
				    }
				    lod.scale.set( transform.scale, transform.scale, transform.scale );
				    lod.position.set(transform.position.x, -transform.position.y, -transform.position.z);

					lod.updateMatrix();
		    		lod.matrixAutoUpdate = false;

		    		lod.boundingSphereRadius = boundingSphere.radius * prop.scale;

					/// Show highest level always
		    		//lod.update(lod);

			    	/// Add LOD containing mesh instances to scenerender: function(propertiesChunkHeader, map, localReader, renderCallback){
			    	self.output.visibles.push(lod);
			    	self.output.nonCollisions.push(lod);

			    });
		    }
		}

		/// Get meshes
		Utils.getMeshesForFilename(prop.filename, prop.color, self.localReader,
			function(meshes, isCached, boundingSphere){
			
				if(meshes){
					addMeshesToScene(meshes, isCached, boundingSphere);
				}

				/// Render next prop
				renderIndex(idx+1);
			}
		);

		

	};

	/// Start serial loading and redering. (to allow re-using meshes and textures)
	/// textures prolly won't work?
	renderIndex(0);
}


/**
 * TODO: write description. Used for export feature
 * @method getFileIdsAsync
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
PropertiesRenderer.prototype.getFileIdsAsync = function(callback){
	var fileIds = [];

	var propertiesChunkData =  this.mapFile.getChunk("prp2").data;

	var props = propertiesChunkData.propArray;
	var animProps = propertiesChunkData.propAnimArray;
	var instanceProps = propertiesChunkData.propInstanceArray;
	var metaProps = propertiesChunkData.propMetaArray;

	props = props
		.concat(animProps)
		.concat(instanceProps)
		.concat(metaProps);

	var getIdsForProp = function(idx){

		if(idx>=props.length){
			callback(fileIds);
			return;
		}

		if(idx%100==0){
			console.log("getting ids for entry",idx,"of",props.length);
		}

		var prop = props[idx];
		Utils.getFilesUsedByModel(
			prop.filename,
			localReader,
			function(propFileIds){
				fileIds = fileIds.concat(propFileIds);
				getIdsForProp(idx+1);
			}
		);

	};

	getIdsForProp(0);
};

module.exports = PropertiesRenderer;
},{"../util/RenderUtils":34,"./DataRenderer":4}],8:[function(_dereq_,module,exports){
var Utils = _dereq_("../util/RenderUtils");
var DataRenderer = _dereq_('./DataRenderer');
var PagedImageFile = _dereq_("../format/file/PagedImageFile.js"); 

/**
 * @class TerrainRenderer
 * @constructor
 * @extends DataRenderer
 * @param  {[type]} localReader  [description]
 * @param  {[type]} mapFile  [description]
 * @param  {[type]} settings [description]
 * @param  {[type]} output   [description]
 */
function TerrainRenderer(localReader, mapFile, settings, output){
	DataRenderer.call(this, localReader, mapFile, settings, output);

	this.drawWater = function(rect){
		
		/// Add Water
		var material = material || new THREE.MeshBasicMaterial(
			{
				color: 0x5bb1e8,
				wireframe:false,
			 	opacity: 0.35
			}
		);

		material.transparent = true;
		return Utils.renderRect(rect, 0, material);
	}

	this.parseNumChunks = function(terrainData){
		terrainData.numChunksD_1 = Math.sqrt(
			terrainData.dims.dim1 *
			terrainData.chunks.length /
			terrainData.dims.dim2
		);
		terrainData.numChunksD_2 =
			terrainData.chunks.length / terrainData.numChunksD_1;
	}

	this.loadPagedImageCallback = function(callback, infaltedBuffer){
		var self = this;

		/// Now we've collected all files needed, do the acutal rendering

		var pimgDS = new DataStream(infaltedBuffer);
		var pimgFile = new PagedImageFile(pimgDS,0);
		var pimgTableDataChunk = pimgFile.getChunk("pgtb");
		var pimgData = pimgTableDataChunk && pimgTableDataChunk.data;

		this.mapRect = null;

		/// Fetch chunks
		var terrainData = this.mapFile.getChunk("trn").data;
		var parameterData = this.mapFile.getChunk("parm").data;

		/// Read settings
		var maxAnisotropy = this.settings.anisotropy ? this.settings.anisotropy : 1;

		var chunks = [];	
		var chunkW = 35;

		/// Calculate numChunksD_1 and _2
		this.parseNumChunks(terrainData);

		var xChunks = terrainData.numChunksD_1;
		var yChunks = terrainData.numChunksD_2;

		var allMaterials = terrainData.materials.materials;
		var allTextures = terrainData.materials.texFileArray;

		//Total map dx and dy
		var dx = parameterData.rect.x2 - parameterData.rect.x1;
		var dy = parameterData.rect.y2 - parameterData.rect.y1;

		//Each chunk dx and dy
		var cdx = dx/terrainData.numChunksD_1 * 1;//  35/33;
		var cdy =dy/terrainData.numChunksD_2 * 1;//35/33;
		var n=0;
		var allMats = [];
		var customMaterial = new THREE.MeshLambertMaterial({side: THREE.DoubleSide, color:0x666666, shading: THREE.FlatShading}); 
		var texMats = {};

		/// Load textures from PIMG and inject as material maps (textures)
		var chunkTextures={};
		
		/// Load textures
		if(pimgData){
			var strippedPages = pimgData.strippedPages;

			///Only use layer 0
			strippedPages.forEach(function(page){
				
				/// Only load layer 0 and 1
				if(page.layer<=1){
					var filename = page.filename;
					var color = page.solidColor;
					var coord = page.coord;

					var matName = coord[0]+","+coord[1];
					if(page.layer == 1)
						matName+="-2";


					/// Add texture to list, note that coord name is used, not actual file name
					if(!chunkTextures[matName]){

						/// Load local texture, here we use file name!
						var chunkTex = Utils.loadLocalTexture(self.localReader, filename);

						if(chunkTex){
							/// Set repeat, antistropy and repeat Y
							chunkTex.anisotropy = maxAnisotropy;
							chunkTex.wrapT = chunkTex.wrapS = THREE.RepeatWrapping;					
						}

						///...But store in coord name
						chunkTextures[matName] = chunkTex;					
						
					}
				}

			});/// end for each stripped page in pimgData
		}
		
		
				
		/// Render Each chunk
		/// We'll make this async in order for the screen to be able to update

		var renderChunk = function(cx,cy){
			var chunkIndex = cy*xChunks + cx;

			var pageX = Math.floor(cx/4);
			var pageY = Math.floor(cy/4);

			var chunkTextureIndices = allMaterials[chunkIndex].loResMaterial.texIndexArray;
			var matFileName = allMaterials[chunkIndex].loResMaterial.materialFile;		
			var chunkData = terrainData.chunks[chunkIndex];

			var mainTex = allTextures[chunkTextureIndices[0]];
			var mat = customMaterial;

			/// TODO: just tick invert y = false...?
			var pageOffetX = (cx % 4)/4.0;
			var pageOffetY = 0.75 - (cy % 4)/4.0;

			//offset 0 -> 0.75
			
			
			//Make sure we have shared textures

			/// Load and store all tiled textures
			var fileNames = [];
			for(var gi=0;gi<chunkTextureIndices.length/2;gi++){
				var textureFileName = allTextures[chunkTextureIndices[gi]].filename;

				fileNames.push(textureFileName);
				
				/// If the texture is not already loaded, read it from the .dat!
				if(!chunkTextures[textureFileName]){

					/// Load local texture
					var chunkTex = Utils.loadLocalTexture(self.localReader, textureFileName);

					if(chunkTex){
						/// Set repeat, antistropy and repeat Y
						chunkTex.anisotropy = maxAnisotropy;
						chunkTex.wrapT = chunkTex.wrapS = THREE.RepeatWrapping;		
					}

					chunkTextures[textureFileName] = chunkTex;					
				}
			}/// End for each chunkTextureIndices


			/// Create Composite texture material, refering the shared textures
			var pageTexName=  pageX+","+pageY;				
			var pageTexName2=  pageX+","+pageY+"-2";				


			/// TODO USe mapData
			//var fog = SceneUtils.getScene().fog;
			var fog = {
				color: {r:255,g:255,b:255},
				near: 0,
				far: 0
			}
			
			var uniforms = THREE.UniformsUtils.merge([
	    		THREE.UniformsLib['lights'],
			]);

			/// FOG
			uniforms.fogColor = { type: "v3", value: new THREE.Vector3( fog.color.r, fog.color.g, fog.color.b ) },
			uniforms.fogNear = { type: "f",value: fog.near },
			uniforms.fogFar = { type: "f", value: fog.far },


			/// TODO: READ FROM VO, don't default to hard coded scale
			uniforms.uvScale = { type: "v2", value: new THREE.Vector2( 8.0, 8.0 ) };
			uniforms.offset = { type: "v2", value: new THREE.Vector2( pageOffetX, pageOffetY) };

			uniforms.texturePicker = {type: "t", value: chunkTextures[pageTexName]};
			uniforms.texturePicker2 = {type: "t", value: chunkTextures[pageTexName2]};

			uniforms.texture1 = { type: "t", value: chunkTextures[fileNames[0]]};
			uniforms.texture2 = { type: "t", value: chunkTextures[fileNames[1]]};
			uniforms.texture3 = { type: "t", value: chunkTextures[fileNames[2]]};
			uniforms.texture4 = { type: "t", value: chunkTextures[fileNames[3]]};
			

			mat = new THREE.ShaderMaterial( {
				uniforms: uniforms,
				vertexShader: document.getElementById( 'vertexShader' ).textContent,
				fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
				lights: true
			} );

			///Store referenceto each material
			allMats.push(mat);

			
			/// -1 for faces -> vertices , -2 for ignoring outer faces
			var chunkGeo =  new THREE.PlaneBufferGeometry ( cdx, cdy, chunkW-3, chunkW-3);

			var cn = 0;

			///Render chunk

			/// Each chunk vertex
			for(var y=0; y<chunkW; y++){

				for(var x=0; x<chunkW; x++){
			
					if(  x != 0 && x !=chunkW-1 && y!=0 && y !=chunkW-1 )
					{
						chunkGeo.attributes.position.array[cn*3+2] = terrainData.heightMap[n];
						cn++;
					}
					
					n++;					
				}
			} // End each chunk vertex

			
			/// Flip the plane to fit wonky THREE js world axes
			var mS = (new THREE.Matrix4()).identity();
			mS.elements[5] = -1;
			chunkGeo.applyMatrix(mS);

			/// Compute face normals for lighting, not used when textured
			chunkGeo.computeFaceNormals();
			//chunkGeo.computeVertexNormals();


			/// Build chunk mesh!
			var chunk;
			chunk = new THREE.Mesh(	chunkGeo , customMaterial );
			if(mat.length){
				chunk = THREE.SceneUtils.createMultiMaterialObject( chunkGeo, mat );
			}
			else{
				chunk = new THREE.Mesh(	chunkGeo , mat );	
			}	


			///Move and rotate Mesh to fit in place
			chunk.rotation.set(Math.PI/2,0,0);
			
			/// Last term is the new one: -cdx*(2/35)
			var globalOffsetX = parameterData.rect.x1 + cdx/2;
			var chunkOffsetX = cx * cdx;

			chunk.position.x = globalOffsetX + chunkOffsetX;

			///Adjust for odd / even number of chunks
			if(terrainData.numChunksD_2 % 2 == 0){
				
				/// Last term is the new one: -cdx*(2/35)
				var globalOffsetY = parameterData.rect.y1 + cdy/2 -0;// -cdy*(1/35);
				var chunkOffsetY = cy * cdy * 1;//33/35;

				chunk.position.z =  chunkOffsetY + globalOffsetY;
			}
			else{

				var globalOffsetY =  parameterData.rect.y1 - cdy/2 + 0; //cdy*(1/35);
				var chunkOffsetY = cy * cdy * 1;//33/35;

				chunk.position.z = globalOffsetY +  chunkOffsetY;	
			}


			var px = chunk.position.x;
			var py = chunk.position.z;


			if(!self.mapRect){
				self.mapRect = {
					x1:px-cdx/2, x2:px+cdx/2,
					y1:py-cdy/2, y2:py+cdy/2 };
			}
			
			self.mapRect.x1 = Math.min(self.mapRect.x1, px -cdx/2);
			self.mapRect.x2 = Math.max(self.mapRect.x2, px +cdx/2);

			self.mapRect.y1 = Math.min(self.mapRect.y1, py -cdy/2);
			self.mapRect.y2 = Math.max(self.mapRect.y2, py +cdy/2);
			
			chunk.updateMatrix();
			chunk.updateMatrixWorld ();

			/// Add to list of stuff to render
			/// TODO: Perhaps use some kind of props for each entry instead?
			self.output.terrainTiles.push( chunk );
			self.output.collisions.push( chunk );
			self.output.visibles.push( chunk );

			

		} /// End render chunk function


		var stepChunk = function(cx,cy){
			if(cx>=xChunks){
				cx = 0;
				cy++;
			}

			if(cy>=yChunks){

				/// Draw water surface using map bounds				
				var water = self.drawWater(self.mapRect);
				self.output.visibles.push(water);

				/// Set bounds in output VO
				self.output.bounds = self.mapRect;

				/// Fire call back, we're done rendering.
				callback();
				return;
			}

			var pct =  Math.floor( 100*(cy * xChunks + cx) /( xChunks * yChunks ) );

			self.log("Loading Terrain", pct+"%");

			renderChunk(cx,cy);
			setTimeout(stepChunk,1,cx+1,cy);
		}

		stepChunk(0,0);
	};
}


/// DataRenderer inherrintance:
TerrainRenderer.prototype = Object.create(DataRenderer.prototype);
TerrainRenderer.prototype.constructor = TerrainRenderer;

/// DataRenderer overwrite:
/**
* Renders terrain
*/
TerrainRenderer.prototype.renderAsync = function(callback){
	
	/// Load all paged Images, requires inflation of other pack files!
	var pagedImageId =  this.mapFile.getChunk("trn").data.materials.pagedImage;
	this.localReader.loadFile(pagedImageId,this.loadPagedImageCallback.bind(this, callback));
}

/**
 * TODO: write description. Used for export feature
 * @method getFileIdsAsync
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
TerrainRenderer.prototype.getFileIdsAsync = function(callback){

	var terrainChunk = this.mapFile.getChunk("trn");
	var pimgTableDataChunk = this.mapFile.getChunk("pimg");
	var fileIds = [];


	/// ------------ SPLASH TEXTURES ------------
	var pimgData = pimgTableDataChunk && pimgTableDataChunk.data;
	var strippedPages = pimgData.strippedPages;
	
	///Only use layer 0
	strippedPages.forEach(function(page){
			
		/// Only load layer 0 and 1
		if(page.layer<=1 && page.filename>0){
			fileIds.push( page.filename );
		}
	});
	/// ------------ END SPLASH TEXTURES ------------



	/// ------------ TILED IMAGES ------------
	var terrainData = terrainChunk.data;
	var allTextures = terrainData.materials.texFileArray;
	allTextures.forEach(function(texture){
		if(texture.filename>0)
			fileIds.push(texture.filename);
	})
	/// ------------ END TILED IMAGES ------------



	return fileIds;
};

module.exports = TerrainRenderer;
},{"../format/file/PagedImageFile.js":30,"../util/RenderUtils":34,"./DataRenderer":4}],9:[function(_dereq_,module,exports){
var Utils = _dereq_("../util/RenderUtils");
var DataRenderer = _dereq_('./DataRenderer');

/**
 * @class ZoneRenderer
 * @constructor
 * @extends DataRenderer
 * @param  {[type]} localReader  [description]
 * @param  {[type]} mapFile  [description]
 * @param  {[type]} settings [description]
 * @param  {[type]} output   [description]
 */
function ZoneRenderer(localReader, mapFile, settings, output){
	DataRenderer.call(this, localReader, mapFile, settings, output);

	/**
	 * TODO
	 * @method  renderZone
	 * @param  {[type]} zone               [description]
	 * @param  {[type]} zoneDefs           [description]
	 * @param  {[type]} mapRect            [description]
	 * @param  {[type]} renderZoneCallback [description]
	 * @return {[type]}                    [description]
	 */
	this.renderZone = function(zone, zoneDefs, mapRect, renderZoneCallback){
		var self = this;

		/// Get Zone Definition
		var zoneDef = null;
		zoneDefs.forEach(function(zd){
			if(!zoneDef && zd.token == zone.defToken)
				zoneDef = zd;
		});

		/// Create array of all models to add:
		var models = [];
		var modelGroups = this.getModelGroups(zone, zoneDef, mapRect);

		/**
		 * Steps trough each model and renders it to the scene, allowing for efficient caching.
		 * @param  {Number} i - Current index within the models array
		 */
		//var lastPct = -1;
		var groupKeys = Object.keys(modelGroups);
		function stepModels(i){

			/*var pct = Math.round(100.0*i / groupKeys.length);
			if(lastPct!=pct){
				console.log("Rendering ZONE models "+pct+"%");
				lastPct = pct;
			}*/

			if(i>=groupKeys.length){

				/// Tell caller this zone is done loading
				renderZoneCallback();
				return;
			}

			/// Read model at index
			/// var model = models[i];
			var key = groupKeys[i]; /// key is model filename
			var group = modelGroups[key];

			var meshGroups = [];
			
			/// Get model just once for this group
			Utils.getMeshesForFilename(key, null, self.localReader,

				function(meshes, isCached){
					
					/// If there were meshes, add them to the scene with correct scaling rotation etc.
					if(meshes /* && meshes.length == 3*/){

						/// Add one copy per model instance
						/// TODO: add rotation!
						/// TODO: fine tune position?
						/// TODO: POTIMIZE!
						

						group.forEach(function(model, instanceIdx){

							var isCached = true;
							var scale = 1.0;

							/// For each Mesh in the model
							meshes.forEach(function(mesh, meshIdx){

								if(mesh.materialFlags == 525 /* || mesh.materialFlags == 520 || mesh.materialFlags == 521*/ ){
									//console.log("Skipping lod");
									return;
								}


								

								var move = {x:0,y:0,z:0};

							 	/// Add to big mesh
							 	if(!meshGroups[meshIdx]){
							 		var mg = mesh.geometry.clone();
							 		var mga = mg.attributes;
							 		meshGroups[meshIdx] = {
							 			readVerts : mga.position.array,
							 			verts: new Float32Array( group.length * mga.position.array.length ),

							 			readIndices : mga.index.array,
							 			indices: new Uint32Array( group.length * mga.index.array.length ),

							 			readUVs : mga.uv.array,
							 			uvs: new Float32Array( group.length * mga.uv.array.length ),

							 			readNormals : mga.normal.array,
							 			normals: new Float32Array( group.length * mga.normal.array.length ),

							 			material:mesh.material,
							 			//material:new THREE.MeshBasicMaterial( {color: 0xffcccc, wireframe:true} ),
							 			/*material : new THREE.PointCloudMaterial ({
									      color: 0xFF0000,
									      size: 20
									    }),*/
							 			position:{x:model.x, y:model.y, z:model.z}
							 		}
							 	}
							 	else{
							 		/// Translate
						 			move.x = model.x - meshGroups[meshIdx].position.x;
						 			move.y = model.z - meshGroups[meshIdx].position.z;
						 			move.z = model.y - meshGroups[meshIdx].position.y;
							 	}

							 	/// Add geom verts
							 	var readVerts = meshGroups[meshIdx].readVerts;
							 	var writeVerts = meshGroups[meshIdx].verts;
							 	var stride = readVerts.length;

							 	for ( var i = 0, j = instanceIdx * stride; i < stride; i +=3, j +=3 ) {
									writeVerts[ j + 0 ] = readVerts[ i + 0 ] + move.x;
									writeVerts[ j + 1 ] = readVerts[ i + 1 ] + move.y;
									writeVerts[ j + 2 ] = readVerts[ i + 2 ] + move.z;
								}

								var readIndices = meshGroups[meshIdx].readIndices;
							 	var writeIndices = meshGroups[meshIdx].indices;
							 	var strideIndices = readIndices.length;
							 	var shift = stride * instanceIdx  / 3;

								for ( var i = 0, j = instanceIdx * strideIndices; i < strideIndices; i ++, j ++ ) {
									writeIndices[ j ] = readIndices[ i ] + shift;
								}


								var readUVs = meshGroups[meshIdx].readUVs;
								var writeUvs = meshGroups[meshIdx].uvs;
								var uvStride = readUVs.length;
								for ( var i = 0, j = instanceIdx * uvStride; i < uvStride; i ++, j ++ ) {
									writeUvs[ j ] = readUVs[ i ];
								}

								var readNormals = meshGroups[meshIdx].readNormals;
								var writeNormals = meshGroups[meshIdx].normals;
								var normalStride = readNormals.length;
								for ( var i = 0, j = instanceIdx * normalStride; i < normalStride; i ++, j ++ ) {
									writeNormals[ j ] = readNormals[ i ];
								}
								

							});

						});// End for each model in group
						
					}/// End if meshes

					/// Add each cluster of merged meshes to scene
					meshGroups.forEach(function(meshGroup){

						var mergedGeom = new THREE.BufferGeometry();
						
						mergedGeom.addAttribute( 'position', new THREE.BufferAttribute( meshGroup.verts, 3 ) );
						mergedGeom.addAttribute( 'index', new THREE.BufferAttribute( meshGroup.indices, 1) );
						mergedGeom.addAttribute( 'normal', new THREE.BufferAttribute( meshGroup.normals, 3 ) );
						mergedGeom.addAttribute( 'uv', new THREE.BufferAttribute( meshGroup.uvs, 2) );

						mergedGeom.buffersNeedUpdate = true;

						mesh = new THREE.Mesh( mergedGeom, meshGroup.material );
						mesh.position.set(meshGroup.position.x, meshGroup.position.z, meshGroup.position.y);

						self.output.visibles.push(mesh);
						self.output.nonCollisions.push(mesh);

					});// End for each meshgroup


					/// Rendering is done, render next.
					stepModels(i+1);
				}

			);
			
			
			
			
		}/// End function stepModels

		/// Begin stepping trough the models, rendering them.
		stepModels(0);
	}


	/**
	 * TODO
	 * @method  getModelGroups
	 * @param  {[type]} zone    [description]
	 * @param  {[type]} zoneDef [description]
	 * @param  {[type]} mapRect [description]
	 * @return {[type]}         [description]
	 */
	this.getModelGroups = function(zone, zoneDef, mapRect){

		/// Calculate rect in global coordinates
		var zPos = zone.zPos;
		var mapX = mapRect.x1;
		var mapY = mapRect.y1;
		var c  = 32+16;

		var zoneRect = {
			x1:zone.vertRect.x1*c+mapX,
			x2:zone.vertRect.x2*c+mapX,
			y1:zone.vertRect.y1*-c-mapY,
			y2:zone.vertRect.y2*-c-mapY
		};	

		

		/// Zone width and depth in local corrdinates
		/*var zdx = zone.vertRect.x1-zone.vertRect.x2;
		var zdy = zone.vertRect.y1-zone.vertRect.y2;*/
		
		/// These zones seems to overflow :/
		if(zone.encodeData.length == 0){
			return {};
		}
		
		//console.log("Get mdl groups", zone);
		/// Testing: Render Zone Vert Rect
		//Utils.renderRect(zoneRect, -zPos);

		var zdx = zone.vertRect.x1-zone.vertRect.x2;
		var zdy = zone.vertRect.y1-zone.vertRect.y2;

		/// Zone Flags increases a linear position, used to step trough the Zone.
		var linearPos = 0;

		var modelGroups = {};

		for(var i = 0; i< zone.flags.length; i+=2){

			/// Step forward
			linearPos += zone.flags[i];

			/// Check if a model should be placed
			var flag = zone.flags[i+1];
			if(flag!=0){
				
				/// Extract flag data
				/// Layer is written in the last 4 bytes
				var zoneDefLayer = flag >> 4;

				/// Get Zone Definition Layer
				var layer = zoneDef.layerDefs[zoneDefLayer-1];

				/// TESTING Only show layers with height >= 3
				if(layer/* && layer.height >= 0*/){

					/// Get X and Y from linear position
					var modelX = (linearPos % zdx)*c + zoneRect.x1;
					var modelY = Math.floor(linearPos / zdx)*c + zoneRect.y1;

					/// Get Z from intersection with terrain
					var modelZ = null;

					var startZ = 100000;

					var raycaster = new THREE.Raycaster(
						new THREE.Vector3(modelX, startZ, modelY),
						new THREE.Vector3(0, -1, 0)
					);

					/// TODO: OPT?
					this.output.terrainTiles.forEach(function(chunk){
						if(modelZ === null){
							var intersections = raycaster.intersectObject(chunk);
							if(intersections.length>0){
								modelZ = startZ - intersections[0].distance;
							}
						}
					});

					/// Get model id
					/// TODO: check with modelIdx = flag & 0xf;
					var modelIdx = 0; 
					var model = layer.modelArray[modelIdx];
					var modelFilename = model.filename;
					var zOffsets = model.zOffsets;

					var layerFlags = layer.layerFlags; //NOrmaly 128, 128
					
					//TODO: flip z,y?
					var rotRangeX = layer.rotRangeX;//max min
					var rotRangeY = layer.rotRangeY;//max min
					var rotRangeZ = layer.rotRangeZ;//max min
					var scaleRange = layer.scaleRange;//max min
					var fadeRange = layer.fadeRange;//max min

					//Unused
					//tiling: 3
					//type: 1
					//width: 2
					//radiusGround: 2

					/// Create modelGroup (this zone only)
					if(!modelGroups[modelFilename]){
						modelGroups[modelFilename] = [];
					}

					/// Add entry to model group
					modelGroups[modelFilename].push({
						x:modelX,
						y:modelY,
						z:modelZ,
						rotRangeX:rotRangeX,
						rotRangeY:rotRangeY,
						rotRangeZ:rotRangeZ,
						scaleRange:scaleRange,
						fadeRange:fadeRange

					});
					

				}/// End if layer

				
			}/// End if flag != 0

		} /// End for each flag

		return modelGroups;
	}
}

/// NOT USED??
function addZoneMeshesToScene(meshes, isCached, position, scale, rotation){
	
	/// Called for each mesh in the zone
	/// TODO: Opt opt opt...

    meshes.forEach(function(mesh){

    	/// Create new mesh if we got back a cached original.
		if(isCached)
			mesh = new THREE.Mesh( mesh.geometry, mesh.material );

    	/// Scale, position and rotate.
    	mesh.scale.set( scale, scale, scale );
    	if(rotation){
	    	mesh.rotation.order = "ZXY";
	    	mesh.rotation.set(rotation.x, rotation.y, rotation.z);
	    }
    	mesh.position.set(position.x, position.y, position.z);

    	/// Add to export
    	this.output.visibles.push(mesh);
    	this.output.nonCollisions.push(mesh);
	});

}


/// DataRenderer inherrintance:
ZoneRenderer.prototype = Object.create(DataRenderer.prototype);
ZoneRenderer.prototype.constructor = ZoneRenderer;

/// DataRenderer overwrite:
/**
* Renders terrain
*/
ZoneRenderer.prototype.renderAsync = function(callback){
	var self = this;
	
	var zoneChunkData = this.mapFile.getChunk("zon2").data;
	var parameterChunkData = this.mapFile.getChunk("parm").data;
	var terrainChunkData = this.mapFile.getChunk("trn").data;
	var mapRect = parameterChunkData.rect;
				
	/// Zone data
	var zones = zoneChunkData.zones;
	var zoneDefs = zoneChunkData.zoneDefs;

	/// Render each zone
	lastPct = -1;		

	/// Main render loop, render each zone
	function stepZone(i){

		var pct = Math.round(100.0*i / zones.length);
		if(lastPct!=pct){
			self.log("Loading 3D Models (Zone)", pct+"%");
			lastPct = pct;
		}

		if(i >= zones.length){
			callback();
			return;
		}

		/// Main zone render function call
		self.renderZone(zones[i], zoneDefs, mapRect,
			stepZone.bind(self,i+1)
		);	

	}

	stepZone(0);
}

module.exports = ZoneRenderer;

//// Not used: zone defintion per chunk data "images" 32*32 points
/*
//Total map dx and dy
var d = terrainChunkHeader.data;
var pd = parameterChunkHeader.data;
var dx = (pd.rect.x2-pd.rect.x1);
var dy = (pd.rect.y2-pd.rect.y1);

//Each chunk dx and dy

var c =1;
var cdx = c*dx/d.dims.dim1;

var cdy = c*dy/d.dims.dim2;

var cdx = dx/(d.numChunksD_1*2);
var cdy =dy/(d.numChunksD_2*2);


for(var i=0; i<zoneDefs.length; i++){
	var zoneDef = zoneDefs[i];
	
	//TODO: opt!
	zoneDef.layerDefs.forEach(function(layer){

		layer.modelArray.forEach(function(model){
		
			
		});
		

	});

	var chunkMat = new THREE.MeshBasicMaterial(
		{
			color: 0x00ff00,
			wireframe:true,
		 	opacity: 1.0,
		}
	);

	//TODO: opt!
	
	if(
		zoneDef.token == 597  ||
		zoneDef.token == 1369  ||
		zoneDef.token == 903  
	){

		zoneDef.pageTable.pageArray.forEach(function(page){
			var flags = page.flags;
			var coord = page.chunkCoord;


			//Hightlight this coord
			var rect = {};

			
			//var globalOffsetX = pd.rect.x2 - cdx;
			var globalOffsetX = pd.rect.x1 + cdx/2;
			var chunkOffsetX = coord[0] * cdx;

			rect.x1  = globalOffsetX + chunkOffsetX;

			///Adjust for odd / even number of chunks
			if(d.numChunksD_2 % 2 == 0){
				
				var globalOffsetY = -pd.rect.y1;
				var chunkOffsetY = -coord[1] * cdy;

				rect.y1  =  chunkOffsetY + globalOffsetY;
			}
			else{

				var globalOffsetY =  -pd.rect.y1;
				var chunkOffsetY = -coord[1] * cdy;

				rect.y1 = globalOffsetY +  chunkOffsetY;	
			}

			rect.x2 = rect.x1+cdx;
			rect.y2 = rect.y1+cdy;


			

			Utils.renderRect(rect, 4000,chunkMat, 4000);

			//for(var j=0; j<flags.length; j++){
			//	if(flags[j]>0){
			//		console.log("Found flag",flags[j],"@ zoneDef",zoneDef.token,"coord",coord,"index",j);
			//	}
			//}
		});

	}

}*/
},{"../util/RenderUtils":34,"./DataRenderer":4}],10:[function(_dereq_,module,exports){
module.exports  = [
    //uint8_t  version;
    "version","uint8",                      

    //uint8_t  magic[3];
    "magic", "string:3",                    

    //uint32_t headerSize;
    "headerSize","uint32",                  

    //uint32_t unknown1;
    "unknown1","uint32",                    

    //uint32_t chunkSize;
    "chunkSize","uint32",                   
    
    //uint32_t crc;
    "crc","uint32",                         
    
    //uint32_t unknown2;
    "unknown2","uint32",                    
    
    //uint64_t mftOffset;
    "mftOffset",[ "[]","uint32", 2 ],
    
    //uint32_t mftSize;
    "mftSize","uint32",
    
    //uint32_t flags;
    "flags","uint32",
];
},{}],11:[function(_dereq_,module,exports){
module.exports = [
	//uint8_t  magic[4];
	"magic","string:4",
		
	//uint64_t unknown1;
	"unknown1",[ "[]","uint32", 2 ],

	//uint32_t nbOfEntries;
	"nbOfEntries","uint32",

	//uint32_t unknown2;
	"unknown2","uint32",
	
	//uint32_t unknown3;
	"unknown3","uint32",
];
},{}],12:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [

	//byte texArrayRange;
	"texArrayRange", "uint8",

    //byte texCount;
    "texCount", "uint8",

    //byte texTransformRange;
    "texTransformRange", "uint8",

    //byte sortOrder;
    "sortOrder", "uint8",

    //byte sortTri;
    "sortTri", "uint8",

    //byte procAnim;
    "procAnim", "uint8",

    //dword debugFlags;
    "debugFlags", "uint32",

    //dword flags;
    "flags", "uint32",

    //dword texType;
    "texType", "uint32",

    //dword textureMasks[4];
    "textureMasks", ["[]", "uint32", 4],

    //helpers::Array<qword> texTokens;
    "texTokens", Utils.getArrayReader([
    	"val",Utils.getQWordReader()
    	])
	
];
},{"../../../util/ParserUtils":33}],13:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
	//helpers::Array<AmatDx9SamplerV10> samplers;
	"samplers", Utils.getArrayReader([
			
			//dword textureIndex;
			"textureIndex", "uint32",
    		
    		//helpers::Array<dword> state;
    		"state", Utils.getArrayReader("uint32"),

		]),

    //helpers::Array<AmatDx9ShaderV10> shaders;
    "shaders", Utils.getArrayReader([
    		
    		//helpers::Array<dword> shader;
    		"shader", Utils.getArrayReader("uint32"),

		    //helpers::Array<dword> constRegisters;
		    "constRegisters", Utils.getArrayReader("uint32"),

		    //helpers::Array<dword> constTokens;
		    "constTokens", Utils.getArrayReader("uint32"),

		    //word instructionCount;
		    "instructionCount", "uint16"

     	]),


    //helpers::Array<AmatDx9TechniqueV10> techniques;	
    "techniques", Utils.getArrayReader([
    		//helpers::String name;
    		"name", Utils.getStringReader(),


		    //helpers::Array<AmatDx9PassV10> passes;
		    "passes", Utils.getArrayReader([
		    		// helpers::RefList<AmatDx9EffectV10> effects;
		    		"effects", Utils.getRefArrayReader([
		    				//qword token;
		    				"token", Utils.getQWordReader(),

						    //helpers::Array<dword> renderStates;
						    "renderStates", Utils.getArrayReader("uint32"),

						    //helpers::Array<dword> samplerIndex;
						    "samplerIndex", Utils.getArrayReader("uint32"),

						    //dword pixelShader;
						    "pixelShader", "uint32",

						    //dword vertexShader;
						    "vertexShader", "uint32",

						    //helpers::Array<dword> texGen;
						    "texGen", Utils.getArrayReader("uint32"),

						    //helpers::Array<dword> texTransform;
						    "texTransform", Utils.getArrayReader("uint32"),

						    //dword vsGenFlags;
						    "vsGenFlags", "uint32",
						    
						    //dword passFlags;
						    "passFlags", "uint32"
		    			])
		    	]),

		    //word maxPsVersion;
		    "maxPsVersion", "uint16",

		    //word maxVsVersion;
		    "maxVsVersion", "uint16"


     	]),
	
];
},{"../../../util/ParserUtils":33}],14:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
	//helpers::Array<PackMapCubeMapSampleV3> sampleArray;
	"sampleArray",Utils.getArrayReader([
			//float3 position;
			"position", ["[]","float32",3],

		    //helpers::FileName filenameDayDefault;
		    "filenameDayDefault",Utils.getFileNameReader(),

		    //helpers::FileName filenameNightDefault;
		    "filenameNightDefault",Utils.getFileNameReader(),

		    //helpers::FileName filenameDayScript;
		    "filenameDayScript",Utils.getFileNameReader(),

		    //helpers::FileName filenameNightScript;
		    "filenameNightScript",Utils.getFileNameReader(),

		    //qword envID;
		    "envId_1","uint32",
		    "envId_2","uint32"
		]),
	
	//helpers::Array<PackMapCubeMapParamsV3> paramsArray;
	"paramsArray",Utils.getArrayReader([
			//dword modulateColor;
			"modulateColor","uint32",

		    //float brightness;
		    "brightness","float32",
		    
		    //float contrast;
		    "contrast","float32",
		    
		    //dword blurPasses;
		    "blurPasses","uint32",
		    
		    //helpers::WString envVolume;
		    "envVolume_p","uint32"
		])
	
]
},{"../../../util/ParserUtils":33}],15:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

var skycardAttributesDef = [
	//float azimuth;
	"azimuth", "float32",
    
    //float density;
    "density", "float32",
    
    //float hazeDensity;
    "hazeDensity", "float32",
    
    //float latitude;
    "latitude", "float32",

    //float lightIntensity;
    "lightIntensity", "float32",

    //float minHaze;
    "minHaze", "float32",

    //float2 scale;
    "scale", ["[]","float32",2],

    //float speed;
    "speed", "float32",

    //helpers::FileName texture;
    "texture", Utils.getFileNameReader(),

    //float4 textureUV;
    "textureUV", ["[]", "float32", 4],

    //float brightness;
    "brightness", "float32"
];

module.exports = [

	//helpers::Array<PackMapEnvDataLocalV74> dataLocalArray;
	"dataLocalArray", ["[]","uint32", 2]/*Utils.getArrayReader([
			//helpers::Array<PackMapEnvDataLightingV74> lighting;
			"lighting",["[]","uint32",2],

		    //helpers::Array<PackMapEnvDataLightingCharGroupV45> lightingCharGroups;
		    "lightingCharGroups",["[]","uint32",2],

		    //helpers::Ptr<PackMapEnvDataCloudsV74> clouds;
		    "clouds", "uint32",

		    //helpers::RefList<PackMapEnvDataColoredLightRingsV45> coloredLightRings;
		    "coloredLightRings",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataEffectV74> effect;
		    "effect",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataHazeV74> haze;
		    "haze",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataPFieldV74> particleFields;
		    "particleFields",["[]","uint32",2],

		    //helpers::Array<PackMapEnvDataPFieldCutoutV45> particleFieldCutouts;
		    "particleFieldCutouts",["[]","uint32",2],

		    //helpers::Ptr<PackMapEnvDataSkyV74> sky;
		    "sky", "uint32",
		    //helpers::Ptr<PackMapEnvDataSkyCardsV74> skyCards;
		    "skyCards", "uint32",
		    //helpers::Ptr<PackMapEnvDataSpawnGroupsV45> spawns;
		    "spawns", "uint32",

		    //helpers::RefList<PackMapEnvDataWaterV74> water;
		    "water",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataWindV74> wind;
		    "wind",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataAudioV45> audio;
		    "audio",["[]","uint32",2],

		    //helpers::WString name;
		    "name", "uint32",

		    //helpers::Array<byte> nightMods;
		    "nightMods",["[]","uint32",2],

		    //qword bindTarget;
		    "bindTarget",["[]","uint32",2],

		    //helpers::WString reserved;
		    "reserved", "uint32",

		    //byte type;
		    "type", "uint8",

		    //qword guid;
		    "guid",["[]","uint32",2],

		    //helpers::Array<PackMapEnvDataShapeV45> shapeArray;
		    "shapeArray",["[]","uint32",2],
		])*/,

    //helpers::Ptr<PackMapEnvDataGlobalV74> dataGlobal;
    "dataGlobal", Utils.getPointerReader([

			//helpers::Array<PackMapEnvDataLightingV74> lighting;
			"lighting",Utils.getArrayReader([
					//helpers::RefList<PackMapEnvDataLightV74> lights;
					"lights", Utils.getRefArrayReader([
						//byte3 color;
						"color", ["[]", "uint8", 3],

					    //float intensity;
					    "intensity", "float32",

					    //float3 direction;
					    "direction", ["[]", "float32", 3],
					]),
				    
				    //float shadowInfluence;
				    "shadowInfluence", "float32",
				    
				    //byte3 backlightColor;
				    "backlightColor", ["[]", "uint8", 3],

				    //float backlightIntensity;
				    "backlightIntensity", "float32",
				]),
		    
		    //helpers::Array<PackMapEnvDataLightingCharGroupV45> lightingCharGroups;
		    "lightingCharGroups",["[]","uint32",2],
		    
		    //helpers::Ptr<PackMapEnvDataCloudsV74> clouds;
		    "clouds", "uint32",

		    //helpers::RefList<PackMapEnvDataColoredLightRingsV45> coloredLightRings;
		    "coloredLightRings",["[]","uint32",2],
		    //helpers::RefList<PackMapEnvDataEffectV74> effect;
		    "effect",["[]","uint32",2],
		    
		    //helpers::RefList<PackMapEnvDataHazeV74> haze;
		    "haze",Utils.getRefArrayReader([
		    		//byte4 nearColor;
		    		"nearColor", ["[]", "uint8", 4],/// B G R A

				    //byte4 farColor;
				    "farColor", ["[]", "uint8", 4],/// B G R A

				    //float2 distRange;
				    "distRange", ["[]", "float32", 2],

				    //byte4 heightColor;
				    "heightColor", ["[]", "uint8", 4],

				    //float2 heightRange;
				    "heightRange", ["[]", "float32", 2],

				    //float depthCue;
				    "depthCue", "float32",

				    //float2 sunDirRange;
				    "sunDirRange", ["[]", "float32", 2],

		    	]),


		    //helpers::RefList<PackMapEnvDataPFieldV74> particleFields;
		    "particleFields",["[]","uint32",2],

		    //helpers::Array<PackMapEnvDataPFieldCutoutV45> particleFieldCutouts;
		    "particleFieldCutouts",["[]","uint32",2],

		    //helpers::Ptr<PackMapEnvDataSkyV74> sky;
		    "sky", Utils.getPointerReader([
			    	//byte flags;
			    	"flags", "uint8",

				    //float dayBrightness;
				    "dayBrightness", "float32",

				    //float dayHazeBottom;
				    "dayHazeBottom", "float32",

				    //float dayHazeDensity;
				    "dayHazeDensity", "float32",

				    //float dayHazeFalloff;
				    "dayHazeFalloff", "float32",

				    //float dayLightIntensity;
				    "dayLightIntensity", "float32",

				    //float dayStarDensity;
				    "dayStarDensity", "float32",

				    //float nightBrightness;
				    "nightBrightness", "float32",

				    //float nightHazeBottom;
				    "nightHazeBottom", "float32",

				    //float nightHazeDensity;
				    "nightHazeDensity", "float32",

				    //float nightHazeFalloff;
				    "nightHazeFalloff", "float32",

				    //float nightLightIntensity;
				    "nightLightIntensity", "float32",

				    //float nightStarDensity;
				    "nightStarDensity", "float32",

				    //float verticalOffset;
				    "verticalOffset", "float32",
		    	]),
		    
		    //helpers::Ptr<PackMapEnvDataSkyCardsV74> skyCards;
		    "skyCards", Utils.getPointerReader([
		    	 //helpers::Array<PackMapEnvDataSkyCardV74> cards;
		    	 "cards", Utils.getArrayReader([
		    	 		// PackMapEnvDataSkyCardAttributesV74 day;
		    	 		"day", skycardAttributesDef,
					    // PackMapEnvDataSkyCardAttributesV74 night;
					    "night", skycardAttributesDef,
					    // dword flags;
					    "flags", "uint32",
					    // helpers::WString name;
					    "name", "uint32"
		    	 	])
		    ]),

		    //helpers::Ptr<PackMapEnvDataSpawnGroupsV45> spawns;
		    "spawns", "uint32",

		    //helpers::RefList<PackMapEnvDataWaterV74> water;
		    "water",["[]","uint32",2],
		    //helpers::RefList<PackMapEnvDataWindV74> wind;
		    "wind",["[]","uint32",2],
		    //helpers::RefList<PackMapEnvDataAudioV45> audio;
		    "audio",["[]","uint32",2],

		    //helpers::WString name;
		    "name", "uint32",
		    
		    //helpers::Array<byte> nightMods;
		    "nightMods",["[]","uint32",2],
		    
		    //qword bindTarget;
		    "bindTarget",["[]","uint32",2],

		    //helpers::WString reserved;
		    "reserved", "uint32",

		    /// NOT PART OF GW2 FORMATS, BUT DATA IN SKY MODE TEX
		    /// DOES NOT MAKE SENSE WIHTOUT SHIFTING...
		    "reserved2", "uint32",

		    //helpers::Array<PackMapEnvDataSkyModeTexV74> skyModeTex;
		    //
		    //
		    //KHYLO gets something that could NOT be length, pointer here!.. .like
		    // 2868 , 4
		    "skyModeTex", /*["[]","uint32",2]*/Utils.getArrayReader([
		    		//helpers::FileName texPathNE;
		    		"texPathNE", Utils.getFileNameReader(),
				    //helpers::FileName texPathSW;
				    "texPathSW", Utils.getFileNameReader(),
				    //helpers::FileName texPathT;
				    "texPathT", Utils.getFileNameReader(),
		    	])/**/,

		    //helpers::FileName starFile;
		    "starFile", Utils.getFileNameReader()
    	])
];
},{"../../../util/ParserUtils":33}],16:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
	//float3 boundsMin;
	//float3 boundsMax;   
	"boundsMin",["x","float32","z","float32","y","float32"],		
	"boundsMax",["x","float32","z","float32","y","float32"],
	
	//helpers::Array<PackMapCollideCollisionV14> collisions;
	"collisions", Utils.getArrayReader([
			// helpers::Array<word> indices;
	    	"indices", Utils.getArrayReader("uint16"),

	    	//helpers::Array<float3> vertices;
	    	"vertices", Utils.getArrayReader(["x","float32","z","float32","y","float32"]),
	    	
	    	//helpers::Array<word> surfaces;
	    	"surfaces", Utils.getArrayReader("uint16"),

	    	//PackMoppType moppCodeData; (byte array)
	    	// compressed bounding volume data
	    	"moppCodeDataCount", 'uint32',
	    	"moppCodeDataOffset", 'uint32'
		]),
	
	//helpers::Array<PackMapCollideBlockerV14> blockers;
	"blockersCount" , "uint32", 									
    "blockersOffset" , "uint32",									
    
    //helpers::Array<PackMapCollideNavMeshV14> navMeshes;
    "navMeshesCount" , "uint32", 									
    "navMeshesOffset" , "uint32", 									
    
    //helpers::Array<PackMapCollideAnimationV14> animations;
    "animations", Utils.getArrayReader([
    		//qword sequence;
			"sequencePT1","uint32",
			"sequencePT2","uint32",

			//helpers::Array<dword> collisionIndices; (dword = uint32)
			"collisionIndices",Utils.getArrayReader("uint32"),

			//helpers::Array<dword> blockerIndices;
			"blockerIndices",Utils.getArrayReader("uint32"),
		]),
    
    //helpers::Array<PackMapCollideGeometryV14> geometries;
    "geometries", Utils.getArrayReader([

		//byte quantizedExtents;
		"quantizedExtents", "uint8",
	   
	    //helpers::Array<dword> animations;
	    "animations", Utils.getArrayReader("uint32"),
		
		//word navMeshIndex;
		"navMeshIndex", "uint16"
		
    	]),
     
    //helpers::Array<PackMapCollideModelObsV14> obsModels;
    "obsModels", Utils.getArrayReader([
	    	// float3 translate
    		"translate", ["x","float32","z","float32","y","float32"],
	    	
	    	// dword geometryIndex  ( dword = uint32 )
    		"geometryIndex", "uint32", 
    	]),
    
    // helpers::Array<PackMapCollideModelPropV14> propModels;
    "propModels", Utils.getArrayReader([
    		// qword token
    		"token_pt1", "uint32",
    		"token_pt2", "uint32",	    		
    		
    		// qword sequence;
    		"sequence_pt1", "uint32",
    		"sequence_pt2", "uint32",

    		// float scale    		
    		"scale", "float32",	
    		
    		// float3 translate
    		"translate", ["x","float32","z","float32","y","float32"],

    		// float3 rotate
    		"rotation", ["x","float32","z","float32","y","float32"], 
	    	
	    	// dword geometryIndex  ( dword = uint32 )
    		"geometryIndex", "uint32", 
    	]),
    
    //helpers::Array<PackMapCollideModelZoneV14> zoneModels;
    "zoneModels", Utils.getArrayReader([
	    	// float scale    		
    		"scale", "float32",	
    		
    		// float3 translate
    		"translate", ["x","float32","z","float32","y","float32"],

    		// float3 rotate
    		"rotation", ["x","float32","z","float32","y","float32"], 
	    	
	    	// dword geometryIndex  ( dword = uint32 )
    		"geometryIndex", "uint32"
    	])
];
},{"../../../util/ParserUtils":33}],17:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
	//float4 rect;
	"rect", [
		"x1", "float32",
		"y1", "float32",
		"x2", "float32",
		"y2", "float32"
		],

	//dword flags;
	"flags", "uint32",

	//byte16 guid;
	"guid", "uint16",
];
},{"../../../util/ParserUtils":33}],18:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

function getBasePropDef(){
	return [
		//helpers::FileName filename;
		"filename", Utils.getFileNameReader(),

	    //helpers::Array<helpers::FileName> blitTextures;
	    "blitTextures", Utils.getArrayReader(Utils.getFileNameReader()),

	    //helpers::Array<PackMapPropConstantV18> constants;
	    "constants", Utils.getArrayReader([
	    		// dword token;
	    		"token", "uint32",

			    //float4 constant;
			    "constant", ["[]","float32",4],

			    //dword submodel;
			    "submodel", "uint32"
	    	]),

	    //qword guid;
	    "guid",Utils.getQWordReader(), //// Crashes Dredgehaunt!!
	    //"guid_pt1", "uint32", "guid_pt2", "uint32",

	    //qword permutation;
	    "permutation_pt1", "uint32", "permutation_pt2", "uint32",

	    //float4 bounds;
	    "bounds", ["[]","float32",4],

	    //float3 position;
	    "position", ["x","float32","z","float32","y","float32"],

	    //float3 rotation;
	    "rotation", ["x","float32","z","float32","y","float32"],

	    //byte4 color;
	    "color", ["[]","uint8",4],

	    //float scale;
	    "scale", "float32",

	    //float lod1;
	    "lod1", "float32",

	    //float lod2;
	    "lod2", "float32",

	    //dword flags;
	    "flags", "uint32",

	    //dword reserved;
	    "reserved", "uint32",

	    //word broadId;
	    "broadId", "uint16",

	    //word bucketId;
	    "bucketId", "uint16",

	    //byte byteData;
	    "byteData", "uint8",

	    //byte sortLayer;
	    "sortLayer", "uint8"
	];
}

//PackMapPropObjV21
var propDef = getBasePropDef();

//PackMapPropObjAnimSeqV21
var animArrayDef = getBasePropDef();
animArrayDef.push(
	// qword animSequence;
	"animSequence_pt1","uint32",
	"animSequence_pt2","uint32"
);

var instanceArrayDef = getBasePropDef();
instanceArrayDef.push(
	//helpers::Array<PackMapPropTransformV21> transforms;
    "transforms", Utils.getArrayReader([
		    //float3 position;
		    "position", ["x","float32","z","float32","y","float32"],
		    //float3 rotation;
		    "rotation", ["x","float32","z","float32","y","float32"],
		    //float scale;
		    "scale", "float32",
    	]),

    //helpers::Array<qword> origGuidArray;
	"origGuidArray_len","uint32",
	"origGuidArray_ptr","uint32"
);


var metaArrayDef = getBasePropDef();
metaArrayDef.push(
	//dword layerMask;
	"layerMask","uint32",
    
    //byte glomType;
    "glomType","uint8",
    
    //qword parent;
    "parent_pt1","uint32",
	"parent_pt2","uint32",

    //float3 glomOrigin;
    "glomOrigin", ["x","float32","z","float32","y","float32"]
);

module.exports = [
	//helpers::Array<PackMapPropObjV21> propArray;
	"propArray", Utils.getArrayReader(propDef), 

    //helpers::Array<PackMapPropObjAnimSeqV21> propAnimArray;
    "propAnimArray", Utils.getArrayReader(animArrayDef),

    //helpers::Array<PackMapPropObjInstanceV21> propInstanceArray;
    "propInstanceArray", Utils.getArrayReader(instanceArrayDef),

    //helpers::Array<PackMapPropObjToolV21> propToolArray;
    "propToolArray_len","uint32",
    "propToolArray_ptr","uint32",
    
    //helpers::Array<PackMapPropObjMetaV21> propMetaArray;
    "propMetaArray", Utils.getArrayReader(metaArrayDef),

    //helpers::Array<PackMapPropObjVolumeV21> propVolumeArray;
    "propVolumeArray_len","uint32",
    "propVolumeArray_ptr","uint32",


    //helpers::WString reserved;
    //PackBroadphaseType broadPhase;
    //dword nextBroadId;
]
},{"../../../util/ParserUtils":33}],19:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
	//  helpers::Array<MapShoreChainV0> chains;
	"chains", Utils.getArrayReader([
		//float offset;
		"offset", "float32",

	    //float opacity;
	    "opacity", "float32",

	    //float animationSpeed;
	    "animationSpeed", "float32",

	    //float2 edgeSize;
	    "edgeSize", ["sizeX", "float32", "sizeZ","float32"],

	    //dword flags;
	    "flags", "uint32",

	    //helpers::Array<float2> points;
	    "points", Utils.getArrayReader(["x", "float32", "z", "float32"]),

	    //helpers::FileName materialFilename;
	    "fileName", "uint32",

	    //helpers::Array<helpers::FileName> textureFilenames;
	    "points", Utils.getArrayReader("uint32"),

	    //float restTime;
	    "restTime", "float32",

	    //float2 fadeRanges[4];
	    "fadeRanges",  ["[]", ["min", "float32", "max","float32"], 4],

	    //float simplifyDistMin;
	    "simplifyDistMin", "float32",

	    //float simplifyDistMax;
	    "simplifyDistMax", "float32",

	    //float simplifyDot;
	    "simplifyDot", "float32"

	])
];
},{"../../../util/ParserUtils":33}],20:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
		
	//dword2 dims;
	"dims",["dim1","uint32", "dim2", "uint32"],

	//float swapDistance;
	"swapDistance","float32",		
	
	//helpers::Array<float> heightMapArray;
    "heightMap", Utils.getArrayReader("float32"),

    //helpers::Array<dword> tileFlagArray;
    "tileFlags", Utils.getArrayReader(
			"uint32"
			//["[]","uint8",4]
    	),

    //helpers::Array<PackMapTerrainChunkV14> chunkArray;
	"chunks", Utils.getArrayReader([
			
			//dword chunkFlags;
			"chunkFlags", "uint32",

			//helpers::Array<word> surfaceIndexArray;
			"surfaceIndexArray", Utils.getArrayReader("uint16"),

			//helpers::Array<qword> surfaceTokenArray;
			"surfaceTokenArray", Utils.getArrayReader([
					"token_pt1", "uint32",
					"token_pt2", "uint32"
				])

		]),

    //helpers::Ptr<PackMapTerrainMaterialsV14> materials;
	//"materialPointer" , "uint32", 
	"materials",
	function(ds, struct){
    	var ptr = ds.position + ds.readUint32();
    	//var size = ds.readUint32();
    	var _pos = ds.position;	    	

    	/// Go to pointer
    	ds.seek( ptr );
    	
    	var _def = [
    		
			//helpers::FileName pagedImage;
			"pagedImage", Utils.getFileNameReader(),
		    
		    //helpers::Array<PackMapTerrainConstV14> constArray;
		    "constArray", Utils.getArrayReader([
	    		//dword tokenName;
	    		"tokenName", "uint32",

				//float4 value;
				"float4", [
					"a", "float32",
					"b", "float32",
					"c", "float32",
					"d", "float32"
				],
	    	]),
		    
		    //helpers::Array<PackMapTerrainTexV14> texFileArray;
		    "texFileArray", Utils.getArrayReader([
	    		//dword tokenName;
			    "tokenName", "uint32",

			    //dword flags;
			    "flags", "uint32",

			    //helpers::FileName filename;
			    "filename", Utils.getFileNameReader(),

			    //dword2 flagsVector;
			    "flagsVector",["flag1","uint32", "flag2", "uint32"],

			    //dword layer;
			    "layer", "uint32",
	    	]),
		    
		    //helpers::Array<PackMapTerrrainChunkMaterialV14> materials;
		    "materials", Utils.getArrayReader([
		    	//byte tiling[3];
		    	"tiling", ["[]","uint8",3],

			    //PackMapTerrainMaterialV14 hiResMaterial;
			    "hiResMaterial", [
			    	//helpers::FileName materialFile;
			    	"materialFile", Utils.getFileNameReader(),
				    
				    //dword fvf;
				    "fvf", "uint32",						    

				    //helpers::Array<dword> constIndexArray;
				    "constIndexArray", Utils.getArrayReader("uint32"),

				    //helpers::Array<dword> texIndexArray;
				    "texIndexArray", Utils.getArrayReader("uint32"),
			    ],

			    //PackMapTerrainMaterialV14 loResMaterial;
			    "loResMaterial", [
			    	//helpers::FileName materialFile;
			    	"materialFile", Utils.getFileNameReader(),
				    
				    //dword fvf;
				    "fvf", "uint32",						    

				    //helpers::Array<dword> constIndexArray;
				    "constIndexArray", Utils.getArrayReader("uint32"),

				    //helpers::Array<dword> texIndexArray;
				    "texIndexArray", Utils.getArrayReader("uint32"),
			    ],

			    //PackMapTerrainMaterialV14 faderMaterial;
			    "faderMaterial", [
			    	//helpers::FileName materialFile;
			    	"materialFile", Utils.getFileNameReader(),
				    
				    //dword fvf;
				    "fvf", "uint32",						    

				    //helpers::Array<dword> constIndexArray;
				    "constIndexArray", Utils.getArrayReader("uint32"),

				    //helpers::Array<dword> texIndexArray;
				    "texIndexArray", Utils.getArrayReader("uint32"),
			    ],

			    //helpers::Ptr<PackMapTerrainChunkUVDataV14> uvData;
			    "uvDataPtr", "uint32"
	    	]),
		    
		    //float2 midFade;
		    "midFade", ["min","float32","max","float32"],
		    
		    //float2 farFade;
		    "farFade", ["min","float32","max","float32"]
    	];
    	var ret = ds.readStruct(_def);


    	/// Go back
    	ds.seek(_pos);
    	return ret;
    }
];
},{"../../../util/ParserUtils":33}],21:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
		
	//helpers::Array<PackMapZoneDefV22> zoneDefArray;
	"zoneDefs", Utils.getArrayReader([
		//helpers::FileName defFilename; //pointer
		"defFilename", "uint32",									//4 -> 4

		//dword token;
		"token", "uint32",											//4 -> 8

		//helpers::Array<PackMapZoneLayerDefV22> layerDefArray;
		"layerDefs", Utils.getArrayReader([
			//byte type;
    		"type", "uint8",

		    //byte height;
		    "height", "uint8",

		    //byte width;
		    "width", "uint8",

		    //byte radiusGround;
		    "radiusGround", "uint8",

		    //byte sortGroup;
		    "sortGroup", "uint8",

		    //byte tiling;
		    "tiling", "uint8",

		    //float2 scaleRange;
		    "scaleRange", ["min", "float32", "max", "float32"],

		    //float probability;
		    "probability", "float32",

		    //float2 fadeRange;
		    "fadeRange", ["min", "float32", "max", "float32"],

		    //float2 rotRange[3];
		    "rotRangeX", ["min", "float32", "max", "float32"],
		    "rotRangeY", ["min", "float32", "max", "float32"],
		    "rotRangeZ", ["min", "float32", "max", "float32"],
		    

		    //float2 hslRanges[4];
		    "hslRanges_1", ["min", "float32", "max", "float32"],
		    "hslRanges_2", ["min", "float32", "max", "float32"],
		    "hslRanges_3", ["min", "float32", "max", "float32"],
		    "hslRanges_4", ["min", "float32", "max", "float32"],

		    //float instanceScaleJitter;
		    "instanceScaleJitter", "float32",

		    //byte noise;
		    "noise", "uint8",

		    //dword layerFlags;
		    "layerFlags", "uint32",

		    //helpers::FileName materialname;
		    "materialname", "uint32",

		    //helpers::Array<PackMapZoneModelV22> modelArray;
		    "modelArray", Utils.getArrayReader([
		    		//helpers::FileName filename;
		    		"filename", Utils.getFileNameReader(),

				    //float probability;
				    "probability", "float32",

				    //dword flags;
				    "flags", "uint32",

				    //float3 hslOffset;
				    "hslOffset", ["[]","float32",3],

				    //byte zOffsets[2];
				    "zOffsets", ["[]","uint8",2]
		    	]),

		    //helpers::Ptr<PackMapZoneModelV22> subModel;
		    "subModel", Utils.getPointerReader([
		    		//helpers::FileName filename;
		    		"filename", Utils.getFileNameReader(),

				    //float probability;
				    "probability", "float32",

				    //dword flags;
				    "flags", "uint32",

				    //float3 hslOffset;
				    "hslOffset", ["[]","float32",3],

				    //byte zOffsets[2];
				    "zOffsets", ["[]","uint8",2]
		    	]),

		    //helpers::WString reserved;
		    "reserved", "uint32"
		]),

		//qword timeStamp;
		"timeStamp_pt1", "uint32",				
		"timeStamp_pt2", "uint32",

    	//helpers::Ptr<PackMapZonePageTableV10> pageTable;
    	"pageTable", Utils.getPointerReader([
    				//helpers::Array<PackMapZonePageV10> pageArray;
    				"pageArray",Utils.getArrayReader([
    						// helpers::Array<byte> flags;
    						"flags",Utils.getArrayReader("uint8"),
						    //dword2 chunkCoord;
						    "chunkCoord", ["[]","uint32",2],
						    //byte seed;
						    "seed", "uint8",
						    //helpers::Array<dword> paintFlags;
						    "paintFlags",Utils.getArrayReader("uint32"),
						    //helpers::WString string;
						    "string_ptr","uint32",
    					]),
					//dword flags;
					"flags","uint32"
    		]),

    	//helpers::WString reserved;
    	"reserved", "uint32"
	]),

	"zones", Utils.getArrayReader([
		// dword zoneFlags  ( dword = uint32 )
		"zoneFlags", "uint32",

		// dword4 vertRect;
		"vertRect", ["x1","uint32","y1","uint32","x2","uint32", "y2", "uint32"],

		//float waterHeight;
		"waterHeight", "float32",

		//byte seed;
		"seed", "uint8",

		//dword defToken;
		"defToken", "uint32",

		//float2 range;
		"range", ["min", "float32", "max", "float32"],

	    //float zPos;
	    "zPos", "float32",

	    //helpers::Array<byte> flags;
	    "flags", Utils.getArrayReader("uint8"),

	    //helpers::Array<PackMapZoneEncodingDataV22> encodeData;
	    "encodeData", Utils.getArrayReader([
	    	//word index;
	    	"index","uint16",
			//byte offset;
			"offset","uint8"		    	
	    	]),

	    //helpers::Array<PackMapZoneCollideDataV22> collideData;
	    "collideData", Utils.getArrayReader([
	    	
	    	//float normalX;
			"normalX", "float32",
			
			//float normalY;
			"normalY", "float32",
			
			//float zPos;
			"zPos", "float32"
		]),

	    //helpers::Array<word> offsetData;
	    "offsetData", Utils.getArrayReader("uint16"),

	    //helpers::Array<float2> vertices;
	    "vertices", Utils.getArrayReader(["x", "float32", "z", "float32"]),

	    //word broadId;
	    "broadId","uint16",	

	    //helpers::WString reserved; pointer to a string??
	    "reserved","uint32"
	]),

	
    //PackBroadphaseType broadPhase;
    //		 PackBoradPhaseType:
	// 		 helpers::Array<byte> broadphaseData;
	"broadPhase", Utils.getArrayReader("uint8"),
    
    //word maxBroadId;
    "maxBroadId" , "uint16",
    
    //helpers::WString string; (pointer to char16 string)
    "string" , "uint32",
    
]
},{"../../../util/ParserUtils":33}],22:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
	//helpers::RefList<ModelMeshDataV66> meshes;
	"meshes", Utils.getRefArrayReader([
			//qword visBone;
			"visBone_pt1", "uint32",
			"visBone_pt2", "uint32",

		    //helpers::Array<ModelMeshMorphTargetV66> morphTargets;
		    "num_morphTargets","uint32",
		    "ptr_morphTargets","uint32",

		    //dword flags;
		    "flags", "uint32",

		    //helpers::Array<dword> seamVertIndices;
		    "seamVertIndices", Utils.getArrayReader("uint32"),
		    
		    //qword meshName;
		    "meshName_pt1","uint32",
		    "meshName_pt2","uint32",

		    //float3 minBound;
		    "minBound", ["x","float32","z","float32","y","float32"],
		    
		    //float3 maxBound;
		    "maxBound", ["x","float32","z","float32","y","float32"],

		    //helpers::Array<GrBoundData> bounds;
		    "bounds", Utils.getArrayReader([
			    	//float3 center;
			    	"center", ["x","float32","z","float32","y","float32"],
				    //float3 boxExtent;
				    "boxExtent", ["x","float32","z","float32","y","float32"],
				    //float sphereRadius;
				    "sphereRadius", "float32",
		    	]),

		    //dword materialIndex;
		    "materialIndex","uint32",

		    //helpers::String materialName;
		    //"materialName","uint32",
		    "materialName",Utils.getStringReader(),

		    //helpers::Array<qword> boneBindings;
		    "boneBindings", Utils.getArrayReader([
		    		"pt1","uint32",
		    		"pt2","uint32",
		    	]),

		    //helpers::Ptr<ModelMeshGeometryV1> geometry;
		    "geometry", Utils.getPointerReader([
		    		//ModelMeshVertexDataV1 verts;
		    		//"verts",[
		    			//dword vertexCount;
		    			"vertexCount", "uint32",
						//PackVertexType mesh;
					//	"mesh", [
							//dword fvf;
							"fvf", "uint32",
							//helpers::Array<byte> vertices;
							"vertices", Utils.getArrayReader("uint8"),
					//	]
		    		//],

				    //ModelMeshIndexDataV1 indices;
				    //"indices",[
				    	//helpers::Array<word> indices;
				    	"indices", Utils.getArrayReader("uint16"),
				    //],
				    	

				    //helpers::Array<ModelMeshIndexDataV1> lods;
				    "numLods","uint32","ptrLods","uint32",
				    //helpers::Array<dword> transforms;
				    "numTransforms","uint32","ptrtransforms","uint32"
		    	])
		]),
	
]
},{"../../../util/ParserUtils":33}],23:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

module.exports = [
    //helpers::Array<ModelPermutationDataV65> permutations;
    "permutations",Utils.getArrayReader([
    		//qword token;
    		"token_pt1","uint32","token_pt2","uint32",

			//helpers::RefList<ModelMaterialDataV65> materials;
			"materials", Utils.getRefArrayReader([
				    //qword token;
				    //"token_pt1","uint32","token_pt2","uint32",
				    "token", Utils.getQWordReader(),

				    //dword materialId;
				    "materialId","uint32",

				    //helpers::FileName filename;
				    "filename",/*"uint32",//*/Utils.getFileNameReader(),

				    //dword materialFlags;
				    "materialFlags","uint32",

				    //dword sortOrder;
				    "sortOrder","uint32",

				    //helpers::Array<ModelTextureDataV65> textures;
				    "textures", Utils.getArrayReader([
				    		//helpers::FileName filename;
				    		"filename",Utils.getFileNameReader(),

						    //dword textureFlags;
						    "textureFlags","uint32",

						    //qword token;
						    //"token","uint32","token_pt2","uint32",
						    "token",Utils.getQWordReader(),
						    

						    //qword blitId;
						    //"blitId_pt1","uint32","blitId_pt2","uint32",
						    "blitId",Utils.getQWordReader(),
						    

						    //dword uvAnimId;
						    "uvAnimId","uint32",

						    //byte uvPSInputIndex;
						    "uvPSInputIndex","uint8",
				    	],100),


				    //helpers::Array<ModelConstantDataV65> constants;
				    "constants",Utils.getArrayReader([
				    		//dword name;
				    		"name", "uint32",
						    //float4 value;
						    "value", ["[]","float32",4],
						    //dword constantFlags;
						    "constantFlags", "uint32"
				    	]),
				    
				    //helpers::Array<ModelMatConstLinkV65> matConstLinks;
				    "matConstLinks",["[]","uint32",2],

				    //helpers::Array<ModelUVTransLinkV65> uvTransLinks;
				    "uvTransLinks",["[]","uint32",2],

				    //helpers::Array<ModelMaterialTexTransformV65> texTransforms;
				    "texTransforms",["[]","uint32",2],

				    //byte texCoordCount;
				    "texCoordCount", "uint8"
				])

    	]),

    //helpers::Ptr<ModelCloudDataV65> cloudData;
    "cloudData_p","uint32",
    //helpers::Array<ModelObstacleDataV65> obstacles;
    "obstacles_n","uint32","obstacles_p","uint32",
    //helpers::Ptr<ModelStreakDataV65> streakData;
    "streakData_p","uint32",
    //helpers::Ptr<ModelLightDataV65> lightData;
    "lightData_p","uint32",
    //helpers::Array<ModelClothDataV65> clothData;
    "clothData_n","uint32","clothData_p","uint32",
    //helpers::Ptr<ModelWindDataV65> windData;
    "windData_p","uint32",
    //helpers::Array<qword> actionOffsetNames;
    "actionOffsetNames_n","uint32","actionOffsetNames_p","uint32",
    //helpers::Array<float3> actionOffsets;
    "actionOffsets_n","uint32","actionOffsets_p","uint32",
    //float lodOverride[2];
    "lodOverride",["[]","float32",2],
    //helpers::FileName soundScript;
    "soundScript_p","uint32",
    //helpers::Ptr<ModelLightningDataV65> lightningData;
    "lightningData_p","uint32",
    //helpers::Array<ModelSoftBodyDataV65> softBodyData;
    "softBodyData_n","uint32",
    "softBodyData_p","uint32",
    //helpers::Array<ModelBoneOffsetDataV65> boneOffsetData;
    "boneOffsetData_n","uint32",
    "boneOffsetData_p","uint32",
    //helpers::Ptr<ModelBoundingSphereV65> boundingSphere;
    "boundingSphere",Utils.getPointerReader([
    		//float3 center;
    		"center", ["x","float32","z","float32","y","float32"],
			//float radius;
			"radius","float32"
    	]),
]
},{"../../../util/ParserUtils":33}],24:[function(_dereq_,module,exports){
var Utils = _dereq_('../../../util/ParserUtils');

pageDataDef = Utils.getArrayReader([
	//dword layer;
	"layer", "uint32",
    //dword2 coord;
    "coord", ["[]","uint32",2],
    //helpers::FileName filename;
    "filename",Utils.getFileNameReader(),
    //dword flags;
    "flags","uint32",
    //byte4 solidColor;
    "solidColor", ["[]","uint8",4]
]);

module.exports = [
	//helpers::Array<PagedImageLayerDataV3> layers;
    "layers_n","uint32","layers_p","uint32",

    //helpers::Array<PagedImagePageDataV3> rawPages;
    "rawPages", pageDataDef,

    //helpers::Array<PagedImagePageDataV3> strippedPages;
    "strippedPages", pageDataDef,
    //dword flags;
    "flags","uint32"
]

},{"../../../util/ParserUtils":33}],25:[function(_dereq_,module,exports){
var HEAD_STRUCT = [
	'type', 'cstring:4',
	'chunkDataSize', 'uint32',
	'chunkVersion', 'uint16',
	'chunkHeaderSize', 'uint16',
	'offsetTableOffset', 'uint32',
];

/**
 * Basic chunk parsing functionality for Guild Wars 2 file chunks
 * @class GW2Chunk
 * @constructor
 * @param {DataStream} ds A DataStream containing deflated chunk binary data.
 * @param {Number} addr Offset of chunk start within the DataStream
 */
var Chunk = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.headerLength  = NaN;
	
	this.loadHead();
};

Chunk.prototype.loadHead=function(){
	this.ds.seek(this.addr);	
	this.header = this.ds.readStruct(HEAD_STRUCT);

	this.headerLength = this.ds.position - this.addr;
};

Chunk.prototype.loadData=function(dataStruct){
	this.ds.seek(this.addr + this.headerLength);
	this.data =  this.ds.readStruct(dataStruct);
};

Chunk.prototype.next = function(){
	try{
		// Calculate actual data size, as mChunkDataSize
		// does not count the size of some header variables
		return new Chunk(this.ds,this.addr + 8 + this.header.chunkDataSize);
	}
	catch(e){
		/// Out of bounds probably
		return null;
	}
};

module.exports = Chunk;
},{}],26:[function(_dereq_,module,exports){
var Chunk = _dereq_('./GW2Chunk');

var HEAD_STRUCT = [
	'identifier', 'cstring:2',
	'unknownField1', 'uint16',
	'unknownField2', 'uint16',
	'pkFileVersion', 'uint16',
	'type', 'cstring:4'
];


/**
 * Basic header and chunk parsing functionality for Guild Wars 2 files
 * @class GW2File
 * @constructor
 * @param {DataStream} ds A DataStream containing deflated file binary data.
 * @param {Number} addr Offset of file start within the DataStream
 */
var File = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.headerLength  = NaN;

	/**
	 * All {{#crossLink "GW2Chunk"}}chunks{{/crossLink}} contained in the file.
	 *
	 * @property chunks
	 * @type GW2Chunk[]
	 */
	this.chunks = [];
	
	this.readHead();
	this.readChunks();
};



/**
 * Parses the file header data, populating the header property.
 * @method readHead
 */
File.prototype.readHead = function(){
	this.ds.seek(this.addr);
	this.header = this.ds.readStruct(HEAD_STRUCT);
	this.headerLength = this.ds.position - this.addr;
};

/**
 * Parses the file headers and populates the chunks property.
 * @method readChunks
 */
File.prototype.readChunks = function(){

	/// Reset chunks
	this.chunks = [];

	var structs = this.getChunkStructs && this.getChunkStructs();

	/// Load basic Chunk in order to read the chunk header.
	var ch = new Chunk(this.ds, this.headerLength + this.addr);	

    while(structs && ch!=null && ch.header.type){
    	

    	/// Load the chunk data if a structure for this chunk type is defined.
		if( structs.hasOwnProperty(ch.header.type.toLowerCase()) ){

			var chunkStruct = structs[ch.header.type.toLowerCase()];
			if(chunkStruct){
				ch.loadData(chunkStruct);
			}
			

			/// Save chunk reference in this file.
	    	this.chunks.push(ch);
    	}

    	/// Load next basic Chunk in order to read the chunk header.
    	ch = ch.next();
    }
};

/**
 * Get a GW2Chunk from this file
 * @method getChunk
 * @param  {String} type The name, or type of the desired chunk.
 * @return {GW2Chunk} The first GW2Chunk in this file matching the type name, or null if no matching GW2Chunk was found.
 */
File.prototype.getChunk = function (type){
	for(var i=0; i<this.chunks.length; i++){
		if( this.chunks[i].header.type.toLowerCase() == type.toLowerCase() )
			return this.chunks[i]; 
	}
	return null;
};

/**
 * Provides a list of known header types and their parsing structure. Should be defined by each file type individually.
 * @method getChunkStructs
 * @return {Object} An object mapping chunk identifiers to DataStream structure descriptors.
 */
File.prototype.getChunkStructs = function(){
	return {}
};

module.exports = File;
},{"./GW2Chunk":25}],27:[function(_dereq_,module,exports){
var GW2File = _dereq_('./GW2File');

/**
 * "mapc" File
 * @class MapFile
 * @constructor
 * @extends GW2File
 * @param {DataStream} ds A DataStream containing deflated map file binary data.
 * @param {Number} addr Offset of file start within the DataStream
 */
function MapFile(ds, addr){
	GW2File.call(this, ds, addr);
};
MapFile.prototype = Object.create(GW2File.prototype);
MapFile.prototype.constructor = MapFile;


MapFile.prototype.getChunkStructs = function(){
	return {
		"havk":_dereq_(	'../definition/mapc/HavokDefinition'),
		"trn":_dereq_(	'../definition/mapc/TerrainDefinition'),
		"parm":_dereq_(	'../definition/mapc/ParameterDefinition'),
		"shor":_dereq_(	'../definition/mapc/ShoreDefinition'),
		"zon2":_dereq_(	'../definition/mapc/ZoneDefinition'),
		"prp2":_dereq_(	'../definition/mapc/PropertiesDefinition'),
		"cube":_dereq_(	'../definition/mapc/CubeMapDefinition'),
		"env":_dereq_(	'../definition/mapc/EnvironmentDefinition'),
	};
};

module.exports = MapFile;
},{"../definition/mapc/CubeMapDefinition":14,"../definition/mapc/EnvironmentDefinition":15,"../definition/mapc/HavokDefinition":16,"../definition/mapc/ParameterDefinition":17,"../definition/mapc/PropertiesDefinition":18,"../definition/mapc/ShoreDefinition":19,"../definition/mapc/TerrainDefinition":20,"../definition/mapc/ZoneDefinition":21,"./GW2File":26}],28:[function(_dereq_,module,exports){
var GW2File = _dereq_('./GW2File');

function MaterialFile(ds, addr){
	GW2File.call(this, ds, addr);
};
MaterialFile.prototype = Object.create(GW2File.prototype);
MaterialFile.prototype.constructor = MaterialFile;

MaterialFile.prototype.getChunkStructs = function(){
	return {
		"dx9s":_dereq_('../definition/amat/Dx9MaterialDefinition'),
		"grmt":_dereq_('../definition/amat/AmatGrDefinition'),
	};
};

module.exports = MaterialFile;
},{"../definition/amat/AmatGrDefinition":12,"../definition/amat/Dx9MaterialDefinition":13,"./GW2File":26}],29:[function(_dereq_,module,exports){
var GW2File = _dereq_('./GW2File');

function ModelFile(ds, addr){
	GW2File.call(this, ds, addr);
};
ModelFile.prototype = Object.create(GW2File.prototype);
ModelFile.prototype.constructor = ModelFile;

ModelFile.prototype.getChunkStructs = function(){
	return {
		"modl":_dereq_('../definition/modl/ModelDataDefinition'),
		"geom":_dereq_('../definition/modl/GeometryDefinition')
	};
};

module.exports = ModelFile;
},{"../definition/modl/GeometryDefinition":22,"../definition/modl/ModelDataDefinition":23,"./GW2File":26}],30:[function(_dereq_,module,exports){
var GW2File = _dereq_('./GW2File');

function PagedImageFile(ds, addr){
	GW2File.call(this, ds, addr);
};
PagedImageFile.prototype = Object.create(GW2File.prototype);
PagedImageFile.prototype.constructor = PagedImageFile;

PagedImageFile.prototype.getChunkStructs = function(){
	return {
		"pgtb":_dereq_('../definition/pimg/PagedImageTableDataDefinition')
	};
};

module.exports = PagedImageFile;
},{"../definition/pimg/PagedImageTableDataDefinition":24,"./GW2File":26}],31:[function(_dereq_,module,exports){
/*
	guid 1683952224941671000 is fucked up floor in SAB HUB
	materialFilename for that mesh is 564821, shared with lots of stuff
	lod 1 and 2 are both 0
	material flags is 2056
*/
/*if(mat){
	textures = mat.textures;
	texIndex = Math.min(7,textures.length-1);	
}*/
				
var ModelFile = _dereq_("../format/file/ModelFile.js");
var MaterialFile = _dereq_("../format/file/MaterialFile.js");

/// Shared data
/// 	Textures shared between meshes
var sharedTextures = {};


var ME = module.exports = {};


//TODO: MOVE!
function intArrayToHexString(s){
	var hexs = "";
	for (var i = 0; i < s.length; i++) {
		hexs+=toHex32(s[i]);
	};
	return hexs;
}
function toHex32(v){
	var val = v.toString(16);
	var len = 8 - val.length;
	while(len>0){
		val = "0"+val;
		len--;
	}
	return val;
}


function buildVS(numUv){

	var vdefs = "";
	var adefs = "";
	var reads = "";
	for(var i=0; i< numUv; i++){
		vdefs += "varying vec2 vUv_"+(i+1)+";\n";
		
		/// uv and uv2 are defined by THREE
		if(i>1)
			adefs += "attribute vec2 uv"+(i+1)+";\n";


		reads += "vUv_" + (i+1) + " = uv"+(i>0?(i+1):"")+";\n";
	}

	return adefs + vdefs +
	    "void main()\n"+
	    "{\n"+
	        reads+
	        "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n"+
	        "gl_Position = projectionMatrix * mvPosition;\n"+
	    "}";
}

function buildPS(textures, numUv, alphaTest, lightMap){
	var t1uv = "vUv_"+(textures[0].uvIdx+1);
	

	var discard = "";

	if(alphaTest){
		discard = "    if (c1.a < 0.5) \n"+
    	"       discard;\n";	
	}

    /// Color from 1st text or lighted by 2nd?
    var writeColor = "gl_FragColor = c1;\n";

    if(lightMap){
    	var texIdx = 0;
    	//var t2uv = "vUv_4";//+(3-textures[texIdx].uvIdx+1);
    	var t2uv = "vUv_1";// + (textures[texIdx].uvIdx+1);
    	//console.log("t2uv",t2uv);

    	writeColor = "   vec4 c2 = texture2D( texture"+(texIdx+1)+", "+t2uv+" );\n"+
	    "     gl_FragColor = c2;\n";
	    //"     gl_FragColor = vec4(c2.rgb * c1.r/.5, c2.a);\n";
    }


    var uniforms = ""
    textures.forEach(function(t,idx){
    	uniforms += "uniform sampler2D texture"+(idx+1)+";\n";
    });
	/*uniforms += "uniform sampler2D texture1;\n";
	if(lightMap)
		uniforms += "uniform sampler2D texture2;\n";*/

	var varyings = "";	
	for(var i=0; i< numUv; i++){
		varyings += "varying vec2 vUv_"+(i+1)+";\n";

	}

	return uniforms + varyings +
    "void main( void ) {\n"+
    "    vec4 c1 = texture2D( texture1, "+t1uv+" );\n"+
    discard +
    writeColor +
    "}";
}

function getUVMat(textures, numUV, alphaTest){

	var lightMap = false;
	var uniforms = {};

	textures.forEach(function(t,idx){
		uniforms["texture"+idx] = { type: "t", value: t };
	});

	if(textures.length>1){
		lightMap = true;		
	}

	var attributes = {};

	for(var i=2; i<numUV; i++){
		attributes["uv"+(i+1)] =  { type: 'v2', value: [] };
	}

	var vs = buildVS(numUV);

	return new THREE.ShaderMaterial( {
		uniforms: uniforms,
		vertexShader: vs,
		fragmentShader: buildPS(
				textures,
				numUV,
				alphaTest,
				lightMap
			), 
		attributes: attributes,
		side: THREE.DoubleSide,
	} );

}

var getMaterial = ME.getMaterial = function(material, materialFile, localReader){
	if(!materialFile)
		return;
	var dxChunk =  materialFile.getChunk("dx9s");
	var grChunk = materialFile.getChunk("grmt");


	/*if(material.filename != 564821){//hills
		return;
	} 
	if(grChunk.data.flags != 76){//16460){
		return;
	}*/

	//console.log("num textures", material.textures.length);


	/// Append all textures to the custom material
	var finalTextures = [];
	
	//Some materials don't use textures..
	if(material && material.textures.length/* && material.textures[texIndex]*/){

		/// TODO: check for flags!			
		
		/// Find the diffuse texture... this is kinda bullshit
		/// (method used by GW2Browser)
		/// 
		/// Material File Group Chunk has array:
		/// 
		/// texTokens<qword>
		/// 
		/// Material File DX9S Chunk has array:
		/// 
		/// samplers<AmatDx9SamplerV10>   -> smapler.textureIndex
		/// 
		/// techinques[] -> passes[] -> effects[] -> samplerIndex[]
		/// 

		

		
		//console.log("num effects",dxChunk.data.techniques[0].passes[0].effects.length);
		


		//if(grChunk.data.flags!=76)
		//	return;

		/// 3 teqs : high medium low								GRAPHICS LEVEL SETTINGS
		/// 1 passes												DON'T CARE
		/// 15 effects			Each effect has a pixel shader 		HOW??
		/// 1 or 2 sampler indices 									USE ALL! (Multi material)
		
		var effects = dxChunk.data.techniques[0].passes[0].effects;
		//var effect = effects[10];
		var effect = effects[0];

		var shader = dxChunk.data.shaders[effect.pixelShader];
		
		//debugger;
		//var shaderString = intArrayToHexString(shader.shader);


		/*effects.forEach(function (eff) {
			if(eff.samplerIndex.length > effect.samplerIndex.length)
				effect = eff;
		});*/
		//var samplerIdx = effect.samplerIndex[0];

		var samplerTextures = [];
		for(var i=0; i<effect.samplerIndex.length; i++)
		{

			var samplerIdx = effect.samplerIndex[i];
			var sampler = dxChunk.data.samplers[samplerIdx];

			/// SHOULD NEVER HAPPEN, hide mesh!
			if(!sampler)
				return;

			var textureToken = sampler && grChunk.data.texTokens[sampler.textureIndex];
			if(!textureToken)
				textureToken = "0-0";
			else
				textureToken =textureToken.val;

			/// Find the texture reffered by this sampler
			var samplerTex = null;
			material.textures.forEach(function(tex, index){

				///Seems like only 1st part of token is used...
				if(!samplerTex && tex.token.split("-")[0] == textureToken.split("-")[0]){
					//console.log("TEX match",tex.token, textureToken)
					samplerTex = tex;
				}
			});

			/// Add this sampler's texture to the collection of all textures
			if(samplerTex){
				samplerTextures.push(samplerTex);
			}
			else{
				///FALLBACK, just guess what texture we should use
				if(sampler)
					samplerTextures.push(material.textures[sampler.textureIndex]);
				else if(material.textures.length>0)
					samplerTextures.push(material.textures[0]);
				else return;
			}


		}/// END for each sampler index in effect

		/// We now have all textures
		//console.log("textures from sampler", samplerTextures);
				

		/// Fallback to using whatever texture there is.
		if(samplerTextures.length <= 0){
			return;
			//mainTex =  material.textures[0];			
		}


		//console.log("num samplers ",samplerTextures.length);
		samplerTextures.forEach(function(texture, idx){

			if(!texture)
				return;
			
			/// Set texture "URL"
			var texURL = texture && texture.filename;

			/// Load texture from RAM or local reader:
			finalTextures[idx] = getTexture(texURL, localReader)
			finalTextures[idx].uvIdx = texture.uvPSInputIndex;



		});
		

	}/// End if material and texture			

	
	

	var finalMaterial;
	//var finalMaterial = new THREE.MeshPhongMaterial({
	//var finalMaterial = new THREE.MeshBasicMaterial({
	//var finalMaterial = new THREE.MeshLambertMaterial({	
	


	/// Create custom shader material if there are textures
	if(finalTextures){

		if(false && finalTextures.length>0){
			finalMaterial = getUVMat( finalTextures, material.texCoordCount, grChunk.data.flags!=16460 );	
		}
		else{
			var ft=false;
			material.textures.forEach(function(t){
				if(!ft && t.token.split("-")[0] == "1733499172")
					ft = t;
			});
			
			if(!ft || ft.filename<=0)
				return;

			//finalMaterial = new THREE.MeshBasicMaterial({
			finalMaterial = new THREE.MeshLambertMaterial({
				side: THREE.DoubleSide, map:getTexture(ft.filename, localReader)
			}); 
			if(grChunk.data.flags!=16460){
				//console.log("Setting alpha flag for ",grChunk.data.flags)
				finalMaterial.alphaTest = 0.05;
			}
		}
			
	}

	/// Fallback material is monocolored red
	else{
		finalMaterial = new THREE.MeshBasicMaterial({
			side: THREE.DoubleSide,
			color:0xff0000,
			shading: THREE.FlatShading}); 
	}

	
	finalMaterial.needsUpdate = true;


	/// Set material props
	/// disable for now in order for custom shaders not to fuck up
	
	if(material){

		var alphaMask0 = 0x0001;// + 0x0100 + 0x0200;
    	var alphaMask1 = 0x0010
    	var alphaMask2 = 0x0100 + 0x0200;
    	var alphaMask2b =  0x0200;

		
		var grChunk = materialFile.getChunk("grmt");

		//Enable alpha test for transparent flags
    	if( (
    		 material.materialFlags & alphaMask0 ||
    		 material.materialFlags & alphaMask1 ||
    		 material.materialFlags & alphaMask2
    		) //&& solidColor != null
		){
    		//return;
    		//mesh.material.transparent = true;
    		//mesh.material.opacity = 2.0;

    		//var clr = solidColor;
    		//var propAlpha = 0;

    		///Backgroud color adds to alpha
    		//if( mesh.materialFlags == 2569  ){

    		/// This is rly just guesswork
    		/// Check material flag  2568 (as int) and compare material filename 27353 to 20041
    		/// Same flags but some have alpha and some don't
    		//if( mesh.materialFlags & alphaMask2b  ){
    		//	propAlpha =  (clr[3] - 128)/128;
    		//	//propAlpha = Math.max(0,propAlpha);
    		//}

    		//mesh.material.alphaTest = Math.max(0, 0.1 );//- propAlpha*2);
    		
    	}


		/// GRCHUNK -> DATA -> FLAGS

		///HAS LIGHT - TEX - ? - EMISSIVE16460
		///
		
		/// 56533 LOD FOR TOMBSTONE?
		
		//	16460			0100 0000 0100 1100			"standard" stuff rendering OK in SAB (no alpha test)

		//	
		//	16452(SAB)		0100 0000 0100 0100			yellow numbers in sab signs
		//	16448(SAB)		0100 0000 0100 0000			faces on rocks, cloudmen, skybox portal images, holes in walls, floor plates...
		//												no lighting??
		//	
		//	 8268			0010 0000 0100 1100	
		//	 3392			0000 1101 0100 0000			Moto machine light bulbs
		//	 2380			0000 1001 0100 1100
		//	 2368			0000 1001 0100 0000			Fountain water with rings, portal border and circular "light"
		//	  332			0000 0001 0100 1100
		//	  324			0000 0001 0100 0100			Moto face sprites
		//	  
		//	  320(SAB)		0000 0001 0100 0000			portal textures (normal maps ish)
		//	  
		//	   76			0000 0000 0100 1100			LOTS OF STUFF
		//	   											Tree leaves, ground, hills, some roofs, flags, street lights
		//	   											sheild textures, some fences, water tops, waterfall
		//	   											
		//	   											IN KHYLO "everything with alpha"
		//	   
		//	   
		//	   68			0000 0000 0100 0100			Some flowers (lo res?) fountain edges foam
		//	   
		//	   64(SAB)		0000 0000 0100 0000			clouds, sun iamge


		var lightMask = 8;
		
		var knownFileFlags = [
			16460,
			16452,
			16448,
			8268,
			3392,
			2380,
			2368,
			332,
			324,
			320,
			76,
			68,
			64];

		if(knownFileFlags.indexOf(grChunk.data.flags)<0){
			console.log("unknown GR flag",grChunk.data.flags);
		}

		

		//console.log("mat filename", material.filename);
		//if(mat.filename != 20041)		//trees, street lights
		//if(mat.filename != 295021) //waterfall
		//if(mat.filename != 69851) // tree tops, green on top of hills
		
		//if(material.filename != 564821) //hills
		//		return;

		//console.log("model material flag",mat.materialFlags); ALWAYS 2056 (solids and broken animations.)

		if( !(grChunk.data.flags & lightMask) ){
			//console.log("no light");
			finalMaterial =  new THREE.MeshBasicMaterial({
				side: THREE.DoubleSide,
				map: finalMaterial.map
			});

		}
		
		if(grChunk.data.flags!=16460){
			finalMaterial.alphaTest = 0.05;
		}



	}/// End if material
	

	return finalMaterial;

}









var getTexture = ME.getTexture = function(texURL, localReader){

	/// Read texture from shared array of loaded textures
	/// or read it from URL and add to shared ones!			
	if(texURL && sharedTextures[texURL]){

		/// Just read from already loaded textures.
		finalTexture = sharedTextures[texURL];

	}
	else if(texURL){

		/// Load and add to shared array.
		finalTexture = loadLocalTexture(localReader,texURL);

		/// Set standard texture functionality.
		finalTexture.wrapT = THREE.RepeatWrapping;
		finalTexture.wrapS = THREE.RepeatWrapping;
		finalTexture.flipY = false;

		sharedTextures[texURL] = finalTexture;
	}

	return finalTexture;
}







/**
 * Load image data into a THREE.Texture from a file within the GW2 .dat file using a LocalReader.
 
 * @param {LocalReader} localReader - The LocalReader to load the file contents from.
 * @param {Number} fileId - The fileId or baseId of the file to load image data from.
 * @param {Number} mapping - What THREE mapping the returned texture will use, not implemented.
 
 * @returns {THREE.Texture} A texture that will be populated by the file data when it is loaded.
 */
var loadLocalTexture = ME.loadLocalTexture = function(localReader, fileId, mapping, defaultColor, onerror){
	
	if(defaultColor === undefined){
		defaultColor = Math.floor( 0xffffff * Math.random() )
	}

	/// Temporary texture that will be returned by the function.
	/// Color is randomized in order to differentiate different textures during loading.
	var texture =  THREE.ImageUtils.generateDataTexture(
		1, // Width
		1, // Height
		new THREE.Color( defaultColor ) // Color
	);

	/// Only allow non-zero fileId, otherwise jsut return static texture
	if( parseInt(fileId) <= 0 ){
		if(onerror)
			onerror();
		return texture;
	}

	/// Load file using LocalReader.
	localReader.loadTextureFile(fileId,
		function(inflatedData, dxtType, imageWidth, imageHeigth){

			/// Require infalted data to be returned.
			if(!inflatedData){
				if(onerror)
					onerror();
				return;
			}

			/// Create image using returned data.
			var image = {
				data   : new Uint8Array(inflatedData),
				width  : imageWidth,
				height : imageHeigth
			};

			/// Use RGBA for all textures for now...
			/// TODO: don't use alpha for some formats!
			texture.format = (dxtType==3 || dxtType==5 || true) ? THREE.RGBAFormat : THREE.RGBFormat;

			/// Update texture with the loaded image.
			texture.image = image;
			texture.needsUpdate = true;
		}
	);	

	/// Return texture with temporary content.
	return texture;
};
},{"../format/file/MaterialFile.js":28,"../format/file/ModelFile.js":29}],32:[function(_dereq_,module,exports){
var ME = module.exports = {};

/**
 * TODO
 * @param  {[type]} h [description]
 * @return {[type]}   [description]
 */
ME.f16 = function(h) {
    var s = (h & 0x8000) >> 15;
    var e = (h & 0x7C00) >> 10;
    var f = h & 0x03FF;

    if(e == 0) {
        return (s?-1:1) * Math.pow(2,-14) * (f/Math.pow(2, 10));
    } else if (e == 0x1F) {
        return f?NaN:((s?-1:1)*Infinity);
    }

    return(s?-1:1) * Math.pow(2, e-15) * (1+(f/Math.pow(2, 10)));
}


/**
 * TODO
 * @param  {[type]} bits [description]
 * @return {[type]}      [description]
 */
ME.popcount = function(bits) {
  var SK5  = 0x55555555,
      SK3  = 0x33333333,
      SKF0 = 0x0f0f0f0f,
      SKFF = 0xff00ff;

  bits -= (bits >> 1) & SK5;
  bits  = (bits & SK3) + ((bits >> 2) & SK3);
  bits  = (bits & SKF0) + ((bits >> 4) & SKF0);
  bits += bits >> 8;

  return (bits + (bits >> 15)) & 63;
}


/**
 * TODO
 * @type {[type]}
 */
var base32Max = Math.pow(2,32);
ME.arr32To64 = function(arr){
  /// Re-read as uint64 (still little endian)
  /// Warn: this will not work for ~50+ bit longs cus all JS numbers are 64 bit floats...
  return base32Max*arr[1] + arr[0];
};


/**
 * TODO
 * @param  {[type]} arr_in     [description]
 * @param  {[type]} comparator [description]
 * @return {[type]}            [description]
 */
ME.sort_unique = function(arr_in, comparator) {
  var arr = Array.prototype.sort.call(arr_in, comparator);
   
    var u = {}, a = [];
  for(var i = 0, l = arr.length; i < l; ++i){
    if(u.hasOwnProperty(arr[i])) {
      continue;
    }
    a.push(arr[i]);
    u[arr[i]] = 1;
  }

  return a;
}
},{}],33:[function(_dereq_,module,exports){
module.exports = {
	getArrayReader : function(structDef, maxCount){
		return function(ds, struct){
			var ret = [];
			try{
		    	var arr_len = ds.readUint32();
		    	var arr_ptr = ds.position + ds.readUint32();


		    	if(maxCount && arr_len > maxCount){
		    		throw("Array length "+arr_len+" exceeded allowed maximum " + maxCount);
		    	}

		    	var pos = ds.position;	    	
		    	
	    	
		    	ds.seek( arr_ptr );
		    	ret = ds.readType (['[]',structDef,arr_len], struct);
		    	ds.seek(pos);
	    	}
	    	catch(e){
	    		console.warn("getArrayReader Failed loading array", e);
	    	}
	    	return ret;
	    }
	},

	getRefArrayReader : function(structDef){
		return function(ds, struct){
	    	var arr_len = ds.readUint32();	    	
	    	var arr_ptr = ds.position + ds.readUint32();
	    	var pos = ds.position;	 

	    	var ret_arr=[];
	    	
	    	/// Pointer points to first int of ACTUAL pointers to the data
	    	
	    	for(var i=0; i<arr_len; i++){
	    		
	    		// Pointer to each value is at pos + i*sizeof(uint32):
	    		ds.seek( arr_ptr + i*4 );

	    		var entry_ptr = ds.position + ds.readUint32();

	    		//Read data
	    		ds.seek( entry_ptr );
	    		try{
	    			ret_arr.push(ds.readStruct(structDef));	
	    		}
	    		catch(e){
	    			//ret_arr.push(null);
	    			console.warn("getRefArrayReader could not find refered data");
	    		}
	    	}

	    	ds.seek(pos);
	    	return ret_arr;
	    }
	},

	getQWordReader:function(){
		var base32Max = 4294967296;
		return function(ds, struct){
			return ds.readUint32()+"-"+ds.readUint32();

			var p0= ds.readUint32();
			var p1= ds.readUint32();
			return base32Max*p1 + p0;
		}
		
	},
	
	getStringReader : function(){
		return function(ds, struct){
			var ptr = ds.position + ds.readUint32();
	    	var pos = ds.position;	    	

	    	/// Go to pointer
	    	ds.seek( ptr );

	    	var ret = ds.readCString();

			/// Go back to where we were
	    	ds.seek( pos );

	    	return ret;
	    }
	},

	getPointerReader : function(structDef){
		return function(ds, struct){
			var ptr = ds.position + ds.readUint32();

			//if(ptr == ds.position)
			//	return {};

	    	var pos = ds.position;	    	

	    	/// Go to pointer
	    	ds.seek( ptr );
	    	
	    	var ret = ds.readStruct(structDef);

			/// Go back to where we were
	    	ds.seek( pos );


	    	return ret;
	    }
	},

	getFileNameReader : function(){
		return function(ds, struct){
			try{
				var ptr = ds.position + ds.readUint32();
	    		var pos = ds.position;
	    	
	    	
		    	/// Go to pointer
		    	ds.seek( ptr );

		    	var ret = ds.readStruct([
		    		"m_lowPart", "uint16", //uint16 m_lowPart;
				    "m_highPart", "uint16", //uint16 m_highPart;
				    "m_terminator", "uint16",//uint16 m_terminator;
				]);


				/// Getting the file name...
				/// Both need to be >= than 256 (terminator is 0)
				ret = (ret.m_highPart - 0x100) * 0xff00 + (ret.m_lowPart - 0xff);


		    	/// Go back to where we were
		    	ds.seek( pos );

		    	return ret;
	    	}
	    	catch(e){
	    		/// Go back to where we were
		    	ds.seek( pos );

		    	return -1;
	    	}	    	
	    }
	}
}
},{}],34:[function(_dereq_,module,exports){
/**
 * @author RequestTimeout
 */
var ModelFile = _dereq_("../format/file/ModelFile");
var MaterialFile = _dereq_("../format/file/MaterialFile");
var MaterialUtils = _dereq_("./MaterialUtils");
var MathUtils = _dereq_("./MathUtils");

/// Formats
var fvfFormat = {
	Position              : 0x00000001,   /**< 12 bytes. Position as three 32-bit floats in the order x, y, z. */
	Weights               : 0x00000002,   /**< 4 bytes. Contains bone weights. */
    Group                 : 0x00000004,   /**< 4 bytes. Related to bone weights. */
    Normal                : 0x00000008,   /**< 12 bytes. Normal as three 32-bit floats in the order x, y, z. */
    Color                 : 0x00000010,   /**< 4 bytes. Vertex color. */
    Tangent               : 0x00000020,   /**< 12 bytes. Tangent as three 32-bit floats in the order x, y, z. */
    Bitangent             : 0x00000040,   /**< 12 bytes. Bitangent as three 32-bit floats in the order x, y, z. */
    TangentFrame          : 0x00000080,   /**< 12 bytes. */
    UV32Mask              : 0x0000ff00,   /**< 8 bytes for each set bit. Contains UV-coords as two 32-bit floats in the order u, v. */
    UV16Mask              : 0x00ff0000,   /**< 4 bytes for each set bit. Contains UV-coords as two 16-bit floats in the order u, v. */
    Unknown1              : 0x01000000,   /**< 48 bytes. Unknown data. */
    Unknown2              : 0x02000000,   /**< 4 bytes. Unknown data. */
    Unknown3              : 0x04000000,   /**< 4 bytes. Unknown data. */
    Unknown4              : 0x08000000,   /**< 16 bytes. Unknown data. */
    PositionCompressed    : 0x10000000,   /**< 6 bytes. Position as three 16-bit floats in the order x, y, z. */
    Unknown5              : 0x20000000,   /**< 12 bytes. Unknown data. **/
};
/// End Formats

/// Shared data
/// 	Meshes shared between propArray entries
var sharedMeshes = {};
/// End Shared data

var ME = module.exports = {};

var renderRect = ME.renderRect = function(rect, yPos, material, dy){
	var dx = rect.x1 - rect.x2;
	var dz = rect.y1 - rect.y2;
	if(!dy)
		dy = 1;

	var cx = (rect.x1 + rect.x2)/2;
	var cz = (rect.y1 + rect.y2)/2;
	var cy = yPos;

	var geometry = new THREE.BoxGeometry( dx, dy, dz );


	material = material || new THREE.MeshBasicMaterial(
		{
		 	color: 0xff0000,
			wireframe:true,
		}
	);
	var plane = new THREE.Mesh( geometry, material );
	plane.overdraw = true;
	
	plane.position.x = cx;
	plane.position.y = cy;
	plane.position.z = cz;

	return plane;
};


/**
 * Load image data into a THREE.Texture from a file within the GW2 .dat file using a LocalReader.
 
 * @param {LocalReader} localReader - The LocalReader to load the file contents from.
 * @param {Number} fileId - The fileId or baseId of the file to load image data from.
 * @param {Number} mapping - What THREE mapping the returned texture will use, not implemented.
 
 * @returns {THREE.Texture} A texture that will be populated by the file data when it is loaded.
 */
var loadLocalTexture = ME.loadLocalTexture = function(localReader, fileId, mapping, defaultColor, onerror){
	return MaterialUtils.loadLocalTexture(localReader, fileId, mapping, defaultColor, onerror);
};

/**
* Returns a THREE representation of the data contained by a GW2 model file.
* The data is read using a LocalReader reference into the GW2 .dat.

* @param {LocalReader} localReader - The LocalReader to load the file contents from.
* @param {Object} chunk - Model GEOM chunk
* @param {Object} modelDataChunk - Model MODL chunk

* @returns {Array.<THREE.Mesh>} Each geometry in the model file represented by a textured THREE.Mesh
*/
var renderGeomChunk = ME.renderGeomChunk = function(localReader, chunk, modelDataChunk){

	var rawMeshes = chunk.data.meshes;
	var meshes = [];
	var mats = modelDataChunk.data.permutations[0].materials;
	
	rawMeshes.forEach(function(rawMesh){

		var rawGeom = rawMesh.geometry;
		var fvf = rawGeom.fvf;

		var numVerts = rawGeom.vertexCount;
		var rawVerts = rawGeom.vertices;

		var indices = rawGeom.indices;

		var geom = new THREE.BufferGeometry();

		var vertDS =  new DataStream(rawVerts.buffer);

		//Dirty step length for now:
		var stride = rawVerts.length / numVerts;

		//Each vertex
		//DO UV as well
		var vertices = new Float32Array( numVerts * 3 );
		var tangents = null; 
		var normals = null;
		var uvs = []; 
		

		/// Calculate the distance to the first pair of UV data from the
		/// start of the vertex entry
		/// 
		var distToNormals = 
			!!(fvf & fvfFormat.Position) 		* 12 +
			!!(fvf & fvfFormat.Weights) 		*  4 +
			!!(fvf & fvfFormat.Group) 			*  4 ;

		var distToTangent = 
			distToNormals							 +
			!!(fvf & fvfFormat.Normal) 			* 12 +
			!!(fvf & fvfFormat.Color) 			*  4;

		var distToBittangent = 
			distToTangent						 +
			!!(fvf & fvfFormat.Tangent) 		* 12;

		var distToTangentFrame = distToBittangent	 +
			!!(fvf & fvfFormat.Bitangent) 		* 12;

		var distToUV = 
			distToTangentFrame						 +
			!!(fvf & fvfFormat.TangentFrame) 	* 12;

		/// Check if the UV is 32 bit float or 16 bit float.
		var uv32Flag = (fvf & fvfFormat.UV32Mask) >> 8;
		var uv16Flag = (fvf & fvfFormat.UV16Mask) >> 16;
		var isUV32 = !!uv32Flag;
		var hasUV = !!uv16Flag || !!uv32Flag;
		
		/// Popcount (count the number of binary 1's) in the UV flag
		/// to get the number of UV pairs used in this vertex format.
		var masked = isUV32 ? uv32Flag : uv16Flag;
		var numUV = MathUtils.popcount(masked);

		numUV = Math.min(numUV,1.0);


		/// Create typed UV arrays
		if(hasUV){
			for(var i=0; i<numUV; i++){
				uvs[i] = new Float32Array( numVerts * 2 );
			}
		}
		

		
	
		if( !!(fvf & fvfFormat.Normal) ){
			
			//console.log("HAS Normal");

		}
	
		if( !!(fvf & fvfFormat.Tangent) ){
			
			//console.log("HAS Tangent");

		}

		if( !!(fvf & fvfFormat.Bitangent) ){
			
			//console.log("HAS Bitangent");

		}
		if( !!(fvf & fvfFormat.TangentFrame) ){
			
			//console.log("HAS TangentFrame");

		}

		/// Read data from each vertex data entry
		for(var i=0; i<numVerts; i++){

			/// Go to vertex memory position
			vertDS.seek(i*stride);

			/// Read position data
			/// (we just hope all meshes has 32 bit position...)
			var x = vertDS.readFloat32();
			var z = vertDS.readFloat32();
			var y = vertDS.readFloat32();

			/// Write position data, transformed to Tyria3D coordinate system.
			vertices[i*3 + 0] =  x ;//- c.x;
			vertices[i*3 + 1] = -y ;//+ c.y;
			vertices[i*3 + 2] = -z ;//+ c.z;

			/// Read data at UV position
			if(hasUV){
				
				for(var uvIdx=0; uvIdx<numUV; uvIdx++){

					vertDS.seek(
						i*stride + 
						distToUV + 
						uvIdx*(isUV32 ? 8 : 4)
					);

					/// Add one UV pair:
			
					var u,v;
					if(isUV32){
						u = vertDS.readUint32();
						v = vertDS.readUint32();
					}
					else{
						u = MathUtils.f16(vertDS.readUint16());
						v = MathUtils.f16(vertDS.readUint16());				
					}
					
					/// Push to correct UV array
					uvs[uvIdx][i*2 + 0] = u;
					uvs[uvIdx][i*2 + 1] = v;
				}

				
			} /// End if has UV
				

			

		} /// End each vertex

		/// Each face descripbed in indices
		var faces = new Uint16Array( indices.length );		
		for(var i=0; i<indices.length; i+=3){

			// This is ONE face
			faces[i + 0] = indices[i + 0];
			faces[i + 1] = indices[i + 1];
			faces[i + 2] = indices[i + 2];

		}// End each index aka "face"



		/// Add position, index and uv props to buffered geometry
		geom.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
		geom.addAttribute( 'index', new THREE.BufferAttribute( faces, 1) );

		if(normals){
			console.log("adding normals");
			geom.addAttribute( 'normal', new THREE.BufferAttribute( normals, 3 ) );
			geom.normalizeNormals();
			geom.normalsNeedUpdate = true;
		}
		else{
			/// Calculate normals
			geom.computeVertexNormals();
		}

		
		if(hasUV){

			for(var uvIdx=0; uvIdx<numUV; uvIdx++){

				/// Names are "uv", "uv2", "uv3", ... , "uvN"
				var uvName = "uv" + ( uvIdx > 0 ? uvIdx+1 : "" );
				
				/// Set "custom" attribute uvN
				geom.addAttribute( uvName, new THREE.BufferAttribute( uvs[uvIdx], 2 ) );

				/// Flag for update
				geom.attributes[uvName].needsUpdate = true;	
			}
			

			/// Not needed anymore?
			geom.uvsNeedUpdate = true;	
		}
		

		/// Tell geometry to update its UVs and buffers
		geom.buffersNeedUpdate = true;

		/// DONE READING VERTEX DATA
		

		/// Get material used for this mesh
		var matIdx = rawMesh.materialIndex;
		var mat = mats[matIdx];
		var materialFile = null

		if(mat && matFiles[mat.filename]){
			materialFile = matFiles[mat.filename];			
		}

		var finalMaterial =  MaterialUtils.getMaterial(mat, materialFile, localReader);	

		///TESTING
		if(!finalMaterial)
			return;

		

		/// Create the final mesh from the BufferedGeometry and MeshBasicMaterial
		var finalMesh = new THREE.Mesh(geom, finalMaterial);

		
		/// Set material info on the returned mesh
		if(mat){
			finalMesh.materialFlags = mat.materialFlags;
			finalMesh.materialFilename = mat.filename;
		}

		finalMesh.materialName = rawMesh.materialName;

		/// Set lod info on the returned mesh
		finalMesh.numLods = rawMesh.geometry.numLods;
		finalMesh.lodOverride = modelDataChunk.data.lodOverride;

		/// Set flag and UV info on the returned mehs
		finalMesh.flags = rawMesh.flags;
		finalMesh.numUV = numUV;
		/// ---------------------------------------

		/// Add mesh to returned Array
		meshes.push( finalMesh );


	});/// End rawMeshes forEach
	
	return meshes;	
};

/**
 * Loads mesh array from Model file and sends as argument to callback.
 * @param  {Number} filename - Name of the model file to load data from.
 * @param  {Array<Number>} solidColor - RGBA 
 * @param  {[type]} localReader - TODO
 * @param  {Function} callback - TODO
 */
var matFiles = {};
var loadMeshFromModelFile = ME.loadMeshFromModelFile =
function(filename, solidColor, localReader, callback){

	//Short handles prop attributes
	var finalMeshes = [];

	///Load file
	localReader.loadFile(filename,function(inflatedData){
		try{
			if(!inflatedData){
				throw "Could not find MFT entry for "+filename;
			}

			var ds = new DataStream(inflatedData);
			var modelFile = new ModelFile(ds,0);

			//MODL for materials -> textures
			var modelDataChunk = modelFile.getChunk("modl");
		    
		    //GEOM for geometry
		    var geometryDataChunk = modelFile.getChunk("geom");


		    /// Hacky fix for not being able to adjust for position
		    var boundingSphere = modelDataChunk.data.boundingSphere;
		    var bsc = boundingSphere.center;
		    boundingSphere.radius+= Math.sqrt( bsc.x*bsc.x + Math.sqrt(bsc.y*bsc.y + bsc.z*bsc.z) );


		    /// Load all material files
		    var allMats = modelDataChunk.data.permutations[0].materials;

		    function loadMaterialIndex(mIdx, matCallback){
		    	
		    	if(mIdx>=allMats.length){
		    		
		    		matCallback();
		    		return;
		    	}

		    	var mat = allMats[mIdx];

		    	/// Skip if file is loaded
		    	if(matFiles[mat.filename]){
		    		loadMaterialIndex(mIdx+1,matCallback);
		    		return;
		    	}

				localReader.loadFile(mat.filename,
					function(inflatedData){
						if(inflatedData){
							var ds = new DataStream(inflatedData);
							var materialFile = new MaterialFile(ds,0);
							matFiles[mat.filename] = materialFile;	
						}
						
						loadMaterialIndex(mIdx+1,matCallback);	
						
					}					
				);
		    }



		    loadMaterialIndex(0, function(){

		    	/// Create meshes
			    var meshes = renderGeomChunk(localReader, geometryDataChunk, modelDataChunk);			    

			    // Build mesh group
			    meshes.forEach(function(mesh){
			    	
			    	/// Material flags
			    	var knownflags = [

			    		/*
							1-5
							Has Tex?	IDK			Light?		Alpha?

							5-8
							0			0	 		IDK		 	Water?

							9-12
							Has Tex?	0			Alpha?		Alpha?

							13
							IDK KEV
			    		*/

			    		0,			//0 0000 0000 0000		Ground / Wall splashes
			    		8,			//0 0000 0000 1000		Broken Khylo roof DDS
			    		9,			//0 0000 0000 1001		Tree leaves

			    		520,		//0 0010 0000 1000		Some LOD modules, fires, smoke, inside of tents (some DSS textures)
			    		

			    		2056,		//0 1000 0000 1000		Solid objects, also broken animations

			    		///Solids here are unhappy, or are they? could be animations etc
			    		2057,		//0 1000 0000 1001		Windmill sails, bushes, trees, but also a statue and a few pieces of wall

			    		2060,		//0 1000 0000 1100		A few solid objects, like wooden barricades, one(!) painting
			    		2061,		//0 1000 0000 1101		A few bushes, two paintings
			    		
			    		
			    		2312,		//0 1001 0000 1000		Opaque Clock tower main walls AND IVY
			    		2316,		//0 1001 0000 1100		Bushes, inner flower walkway a ramp and a box

			    		// Number 10
			    		2568,		//0 1010 0000 1000		Lots of solids; walls, tents also some tent details WITH alpa

			    		//Number 11
			    		2569,		//0 1010 0000 1001		Solids like walls and roofs and appernt non solids like ropes

			    		2572,		//0 1010 0000 1100		Solid wooden beems, lamp posts
			    		2573,		//0 1010 0000 1101		Lamp holders, bushes, fences, apparent non solids
			    		2584,		//0 1010 0001 1000		Fountain Well water

			    		2824,		//0 1011 0000 1000		Windows, sign arrows, cloth roofs (non solids) BUT straw roofs
			    		2828,		//0 1011 0000 1100		A few fence post (non solids)
			    		2840,		//0 1011 0001 1000		Fountain running water + pipe water

			    		4617,		//1 0010 0000 1001		Found nothing
			    		6664		//1 1010 0000 1000		Two groups of solid boxes
			    	];

			    	var alphaMask0 = 0x0001;// + 0x0100 + 0x0200;
			    	var alphaMask1 = 0x0010
			    	var alphaMask2 = 0x0100 + 0x0200;
			    	var alphaMask2b =  0x0200;

			    	var texMask = 0x8 + 0x0800;

			    	//console.log("Mat flagz",mesh.materialFlags);
			    		 	

			    	if(knownflags.indexOf(mesh.materialFlags)!==11){
			    		//return;
			    	}

			    	// No smoke etc
			    	if( mesh.materialFlags == 520 ){
			    		//return;
			    	}

			    	//Must have texture
			    	if( !(mesh.materialFlags & texMask) ){
			    		return;
			    	}

					//NO lods
			    	if(mesh.flags == 4 || mesh.flags == 1 || mesh.flags == 0){
			    		//return;
			    	}

					//Add to final colection
					finalMeshes.push(mesh);

			    });/// END FOR EACH meshes

				callback(finalMeshes, boundingSphere);

			    
		    });/// END LOAD MATERIALS CALLBACK


		}
	    catch(e){
	    	console.warn("Failed rendering model "+filename,e);
	    	var mesh = new THREE.Mesh( new THREE.BoxGeometry( 200, 2000, 200 ), new THREE.MeshNormalMaterial() );
	    	mesh.flags = 4;
	    	mesh.materialFlags = 2056;
	    	mesh.lodOverride = [1000000,1000000];
	    	finalMeshes.push(mesh);

	    	/// Send the final meshes to callback function
	    	callback(finalMeshes);
	    }

	    


		    

	});/// END FILE LOADED CALLBACK FUNCTION
};

/**
 * Loads mesh array from Model file and sends as argument to callback. 
 * @param  {Number} filename - The fileId or baseId of the Model file to load
 * @param  {Array<Number>} color - TODO
 * @param  {LocalReader} localReader - The LocalReader object used to read data from the GW2 .dat file.
 * @param  {Function} callback - TODO... The callbackfunction to receive the results. Arguments will be (meshes, isCached)
 */
var getMeshesForFilename = ME.getMeshesForFilename = function(filename, color, localReader, callback){

	/// If this file has already been loaded, just return a reference to the meshes.
	/// isCached will be set to true to inform the caller the meshes will probably
	/// have to be cloned in some way.
	if( sharedMeshes[filename] ){
		callback(sharedMeshes[filename].meshes, true, sharedMeshes[filename].boundingSphere)
	}

	/// If this file has never been loaded, load it using loadMeshFromModelFile
	/// the resulting mesh array will be cached within this model's scope.
	else{

		loadMeshFromModelFile(filename, color, localReader, function(meshes, boundingSphere){

			/// Cache result if any.
			if(meshes){
				sharedMeshes[filename] ={
					meshes : meshes,
					boundingSphere : boundingSphere
				}
			}

			/// Allways fire callback.
			callback(meshes, false, boundingSphere);

		});
	}
}


var getFilesUsedByModel = ME.getFilesUsedByModel = function(filename, localReader, callback){
	var fileIds = [filename];

	///Load model file
	localReader.loadFile(filename,function(inflatedData){
		
		try{
			if(!inflatedData){
				throw "Could not find MFT entry for "+filename;
			}

			var ds = new DataStream(inflatedData);
			var modelFile = new ModelFile(ds,0);

			//MODL for materials -> textures
			var modelDataChunk = modelFile.getChunk("modl");

			/// Get materials used by model
			var mats = modelDataChunk.data.permutations[0].materials;

			/// Add each material file AND referenced TEXTURES
			mats.forEach(function(mat){

				/// Add material file id
				var matFileName = mat.filename;
				fileIds.push(matFileName);

				/// Add each texture file id
				mat.textures.forEach(function(tex){
					fileIds.push(tex.filename);
				})
				
			});
			
		}
		catch(e){
			console.warn("Could not export any data",e);
		}

		callback(fileIds);
	});

}


},{"../format/file/MaterialFile":28,"../format/file/ModelFile":29,"./MaterialUtils":31,"./MathUtils":32}]},{},[3])
(3)
});