YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "GW2Chunk",
        "GW2File",
        "LocalReader",
        "MapFile",
        "T3D"
    ],
    "modules": [
        "T3D"
    ],
    "allModules": [
        {
            "displayName": "T3D",
            "name": "T3D",
            "description": "Tyria 3D Main API\n\nUse this static class to access file parsers- and data renderer classes.\n\nThis class also works as a factory for creating\nLocalReader instances that looks up and inflates files from the Guild Wars 2 .dat."
        }
    ],
    "elements": []
} };
});