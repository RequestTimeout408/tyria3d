/// Globals variables
var $ = require('jquery');
var THREE = require('THREE');
require("THREE-OrbitControls");
var MeshPhongGlowMaterial = require("./materials/MeshPhongGlowMaterial");


var scene, camera, controls, renderer,
geometry, material, mesh,
diffuseMap, glowMap, normalMap,
directionalLight, ambientLight;

/// Settings

var shamanPath = 'img/map.jpg';
var shamanLabelsPath = 'img/map_labels.jpg';
//var originalPath = 'img/overlayed.jpg';
var originalPath = 'img/map_org.png';

var cameraOpts = {
	fov : 60,
	near : 1,
	far : 10000,
	pos : {
		x : 0,
		y : 0,
		z : 1000
	}
},
renderOpts = {
	antialias : true,
	alpha : true,
	clearClr : 0x000000,
	clearAlpha : .5
},
controlOpts = {
	damping : .2
},
lightOpts = {
	directionalClr : 0xaaaaaa,
	ambientClr : 0x888888,
	directionalClrShaman : 0x888888,
	ambientClrShaman : 0x55555,
	directionalPos : {
		x : 200,
		y : 200,
		z : 200
	},
	glow : true,
	normals : true
},
globeOpts = {
	texturePath : originalPath,
	glowPath : 'img/alphamap8.png',
	normalPath : 'img/normal.png',
	radius : 250,
	segs : 200,
	rotation : {
		x : 0 * Math.PI / 8,
		y : -3 * Math.PI / 8,
		z : 0
	}
};

$("body").height(window.innerHeight);
window.setTimeout(init, 1);

function hasWebGL(canvas) {
	try {
		gl = canvas.getContext("webgl");
	} catch (x) {
		gl = null;
	}

	if (gl === null) {
		try {
			gl = canvas.getContext("experimental-webgl");
			glExperimental = true;
		} catch (x) {
			gl = null;
		}
	}

	if (gl) {
		return true;
	} else if ("WebGLRenderingContext" in window) {
		return false;
	} else {
		return false;
	}
}

function init() {

	///Check for webgl
	if (!window.WebGLRenderingContext) {
		window.setTimeout(function() {
			$("#noGL").fadeToggle();
		}, 500);
		return;
	}

	var canvas = document.createElement("canvas");

	//gl=true;

	if (!hasWebGL(canvas)) {
		window.setTimeout(function() {
			$("#errorGL").fadeToggle();
		}, 500);
		$("body").height($(window).height());
		return;
	}

	/// Create scene instance
	scene = new THREE.Scene();

	/// Create a camera, aim and place it.
	camera = new THREE.PerspectiveCamera(cameraOpts.fov,
		window.innerWidth / window.innerHeight,
		cameraOpts.near, cameraOpts.far);
		
	camera.position.set(cameraOpts.pos.x, cameraOpts.pos.y, cameraOpts.pos.z);

	/// Camera Orbit control
	controls = new THREE.OrbitControls(camera);
	controls.damping = controlOpts.damping;
	controls.addEventListener('change', render);

	// Add ambient lighting
	ambientLight = new THREE.AmbientLight(lightOpts.ambientClr);
	scene.add(ambientLight);

	// Add directional lighting
	directionalLight = new THREE.DirectionalLight(lightOpts.directionalClr);
	directionalLight.position.set(lightOpts.directionalPos.x, lightOpts.directionalPos.y, lightOpts.directionalPos.z).normalize();
	scene.add(directionalLight);

	/// Add scene objects
	addSphere(scene);

	/// Create canvas rendering output
	renderer = new THREE.WebGLRenderer({
		antialias : renderOpts.antialias,
		alpha : renderOpts.alpha
	});
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.setClearColor(renderOpts.clearClr, renderOpts.clearAlpha);

	///Add renderer to html DOM
	document.body.appendChild(renderer.domElement);

	///Adjust camera on window resize
	$(window).resize(function(event) {
		$("body").height(window.innerHeight);
		renderer.setSize(window.innerWidth, window.innerHeight);
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
	});

	///Listen to settings buttons and info button clicks
	$("#map_original").click(useOriginalMap);
	$("#map_shaman").click(useShamanMap);
	$("#map_shaman_labels").click(useShamanLabelMap);
	$("#glow").click(toggleGlow);
	$("#normals").click(toggleNormals);
	$("#info").click(toggleInfo);
	$("#about").click(toggleInfo);
	$("#about div").click(function(evt) {
		evt.stopPropagation();
	});

	///Start animation
	animate();
}

function toggleInfo() {
	$("#info").toggleClass("active");
	$("#about").fadeToggle();
	controls.enabled = !controls.enabled;
}

function clearActiveButtons() {
	$("#map_original").removeClass("active");
	$("#map_shaman").removeClass("active");
	$("#map_shaman_labels").removeClass("active");
}

function setMapTexturePath(path) {
	diffuseMap = THREE.ImageUtils.loadTexture(path);
	material = new MeshPhongGlowMaterial(diffuseMap, glowMap, lightOpts.normals ? normalMap : undefined);
	mesh.material = material;
}

function useOriginalMap() {
	clearActiveButtons();
	$(this).addClass("active");

	setMapTexturePath(originalPath);
	directionalLight.color.set(lightOpts.directionalClr);
	ambientLight.color.set(lightOpts.ambientClr);
}

function useShamanMap() {
	clearActiveButtons();
	$(this).addClass("active");

	setMapTexturePath(shamanPath);
	directionalLight.color.set(lightOpts.directionalClrShaman);
	ambientLight.color.set(lightOpts.ambientClrShaman);
}

function useShamanLabelMap() {
	clearActiveButtons();
	$(this).addClass("active");

	setMapTexturePath(shamanLabelsPath);
	directionalLight.color.set(lightOpts.directionalClrShaman);
	ambientLight.color.set(lightOpts.ambientClrShaman);
}

function toggleGlow() {
	$(this).toggleClass("active");
	lightOpts.glow = !lightOpts.glow;
}

function toggleNormals() {
	$(this).toggleClass("active");
	lightOpts.normals = !lightOpts.normals;
	if (lightOpts.normals)
		material = new MeshPhongGlowMaterial(diffuseMap, glowMap, normalMap);
	else
		material = new MeshPhongGlowMaterial(diffuseMap, glowMap);
	mesh.material = material;
}

function addSphere(scene) {
	/// Create Sphere Geometry
	geometry = new THREE.SphereGeometry(globeOpts.radius, globeOpts.segs, globeOpts.segs);

	diffuseMap = THREE.ImageUtils.loadTexture(globeOpts.texturePath);
	glowMap = THREE.ImageUtils.loadTexture(globeOpts.glowPath);
	normalMap = THREE.ImageUtils.loadTexture(globeOpts.normalPath);

	material = new MeshPhongGlowMaterial(diffuseMap, glowMap, normalMap);

	/// Create mesh from geometry and material
	mesh = new THREE.Mesh(geometry, material);
	mesh.overdraw = true;

	mesh.rotation.set(globeOpts.rotation.x, globeOpts.rotation.y, globeOpts.rotation.z);

	/// Add to the scene
	scene.add(mesh);
}

function render() {
	directionalLight.position.set(camera.position.x, camera.position.y, camera.position.z).normalize();
	//directionalLight.translateY(1).translateX(1);

	directionalLight.target.position = (0, 0, 0);
	renderer.render(scene, camera);

}

var count = 0;
function animate() {
	/// Fire next frame
	requestAnimationFrame(animate);

	/// Update glow intensity
	var t = 0.5 + 0.5 * Math.sin(count);

	mesh.material.glowIntensity = lightOpts.glow ? 0.05 + 0.1 * t : 0;

	renderer.setClearColor(renderOpts.clearClr, lightOpts.glow ? .4 * (1 - t) : 0);

	///Render scene
	render();

	///Update Clock
	count += 0.02;
}

