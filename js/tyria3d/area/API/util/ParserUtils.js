module.exports = {
	getArrayReader : function(structDef, maxCount){
		return function(ds, struct){
			var ret = [];
			try{
		    	var arr_len = ds.readUint32();
		    	var arr_ptr = ds.position + ds.readUint32();


		    	if(maxCount && arr_len > maxCount){
		    		throw("Array length "+arr_len+" exceeded allowed maximum " + maxCount);
		    	}

		    	var pos = ds.position;	    	
		    	
	    	
		    	ds.seek( arr_ptr );
		    	ret = ds.readType (['[]',structDef,arr_len], struct);
		    	ds.seek(pos);
	    	}
	    	catch(e){
	    		console.warn("getArrayReader Failed loading array", e);
	    	}
	    	return ret;
	    }
	},

	getRefArrayReader : function(structDef){
		return function(ds, struct){
	    	var arr_len = ds.readUint32();	    	
	    	var arr_ptr = ds.position + ds.readUint32();
	    	var pos = ds.position;	 

	    	var ret_arr=[];
	    	
	    	/// Pointer points to first int of ACTUAL pointers to the data
	    	
	    	for(var i=0; i<arr_len; i++){
	    		
	    		// Pointer to each value is at pos + i*sizeof(uint32):
	    		ds.seek( arr_ptr + i*4 );

	    		var entry_ptr = ds.position + ds.readUint32();

	    		//Read data
	    		ds.seek( entry_ptr );
	    		try{
	    			ret_arr.push(ds.readStruct(structDef));	
	    		}
	    		catch(e){
	    			//ret_arr.push(null);
	    			console.warn("getRefArrayReader could not find refered data");
	    		}
	    	}

	    	ds.seek(pos);
	    	return ret_arr;
	    }
	},

	getQWordReader:function(){
		var base32Max = 4294967296;
		return function(ds, struct){
			return ds.readUint32()+"-"+ds.readUint32();

			var p0= ds.readUint32();
			var p1= ds.readUint32();
			return base32Max*p1 + p0;
		}
		
	},
	
	getStringReader : function(){
		return function(ds, struct){
			var ptr = ds.position + ds.readUint32();
	    	var pos = ds.position;	    	

	    	/// Go to pointer
	    	ds.seek( ptr );

	    	var ret = ds.readCString();

			/// Go back to where we were
	    	ds.seek( pos );

	    	return ret;
	    }
	},

	getPointerReader : function(structDef){
		return function(ds, struct){
			var ptr = ds.position + ds.readUint32();

			//if(ptr == ds.position)
			//	return {};

	    	var pos = ds.position;	    	

	    	/// Go to pointer
	    	ds.seek( ptr );
	    	
	    	var ret = ds.readStruct(structDef);

			/// Go back to where we were
	    	ds.seek( pos );


	    	return ret;
	    }
	},

	getFileNameReader : function(){
		return function(ds, struct){
			try{
				var ptr = ds.position + ds.readUint32();
	    		var pos = ds.position;
	    	
	    	
		    	/// Go to pointer
		    	ds.seek( ptr );

		    	var ret = ds.readStruct([
		    		"m_lowPart", "uint16", //uint16 m_lowPart;
				    "m_highPart", "uint16", //uint16 m_highPart;
				    "m_terminator", "uint16",//uint16 m_terminator;
				]);


				/// Getting the file name...
				/// Both need to be >= than 256 (terminator is 0)
				ret = (ret.m_highPart - 0x100) * 0xff00 + (ret.m_lowPart - 0xff);


		    	/// Go back to where we were
		    	ds.seek( pos );

		    	return ret;
	    	}
	    	catch(e){
	    		/// Go back to where we were
		    	ds.seek( pos );

		    	return -1;
	    	}	    	
	    }
	}
}