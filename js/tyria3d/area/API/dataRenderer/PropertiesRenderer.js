var Utils = require("../util/RenderUtils");
var DataRenderer = require('./DataRenderer');

/**
 * @class PropertiesRenderer
 * @constructor
 * @extends DataRenderer
 * @param  {[type]} localReader  [description]
 * @param  {[type]} mapFile  [description]
 * @param  {[type]} settings [description]
 * @param  {[type]} output   [description]
 */
function PropertiesRenderer(localReader, mapFile, settings, output){
	DataRenderer.call(this, localReader, mapFile, settings, output);
}

/// DataRenderer inherrintance:
PropertiesRenderer.prototype = Object.create(DataRenderer.prototype);
PropertiesRenderer.prototype.constructor = PropertiesRenderer;

/// DataRenderer overwrite:

/**
* Renders all property meshes in a GW2 map described by the map's PROP chunk.
*/
PropertiesRenderer.prototype.renderAsync = function(callback){
	var self = this;

	var propertiesChunkData =  this.mapFile.getChunk("prp2").data;

	if(!propertiesChunkData){
		renderCallback();
		return;
	}

	var props = propertiesChunkData.propArray;
	var animProps =propertiesChunkData.propAnimArray;
	var instanceProps = propertiesChunkData.propInstanceArray;
	var metaProps = propertiesChunkData.propMetaArray;

	/// Concat all prop types
	props = props
		.concat(animProps)
		.concat(instanceProps)
		.concat(metaProps);

	// For now, we'll do all load in serial
	// TODO: load unique meshes and textures in parallell (asynch), then render!
	var lastPct = -1;

	var renderIndex = function(idx){

		if(idx>= props.length){
			callback();
			return;
		}

		var pct = Math.round(1000.0*idx / props.length);
		pct/=10.0;
		
		/// Log progress
		if(lastPct!=pct){
			var pctStr = pct +
				( pct.toString().indexOf(".")<0 ? ".0":"" ) +
				"%";
			self.log("Loading 3D Models (Props)", pctStr);
			lastPct = pct;
		}

		/// Read prop at index.
		var prop = props[idx];								

	    /// Adds a single mesh to a group.
		var addMeshToLOD = function(mesh, groups, lod, prop, needsClone){

			/// Read lod distance before overwriting mesh variable
		    var lodDist = prop.lod2 != 0 ? prop.lod2 : mesh.lodOverride[1];

		    /// Read flags before overwriting mesh variable
	    	var flags = mesh.flags;

	    	/// Mesh flags are 0 1 4
	    	/// For now, use flag 0 as the default level of detail
	    	if(flags==0)
	    		lodDist=0;
	    	
	    	/// Create new empty mesh if needed
	    	if(needsClone){
	    		mesh = new THREE.Mesh( mesh.geometry, mesh.material );
	    	}

	    	mesh.updateMatrix();
			mesh.matrixAutoUpdate = false;

	    	// Find group for this LOD distance
	    	if(groups[lodDist]){
	    		groups[lodDist].add(mesh);
	    	}
	    	// Or create LOD group and add to a level of detail
	    	// WIP, needs some testing!
	    	else{
	    		var group = new THREE.Group();
	    		group.updateMatrix();
				group.matrixAutoUpdate = false;
	    		group.add(mesh);
	    		groups[lodDist] = group;
	    		lod.addLevel(group,lodDist);
	    	}

	    	return lodDist;
	    }

	    /// Adds array of meshes to the scene, also adds transform clones
		var addMeshesToScene = function(meshArray, needsClone, boundingSphere){
			
		    ///Add original 

		    /// Make LOD object and an array of groups for each LOD level
		    var groups = {};
		    var lod = new THREE.LOD();

		    /// Each mesh is added to a group corresponding to its LOD distane
		    var maxDist = 0;
		    meshArray.forEach(function(mesh){
		    	maxDist = Math.max( maxDist, addMeshToLOD(mesh,groups,lod,prop,needsClone) );
	    	});

	    	/// Add invisible level
	    	//lod.addLevel(new THREE.Group(),10000);

		    /// Set position, scale and rotation of the LOD object
			if(prop.rotation){
		    	lod.rotation.order = "ZXY";
		    	lod.rotation.set(prop.rotation.x, -prop.rotation.y, -prop.rotation.z);
		    }
		    lod.scale.set( prop.scale, prop.scale, prop.scale );
		    lod.position.set(prop.position.x, -prop.position.y, -prop.position.z);
		   	

		   	lod.boundingSphereRadius = boundingSphere.radius * prop.scale;

		    lod.updateMatrix();
		    lod.matrixAutoUpdate = false;

		    /// Show highest level always
		    //lod.update(lod);

	    	//Add LOD containing mesh instances to scene
	    	self.output.visibles.push(lod);
	    	self.output.nonCollisions.push(lod);
	    				    
		    // Add one copy per transform, needs to be within it's own LOD
		    if(prop.transforms){

		    	prop.transforms.forEach(function(transform){
		    		
		    		/// Make LOD object and an array of groups for each LOD level
		    		var groups = {};
		    		var lod = new THREE.LOD();

		    		/// Each mesh is added to a group corresponding to its LOD distane
		    		var maxDist = 0;
			    	meshArray.forEach(function(mesh){
			    		maxDist = Math.max( maxDist, addMeshToLOD(mesh,groups,lod,prop,true) );
			    	});

			    	/// Add invisible level
	    			//lod.addLevel(new THREE.Group(),10000);

			    	/// Set position, scale and rotation of the LOD object
					if(transform.rotation){
				    	lod.rotation.order = "ZXY";
				    	lod.rotation.set(transform.rotation.x, -transform.rotation.y, -transform.rotation.z);
				    }
				    lod.scale.set( transform.scale, transform.scale, transform.scale );
				    lod.position.set(transform.position.x, -transform.position.y, -transform.position.z);

					lod.updateMatrix();
		    		lod.matrixAutoUpdate = false;

		    		lod.boundingSphereRadius = boundingSphere.radius * prop.scale;

					/// Show highest level always
		    		//lod.update(lod);

			    	/// Add LOD containing mesh instances to scenerender: function(propertiesChunkHeader, map, localReader, renderCallback){
			    	self.output.visibles.push(lod);
			    	self.output.nonCollisions.push(lod);

			    });
		    }
		}

		/// Get meshes
		Utils.getMeshesForFilename(prop.filename, prop.color, self.localReader,
			function(meshes, isCached, boundingSphere){
			
				if(meshes){
					addMeshesToScene(meshes, isCached, boundingSphere);
				}

				/// Render next prop
				renderIndex(idx+1);
			}
		);

		

	};

	/// Start serial loading and redering. (to allow re-using meshes and textures)
	/// textures prolly won't work?
	renderIndex(0);
}


/**
 * TODO: write description. Used for export feature
 * @method getFileIdsAsync
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
PropertiesRenderer.prototype.getFileIdsAsync = function(callback){
	var fileIds = [];

	var propertiesChunkData =  this.mapFile.getChunk("prp2").data;

	var props = propertiesChunkData.propArray;
	var animProps = propertiesChunkData.propAnimArray;
	var instanceProps = propertiesChunkData.propInstanceArray;
	var metaProps = propertiesChunkData.propMetaArray;

	props = props
		.concat(animProps)
		.concat(instanceProps)
		.concat(metaProps);

	var getIdsForProp = function(idx){

		if(idx>=props.length){
			callback(fileIds);
			return;
		}

		if(idx%100==0){
			console.log("getting ids for entry",idx,"of",props.length);
		}

		var prop = props[idx];
		Utils.getFilesUsedByModel(
			prop.filename,
			localReader,
			function(propFileIds){
				fileIds = fileIds.concat(propFileIds);
				getIdsForProp(idx+1);
			}
		);

	};

	getIdsForProp(0);
};

module.exports = PropertiesRenderer;