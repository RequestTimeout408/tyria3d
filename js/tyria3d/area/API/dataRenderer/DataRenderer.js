/**
 * Base class for data interpretors
 * @class DataRenderer
 * @param  {[type]} localReader  [description]
 * @param  {[type]} mapFile      [description]
 * @param  {[type]} settings     [description]
 * @param  {[type]} output       [description]
 */
var DataRenderer = module.exports = function(localReader, mapFile, settings, output) {
	this.localReader = localReader;
	this.mapFile = mapFile;
	this.settings = settings;
	this.output = output;
	
	/**
	 * TODO
	 * @method log
	 * @param  {[type]} title    [description]
	 * @param  {[type]} message  [description]
	 * @param  {[type]} severity [description]
	 * @return {[type]}          [description]
	 */
	this.log = function(title, message, severity){
		logElem = $("#progressPanel");
		logElem.find(".title").html(title);
		logElem.find(".progress").html(message);
	}
}

/**
 * ABOUT RENDER! TODO
 * @method renderAsync
 * @async
 */
DataRenderer.prototype.renderAsync = function(callback){}