var SceneUtils = require("../../SceneUtils.js");

/// static ref. to bounds
var maxBounds;
var mapRect;
var hasLight;

/// havok
var HavokRenderer = require("./HavokRenderer.js");

/// zone
var ZoneRenderer =  require("./ZoneRenderer.js"); 

/// terrain
var TerrainRenderer = require("./TerrainRenderer.js"); 

/// props
var PropertiesRenderer = require("./PropertiesRenderer.js"); 

/// env
var EnvironmentRenderer = require("./EnvironmentRenderer.js"); 

function renderHavok(localReader, mapFile, mapData, cb){
	var hr = new HavokRenderer(
		localReader,
		mapFile,
		{
			visible: $("#showHavok")[0].checked
		},
		mapData
	);

	hr.renderAsync(cb);
}

function renderZone(localReader, mapFile, mapData, cb){
	var zr = new ZoneRenderer(
		localReader,
		mapFile,
		{},
		mapData
	);

	zr.renderAsync(cb);
}

function renderTerrain(localReader, mapFile, mapData, cb){
	
	var settings = {
		anisotropy : SceneUtils.getRenderer().getMaxAnisotropy()
	};

	var tr = new TerrainRenderer(
		localReader,
		mapFile,
		settings,
		mapData
	);

	tr.renderAsync(cb);	
}

function renderParameters(localReader, mapFile, mapData, cb){
	var parameterChunk = mapFile.getChunk("parm");
	ParameterRenderer.render(parameterChunk);
	cb();
}


function renderEnvironment(localReader, mapFile , mapData, cb){

	var er = new EnvironmentRenderer(
		localReader,
		mapFile,
		{},
		mapData
	);

	er.renderAsync(cb);
}

function renderShores(localReader, mapFile, mapData, cb){
	var shoreChunk = mapFile.getChunk("shor");
	console.log("Shore Chunk",shoreChunk);
	cb();
}

function renderProperties(localReader, mapFile, mapData, cb){
	var pr = new PropertiesRenderer(
		localReader,
		mapFile,
		{},
		mapData
	);
	
	pr.renderAsync(cb);
}

module.exports = {

	/// TODO: Use settings object!!
	renderAsync: function(mapFile, localReader, callback){
   
   		/// Enables collision
	    var showHavok = true;

	    /// Shows in game terrain (ground)
	    var showTerrain = true;
	    
	    /// Shows in true models
	    var showZone = $('#loadZone').prop("checked");
	    var showProps = $('#loadProp').prop("checked");

	    /// Shows water surface, no moved!
	    var showParameters = false;

	    /// Snows skybox
	    var showEnvironment = true;

	    /// Not implemented
	    var showShores = false;	    
	    var showCubeMap = false;

	    /// Value Object holding all generated objects!
	    var mapData = {
	    	hasLight : 		false,
	    	boundingBox : 	null,
	    	ambientLight : 	null,
	    	
	    	skyElements :	[],
	    	visibles : [],
	    	terrainTiles : [],
	    	collisions : 	[],
	    	nonCollisions: 	[],
	    	lights : 		[],
	    };

		/// Populate VO by running the renderers
	    var runAllRenderers = function(i){
			if(i < rendererFuncs.length ){
				rendererFuncs[i](localReader, mapFile, mapData, function(){
					runAllRenderers(i+1);
				});
			}
			else{
				callback(mapData);
			}
		}

		var rendererFuncs = [];

		/// Environment
		if(showEnvironment){
			rendererFuncs.push( renderEnvironment );
		}
	    
	    /// Havok
	    if(showHavok){
	    	rendererFuncs.push( renderHavok );
		}

		/// Terrain (depends on Environment)
		if(showTerrain){
			rendererFuncs.push( renderTerrain );
		}

		/// Zone (depends on Terrain)
		if(showZone){
			rendererFuncs.push( renderZone );
		}	

		/// Parameters
		if(showParameters){			
			rendererFuncs.push( renderParameters );
		}

		/// Shores
		if(showShores){			
			rendererFuncs.push( renderShores );
		}

		/// Props
		if(showProps){			
			rendererFuncs.push( renderProperties );
		}

		/// Cube map
		if(showCubeMap){			
			rendererFuncs.push( renderCubeMap );
		}

		runAllRenderers(0);

	}


};