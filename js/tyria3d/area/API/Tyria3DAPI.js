/**
 * Provides the static Tyria 3D API Class.
 * @module T3D
 */

/* INCLUDES */
LocalReader = require('./LocalReader/LocalReader.js');

/**
 * Tyria 3D Main API
 * 
 * Use this static class to access file parsers- and data renderer classes.
 * 
 * This class also works as a factory for creating
 * LocalReader instances that looks up and inflates files from the Guild Wars 2 .dat.
 *
 * @main T3D
 * @class T3D
 * @static 
 */
module.exports = T3D;
function T3D() {}

/* PRIVATE VARS */
var _version = "1.0.3";
var _settings = {
	inflaterURL : "modules/nacl/t3dgwtools.nmf"
};

/* PUBLIC PROPERTIES */

/**
 * The current API version. Used to make sure local storage caches are not
 * shared between different releases.
 *
 * @property version
 * @type String
 */
T3D.version = _version;


/* FILES */

/**
 * @property GW2File
 * @type Class
 */
T3D.GW2File =			require("./format/file/GW2File");

/**
 * @property GW2Chunk
 * @type Class
 */
T3D.GW2Chunk = 			require("./format/file/GW2Chunk");

/**
 * @property MapFile
 * @type Class
 */
T3D.MapFile = 			require("./format/file/MapFile");

/**
 * @property MaterialFile
 * @type Class
 */
T3D.MaterialFile = 		require("./format/file/MaterialFile");

/**
 * @property ModelFile
 * @type Class
 */
T3D.ModelFile = 		require("./format/file/ModelFile");

/**
 * @property PagedImageFile
 * @type Class
 */
T3D.PagedImageFile = 	require("./format/file/PagedImageFile");


/* RENDERERS */

/**
 * @property EnvironmentRenderer
 * @type Class
 */
T3D.EnvironmentRenderer = 	require("./dataRenderer/EnvironmentRenderer");

/**
 * @property HavokRenderer
 * @type Class
 */
T3D.HavokRenderer = 		require("./dataRenderer/HavokRenderer");

/**
 * @property PropertiesRenderer
 * @type Class
 */
T3D.PropertiesRenderer = 	require("./dataRenderer/PropertiesRenderer");

/**
 * @property TerrainRenderer
 * @type Class
 */
T3D.TerrainRenderer = 		require("./dataRenderer/TerrainRenderer");

/**
 * @property ZoneRenderer
 * @type Class
 */
T3D.ZoneRenderer = 			require("./dataRenderer/ZoneRenderer");



/* SETTINGS */

/**
 * Contains a list of known map id:s and their names. Used in order to quickly
 * look up what maps are in a .dat file.
 * @property MapFileList
 * @type Array
 */
T3D.MapFileList = 	require("./MapFileList");


/* PRIVATE METHODS */


/* PUBLIC METHODS */


/**
 * Creates a new instance of LocalReader, complete with inflater.
 * @method getLocalReader
 * @async
 * @param  {File}   	file		Core JS File instance, must refeer to a GW2 .dat file
 * @param  {element}	output		jQuery element, must have .title and .progress
 * @param  {Function}	callback	Callback function, fired when the file index is fully constructed
 * takes no parameters.
 * @return {LocalReader}				
 */
T3D.getLocalReader = function(file, output, callback){

	/// Create Inflater for this file reader.
	/// We use a wrapper to catch the events.
	/// We use the embed tag itself for posing messages.
	var pNaClWrapper = $("<div id='pNaClListener'/>");
	
	var pNaClEmbed = $("<embed type='application/x-pnacl'/>");
	pNaClEmbed.css({position:"absolute", width:0, height:0});
	pNaClEmbed.attr("src", _settings.inflaterURL)

	/// Add the objects to the DOM
	pNaClWrapper.append(pNaClEmbed);
	$("body").append(pNaClWrapper);

	/// Connect the provided file reference to a new LocalReader.
	var lrInstance = new LocalReader(file, _version, output);

	/// Give the LocalReader access to the inflater.
	lrInstance.connectInflater(pNaClEmbed, pNaClWrapper);

	/// Parse the DAT file MFT header. This must be done oncein order to access
	/// any files in the DAT.
	lrInstance.parseHeaderAsync(callback);

	/// Return file reader object
	return lrInstance;	
}

/**
 * Reads the map list from localStorage or the dat file. The resulting list is
 * passed via the callback function.
 * @method getMapListAsync
 * @async
 * @param {LocalReader} localReader
 * @param {Function} callback callback function
 * @param {Object} callback.mapList A list of maps grouped by area, for example
 * 
 * 		{	
 * 			maps:[
 * 				{
 * 					name: 'Heart of Maguuma',
 * 					maps: [
 * 						{fileName:1151420, name:'HoT BWE3 Raid'},
 * 						{fileName:969663, name:'Verdant Brink}
 * 					]
 * 				},
 * 				{
 * 					name: 'Unknown maps',
 * 					maps: [
 * 						{fileName:12345678, name:'Unknown map 12345678'}
 * 					]
 * 				}
 * 			]
 
 *	    };
 */
T3D.getMapListAsync = function(localReader, callback){

	/// Check local storage for an existing map list
	var mapList = localReader.loadMapList();

	/// If there is no cached list, read it.
	if(!mapList){
		localReader.readMapListAsync(false, callback);
	}

	/// Otherwise, just fire the callback with the cached list
	else{
		callback(mapList);
	}
	
}

/**
 * @method  renderMapContentsAsync
 * @param  {[type]} fileName [description]
 * @return {[type]}          [description]
 */
T3D.renderMapContentsAsync = function(localReader, fileName, renderers, callback){

	/// Value Object holding all generated objects
    var output = {
    	hasLight : 		false,
    	boundingBox : 	null,
    	ambientLight : 	null,
    	
    	skyElements :	[],
    	visibles : 		[],
    	terrainTiles : 	[],
    	collisions : 	[],
    	nonCollisions: 	[],
    	lights : 		[],
    };

	/// Make sure we got an actuall ID number		
	if(parseInt(fileName)){

		/// File name is baseId, load using local reader.
		localReader.loadFile(
			fileName,
			function(arrayBuffer){

				/// Set up datastream
				var ds = new DataStream(arrayBuffer, 0, DataStream.LITTLE_ENDIAN);

				/// Initiate Map file object. Connect callback
				var mapFile = new T3D.MapFile(ds, 0);

				/// Populate VO by running the renderers
			    var runAllRenderers = function(i){
					
					/// Run each renderer
					if(i < renderers.length ){
						T3D.runRenderer(
							renderers[i].renderClass,
							localReader,
							mapFile,
							renderers[i].settings,
							output,
							runAllRenderers.bind(this,i+1)
						);
					}

					/// Fire callback with VO when done
					else{
						callback(output);
					}
				};

				/// Starting point for running each renderer
				runAllRenderers(0);
			}
		);
	}

	/// Primitive error message...
	else{
		console.error("Map id must be an integer!, was:",fileName);
	}	
}

/**
 * method runRenderer description
 * @param  {[type]}   renderClass [description]
 * @param  {[type]}   localReader [description]
 * @param  {[type]}   mapFile     [description]
 * @param  {[type]}   settings    [description]
 * @param  {[type]}   output      [description]
 * @param  {Function} cb          [description]
 * @return {[type]}               [description]
 */
T3D.runRenderer = function(renderClass, localReader, mapFile , settings, output, cb){

	var r = new renderClass(
		localReader,
		mapFile,
		settings,
		output
	);

	r.renderAsync(cb);
}