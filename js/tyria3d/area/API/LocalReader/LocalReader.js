/// Generic ArenaNet File with basic header functionality
var File = require('../format/file/GW2File.js');

/// Definition of main dat file header
var defANDAT = require('../format/definition/ANDAT');

/// Definition of the MFT index
var defMFT = require('../format/definition/MFT');

/// Small collection of Math and Sort algorithms
var MathUtils = require('../util/MathUtils.js');

/// List of known GW2 maps used to make lookup faster
var MapFileList =  require('../MapFileList');


/**
 * A statefull class that handles reading and inflating data from a local GW2 dat file.
 * @class LocalReader
 * @constructor
 * @param {File}	datFile A core JS File instance, must refer to the GW2 .dat
 * @param {String}	version T3D version.
 * @param {element}	output  jQuery element reference for some output.
 */
var LocalReader = function(datFile, version, output){

	/// Initiate list of file infaltion listeners
	this.fileListeners = [];

	/// Add reference to file object to DAT and jQuery output element
	this.dat = datFile;
	this.version = version;
	this.output = output;
};

/**
 * TODO
 * @method parseHeaderAsync 
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
LocalReader.prototype.parseHeaderAsync = function(callback){
	var self = this;

	///Read dat file header, 40 bytes should always be the length
	this.loadFilePart(self.dat, 0, 40, self.readANDatHeader);

	this.onFullyLoaded = callback;
}

/**
 * TODO
 * @param  {[type]} inflater        [description]
 * @param  {[type]} inflaterWrapper [description]
 * @return {[type]}                 [description]
 */
LocalReader.prototype.connectInflater = function(inflater, inflaterWrapper){
	var self = this;

	/// HTML pNaCl emed elements
	this.NaClInflater = inflater;	

    /// Set up a listener for any messages passed by the pNaCl component
    inflaterWrapper[0].addEventListener(
    	'message',
    	function(message_event){
    		self.NaClListener.call(self, message_event);
    	},
    	true
	);

}

/**
 * TODO
 * @param {[type]} message_event [description]
 */
LocalReader.prototype.NaClListener = function(message_event){
	if( typeof message_event.data === 'string' ) {
		console.warn("NaCl threw an error",message_event.data);
		return;
	}

	
	//console.log("Got back a DS from NaCl RAW", message_event.data);
	//console.log("Got back a DS from NaCl Uint32Array", new Uint32Array(message_event.data));
	var handle = message_event.data[0];

	if(this.fileListeners[handle]){
		this.fileListeners[handle].forEach(function(callback){
			var data = message_event.data;
			/// Array buffer, dxtType, imageWidth, imageHeigh			
			callback(data[1], data[2], data[3], data[4]);	
		});

		// Remove triggered listeners
		this.fileListeners[handle] = null;
	}
	
}

/**
 * TODO
 * @param  {[type]} ds [description]
 * @return {[type]}    [description]
 */
LocalReader.prototype.readANDatHeader = function(ds){


	/// Read file header data struct
	this.fileHeader = ds.readStruct(defANDAT);

	console.log("Loaded Main .dat header", this.fileHeader);

	/// Get pointer to MFT chunk header
	this.fileHeader.mftOffset =  MathUtils.arr32To64(this.fileHeader.mftOffset);

	/// Load MFT
	this.loadFilePart(
		this.dat,
		this.fileHeader.mftOffset,
		this.fileHeader.mftSize,
		this.readMFTHeader );

};

/**
 * TODO
 * @param  {[type]} ds [description]
 * @return {[type]}    [description]
 */
LocalReader.prototype.readMFTHeader = function(ds){

	/// Read MFT header data struct
	/// Global variable mft
	this.mft = ds.readStruct(defMFT);

	var entryStartPtr = ds.position;
	var numEntires = this.mft.nbOfEntries;

	/// MFT has entries with offset, size and compression flag
	/// for all files in the .dat

	/// Read all entry offsets and sizes into uint32 arrays
	this.mft.entryDict = {
		offset_0:new Uint32Array(numEntires),
		offset_1:new Uint32Array(numEntires),
		offset:new Float64Array(numEntires),
		size:new Uint32Array(numEntires),
		compressed:new Uint16Array(numEntires),
	}


	/// Read offset, size and compressed flag of the entries.
	//console.log("reading MFT entries ");	
	for(var i=0; i<numEntires-1; i++){
		
		/// Read first 14 bytes
		this.mft.entryDict.offset_0[i] = ds.readUint32();
		this.mft.entryDict.offset_1[i] = ds.readUint32();
		this.mft.entryDict.size[i] = ds.readUint32();
		this.mft.entryDict.compressed[i] = ds.readUint16();


		this.mft.entryDict.offset[i] =  MathUtils.arr32To64(
			[ this.mft.entryDict.offset_0[i],
			  this.mft.entryDict.offset_1[i] ]
		);

		/// Skip 10
		ds.seek(ds.position + 10);

	}
	
	console.log( "Finished indexing MFT ", this.mft );


	/// Read data pointed to by 2nd mft entry
	/// This entry maps file ID to MFT index	
	var offset = MathUtils.arr32To64(
		[ this.mft.entryDict.offset_0[1],
		  this.mft.entryDict.offset_1[1] ]
	);
	var size = this.mft.entryDict.size[1];

	this.loadFilePart(this.dat, offset, size, this.readMFTIndexFile);

};

/**
 * TODO
 * @param  {[type]} ds   [description]
 * @param  {[type]} size [description]
 * @return {[type]}      [description]
 */
LocalReader.prototype.readMFTIndexFile = function(ds, size){
	
	var length = size / 8;

	/// fileIdTable
	var ids = new Uint32Array(length);
	var mftIndices = new Uint32Array(length);

	/// m_entryToId
	var m_entryToId_baseId = new Uint32Array(length);
	var m_entryToId_fileFileId = new Uint32Array(length);

	
	for(var i=0; i<length; i++){
		ids[i] = ds.readUint32();
		mftIndices[i] = ds.readUint32();
	}
	
	/// Raw map of "ID" to mft index
	this.mft.id2index = {
		ids:ids,
		mftIndices:mftIndices
	}

	/// m_entryToId has both base and filed id
	for(var i=0; i<length; i++){


		if (ids[i] == 0 && mftIndices[i] == 0) {
            continue;
        }
 		
 		var entryIndex = mftIndices[i];
        var entry     = {
        	fileId : m_entryToId_fileFileId[entryIndex],
        	baseId : m_entryToId_baseId[entryIndex]
        }

        if (entry.baseId == 0) {
            entry.baseId = ids[i];            
        } else if (entry.fileId == 0) {
            entry.fileId = ids[i];
        }

        if (entry.baseId > 0 && entry.fileId > 0) {
            if (entry.baseId > entry.fileId) {
            	//std::swap(entry.baseId, entry.fileId);
            	var temp = entry.baseId;
            	entry.baseId = entry.fileId;
            	entry.fileId = temp;                
            }
        }

        //Write back
        m_entryToId_fileFileId[entryIndex] = entry.fileId;
        m_entryToId_baseId[entryIndex] = entry.baseId;
	}

	this.mft.m_entryToId = {
		baseId:m_entryToId_baseId,
		fileId:m_entryToId_fileFileId
	}

	if(this.onFullyLoaded)
		this.onFullyLoaded();
};


/**
 * Reads the cached list of maps corresponding to the reader's .dat from the localStorage.
 * @return {Array} Grouped List of maps
 */
LocalReader.prototype.loadMapList = function(){
	var datFile = this.dat;
	var mapName = "mapList_" + this.version + "." + datFile.name + "_" + datFile.lastModified + "_" + datFile.size;
	var str = localStorage.getItem(mapName);
	if(!str)
		return null;
	try{
		return JSON.parse(str);	
	}
	catch(e){
		
	}
	return null;
	
}

/**
 * Stores a map list array into the localStorage.
 * @param  {[type]} datFile [description]
 * @param  {[type]} mapList [description]
 * @return {[type]}         [description]
 */
LocalReader.prototype.storeMapList = function(datFile, mapList){
	var mapName = "mapList_" + this.version + "." + datFile.name + "_" + datFile.lastModified + "_" + datFile.size;
	localStorage.setItem(mapName, JSON.stringify(mapList) );
}

/**
 * TODO
 * @param  {[type]}   searchAll [description]
 * @param  {Function} callback  [description]
 * @return {[type]}             [description]
 */
LocalReader.prototype.readMapListAsync = function(searchAll, callback){
	var self = this;
	var datFile = this.dat;
	var mftIndices = [];

	/// Number of threads for map lookup
	var N = 8; 

	/// Time spent looking up maps
	var t = new Date().getTime();

	/// Only look for known maps
	if(!searchAll){

		MapFileList.maps.forEach(function(mapCol){
			mapCol.maps.forEach(function(mapEntry){
				var entryBaseId = mapEntry.fileName.split(".")[0];
				var mftIndex = self.getFileIndex(entryBaseId)
				mftIndices.push(mftIndex);
			});
		});
	}

	/// Look for all maps
	else{
		mftIndices = this.mft.id2index.mftIndices;
	}

	/// Get a list of unique indices based on file addresses
	var uniqueIdxs = MathUtils.sort_unique(mftIndices, function(a, b) {
		return self.mft.entryDict.offset[a] - self.mft.entryDict.offset[b];
	});

	/// Callback for map lookup
	var cb = function(result){
		var dt = new Date().getTime() - t;
		console.log("Time elapsed ", Math.round(0.001*dt)," seconds");	

		/// Clear listeners used during indexing
		self.fileListeners = [];

		var localMapList = {maps:[]};

		/// Arrange each found map into a grouped list
		result.forEach(function(mftIndex){

			/// Base Id is used by gw2browser and is therefore the de facto identifier
			/// in the community. Let's use it here too!
			var baseId = self.mft.m_entryToId.baseId[mftIndex+1];

			/// Hack to avoid releasing maps newer than VB
			if(baseId > /*969663*/ 11542000 * 10000){
				return;
			}

			var name = "";
			var group = "";

			MapFileList.maps.forEach(function(mapCol){
				mapCol.maps.forEach(function(mapEntry){
					if(name.length == 0 && mapEntry.fileName.indexOf(baseId+".")>=0){
						name = mapEntry.name;
						group = mapCol.name;
					}
				});
			});

			if(name.length == 0){
				name = "Unknown map "+baseId;
			}

			if(group.length == 0){
				group = "Unknown maps";
			}

			var localGroup = null;
			localMapList.maps.forEach(function(mapCol){
				if(mapCol.name == group){
					localGroup = mapCol;
				}
			});

			if(!localGroup){
				localGroup = {name:group,maps:[]};
				localMapList.maps.push(localGroup);
			}

			localGroup.maps.push({fileName:baseId, name:name});
			
		});
		
		
		/// Store map list into local storage.
		self.storeMapList(datFile, localMapList);

		/// Fire callback and pass the map list
		callback(localMapList);

	}/// End of map lookup callback.


	/// Start map lookup:
	var maxLength = uniqueIdxs.length;
	console.log("Scanning ",maxLength, " files for maps...");	
	this.findType(uniqueIdxs, "mapc", 0, maxLength, N, cb);
}

/**
 * TODO
 * @param  {[type]}   uniqueIdxs [description]
 * @param  {[type]}   type       [description]
 * @param  {[type]}   start      [description]
 * @param  {[type]}   length     [description]
 * @param  {[type]}   N          [description]
 * @param  {Function} callback   [description]
 * @return {[type]}              [description]
 */
LocalReader.prototype.findType = function(uniqueIdxs, type, start, length, N ,callback){
	var self = this;
	var maxlen = start+length;
	var lastPct = 0;
	var pctM = 100.0 / Math.min(length, uniqueIdxs.length - start);
	var threadsDone = 0;

	var result = [];

	var peekCallback=function(inflatedData, i, mftIndex){
		if(!inflatedData){
			console.warn("No infalted data for entry");
			readUniqueId(i+N);
			return;
		}

		var ds = new DataStream(inflatedData);
		var file = new File(ds, 0);

		if(file.header.type == type){
			result.push(mftIndex);
		}

		readUniqueId(i+N);
	}

	
	var readUniqueId = function(i){

		if(i%N==0){
			var pct = Math.min(100, Math.floor( (i-start) * pctM) );
			if(lastPct != pct){
				self.output.find(".title").html("Finding maps (first visit only)");
				self.output.find(".progress").html(pct+"%");
				lastPct = pct;
			}
		}

		if( i>=uniqueIdxs.length || i>=maxlen ){
			console.log("Thread ",i%N+" done");
			threadsDone++;
			if(threadsDone == N){
				callback(result);
			}
			return;
		}

		var mftIndex = uniqueIdxs[i];

		var compressed = self.mft.entryDict.compressed[mftIndex];
		if(!compressed){
			//console.log("File is NOT compressed");
			readUniqueId(i+N);
			return;
		}

		var handle = mftIndex;
		var offset = self.mft.entryDict.offset[mftIndex];
		var size = self.mft.entryDict.size[mftIndex];

		if(size<0x20){
			//console.log("File is too small");
			readUniqueId(i+N);
			return;
		}

		/// Read .dat file part
		self.loadFilePart(self.dat, offset, size,
			function(ds, _size){
				
				///Infalte
				self.inflate(
					ds,
					_size,
					handle,
					///Infaltion  callback
					function(inflatedData){
						peekCallback(inflatedData, i, mftIndex);
					}, 
					false, 0x20
				);


		});

	}

	for(var n=0; n<N; n++){
		readUniqueId(start + n);	
	}
};

/**
 * TODO
 * @param  {[type]} baseOrFileId [description]
 * @return {[type]}              [description]
 */
LocalReader.prototype.getFileIndex = function(baseOrFileId){
	var index = -1;

	/// Get by base id
	
	for(var i = 0; i<this.mft.m_entryToId.baseId.length; i++){
		if( this.mft.m_entryToId.baseId[i+1] == baseOrFileId ){	
			index = i;
			//console.log("Found BASE ID "+baseId +" at INDEX "+i);
		}
	}

	/// Get by file id
	if(index==-1){
		for(var i = 0; i<this.mft.m_entryToId.fileId.length; i++){
			if( this.mft.m_entryToId.fileId[i+1] == baseOrFileId ){	
				index = i;
				//console.log("Found FILE ID "+baseId +" at INDEX "+i);
			}
		}	
	}

	return index;
}

/**
 * TODO
 * @param  {[type]}   baseId   [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
LocalReader.prototype.loadTextureFile = function(baseId, callback){
	this.loadFile(baseId, callback, true);
}

/**
 * TODO
 * @param  {[type]}   baseId   [description]
 * @param  {Function} callback [description]
 * @param  {Boolean}  isImage  [description]
 * @param  {[type]}   raw      [description]
 * @return {[type]}            [description]
 */
LocalReader.prototype.loadFile = function(baseId, callback, isImage, raw){
	
	var self = this;

	var index = this.getFileIndex(baseId);
	var mftIndex = index;

	//If the file doesn't exist still make sure to fire the callback
	if(mftIndex <= 0 ){
		console.log("Could not find file with baseId ",baseId);
		callback(null);
		return;
	}

	//console.log("fetchign  baseId ",baseId);
	
	var offset = MathUtils.arr32To64(
		[ this.mft.entryDict.offset_0[mftIndex],
		  this.mft.entryDict.offset_1[mftIndex] ]
	);

	var size = this.mft.entryDict.size[mftIndex];
	//console.log("File at found index is "+ size +" bytes");
	
	var compressed = this.mft.entryDict.compressed[mftIndex];
	if(!compressed){
		console.log("File is NOT compressed");
		
		callback(null);
		return;
	}

	/// Read map and pass the ds to our pNaCL inflate function
	//TODO: will this work?? Shared base id's and all that...
	var handle = index;

	this.loadFilePart(this.dat, offset, size, function(ds, size){
		
		/// If the raw param was passed return the un-inflated data
		if(raw){
			callback(ds);
		}

		/// If raw parameter was not passed or false, inflate first
		else{
			self.inflate(ds, size, handle, callback, isImage);	
		}
		
	});
};


/**
 * TODO
 * @param  {[type]}   ds        [description]
 * @param  {[type]}   size      [description]
 * @param  {[type]}   handle    [description]
 * @param  {Function} callback  [description]
 * @param  {Boolean}  isImage   [description]
 * @param  {[type]}   capLength [description]
 * @return {[type]}             [description]
 */
LocalReader.prototype.inflate = function(ds, size, handle, callback, isImage, capLength){
	
	
	var arrayBuffer = ds.buffer;	

	if(!capLength){
		//capLength = arrayBuffer.byteLength;
		capLength = 1;
	}
	//console.log("Uncompressing file size ",size, "handle", handle, "cap", capLength, "isImage", isImage);
	
	if(arrayBuffer.byteLength < 12){
		console.log("not inflating, length is too short ("+arrayBuffer.byteLength+")", handle);
		callback(null);
		return;
	}
	if(handle == 123350 || handle == 123409 || handle == 123408 ){
		callback(null);
		return;
	}

	/// Register listener for file load callback
	if(this.fileListeners[handle]){
		this.fileListeners[handle].push(callback);

		///No need to make another call, just wait for callback event to fire.
		return;
	}
	else{
		this.fileListeners[handle] = [callback];	
	}
	
    
    /// Call pNaCl component with the handle and arrayBuffer as an arguments

    //console.log("posting",[handle,arrayBuffer,isImage===true])
	this.NaClInflater[0].postMessage([handle,arrayBuffer,isImage===true, capLength]);
};

/**
 * Reads bytes from a big file. Uses offset and length (in bytes) in order to only load
 * parts of the file that are used.
 * @param  {[type]}   file     [description]
 * @param  {[type]}   offset   [description]
 * @param  {[type]}   length   [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
LocalReader.prototype.loadFilePart = function(file, offset, length, callback){
	var self = this;

	var reader = new FileReader();
		
	reader.onerror = function(fileEvent){
		debugger;
	}
	reader.onload  = function(fileEvent){

		var buffer = fileEvent.target.result;
		var ds = new DataStream(buffer);
	  	ds.endianness = DataStream.LITTLE_ENDIAN;

	  	/// Pass data stream and data length to callback function, keeping "this" scope
	  	callback.call(self, ds, length);
	}
	
  	var start = offset;
	var end = start + length;
	reader.readAsArrayBuffer(file.slice(start, end));
};

module.exports = LocalReader;