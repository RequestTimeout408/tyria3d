var HEAD_STRUCT = [
	'type', 'cstring:4',
	'chunkDataSize', 'uint32',
	'chunkVersion', 'uint16',
	'chunkHeaderSize', 'uint16',
	'offsetTableOffset', 'uint32',
];

/**
 * Basic chunk parsing functionality for Guild Wars 2 file chunks
 * @class GW2Chunk
 * @constructor
 * @param {DataStream} ds A DataStream containing deflated chunk binary data.
 * @param {Number} addr Offset of chunk start within the DataStream
 */
var Chunk = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.headerLength  = NaN;
	
	this.loadHead();
};

Chunk.prototype.loadHead=function(){
	this.ds.seek(this.addr);	
	this.header = this.ds.readStruct(HEAD_STRUCT);

	this.headerLength = this.ds.position - this.addr;
};

Chunk.prototype.loadData=function(dataStruct){
	this.ds.seek(this.addr + this.headerLength);
	this.data =  this.ds.readStruct(dataStruct);
};

Chunk.prototype.next = function(){
	try{
		// Calculate actual data size, as mChunkDataSize
		// does not count the size of some header variables
		return new Chunk(this.ds,this.addr + 8 + this.header.chunkDataSize);
	}
	catch(e){
		/// Out of bounds probably
		return null;
	}
};

module.exports = Chunk;