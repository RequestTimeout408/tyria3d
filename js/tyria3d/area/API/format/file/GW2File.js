var Chunk = require('./GW2Chunk');

var HEAD_STRUCT = [
	'identifier', 'cstring:2',
	'unknownField1', 'uint16',
	'unknownField2', 'uint16',
	'pkFileVersion', 'uint16',
	'type', 'cstring:4'
];


/**
 * Basic header and chunk parsing functionality for Guild Wars 2 files
 * @class GW2File
 * @constructor
 * @param {DataStream} ds A DataStream containing deflated file binary data.
 * @param {Number} addr Offset of file start within the DataStream
 */
var File = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.headerLength  = NaN;

	/**
	 * All {{#crossLink "GW2Chunk"}}chunks{{/crossLink}} contained in the file.
	 *
	 * @property chunks
	 * @type GW2Chunk[]
	 */
	this.chunks = [];
	
	this.readHead();
	this.readChunks();
};



/**
 * Parses the file header data, populating the header property.
 * @method readHead
 */
File.prototype.readHead = function(){
	this.ds.seek(this.addr);
	this.header = this.ds.readStruct(HEAD_STRUCT);
	this.headerLength = this.ds.position - this.addr;
};

/**
 * Parses the file headers and populates the chunks property.
 * @method readChunks
 */
File.prototype.readChunks = function(){

	/// Reset chunks
	this.chunks = [];

	var structs = this.getChunkStructs && this.getChunkStructs();

	/// Load basic Chunk in order to read the chunk header.
	var ch = new Chunk(this.ds, this.headerLength + this.addr);	

    while(structs && ch!=null && ch.header.type){
    	

    	/// Load the chunk data if a structure for this chunk type is defined.
		if( structs.hasOwnProperty(ch.header.type.toLowerCase()) ){

			var chunkStruct = structs[ch.header.type.toLowerCase()];
			if(chunkStruct){
				ch.loadData(chunkStruct);
			}
			

			/// Save chunk reference in this file.
	    	this.chunks.push(ch);
    	}

    	/// Load next basic Chunk in order to read the chunk header.
    	ch = ch.next();
    }
};

/**
 * Get a GW2Chunk from this file
 * @method getChunk
 * @param  {String} type The name, or type of the desired chunk.
 * @return {GW2Chunk} The first GW2Chunk in this file matching the type name, or null if no matching GW2Chunk was found.
 */
File.prototype.getChunk = function (type){
	for(var i=0; i<this.chunks.length; i++){
		if( this.chunks[i].header.type.toLowerCase() == type.toLowerCase() )
			return this.chunks[i]; 
	}
	return null;
};

/**
 * Provides a list of known header types and their parsing structure. Should be defined by each file type individually.
 * @method getChunkStructs
 * @return {Object} An object mapping chunk identifiers to DataStream structure descriptors.
 */
File.prototype.getChunkStructs = function(){
	return {}
};

module.exports = File;