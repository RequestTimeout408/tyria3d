var GW2File = require('./GW2File');

function MaterialFile(ds, addr){
	GW2File.call(this, ds, addr);
};
MaterialFile.prototype = Object.create(GW2File.prototype);
MaterialFile.prototype.constructor = MaterialFile;

MaterialFile.prototype.getChunkStructs = function(){
	return {
		"dx9s":require('../definition/amat/Dx9MaterialDefinition'),
		"grmt":require('../definition/amat/AmatGrDefinition'),
	};
};

module.exports = MaterialFile;