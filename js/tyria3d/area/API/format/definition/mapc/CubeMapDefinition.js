var Utils = require('../../../util/ParserUtils');

module.exports = [
	//helpers::Array<PackMapCubeMapSampleV3> sampleArray;
	"sampleArray",Utils.getArrayReader([
			//float3 position;
			"position", ["[]","float32",3],

		    //helpers::FileName filenameDayDefault;
		    "filenameDayDefault",Utils.getFileNameReader(),

		    //helpers::FileName filenameNightDefault;
		    "filenameNightDefault",Utils.getFileNameReader(),

		    //helpers::FileName filenameDayScript;
		    "filenameDayScript",Utils.getFileNameReader(),

		    //helpers::FileName filenameNightScript;
		    "filenameNightScript",Utils.getFileNameReader(),

		    //qword envID;
		    "envId_1","uint32",
		    "envId_2","uint32"
		]),
	
	//helpers::Array<PackMapCubeMapParamsV3> paramsArray;
	"paramsArray",Utils.getArrayReader([
			//dword modulateColor;
			"modulateColor","uint32",

		    //float brightness;
		    "brightness","float32",
		    
		    //float contrast;
		    "contrast","float32",
		    
		    //dword blurPasses;
		    "blurPasses","uint32",
		    
		    //helpers::WString envVolume;
		    "envVolume_p","uint32"
		])
	
]