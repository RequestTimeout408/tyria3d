var Utils = require('../../../util/ParserUtils');

module.exports = [
	//helpers::Array<AmatDx9SamplerV10> samplers;
	"samplers", Utils.getArrayReader([
			
			//dword textureIndex;
			"textureIndex", "uint32",
    		
    		//helpers::Array<dword> state;
    		"state", Utils.getArrayReader("uint32"),

		]),

    //helpers::Array<AmatDx9ShaderV10> shaders;
    "shaders", Utils.getArrayReader([
    		
    		//helpers::Array<dword> shader;
    		"shader", Utils.getArrayReader("uint32"),

		    //helpers::Array<dword> constRegisters;
		    "constRegisters", Utils.getArrayReader("uint32"),

		    //helpers::Array<dword> constTokens;
		    "constTokens", Utils.getArrayReader("uint32"),

		    //word instructionCount;
		    "instructionCount", "uint16"

     	]),


    //helpers::Array<AmatDx9TechniqueV10> techniques;	
    "techniques", Utils.getArrayReader([
    		//helpers::String name;
    		"name", Utils.getStringReader(),


		    //helpers::Array<AmatDx9PassV10> passes;
		    "passes", Utils.getArrayReader([
		    		// helpers::RefList<AmatDx9EffectV10> effects;
		    		"effects", Utils.getRefArrayReader([
		    				//qword token;
		    				"token", Utils.getQWordReader(),

						    //helpers::Array<dword> renderStates;
						    "renderStates", Utils.getArrayReader("uint32"),

						    //helpers::Array<dword> samplerIndex;
						    "samplerIndex", Utils.getArrayReader("uint32"),

						    //dword pixelShader;
						    "pixelShader", "uint32",

						    //dword vertexShader;
						    "vertexShader", "uint32",

						    //helpers::Array<dword> texGen;
						    "texGen", Utils.getArrayReader("uint32"),

						    //helpers::Array<dword> texTransform;
						    "texTransform", Utils.getArrayReader("uint32"),

						    //dword vsGenFlags;
						    "vsGenFlags", "uint32",
						    
						    //dword passFlags;
						    "passFlags", "uint32"
		    			])
		    	]),

		    //word maxPsVersion;
		    "maxPsVersion", "uint16",

		    //word maxVsVersion;
		    "maxVsVersion", "uint16"


     	]),
	
];