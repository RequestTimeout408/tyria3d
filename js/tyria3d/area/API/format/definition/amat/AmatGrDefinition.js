var Utils = require('../../../util/ParserUtils');

module.exports = [

	//byte texArrayRange;
	"texArrayRange", "uint8",

    //byte texCount;
    "texCount", "uint8",

    //byte texTransformRange;
    "texTransformRange", "uint8",

    //byte sortOrder;
    "sortOrder", "uint8",

    //byte sortTri;
    "sortTri", "uint8",

    //byte procAnim;
    "procAnim", "uint8",

    //dword debugFlags;
    "debugFlags", "uint32",

    //dword flags;
    "flags", "uint32",

    //dword texType;
    "texType", "uint32",

    //dword textureMasks[4];
    "textureMasks", ["[]", "uint32", 4],

    //helpers::Array<qword> texTokens;
    "texTokens", Utils.getArrayReader([
    	"val",Utils.getQWordReader()
    	])
	
];