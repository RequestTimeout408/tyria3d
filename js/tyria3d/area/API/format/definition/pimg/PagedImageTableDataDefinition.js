var Utils = require('../../../util/ParserUtils');

pageDataDef = Utils.getArrayReader([
	//dword layer;
	"layer", "uint32",
    //dword2 coord;
    "coord", ["[]","uint32",2],
    //helpers::FileName filename;
    "filename",Utils.getFileNameReader(),
    //dword flags;
    "flags","uint32",
    //byte4 solidColor;
    "solidColor", ["[]","uint8",4]
]);

module.exports = [
	//helpers::Array<PagedImageLayerDataV3> layers;
    "layers_n","uint32","layers_p","uint32",

    //helpers::Array<PagedImagePageDataV3> rawPages;
    "rawPages", pageDataDef,

    //helpers::Array<PagedImagePageDataV3> strippedPages;
    "strippedPages", pageDataDef,
    //dword flags;
    "flags","uint32"
]
