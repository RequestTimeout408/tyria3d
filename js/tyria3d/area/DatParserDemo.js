
///Set up UI
var UI = $("body");
var filePicker = $("<input type='file' />");
var file;
var mft;
filePicker.change(onReceiveFile);
UI.append(filePicker);


/// .dat file received from the file input, entry point!
function onReceiveFile(evt){
	
	/// Get loaded file reference from event
	///TODO: Check file length etc.
	var files = evt.target.files;

	//Global var file
	file = files[0];

	/// Read the first 40 bytes in order to parse the main header
	loadFilePart(file, 0, 40, readANDatHeader);		
};


function readANDatHeader(ds){

	/// Read file header data struct
	var fileHeader = ds.readStruct(_ANDatDef);

	console.log("Loaded Main .dat header", fileHeader);

	/// Get pointer to MFT chunk header
	fileHeader.mftOffset =  arr32To64(fileHeader.mftOffset);

	/// Load MFT
	loadFilePart(
		file,
		fileHeader.mftOffset,
		fileHeader.mftSize,
		readMFTHeader );

}

function readMFTHeader(ds){

	/// Read MFT header data struct
	/// Global variable mft
	mft = ds.readStruct(_MFTDef);

	var entryStartPtr = ds.position;
	var numEntires = mft.nbOfEntries;

	/// MFT has entries with offset, size and compression flag
	/// for all files in the .dat

	/// Read all entry offsets and sizes into uint32 arrays
	mft.entryDict = {
		offset_0:new Uint32Array(numEntires),
		offset_1:new Uint32Array(numEntires),
		size:new Uint32Array(numEntires),
		compressed:new Uint16Array(numEntires),
	}


	/// Read offset, size and compressed flag of the entries.
	console.log("reading MFT entries ");	
	for(var i=0; i<numEntires-1; i++){
		
		/// Read first 12 bytes
		mft.entryDict.offset_0[i] = ds.readUint32();
		mft.entryDict.offset_1[i] = ds.readUint32();
		mft.entryDict.size[i] = ds.readUint32();
		mft.entryDict.compressed[i] = ds.readUint16();

		/// Skip 10
		ds.seek(ds.position + 10);

	}
	console.log(".done reading MFT entries!");	
	console.log( "MFT", mft );


	/// Read data pointed to by 2nd mft entry
	/// This entry maps file ID to MFT index	
	var offset = arr32To64(
		[ mft.entryDict.offset_0[1],
		  mft.entryDict.offset_1[1] ]
	);
	var size = mft.entryDict.size[1];

	loadFilePart(file, offset, size, readMFTIndexFile);

}


function readMFTIndexFile(ds, size){
	
	console.log("mapping MFT index to file ID");

	var length = size / 8;
	var ids = new Uint32Array(length);
	var mftIndices = new Uint32Array(length);

	
	for(var i=0; i<length; i++){
		ids[i] = ds.readUint32();
		mftIndices[i] = ds.readUint32();
	}
	
	mft.id2index = {
		ids:ids,
		mftIndices:mftIndices
	}
	console.log("...done mapping MFT index to file ID!");


	//TESTING BELOW

	/// Read some file by ids

	//Khylo
	// Known uninflated size is 			798616
	// should be found in entrydict pos 	223252
	// entry dict pos is at index 			305622
	// entry dict ID at that index is 		433675 (we "want" it to be the same as the id below...)
	//var id = 197402; 
	var id = 433675; /// Use this to find Khylo for now...

	var index = -1;
	for(var i = 0; i<length; i++){
		

		if( mft.id2index.ids[i] == id ){

		//KHYLO
		//if( mft.id2index.mftIndices[i] == 223252 ){
		//if( mft.entryDict.size[i] == 798616 ){
			

			index = i;
			console.log("Found ID "+id +" at INDEX "+i);

		}
	}

	var mftIndex = mft.id2index.mftIndices[index];
	
	var offset = arr32To64(
			[ mft.entryDict.offset_0[mftIndex],
			  mft.entryDict.offset_1[mftIndex] ]
	);

	var size = mft.entryDict.size[mftIndex];
	console.log("File at found index is "+ size +" byte");
	
	var compressed = mft.entryDict.compressed[mftIndex];
	if(compressed)
		console.log("File is compressed");

	/// Read map and pass the ds to our pNaCL infalte function
	loadFilePart(file, offset, size, inflate);






	///Find some files!!
	//findSomeFiles(mft, numEntires, file);

}








function inflate(ds, size){
	console.log("Compressed file size ",size);
	
	var arrayBuffer = ds.buffer;
	
	/// HTML pNaCl emed elements
	NaClInflater = document.getElementById('hello_tutorial');
    var listener = document.getElementById('listener');

    /// Set up a listener for any messages passed by the pNaCl component
    listener.addEventListener(
    	'message',
    	function(message_event){

    		console.log("Got back a DS from NaCl RAW", message_event.data);

    		console.log("Got back a DS from NaCl Uint32Array", new Uint32Array(message_event.data));

    		var __ds  = new DataStream(message_event.data);
			console.log("Got back a DS from NaCl as DS", __ds);
			//var fourcc = __ds.readUint8Array(4);
			var fourcc = __ds.readString(4);
			console.log("fourCC",fourcc);

			var fourcc = __ds.readString(4);
			console.log("fourCC",fourcc);

			var fourcc = __ds.readString(4);
			console.log("fourCC",fourcc);

			/// TODO: use a callback function to pass back the extracted data!
    	},
    	true
	);
    
    /// Call pNaCl component with the arrayBuffer as an argument
	NaClInflater.postMessage(arrayBuffer);
}








	








function findSomeFiles(mft, numEntires, file){
	var allsizes = [];

	console.log("Parsing "+numEntires+" entries");
	for(var i=0; i<numEntires; i++){
		var offset = arr32To64(
			[ mft.entryDict.offset_0[i],
		  	  mft.entryDict.offset_1[i] ]
		);
		var size = mft.entryDict.size[i];
		var compressed = mft.entryDict.compressed[i];
		

		if(size<1024*1024*10)
			continue;

		if(offset == 0 || size == 0 /*|| compressed*/)
			continue;



		/// Load internal "file"

		loadFilePart(file, offset, size,function(_i,_o,_s){

			//For PF files of type mapc that are compressed we expect
			/// extracted size uint32
			///  ---data to inflate ---
			/// this data should start with "mapc" aka 0x6370616d

			return function(ds){

				console.log(ds.readString(4));
				ds.seek(0);

				if(ds.readUint32() ==  2147549192 ){

					///Get id from file index
					var index = -1;
					for(var i = 0; i< mft.id2index.mftIndices.length; i++){
						if( mft.id2index.mftIndices[i] == _i ){

							index = i;
							//console.log("Found MFT INDEX "+_i +" at INDEX "+i);

						}
					}
					var fileId = mft.id2index.ids[index];


					var extractedSize = ds.readUint32();

					//allsizes.push(extractedSize/ (1024*1024));
					//ds.readUint32();
					//if(ds.readUint32() == 33627392){
						console.log("map? ",
							"Extracted Size: "+Math.ceil(extractedSize/ (1024))+"kB",
							"Original Size: " +Math.ceil(_s / (1024))+"kB",
							"File Id: "+fileId
							//ds.readString(100)
						);
					//}

					//console.log(allsizes);
				}
				//console.log(_i +"->"+_o+":"+ds.readString(4)+" size:"+Math.ceil(_s/(1024*1024)) );
			}// End internal "file" loadFilePart callback
		}(i, offset, size)// Wrapper to pass index, offset and size
		)

	}// End looking for maps in all entries
}



















/* UTILS */

function loadFilePart(file, offset, length, callback){

	var reader = new FileReader();
		
	reader.onerror  = function(fileEvent){
		debugger;
	}

	reader.onload  = function(fileEvent){

		var buffer = fileEvent.target.result;
		var ds = new DataStream(buffer);
	  	ds.endianness = DataStream.LITTLE_ENDIAN;

	  	/// Callback with datastream
	  	callback(ds, length);
	}
	
  	var start = offset;
	var end = start + length;
	reader.readAsArrayBuffer(file.slice(start, end));
}

var base32Max = Math.pow(2,32);
function arr32To64(arr){
	/// Re-read as uint64 (still little endian)
	/// Warn: this will not work for ~50+ bit longs cus all JS numbers are 64 bit floats...
	return base32Max*arr[1] + arr[0];
}
























/* Structure definitions*/

// AN file header
var _ANDatDef = [
	//uint8_t  version;
	"version","uint8",						// 1

    //uint8_t  magic[3];
    "magic", "string:3",					// 4

    //uint32_t headerSize;
    "headerSize","uint32",					// 8

    //uint32_t unknown1;
    "unknown1","uint32",					// 12

    //uint32_t chunkSize;
    "chunkSize","uint32",					// 16
    
    //uint32_t crc;
    "crc","uint32",							// 20
    
    //uint32_t unknown2;
    "unknown2","uint32",					// 24
    
    //uint64_t mftOffset;
    "mftOffset",[ "[]","uint32", 2 ],
    
    //uint32_t mftSize;
    "mftSize","uint32",
    
    //uint32_t flags;
    "flags","uint32",
];

var _MFTDef = [
	//uint8_t  magic[4];
	"magic","string:4",
		
	//uint64_t unknown1;
	"unknown1",[ "[]","uint32", 2 ],

	//uint32_t nbOfEntries;
	"nbOfEntries","uint32",

	//uint32_t unknown2;
	"unknown2","uint32",
	
	//uint32_t unknown3;
	"unknown3","uint32",
];