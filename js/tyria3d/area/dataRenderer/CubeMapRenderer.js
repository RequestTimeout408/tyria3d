var Utils = require("../util/RenderUtils");
var SceneUtils = require("../util/SceneUtils");

function loadTextureCubeIds( array, localReader, mapping ) {

	var images = [];

	var texture = new THREE.CubeTexture( images, mapping );

	// no flipping needed for cube textures
	texture.flipY = false;

	var loaded = 0;

	var loadTexture = function ( i ) {

		var fileId = array[i];

		localReader.loadTextureFile(
			fileId,
			function(inflatedData, dxtType, imageWidth, imageHeigth){

				if(!inflatedData)
					return;


				var image = {
					data: new  Uint8Array(inflatedData),
					width:imageWidth,
					height:imageHeigth
				}

				texture.format = THREE.RGBAFormat;
				texture.images[ i ] = image;

				loaded += 1;
				if ( loaded === 6 ) {

					texture.needsUpdate = true;

					if ( onLoad ) onLoad( texture );

				}
				

			}
		);	

	}

	for ( var i = 0, il = array.length; i < il; ++ i ) {
		loadTexture( i );
	}

	return texture;

}

module.exports = {
	render: function(cubeMapChunkHeader, map, localReader){
		/// Get all 9 textures
		var samples = cubeMapChunkHeader.data.sampleArray;

		var fileIds = [];

		samples.forEach(function(sample){
			var filename = sample.filenameDayDefault;//filenameNightDefault
			var pos = sample.position; //[0,1,2]

			fileIds.push(filename);
		});


		

		var reflectionCube = loadTextureCubeIds( fileIds, localReader );
		//reflectionCube.format = THREE.RGBFormat;

		// Skybox
		var shader = THREE.ShaderLib[ "cube" ];
		shader.uniforms[ "tCube" ].value = reflectionCube;

		var material = new THREE.ShaderMaterial( {

			fragmentShader: shader.fragmentShader,
			vertexShader: shader.vertexShader,
			uniforms: shader.uniforms,
			depthWrite: false,
			side: THREE.BackSide
		} ),

		mesh = new THREE.Mesh( new THREE.BoxGeometry( 1000, 1000, 1000 ), material );
		SceneUtils.getScene().add( mesh );

	}
};