var Utils = require("../util/RenderUtils");
var SceneUtils = require("../util/SceneUtils");

module.exports = {
	/**
	 * Renders all property meshes in a GW2 map described by the map's PROP chunk.
	 * @param  {Object} propertiesChunkHeader - The PROP chunk data of the map.
	 * @param  {Object} map - The Tyria3D map object... TODO-
	 * @param  {LocalReader} localReader - The LocalReader used to read resources from the GW2 .dat file.
	 * @return {[type]}
	 */
	
	
	render: function(propertiesChunkHeader, map, localReader, renderCallback){
		var self = this;
		SceneUtils.showProgressPanel(function(){
			self.doRender(propertiesChunkHeader, map, localReader, renderCallback);
		});
	},

	doRender: function(propertiesChunkHeader, map, localReader, renderCallback){

		console.log("RENDERING PROPS: ",propertiesChunkHeader);	

		if(!propertiesChunkHeader){
			renderCallback();
			return;
		}

		var props = propertiesChunkHeader.data.propArray;
		var animProps = propertiesChunkHeader.data.propAnimArray;
		var instanceProps = propertiesChunkHeader.data.propInstanceArray;
		var metaProps = propertiesChunkHeader.data.propMetaArray;

		props = props
			.concat(animProps)
			.concat(instanceProps)
			.concat(metaProps)
			;

		// For now, we'll do all load in serial
		// TODO: load unique meshes and textures in parallell (asynch), then render!
		var lastPct = -1;	
		var output = $("#progressPanel");
		

		var renderIndex = function(idx){
			if(idx>= props.length){
				//console.log("Done adding prop models");
				renderCallback();
				return;
			}

			/// Print progress in the console for now.
			var pct = Math.round(1000.0*idx / props.length);
			pct/=10.0;

			
			
			if(lastPct!=pct){
			
				
				output.find(".title").html("Loading 3D Models (Props)");
				output.find(".progress").html(
					pct +
					( pct.toString().indexOf(".")<0 ? ".0":"" ) +
					"%"
				);

				lastPct = pct;
			}

			/// Read prop at index.
			var prop = props[idx];								

		    /// Adds a single mesh to a group.
			var addMeshToLOD = function(mesh, groups, lod, prop, needsClone){

				/// Read lod distance before overwriting mesh variable
			    var lodDist = prop.lod2 != 0 ? prop.lod2 : mesh.lodOverride[1];

			    /// Read flags before overwriting mesh variable
		    	var flags = mesh.flags;

		    	/// Mesh flags are 0 1 4
		    	/// For now, use flag 0 as the default level of detail
		    	if(flags==0)
		    		lodDist=0;
		    	
		    	/// Create new empty mesh if needed
		    	if(needsClone){
		    		mesh = new THREE.Mesh( mesh.geometry, mesh.material );
		    	}

		    	mesh.updateMatrix();
				mesh.matrixAutoUpdate = false;

		    	// Find group for this LOD distance
		    	if(groups[lodDist]){
		    		groups[lodDist].add(mesh);
		    	}
		    	// Or create LOD group and add to a level of detail
		    	// WIP, needs some testing!
		    	else{
		    		var group = new THREE.Group();
		    		group.updateMatrix();
					group.matrixAutoUpdate = false;
		    		group.add(mesh);
		    		groups[lodDist] = group;
		    		lod.addLevel(group,lodDist);
		    	}

		    	return lodDist;
		    }

		    /// Adds array of meshes to the scene, also adds transform clones
			var addMeshesToScene = function(meshArray, needsClone, boundingSphere){
				
			    ///Add original 

			    /// Make LOD object and an array of groups for each LOD level
			    var groups = {};
			    var lod = new THREE.LOD();

			    /// Each mesh is added to a group corresponding to its LOD distane
			    var maxDist = 0;
			    meshArray.forEach(function(mesh){
			    	maxDist = Math.max( maxDist, addMeshToLOD(mesh,groups,lod,prop,needsClone) );
		    	});

		    	/// Add invisible level
		    	//lod.addLevel(new THREE.Group(),10000);

			    /// Set position, scale and rotation of the LOD object
				if(prop.rotation){
			    	lod.rotation.order = "ZXY";
			    	lod.rotation.set(prop.rotation.x, -prop.rotation.y, -prop.rotation.z);
			    }
			    lod.scale.set( prop.scale, prop.scale, prop.scale );
			    lod.position.set(prop.position.x, -prop.position.y, -prop.position.z);
			   	

			   	lod.boundingSphereRadius = boundingSphere.radius * prop.scale;

			    lod.updateMatrix();
			    lod.matrixAutoUpdate = false;

			    /// Show highest level always
			    //lod.update(lod);

		    	//Add LOD containing mesh instances to scene
		    	SceneUtils.getScene().add(lod);
				SceneUtils.getNonColisions().push(lod);
			    
			    // Add one copy per transform, needs to be within it's own LOD
			    if(prop.transforms){

			    	prop.transforms.forEach(function(transform){
			    		
			    		/// Make LOD object and an array of groups for each LOD level
			    		var groups = {};
			    		var lod = new THREE.LOD();

			    		/// Each mesh is added to a group corresponding to its LOD distane
			    		var maxDist = 0;
				    	meshArray.forEach(function(mesh){
				    		maxDist = Math.max( maxDist, addMeshToLOD(mesh,groups,lod,prop,true) );
				    	});

				    	/// Add invisible level
		    			//lod.addLevel(new THREE.Group(),10000);

				    	/// Set position, scale and rotation of the LOD object
						if(transform.rotation){
					    	lod.rotation.order = "ZXY";
					    	lod.rotation.set(transform.rotation.x, -transform.rotation.y, -transform.rotation.z);
					    }
					    lod.scale.set( transform.scale, transform.scale, transform.scale );
					    lod.position.set(transform.position.x, -transform.position.y, -transform.position.z);

						lod.updateMatrix();
			    		lod.matrixAutoUpdate = false;

			    		lod.boundingSphereRadius = boundingSphere.radius * prop.scale;

						/// Show highest level always
			    		//lod.update(lod);

				    	/// Add LOD containing mesh instances to scenerender: function(propertiesChunkHeader, map, localReader, renderCallback){
				    	SceneUtils.getScene().add(lod);
						SceneUtils.getNonColisions().push(lod);

				    });
			    }
			}

			/// Get meshes
			Utils.getMeshesForFilename(prop.filename, prop.color, localReader,
				function(meshes, isCached, boundingSphere){
				
					if(meshes){
						addMeshesToScene(meshes, isCached, boundingSphere);
					}

					/// Render next prop
					renderIndex(idx+1);
				}
			);

			

		};

		/// Start serial loading and redering. (to allow re-using meshes and textures)
		/// textures prolly won't work?
		renderIndex(0);
		
	},


	getFileIds: function(propertiesChunkHeader, localReader, callback){
		var fileIds = [];


		var props = propertiesChunkHeader.data.propArray;
		var animProps = propertiesChunkHeader.data.propAnimArray;
		var instanceProps = propertiesChunkHeader.data.propInstanceArray;
		var metaProps = propertiesChunkHeader.data.propMetaArray;

		props = props
			.concat(animProps)
			.concat(instanceProps)
			.concat(metaProps);

		var getIdsForProp = function(idx){

			if(idx>=props.length){
				callback(fileIds);
				return;
			}

			if(idx%100==0){
				console.log("getting ids for entry",idx,"of",props.length);
			}

			var prop = props[idx];
			Utils.getFilesUsedByModel(
				prop.filename,
				localReader,
				function(propFileIds){
					fileIds = fileIds.concat(propFileIds);
					getIdsForProp(idx+1);
				}
			);

		};

		getIdsForProp(0);
		
	}


};