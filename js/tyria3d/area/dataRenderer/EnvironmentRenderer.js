var Utils = require("../util/RenderUtils");
var SceneUtils = require("../util/SceneUtils");


function setHaze(environmentChunk){
	var hazes = environmentChunk && environmentChunk.data.dataGlobal.haze;
	var hazeColor;

	if(!hazes || hazes.length<=0){
		hazeColor = [190, 160, 60];
	}
	else{
		var haze0 = hazes[0];	
		hazeColor = haze0.farColor;
	}

	

	
	var color = new THREE.Color(hazeColor[2]/255.0, hazeColor[1]/255.0, hazeColor[0]/255.0);

	SceneUtils.getRenderer().setClearColor( color, 1.0 );

	SceneUtils.getScene().fog.color.copy(color);

	return hazeColor[2]*256*256+hazeColor[1]*256+hazeColor[0];
}

function setLights(environmentChunk){

	var lights = environmentChunk ? environmentChunk.data.dataGlobal.lighting : [{
		lights:[],
		backlightIntensity:1.0,
		backlightColor:[255,255,255]
	}];

	var ambientLight;

	//var light = lights[0];
	var hasLight = false;
	lights.forEach(function(light, idx){

		if(hasLight)
			return;

		/// Directional lights
		var sumDirLightIntensity = 0;

		light.lights.forEach(function(dirLightData,idx){

			hasLight = true;
			
			var color = new THREE.Color(
				dirLightData.color[2]/255.0,
				dirLightData.color[1]/255.0,
				dirLightData.color[0]/255.0
			);

			var directionalLight = new THREE.DirectionalLight( color.getHex(), dirLightData.intensity );
			
			directionalLight.position.set(
				-dirLightData.direction[0],
				dirLightData.direction[2],
				dirLightData.direction[1]
			).normalize();
			
			sumDirLightIntensity += dirLightData.intensity;

			SceneUtils.getScene().add(directionalLight);
			SceneUtils.getLights().push(directionalLight);
		});// END for each directional light in light


		/// Ambient light
		//light.backlightIntensity /= sumDirLightIntensity +light.backlightIntensity; 
		light.backlightIntensity =  light.backlightIntensity; 
		var color = new THREE.Color(
			/*light.backlightIntensity * light.backlightColor[2]/255.0,
			light.backlightIntensity * light.backlightColor[1]/255.0,
			light.backlightIntensity * light.backlightColor[0]/255.0*/
			light.backlightIntensity * (255.0-light.backlightColor[2])/255.0,
			light.backlightIntensity * (255.0-light.backlightColor[1])/255.0,
			light.backlightIntensity * (255.0-light.backlightColor[0])/255.0
		);

		 ambientLight = new THREE.AmbientLight(color);

		

	})// END for each light in lighting

	var ambientTotal = 0;
	if(ambientLight){
		ambientTotal = ambientLight.color.r + ambientLight.color.g + ambientLight.color.b;
		SceneUtils.getScene().add(ambientLight);
		SceneUtils.getLights().push(ambientLight);	
	}

	return hasLight || ambientTotal>0;
	
}

module.exports = {
	render: function(environmentChunk, parameterChunk, map, localReader){

		/// Set renderer clear color from env. haze
		var hazeColor = setHaze(environmentChunk);

		/// Add directional lights
		this.hasLight = setLights(environmentChunk);


		// 0 and 1 day
		// 2 and 3 evening
		var skyModeTex = environmentChunk && environmentChunk.data.dataGlobal.skyModeTex[0];

		if(!skyModeTex){
			skyModeTex = {
				texPathNE:1930687,
				texPathSW:193069,
				texPathT:193071
			}
		}

		var bounds = parameterChunk.data.rect;
		var mapW = Math.abs( bounds.x1 -bounds.x2 );
		var mapD = Math.abs( bounds.y1 -bounds.y2 );
		var boundSide = Math.max( mapW, mapD );

		var materialArray = [];

		function getMat(tex){
			return new THREE.MeshBasicMaterial({
				map: tex,
				side: THREE.BackSide,
				fog: false,
				depthWrite: false
			});
		}


		function loadTextureWithFallback(targetMatIndices, filename, fallbackFilename, hazeColor){
		
			function writeMat(mat){
				targetMatIndices.forEach(function(i){
					materialArray[i] = mat; 
				});
			}

			function loadFallback(){
				var mat = getMat(
					THREE.ImageUtils.loadTexture(fallbackFilename)
					/*Utils.loadLocalTexture(
						localReader,
						fallbackFilename,
						null, hazeColor)*/
				);

				writeMat(mat);
			}

			function errorCallback(){
				setTimeout(loadFallback, 1);
			}

			var mat = getMat(
				Utils.loadLocalTexture(
					localReader,
					filename,
					null, hazeColor,
					errorCallback )
			);

			writeMat(mat);			
		}

		/*loadTextureWithFallback([1,4],skyModeTex.texPathNE + 1, 193067 + 1, hazeColor);
		loadTextureWithFallback([0,5],skyModeTex.texPathSW + 1, 193069 + 1, hazeColor);
		loadTextureWithFallback([2],skyModeTex.texPathT + 1, 193071 + 1, hazeColor);*/
		loadTextureWithFallback([1,4],skyModeTex.texPathNE + 1, "img/193068.png", hazeColor);
		loadTextureWithFallback([0,5],skyModeTex.texPathSW + 1, "img/193070.png", hazeColor);
		loadTextureWithFallback([2],skyModeTex.texPathT + 1, "img/193072.png", hazeColor);
		materialArray[3] = new THREE.MeshBasicMaterial({visible:false});

		var boxSize = 1024;
		
		var skyGeometry = new THREE.BoxGeometry( boxSize, boxSize/2 , boxSize ); //Width Height Depth
		skyGeometry.faceVertexUvs[0].forEach(function(vecs, idx){

			var face = Math.floor(idx/2);

			// PX NX
			// PY NY
			// PZ NZ

			/// PX - WEST 	NX - EAST
			if(face == 0 || face == 1){
				vecs.forEach(function(vec2){
					vec2.x = 1 - vec2.x;	
					vec2.y /= 2.0;	
					vec2.y += .5;	
				});
			}

			/// NZ - SOUTH 	PZ - NORTH
			else if(face == 5 || face == 4){
				vecs.forEach(function(vec2){
					vec2.y /= -2.0;	
					vec2.y += .5;	
				});
			}

			else{
				vecs.forEach(function(vec2){
					vec2.x = 1 - vec2.x;	
					//vec2.y = 1 - vec2.y;	
				});
			}

		});

		skyGeometry.uvsNeedUpdate = true;

		/// Z pos is NORTH!
		/// X POS is WEST?
		//var directions  = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
		/*materialArray = [
			getMat(texSW), // West
			getMat(texNE), // East
			getMat(texT), // Up
			//getMat(texT), // Down
			new THREE.MeshBasicMaterial({visible:false}),
			getMat(texNE), // North
			getMat(texSW)  // South
		];*/
		
		var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
		var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );

		skyBox.translateY(boxSize/4);
		//skyBox.translateY( -environmentChunk.data.dataGlobal.sky.verticalOffset );

		SceneUtils.getSkyScene().add( skyBox );
		SceneUtils.getSkyObjects().push(skyBox);    	
	}
}