/// static ref. to bounds
var maxBounds;
var mapRect;

/// havok
var HavokRenderer = require("./HavokRenderer.js");

/// zone
var ZoneRenderer =  require("./ZoneRenderer.js"); 

/// terrain
var TerrainRenderer = require("./TerrainRenderer.js"); 

/// Paged image file
var PagedImageFile = require("../map/PagedImageFile.js"); 

/// parameter
var ParameterRenderer = require("./ParameterRenderer.js"); 

/// props
var PropertiesRenderer = require("./PropertiesRenderer.js"); 

/// cube map
var CubeMapRenderer = require("./CubeMapRenderer.js"); 

///Env
var EnvironmentRenderer = require("./EnvironmentRenderer.js"); 




function renderHavok(map, localReader, cb){

	var havokChunk = map.file.getChunk("havk");

    /// Set static bounds to the bounds of the havk models
    maxBounds = havokChunk.data.boundsMax;

    var visible = $("#showHavok").prop("checked");

    HavokRenderer.render( havokChunk, visible, cb );
}

function renderZone(map, localReader, cb){

	var zoneChunk = map.file.getChunk("zon2");
	var parameterChunk = map.file.getChunk("parm");
	var terrainChunk = map.file.getChunk("trn");

	ZoneRenderer.render(zoneChunk, parameterChunk, terrainChunk, localReader, cb);
}

function renderTerrain(map, localReader, cb){
	
	var parameterChunk = map.file.getChunk("parm");
	var terrainChunk = map.file.getChunk("trn");


	/// Load all paged Images, requires inflation of other pack files!
	var pagedImageId = terrainChunk.data.materials.pagedImage;
	localReader.loadFile(pagedImageId,function(infaltedBuffer){
		
		var pimgDS = new DataStream(infaltedBuffer);
		var pimgFile = new PagedImageFile(pimgDS,0);
		var pimgTableDataChunk = pimgFile.getChunk("pgtb");

		TerrainRenderer.render(terrainChunk, parameterChunk, pimgTableDataChunk, localReader,
			function(){
				
				/// Set static mapRect to the bounds of the terrain chunks
    			mapRect = TerrainRenderer.mapRect;

    			/// Fire provided callback
    			cb();
			}
		);


	});
	
}

function renderParameters(map, localReader, cb){
	
	var parameterChunk = map.file.getChunk("parm");

	ParameterRenderer.render(parameterChunk);
	cb();
}


function renderEnvironment(map, localReader, cb){

	var environmentChunk = map.file.getChunk("env");
	var parameterChunk = map.file.getChunk("parm");
	
	//console.log("ENV",environmentChunk.data);

	EnvironmentRenderer.render(environmentChunk, parameterChunk, map, localReader);

    cb();
}

function renderShores(map, localReader, cb){
	var shoreChunk = map.file.getChunk("shor");
	console.log("Shore Chunk",shoreChunk);
	cb();
}

function renderProperties(map, localReader, cb){
	var propertiesChunk = map.file.getChunk("prp2");

	PropertiesRenderer.render(propertiesChunk, map, localReader, cb);

}

function renderCubeMap(map, localReader, cb){
	var cubeMapChunk = map.file.getChunk("cube");

	CubeMapRenderer.render(cubeMapChunk, map, localReader);

	cb();
}


module.exports = {
	render: function(map, localReader, callback){
   
   		/// Enables collision
	    var showHavok = true;

	    /// Shows in game terrain (ground)
	    var showTerrain = true;
	    
	    /// Shows in game models
	    var showZone = $('#loadZone').prop("checked");
	    var showProps = $('#loadProp').prop("checked");

	    /// Shows water surface
	    var showParameters = false;

	    /// Snows skybox
	    var showEnvironment = true;

	    /// Not implemented
	    var showShores = false;	    
	    var showCubeMap = false;

	    var runAllRenderers = function(i){
			if(i < rendererFuncs.length ){
				rendererFuncs[i](map, localReader, function(){
					runAllRenderers(i+1);
				});
			}
			else{
				callback(maxBounds, mapRect);
			}
		}

		var rendererFuncs = [];

		/// Environment
		if(showEnvironment){
			rendererFuncs.push( renderEnvironment );
		}
	    
	    /// Havok
	    if(showHavok){
	    	rendererFuncs.push( renderHavok );
		}

		/// Terrain (depends on Environment)
		if(showTerrain){
			rendererFuncs.push( renderTerrain );
		}

		/// Zone (depends on Terrain)
		if(showZone){
			rendererFuncs.push( renderZone );
		}

		

		/// Parameters
		if(showParameters){			
			rendererFuncs.push( renderParameters );
		}

		/// Shores
		if(showShores){			
			rendererFuncs.push( renderShores );
		}

		/// Props
		if(showProps){			
			rendererFuncs.push( renderProperties );
		}


		/// Cube map
		if(showCubeMap){			
			rendererFuncs.push( renderCubeMap );
		}

		runAllRenderers(0);

	}


};