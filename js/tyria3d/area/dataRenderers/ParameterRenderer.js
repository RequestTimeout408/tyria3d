var Utils = require("./Utils.js");



module.exports = {
	render: function(parameterChunkHeader){
		var material = material || new THREE.MeshBasicMaterial(
			{
				color: 0x5bb1e8,
				wireframe:false,
			 	opacity: 0.35
			}
		);
		material.transparent = true;
		Utils.renderRect(parameterChunkHeader.data.rect,0,material);
	}
};