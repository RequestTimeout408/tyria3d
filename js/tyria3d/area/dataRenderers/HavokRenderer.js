var SceneUtils = require("../SceneUtils");

var meshes = [];
var output;

function renderModels(title, models, geometries, animations, havokChunkHeader, visible, callback){
	
	output = $("#progressPanel");

	var mat;
	if(!visible){
		mat = new THREE.MeshBasicMaterial( { visible: false } );
	}
	else{
		//meshes[index]= new THREE.Mesh( geom, new THREE.MeshPhongMaterial({side: THREE.DoubleSide, color:0x777777}) ); 
		mat = new THREE.MeshNormalMaterial({side: THREE.DoubleSide}); 
		/*var bright = seedRandom()*0.08+0.44;
		mat = new THREE.MeshPhongMaterial({
			side: THREE.DoubleSide,
			//color:0x707070,
			//color:clr,
			color: new THREE.Color(bright,bright,bright),
			shading: THREE.FlatShading
		});*/
	}

	parseAllModels(mat, title, models, geometries, animations, havokChunkHeader, callback, 200, 0);
}

function getCollisionsForAnimation(animation, collisions){
	var ret = [];
	
	for (var i = 0; i < animation.collisionIndices.length; i++) {
		var index = animation.collisionIndices[i];
		var collision = collisions[ index ];
		collision.index = index;
		ret.push( collision );
	}
	
	return ret;
}; 

var lastP = -1;
function parseAllModels(mat, title, models, geometries, animations, havokChunkHeader, callback, chunkSize, offset){
	var i = offset;
	

	for(; i < offset+chunkSize && i < models.length; i++){
		
		var p = Math.round(i*100/ models.length );
		if( p != lastP){
			output.find(".title").html("Loading Collision Models ("+title+")");
			output.find(".progress").html(p+"%");
			lastP = p;
		}	
	
		var animation =  animationFromGeomIndex( models[i].geometryIndex, geometries, animations );
		
		var collisions = getCollisionsForAnimation( animation, havokChunkHeader.data.collisions);
		
		for(var j=0; j< collisions.length; j++){
			var collision = collisions[j];			
	 		renderMesh( collision, models[i], mat );
		}
	}
	if(i<models.length){
		window.setTimeout(parseAllModels, 10/*time in ms to next call*/,
			mat, title, models, geometries, animations, havokChunkHeader, callback,
			chunkSize, offset+chunkSize);
	}
	else{
		callback();
	}
}

function animationFromGeomIndex(propGeomIndex, geometries, animations){
	// geometries is just list of all geometries.animations[end] for now
	var l = geometries[propGeomIndex].animations.length;
	return animations[ geometries[propGeomIndex].animations[l-1] ];
	//return animations[ geometries[propGeomIndex].animations[0] ];
};

function renderMesh( collision, model, mat ){
    
    var pos = model.translate;
    var rot = model.rotation;
    var scale = 32 * model.scale;    
    
    /// Generate mesh
    var mesh = parseHavokMesh(collision, mat);
    
    /// Position mesh
    mesh.position.set(pos.x, -pos.y, -pos.z);    
    
    /// Scale mesh
    if(scale)
    	mesh.scale.set( scale, scale, scale );

    /// Rotate mesh
    if(rot){
    	mesh.rotation.order = "ZXY";
    	mesh.rotation.set(rot.x, -rot.y, -rot.z);
    }
    	
	/// Add mesh to scene and colisions
	SceneUtils.getScene().add(mesh);
	SceneUtils.getColisions().push(mesh);
}

var seed = 1;
function seedRandom(){
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}

function parseHavokMesh(collision, mat){
	var index = collision.index;

	if(!meshes[index]){

		var geom = new THREE.Geometry();
		
		/// Pass vertices	    		
		for(var i=0; i<collision.vertices.length; i++){
			var v=collision.vertices[i];
			geom.vertices.push( new THREE.Vector3(v.x , v.y , -v.z ) );
		}	    		
			
		/// Pass faces
		for(var i=0; i<collision.indices.length; i+=3){

			var f1=collision.indices[i];
			var f2=collision.indices[i+1];
			var f3=collision.indices[i+2];

			if( f1<=collision.vertices.length &&
				f2<=collision.vertices.length &&
				f3<=collision.vertices.length)
   				geom.faces.push( new THREE.Face3( f1, f2, f3 ) );
   			else
   				console.warn("Errorus index!");
		}

		/// Prepare geometry and pass new mesh
		geom.computeFaceNormals();
		//geom.computeVertexNormals();
		
		
		meshes[index]= new THREE.Mesh( geom, mat ); 

		
		return meshes[index];
	}
	else{
		return meshes[index].clone();
	}
}

module.exports = {

	render: function(havokChunkHeader, visible, callback){
		var self = this;
		SceneUtils.showProgressPanel(function(){
			self.doRender(havokChunkHeader, visible, callback);
		});
	},

	doRender: function(havokChunkHeader, visible, callback){

		/// Clear old meshes
		meshes = [];

		/// Render content
		var propModels = havokChunkHeader.data.propModels;
		var zoneModels = havokChunkHeader.data.zoneModels;
		var obsModels = havokChunkHeader.data.obsModels;
		obsModels.forEach(function(mdl){
			mdl.scale = 1;
		});

		var geometries = havokChunkHeader.data.geometries;

		var animations = havokChunkHeader.data.animations;
			
		/// Build models from havok model data
		renderModels("prop", propModels, geometries, animations, havokChunkHeader, visible, function(){

			renderModels("zone", zoneModels, geometries, animations, havokChunkHeader, visible, function(){

				renderModels("obs", obsModels, geometries, animations, havokChunkHeader, visible, callback);	

			});	
			
			
		});
		
		
	}
};