var SceneUtils = require("../SceneUtils.js");
var RenderUtils = require("./Utils.js");

module.exports = {
	render: function(terrainChunk, parameterChunk, pimgTableDataChunk, localReader, callback){
		var self = this;
		SceneUtils.showProgressPanel(function(){
			self.doRender(terrainChunk, parameterChunk, pimgTableDataChunk, localReader, callback);
		});
	},
	
	doRender: function(terrainChunk, parameterChunk, pimgTableDataChunk, localReader, callback){
		var self = this;
		this.mapRect = null;

		var output = $("#progressPanel");

		var terrainData = terrainChunk.data;
		var parameterData = parameterChunk.data;
		var pimgData = pimgTableDataChunk && pimgTableDataChunk.data;

		var maxAnisotropy = SceneUtils.getRenderer().getMaxAnisotropy();

		var chunks = [];	
		var chunkW = 35;

		terrainData.numChunksD_1 = Math.sqrt(
			terrainData.dims.dim1 *
			terrainData.chunks.length /
			terrainData.dims.dim2
		);
		terrainData.numChunksD_2 =
			terrainData.chunks.length / terrainData.numChunksD_1;

		var xChunks = terrainData.numChunksD_1;
		var yChunks = terrainData.numChunksD_2;


		var allMaterials = terrainData.materials.materials;
		var allTextures = terrainData.materials.texFileArray;



		//Total map dx and dy
		var dx = parameterData.rect.x2 - parameterData.rect.x1;
		var dy = parameterData.rect.y2 - parameterData.rect.y1;

		//Each chunk dx and dy
		var cdx = dx/terrainData.numChunksD_1 * 1;//  35/33;
		var cdy =dy/terrainData.numChunksD_2 * 1;//35/33;

		var n=0;

		var allMats = [];

		//var customMaterial = new THREE.MeshBasicMaterial( {color: 0xcccccc, wireframe:true} );
		//var customMaterial = new THREE.MeshNormalMaterial({side: THREE.DoubleSide, shading: THREE.FlatShading} );
		//var customMaterial = new THREE.MeshPhongMaterial({side: THREE.DoubleSide, color:0x777777}); 
		var customMaterial = new THREE.MeshLambertMaterial({side: THREE.DoubleSide, color:0x666666, shading: THREE.FlatShading}); 


		var texMats = {};


		/// Load textures from PIMG and inject as material maps (textures)

		var chunkTextures={};


		
		/// Load textures
		if(pimgData){
			var strippedPages = pimgData.strippedPages;
			

			///Only use layer 0
			strippedPages.forEach(function(page){
				
				/// Only load layer 0
				if(page.layer<=1){
					var filename = page.filename;
					var color = page.solidColor;
					var coord = page.coord;

					var matName = coord[0]+","+coord[1];
					if(page.layer == 1)
						matName+="-2";


					/// Add texture to list, note that coord name is used, not actual file name
					if(!chunkTextures[matName]){

						/// Load local texture, here we use file name!
						var chunkTex = RenderUtils.loadLocalTexture(localReader, filename);

						if(chunkTex){
							/// Set repeat, antistropy and repeat Y
							chunkTex.anisotropy = maxAnisotropy;
							chunkTex.wrapT = chunkTex.wrapS = THREE.RepeatWrapping;					
						}

						///...But store in coord name
						chunkTextures[matName] = chunkTex;					
						
					}
				}


			});/// end for each stripped page in pimgData
		}
		
		
				
		/// Render Each chunk
		/// We'll make this async in order for the screen to be able to update

		var renderChunk = function(cx,cy){
			var chunkIndex = cy*xChunks + cx;

			var pageX = Math.floor(cx/4);
			var pageY = Math.floor(cy/4);

			var chunkTextureIndices = allMaterials[chunkIndex].loResMaterial.texIndexArray;
			var matFileName = allMaterials[chunkIndex].loResMaterial.materialFile;		
			var chunkData = terrainData.chunks[chunkIndex];

			var mainTex = allTextures[chunkTextureIndices[0]];
			var mat = customMaterial;

			/// TODO: just tick invert y = false...?
			var pageOffetX = (cx % 4)/4.0;
			var pageOffetY = 0.75 - (cy % 4)/4.0;

			//offset 0 -> 0.75
			
			
			//Make sure we have shared textures

			/// Load and store all tiled textures
			var fileNames = [];
			for(var gi=0;gi<chunkTextureIndices.length/2;gi++){
				var textureFileName = allTextures[chunkTextureIndices[gi]].filename;

				fileNames.push(textureFileName);
				
				/// If the texture is not already loaded, read it from the .dat!
				if(!chunkTextures[textureFileName]){

					/// Load local texture
					var chunkTex = RenderUtils.loadLocalTexture(localReader, textureFileName);

					if(chunkTex){
						/// Set repeat, antistropy and repeat Y
						chunkTex.anisotropy = maxAnisotropy;
						chunkTex.wrapT = chunkTex.wrapS = THREE.RepeatWrapping;		
					}

					chunkTextures[textureFileName] = chunkTex;					
				}
			}/// End for each chunkTextureIndices


			/// Create Composite texture material, refering the shared textures
			var pageTexName=  pageX+","+pageY;				
			var pageTexName2=  pageX+","+pageY+"-2";				

			var fog = SceneUtils.getScene().fog;
			
			var uniforms = THREE.UniformsUtils.merge([
        		THREE.UniformsLib['lights'],
			]);

			/// FOG
			uniforms.fogColor = { type: "v3", value: new THREE.Vector3( fog.color.r, fog.color.g, fog.color.b ) },
			uniforms.fogNear = { type: "f",value: fog.near },
			uniforms.fogFar = { type: "f", value: fog.far },


			/// TODO: READ FROM VO, don't default to hard coded scale
			uniforms.uvScale = { type: "v2", value: new THREE.Vector2( 8.0, 8.0 ) };
			uniforms.offset = { type: "v2", value: new THREE.Vector2( pageOffetX, pageOffetY) };

			uniforms.texturePicker = {type: "t", value: chunkTextures[pageTexName]};
			uniforms.texturePicker2 = {type: "t", value: chunkTextures[pageTexName2]};

			uniforms.texture1 = { type: "t", value: chunkTextures[fileNames[0]]};
			uniforms.texture2 = { type: "t", value: chunkTextures[fileNames[1]]};
			uniforms.texture3 = { type: "t", value: chunkTextures[fileNames[2]]};
			uniforms.texture4 = { type: "t", value: chunkTextures[fileNames[3]]};
			

			mat = new THREE.ShaderMaterial( {
				uniforms: uniforms,
				vertexShader: document.getElementById( 'vertexShader' ).textContent,
				fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
				lights: true
			} );

			///Store referenceto each material
			allMats.push(mat);

			
			/// -1 for faces -> vertices , -2 for ignoring outer faces
			var chunkGeo =  new THREE.PlaneBufferGeometry ( cdx, cdy, chunkW-3, chunkW-3);

			var cn = 0;

			///Render chunk

			/// Each chunk vertex
			for(var y=0; y<chunkW; y++){

				for(var x=0; x<chunkW; x++){
			
					if(  x != 0 && x !=chunkW-1 && y!=0 && y !=chunkW-1 )
					{
						chunkGeo.attributes.position.array[cn*3+2] = terrainData.heightMap[n];
						cn++;
					}
					
					n++;					
				}
			} // End each chunk vertex

			
			/// Flip the plane to fit wonky THREE js world axes
			var mS = (new THREE.Matrix4()).identity();
			mS.elements[5] = -1;
			chunkGeo.applyMatrix(mS);

			/// Compute face normals for lighting, not used when textured
			chunkGeo.computeFaceNormals();
			//chunkGeo.computeVertexNormals();


			/// Build chunk mesh!
			var chunk;
			chunk = new THREE.Mesh(	chunkGeo , customMaterial );
			if(mat.length){
				chunk = THREE.SceneUtils.createMultiMaterialObject( chunkGeo, mat );
			}
			else{
				chunk = new THREE.Mesh(	chunkGeo , mat );	
			}	


			///Move and rotate Mesh to fit in place
			chunk.rotation.set(Math.PI/2,0,0);
			
			/// Last term is the new one: -cdx*(2/35)
			var globalOffsetX = parameterData.rect.x1 + cdx/2;
			var chunkOffsetX = cx * cdx;

			chunk.position.x = globalOffsetX + chunkOffsetX;

			///Adjust for odd / even number of chunks
			if(terrainData.numChunksD_2 % 2 == 0){
				
				/// Last term is the new one: -cdx*(2/35)
				var globalOffsetY = parameterData.rect.y1 + cdy/2 -0;// -cdy*(1/35);
				var chunkOffsetY = cy * cdy * 1;//33/35;

				chunk.position.z =  chunkOffsetY + globalOffsetY;
			}
			else{

				var globalOffsetY =  parameterData.rect.y1 - cdy/2 + 0; //cdy*(1/35);
				var chunkOffsetY = cy * cdy * 1;//33/35;

				chunk.position.z = globalOffsetY +  chunkOffsetY;	
			}


			var px = chunk.position.x;
			var py = chunk.position.z;


			if(!self.mapRect){
				self.mapRect = {
					x1:px-cdx/2, x2:px+cdx/2,
					y1:py-cdy/2, y2:py+cdy/2 };
			}
			
			self.mapRect.x1 = Math.min(self.mapRect.x1, px -cdx/2);
			self.mapRect.x2 = Math.max(self.mapRect.x2, px +cdx/2);

			self.mapRect.y1 = Math.min(self.mapRect.y1, py -cdy/2);
			self.mapRect.y2 = Math.max(self.mapRect.y2, py +cdy/2);
			
			

			chunk.updateMatrix();
			chunk.updateMatrixWorld ();
			
			/// Add to list of stuff to render
			SceneUtils.getScene().add( chunk );

			/// Add to list of chunks
			SceneUtils.getTerrainChunks().push( chunk );

			/// Add to list of collisions
			SceneUtils.getColisions().push( chunk );

			

		} /// End render chunk function


		var stepChunk = function(cx,cy){
			if(cx>=xChunks){
				cx = 0;
				cy++;
			}

			if(cy>=yChunks){
				self.drawWater(self.mapRect);
				callback();
				return;
			}

			var pct =  Math.floor( 100*(cy * xChunks + cx) /( xChunks * yChunks ) );

			output.find(".title").html("Loading Terrain");
			output.find(".progress").html(pct+"%");

			renderChunk(cx,cy);
			setTimeout(stepChunk,1,cx+1,cy);
		}

		stepChunk(0,0);



		

	},

	drawWater: function(rect){
		/// Add Water
		var material = material || new THREE.MeshBasicMaterial(
			{
				color: 0x5bb1e8,
				wireframe:false,
			 	opacity: 0.35
			}
		);

		/*
		rect.x1 += (rect.x1 -rect.x2)/2.0;
		rect.x2 += (rect.x1 -rect.x2)/2.0;

		rect.y1 += (rect.y1 -rect.y2)/2.0;
		rect.y2 += (rect.y1 -rect.y2)/2.0;
		*/

		material.transparent = true;
		RenderUtils.renderRect(rect, 0, material);
	}

	
};