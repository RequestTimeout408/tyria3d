var Utils = require("./Utils.js");
var SceneUtils = require("../SceneUtils.js");

function addZoneMeshesToScene(meshes, isCached, position, scale, rotation){

	/// TODO: Opt opt opt...

    meshes.forEach(function(mesh){

    	/// Create new mesh if we got back a cached original.
		if(isCached)
			mesh = new THREE.Mesh( mesh.geometry, mesh.material );

    	/// Scale, position and rotate.
    	mesh.scale.set( scale, scale, scale );
    	if(rotation){
	    	mesh.rotation.order = "ZXY";
	    	mesh.rotation.set(rotation.x, rotation.y, rotation.z);
	    }
    	mesh.position.set(position.x, position.y, position.z);

    	/// Add to scene.
    	SceneUtils.getScene().add(mesh);
		SceneUtils.getNonColisions().push(mesh);    	
	});

	

}

function renderZone(zone, zoneDefs, mapRect, localReader, renderZoneCallback){

	/// Get Zone Definition
	var zoneDef = null;
	zoneDefs.forEach(function(zd){
		if(!zoneDef && zd.token == zone.defToken)
			zoneDef = zd;
	});

	/// Calculate rect in global coordinates
	var zPos = zone.zPos;
	var mapX = mapRect.x1;
	var mapY = mapRect.y1;
	var c  = 32+16;

	var zoneRect = {
		x1:zone.vertRect.x1*c+mapX,
		x2:zone.vertRect.x2*c+mapX,
		y1:zone.vertRect.y1*-c-mapY,
		y2:zone.vertRect.y2*-c-mapY
	};	
	
	/// Testing: Render Zone Vert Rect
	//Utils.renderRect(zoneRect, -zPos);

	/// Zone width and depth in local corrdinates
	var zdx = zone.vertRect.x1-zone.vertRect.x2;
	var zdy = zone.vertRect.y1-zone.vertRect.y2;
	
	/// Zone Flags increases a linear position, used to step trough the Zone.
	var linearPos = 0;

	/// Create array of all models to add:
	var models = [];
	var modelGroups = {};

	for(var i = 0; i< zone.flags.length; i+=2){

		/// Step forward
		linearPos += zone.flags[i];

		/// Check if a model should be placed
		var flag = zone.flags[i+1];
		if(flag!=0){
			
			/// Extract flag data
			/// Layer is written in the last 4 bytes
			var zoneDefLayer = flag >> 4;

			/// Get Zone Definition Layer
			var layer = zoneDef.layerDefs[zoneDefLayer-1];

			/// TESTING Only show layers with height >= 3
			if(layer/* && layer.height >= 0*/){

				/// Get X and Y from linear position
				var modelX = (linearPos % zdx)*c + zoneRect.x1;
				var modelY = Math.floor(linearPos / zdx)*c + zoneRect.y1;

				/// Get Z from intersection with terrain
				var modelZ = null;

				var startZ = 100000;

				var raycaster = new THREE.Raycaster(
					new THREE.Vector3(modelX, startZ, modelY),
					new THREE.Vector3(0, -1, 0)
				);
				SceneUtils.getTerrainChunks().forEach(function(chunk){
					if(modelZ === null){
						var intersections = raycaster.intersectObject(chunk);
						if(intersections.length>0){
							modelZ = startZ - intersections[0].distance;
						}
					}
				});

				/// Get model id
				/// TODO: check with modelIdx = flag & 0xf;
				var modelIdx = 0; 
				var model = layer.modelArray[modelIdx];
				var modelFilename = model.filename;
				var zOffsets = model.zOffsets;

				var layerFlags = layer.layerFlags; //NOrmaly 128, 128
				
				//TODO: flip z,y?
				var rotRangeX = layer.rotRangeX;//max min
				var rotRangeY = layer.rotRangeY;//max min
				var rotRangeZ = layer.rotRangeZ;//max min

				var scaleRange = layer.scaleRange;//max min
				var fadeRange = layer.fadeRange;//max min


				//Unused
				//tiling: 3
				//type: 1
				//width: 2
				//radiusGround: 2

				if(!modelGroups[modelFilename]){
					modelGroups[modelFilename] = [];
				}

				modelGroups[modelFilename].push({
					scale:1.0,
					x:modelX,
					y:modelY,
					z:modelZ
				});
				
				/*models.push({
					filename:modelFilename,
					scale:1.0,
					x:modelX,
					y:modelY,
					z:modelZ
				});*/

			}/// End if layer height >= 3

			
		}/// End if flag != 0

	} /// End for each flag


	/**
	 * Steps trough each model and renders it to the scene, allowing for efficient caching.
	 * @param  {Number} i - Current index within the models array
	 */
	//var lastPct = -1;
	var groupKeys = Object.keys(modelGroups);
	function stepModels(i){

		/*var pct = Math.round(100.0*i / groupKeys.length);
		if(lastPct!=pct){
			console.log("Rendering ZONE models "+pct+"%");
			lastPct = pct;
		}*/

		if(i>=groupKeys.length){

			/// Tell caller this zone is done loading
			renderZoneCallback();
			return;
		}

		/// Read model at index
		/// var model = models[i];
		var  key = groupKeys[i]; /// key is model filename
		var group = modelGroups[key];
		
		/// Get model once per group
		Utils.getMeshesForFilename(key, null, localReader,

			function(meshes, isCached){
				
				/// If there were meshes, add them to the scene with correct scaling rotation etc.
				if(meshes){

					/// Add one copy per model instance
					group.forEach(function(model){
						addZoneMeshesToScene(meshes, true /*isCached*/, {x:model.x, y:model.z, z:model.y}, model.scale);
					});// End for each model in group
					
				}

				/// Rendering is done, render next.
				//window.setTimeout(stepModels, 0, i+2);
				stepModels(i+1);
			}

		);
		
		
		
		
	}/// End function stepModels

	
	/// Begin stepping trough the models, rendering them.
	stepModels(0);
}


module.exports = {
	render: function(zoneChunkHeader, parameterChunkHeader, terrainChunkHeader, localReader, renderCallback){
		var self = this;
		SceneUtils.showProgressPanel(function(){
			self.doRender(zoneChunkHeader, parameterChunkHeader, terrainChunkHeader, localReader, renderCallback);
		});
	},

	doRender: function(zoneChunkHeader, parameterChunkHeader, terrainChunkHeader, localReader, renderCallback){
				
		/// Zone data
		var zones = zoneChunkHeader.data.zones;
		var zoneDefs = zoneChunkHeader.data.zoneDefs;

		var output = $("#progressPanel");

		/// Render each zone
		lastPct = -1;		

		function stepZone(i){

			var pct = Math.round(100.0*i / zones.length);
			if(lastPct!=pct){
				output.find(".title").html("Loading 3D Models (Zone)");
				output.find(".progress").html(pct+"%");
				lastPct = pct;
			}

			if(i >= zones.length){
				renderCallback();
				return;
			}

			

			renderZone(zones[i], zoneDefs, parameterChunkHeader.data.rect, localReader,
				function(){
					stepZone(i+1);
				}
			);	

		}
		stepZone(0);

		


		//// Not used: zone defintion per chunk data "images" 32*32 points
		/*
		//Total map dx and dy
		var d = terrainChunkHeader.data;
		var pd = parameterChunkHeader.data;
		var dx = (pd.rect.x2-pd.rect.x1);
		var dy = (pd.rect.y2-pd.rect.y1);

		//Each chunk dx and dy
		
		var c =1;
		var cdx = c*dx/d.dims.dim1;
		
		var cdy = c*dy/d.dims.dim2;

		var cdx = dx/(d.numChunksD_1*2);
		var cdy =dy/(d.numChunksD_2*2);


		for(var i=0; i<zoneDefs.length; i++){
			var zoneDef = zoneDefs[i];
			
			//TODO: opt!
			zoneDef.layerDefs.forEach(function(layer){

				layer.modelArray.forEach(function(model){
				
					
				});
				

			});

			var chunkMat = new THREE.MeshBasicMaterial(
				{
					color: 0x00ff00,
					wireframe:true,
				 	opacity: 1.0,
				}
			);

			//TODO: opt!
			
			if(
				zoneDef.token == 597  ||
				zoneDef.token == 1369  ||
				zoneDef.token == 903  
			){

				zoneDef.pageTable.pageArray.forEach(function(page){
					var flags = page.flags;
					var coord = page.chunkCoord;


					//Hightlight this coord
					var rect = {};

					
					//var globalOffsetX = pd.rect.x2 - cdx;
					var globalOffsetX = pd.rect.x1 + cdx/2;
					var chunkOffsetX = coord[0] * cdx;

					rect.x1  = globalOffsetX + chunkOffsetX;

					///Adjust for odd / even number of chunks
					if(d.numChunksD_2 % 2 == 0){
						
						var globalOffsetY = -pd.rect.y1;
						var chunkOffsetY = -coord[1] * cdy;

						rect.y1  =  chunkOffsetY + globalOffsetY;
					}
					else{

						var globalOffsetY =  -pd.rect.y1;
						var chunkOffsetY = -coord[1] * cdy;

						rect.y1 = globalOffsetY +  chunkOffsetY;	
					}

					rect.x2 = rect.x1+cdx;
					rect.y2 = rect.y1+cdy;


					

					Utils.renderRect(rect, 4000,chunkMat, 4000);

					//for(var j=0; j<flags.length; j++){
					//	if(flags[j]>0){
					//		console.log("Found flag",flags[j],"@ zoneDef",zoneDef.token,"coord",coord,"index",j);
					//	}
					//}
				});

			}

		}*/
	}
};