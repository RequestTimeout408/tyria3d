var FileHeaderParser = {
    def : [
    	'identifier', 'cstring:2',
    	'unknownField1', 'uint16',
    	'unknownField2', 'uint16',
    	'pkFileVersion', 'uint16',
    	'type', 'cstring:4'
    ],
	parse : function(ds){
	    return ds.readStruct(this.def);
	}
};

module.exports = FileHeaderParser;