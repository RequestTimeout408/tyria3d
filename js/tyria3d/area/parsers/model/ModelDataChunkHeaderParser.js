var Utils = require('../Utils.js');

var ModelDataChunkHeaderParser = {
	def:[
	    //helpers::Array<ModelPermutationDataV65> permutations;
	    "permutations",Utils.getArrayReader([
	    		//qword token;
	    		"token_pt1","uint32","token_pt2","uint32",

    			//helpers::RefList<ModelMaterialDataV65> materials;
    			"materials", Utils.getRefArrayReader([
					    //qword token;
					    "token_pt1","uint32","token_pt2","uint32",

					    //dword materialId;
					    "materialId","uint32",

					    //helpers::FileName filename;
					    "filename","uint32",//Utils.getFileNameReader(),

					    //dword materialFlags;
					    "materialFlags","uint32",

					    //dword sortOrder;
					    "sortOrder","uint32",

					    //helpers::Array<ModelTextureDataV65> textures;
					    "textures", Utils.getArrayReader([
					    		//helpers::FileName filename;
					    		"filename",Utils.getFileNameReader(),

							    //dword textureFlags;
							    "textureFlags","uint32",

							    //qword token;
							    "token_pt1","uint32","token_pt2","uint32",

							    //qword blitId;
							    "blitId_pt1","uint32","blitId_pt2","uint32",

							    //dword uvAnimId;
							    "uvAnimId","uint32",

							    //byte uvPSInputIndex;
							    "uvPSInputIndex","uint8",
					    	]),


					    //helpers::Array<ModelConstantDataV65> constants;
					    //helpers::Array<ModelMatConstLinkV65> matConstLinks;
					    //helpers::Array<ModelUVTransLinkV65> uvTransLinks;
					    //helpers::Array<ModelMaterialTexTransformV65> texTransforms;
    				])

	    	]),

	    //helpers::Ptr<ModelCloudDataV65> cloudData;
	    "cloudData_p","uint32",
	    //helpers::Array<ModelObstacleDataV65> obstacles;
	    "obstacles_n","uint32","obstacles_p","uint32",
	    //helpers::Ptr<ModelStreakDataV65> streakData;
	    "streakData_p","uint32",
	    //helpers::Ptr<ModelLightDataV65> lightData;
	    "lightData_p","uint32",
	    //helpers::Array<ModelClothDataV65> clothData;
	    "clothData_n","uint32","clothData_p","uint32",
	    //helpers::Ptr<ModelWindDataV65> windData;
	    "windData_p","uint32",
	    //helpers::Array<qword> actionOffsetNames;
	    "actionOffsetNames_n","uint32","actionOffsetNames_p","uint32",
	    //helpers::Array<float3> actionOffsets;
	    "actionOffsets_n","uint32","actionOffsets_p","uint32",
	    //float lodOverride[2];
	    "lodOverride",["[]","float32",2],
	    //helpers::FileName soundScript;
	    //helpers::Ptr<ModelLightningDataV65> lightningData;
	    //helpers::Array<ModelSoftBodyDataV65> softBodyData;
	    //helpers::Array<ModelBoneOffsetDataV65> boneOffsetData;
	    //helpers::Ptr<ModelBoundingSphereV65> boundingSphere;
	],

	parse:function(ds){
		return ds.readStruct(this.def);
	},
	
};

module.exports = ModelDataChunkHeaderParser;