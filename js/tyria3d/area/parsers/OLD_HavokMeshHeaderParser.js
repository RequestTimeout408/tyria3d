var HavokMeshHeaderParser = {
	
parseHavokMeshInfo : function(ds){
   var def = [
    	'indexCount',			'uint32',
    	'indexBufferOffset',	'uint32',

    	'vertexCount',			'uint32',
    	'vertexBufferOffset',	'uint32',
    	
    	'unknownInt16Count',	'uint32',
    	'unknownInt16CountOffset',	'uint32',

    	'unknownCount1', 		'uint32', ///Actually counts the number of bytes used
    	'unknownOffset1', 		'uint32',
   ];
   
   var startPos = ds.position;
   var obj = ds.readStruct(def);
   obj.addr = startPos;
   
   
   /// Absoulte adresses
   obj.indexBufferAddr = obj.addr + obj.indexBufferOffset + 4;
   obj.vertexBufferAddr = obj.addr + obj.vertexBufferOffset + 12;
   
   obj.unknownInt16CountOffset = obj.addr + obj.unknownInt16CountOffset + 20;
   obj.unknownAddr_1 = obj.addr + obj.unknownOffset1 + 28;
   
   
	// Unklnown 16int getter
	obj.getUnknownInt16s=function(){
	   ds.seek(this.unknownInt16CountOffset);
	   return ds.readInt16Array(this.unknownInt16Count);
	};
	
	// Connected to vertex format..
	obj.getValue=function(){
		
		var struct= [
		 "f0","float32",
		 "f1","float32",
		 "f2","float32", /// POsition ?
		 "ui16_0","uint16",
		 "ui16_1","uint16",
		 "ui32_0","uint32", /// Bytes to read
		];
	   ds.seek(this.unknownAddr_1);
	   return ds.readStruct(struct);
	   //return ds.readInt32Array(this.unknownCount1);
	};
   
   return obj;
}
};

module.exports = HavokMeshHeaderParser;

