var ChunkHeaderParser = {
    def : [
    	'type', 'cstring:4',
    	'chunkDataSize', 'uint32',
    	'chunkVersion', 'uint16',
    	'chunkHeaderSize', 'uint16',
    	'offsetTableOffset', 'uint32',
    ],
	parse:function(ds){
	    return ds.readStruct(this.def);
	}
};

module.exports = ChunkHeaderParser;
