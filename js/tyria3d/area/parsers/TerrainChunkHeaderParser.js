var Utils = require('./Utils.js');

var TerrainChunkHeaderParser = {
	def:[
		
		//dword2 dims;
		"dims",["dim1","uint32", "dim2", "uint32"],

		//float swapDistance;
		"swapDistance","float32",		
		
		//helpers::Array<float> heightMapArray;
	    "heightMap", Utils.getArrayReader("float32"),

	    //helpers::Array<dword> tileFlagArray;
	    "tileFlags", Utils.getArrayReader(
				"uint32"
				//["[]","uint8",4]
	    	),

	    //helpers::Array<PackMapTerrainChunkV14> chunkArray;
		"chunks", Utils.getArrayReader([
				
				//dword chunkFlags;
				"chunkFlags", "uint32",

    			//helpers::Array<word> surfaceIndexArray;
    			"surfaceIndexArray", Utils.getArrayReader("uint16"),

    			//helpers::Array<qword> surfaceTokenArray;
    			"surfaceTokenArray", Utils.getArrayReader([
    					"token_pt1", "uint32",
    					"token_pt2", "uint32"
    				])

			]),

	    //helpers::Ptr<PackMapTerrainMaterialsV14> materials;
		//"materialPointer" , "uint32", 
		"materials",
		function(ds, struct){
	    	var ptr = ds.position + ds.readUint32();
	    	//var size = ds.readUint32();
	    	var _pos = ds.position;	    	

	    	/// Go to pointer
	    	ds.seek( ptr );
	    	
	    	var _def = [
	    		
    			//helpers::FileName pagedImage;
    			"pagedImage", Utils.getFileNameReader(),
			    
			    //helpers::Array<PackMapTerrainConstV14> constArray;
			    "constArray", Utils.getArrayReader([
		    		//dword tokenName;
		    		"tokenName", "uint32",

					//float4 value;
					"float4", [
						"a", "float32",
						"b", "float32",
						"c", "float32",
						"d", "float32"
					],
		    	]),
			    
			    //helpers::Array<PackMapTerrainTexV14> texFileArray;
			    "texFileArray", Utils.getArrayReader([
		    		//dword tokenName;
				    "tokenName", "uint32",

				    //dword flags;
				    "flags", "uint32",

				    //helpers::FileName filename;
				    "filename", Utils.getFileNameReader(),

				    //dword2 flagsVector;
				    "flagsVector",["flag1","uint32", "flag2", "uint32"],

				    //dword layer;
				    "layer", "uint32",
		    	]),
			    
			    //helpers::Array<PackMapTerrrainChunkMaterialV14> materials;
			    "materials", Utils.getArrayReader([
			    	//byte tiling[3];
			    	"tiling", ["[]","uint8",3],

				    //PackMapTerrainMaterialV14 hiResMaterial;
				    "hiResMaterial", [
				    	//helpers::FileName materialFile;
				    	"materialFile", Utils.getFileNameReader(),
					    
					    //dword fvf;
					    "fvf", "uint32",						    

					    //helpers::Array<dword> constIndexArray;
					    "constIndexArray", Utils.getArrayReader("uint32"),

					    //helpers::Array<dword> texIndexArray;
					    "texIndexArray", Utils.getArrayReader("uint32"),
				    ],

				    //PackMapTerrainMaterialV14 loResMaterial;
				    "loResMaterial", [
				    	//helpers::FileName materialFile;
				    	"materialFile", Utils.getFileNameReader(),
					    
					    //dword fvf;
					    "fvf", "uint32",						    

					    //helpers::Array<dword> constIndexArray;
					    "constIndexArray", Utils.getArrayReader("uint32"),

					    //helpers::Array<dword> texIndexArray;
					    "texIndexArray", Utils.getArrayReader("uint32"),
				    ],

				    //PackMapTerrainMaterialV14 faderMaterial;
				    "faderMaterial", [
				    	//helpers::FileName materialFile;
				    	"materialFile", Utils.getFileNameReader(),
					    
					    //dword fvf;
					    "fvf", "uint32",						    

					    //helpers::Array<dword> constIndexArray;
					    "constIndexArray", Utils.getArrayReader("uint32"),

					    //helpers::Array<dword> texIndexArray;
					    "texIndexArray", Utils.getArrayReader("uint32"),
				    ],

				    //helpers::Ptr<PackMapTerrainChunkUVDataV14> uvData;
				    "uvDataPtr", "uint32"
		    	]),
			    
			    //float2 midFade;
			    "midFade", ["min","float32","max","float32"],
			    
			    //float2 farFade;
			    "farFade", ["min","float32","max","float32"]
	    	];
	    	var ret = ds.readStruct(_def);


	    	/// Go back
	    	ds.seek(_pos);
	    	return ret;
	    }
	],

	parse:function(ds){
		var startPos = ds.position;
		
		var header = ds.readStruct(this.def);
		

		/// Calculate terrain chunk dims (derived)
		header.numChunksD_1 = Math.sqrt( header.dims.dim1 * header.chunks.length / header.dims.dim2);
		header.numChunksD_2 = header.chunks.length / header.numChunksD_1;

		/// Log the filename of all textures
		/*header.materials.texFileArray.forEach(function(texFile){
			if(texFile.filename>0){
				console.log(texFile.filename);
			}
		});*/


		return header;
	},

	
	
};

module.exports = TerrainChunkHeaderParser;