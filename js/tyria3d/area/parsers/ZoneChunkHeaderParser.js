var Utils = require('./Utils.js');

var ZoneChunkHeaderParser = {
	def:[

		
		
		//helpers::Array<PackMapZoneDefV22> zoneDefArray;
		"zoneDefs", Utils.getArrayReader([
			//helpers::FileName defFilename; //pointer
    		"defFilename", "uint32",									//4 -> 4

    		//dword token;
    		"token", "uint32",											//4 -> 8

    		//helpers::Array<PackMapZoneLayerDefV22> layerDefArray;
    		"layerDefs", Utils.getArrayReader([
    			//byte type;
	    		"type", "uint8",

			    //byte height;
			    "height", "uint8",

			    //byte width;
			    "width", "uint8",

			    //byte radiusGround;
			    "radiusGround", "uint8",

			    //byte sortGroup;
			    "sortGroup", "uint8",

			    //byte tiling;
			    "tiling", "uint8",

			    //float2 scaleRange;
			    "scaleRange", ["min", "float32", "max", "float32"],

			    //float probability;
			    "probability", "float32",

			    //float2 fadeRange;
			    "fadeRange", ["min", "float32", "max", "float32"],

			    //float2 rotRange[3];
			    "rotRangeX", ["min", "float32", "max", "float32"],
			    "rotRangeY", ["min", "float32", "max", "float32"],
			    "rotRangeZ", ["min", "float32", "max", "float32"],
			    

			    //float2 hslRanges[4];
			    "hslRanges_1", ["min", "float32", "max", "float32"],
			    "hslRanges_2", ["min", "float32", "max", "float32"],
			    "hslRanges_3", ["min", "float32", "max", "float32"],
			    "hslRanges_4", ["min", "float32", "max", "float32"],

			    //float instanceScaleJitter;
			    "instanceScaleJitter", "float32",

			    //byte noise;
			    "noise", "uint8",

			    //dword layerFlags;
			    "layerFlags", "uint32",

			    //helpers::FileName materialname;
			    "materialname", "uint32",

			    //helpers::Array<PackMapZoneModelV22> modelArray;
			    "modelArrayCount", "uint32",
			    "modelArrayOffset", "uint32",

			    //helpers::Ptr<PackMapZoneModelV22> subModel;
			    "subModel", "uint32",

			    //helpers::WString reserved;
			    "reserved", "uint32"
			]),

    		//qword timeStamp;
    		"timeStamp_pt1", "uint32",									//8 -> 24
    		"timeStamp_pt2", "uint32",

	    	//helpers::Ptr<PackMapZonePageTableV10> pageTable;
	    	"PackMapZonePageTablePointer", "uint32",					//4 -> 28

	    	//helpers::WString reserved;
	    	"reserved", "uint32"
		]),

		"zones", Utils.getArrayReader([
			// dword zoneFlags  ( dword = uint32 )
    		"zoneFlags", "uint32",

    		// dword4 vertRect;
    		"vertRect", ["x1","uint32","y1","uint32","x2","uint32", "y2", "uint32"],

    		//float waterHeight;
    		"waterHeight", "float32",

    		//byte seed;
    		"seed", "uint8",

    		//dword defToken;
    		"defToken", "uint32",

    		//float2 range;
    		"range", ["min", "float32", "max", "float32"],

		    //float zPos;
		    "zPos", "float32",

		    //helpers::Array<byte> flags;
		    "flags", Utils.getArrayReader("uint8"),

		    //helpers::Array<PackMapZoneEncodingDataV22> encodeData;
		    "encodeData", Utils.getArrayReader("uint32"),

		    //helpers::Array<PackMapZoneCollideDataV22> collideData;
		    "collideData", Utils.getArrayReader([
		    	
		    	//float normalX;
    			"normalX", "float32",
    			
    			//float normalY;
    			"normalY", "float32",
    			
    			//float zPos;
    			"zPos", "float32"
			]),

		    //helpers::Array<word> offsetData;
		    "offsetData", Utils.getArrayReader("uint16"),

		    //helpers::Array<float2> vertices;
		    "vertices", Utils.getArrayReader(["x", "float32", "z", "float32"]),

		    //word broadId;
		    "broadId","uint16",	

		    //helpers::WString reserved; pointer to a string??
		    "reserved","uint32"
		]),

		
	    //PackBroadphaseType broadPhase;
	    //		 PackBoradPhaseType:
		// 		 helpers::Array<byte> broadphaseData;
		"broadPhase", Utils.getArrayReader("uint8"),
	    
	    //word maxBroadId;
	    "maxBroadId" , "uint16",
	    
	    //helpers::WString string; (pointer to char16 string)
	    "string" , "uint32",
	    
	],

	parse:function(ds){
		return ds.readStruct(this.def);
	}
	
    
   
	
};

module.exports = ZoneChunkHeaderParser;