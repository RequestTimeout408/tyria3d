var meshes = [];

module.exports= {
	parseHavokMesh: function(collision){
		var index = collision.index;

		if(!meshes[index]){

    		var geom = new THREE.Geometry();
    		
    		/// Pass vertices	    		
    		for(var i=0; i<collision.vertices.length; i++){
    			var v=collision.vertices[i];
    			geom.vertices.push( new THREE.Vector3(v.x , v.y , v.z ) );
    		}	    		
   			
    		/// Pass faces
    		for(var i=0; i<collision.indices.length; i+=3){

    			var f1=collision.indices[i];
    			var f2=collision.indices[i+1];
    			var f3=collision.indices[i+2];

    			if( f1<=collision.vertices.length &&
    				f2<=collision.vertices.length &&
    				f3<=collision.vertices.length)
	   				geom.faces.push( new THREE.Face3( f1, f2, f3 ) );
	   			else
	   				console.warn("Errorus index!");
    		}

			/// Prepare geometry and pass new mesh
			geom.computeFaceNormals();
			geom.computeVertexNormals();
			//meshes[index]= new THREE.Mesh( geom, new THREE.MeshNormalMaterial({side: THREE.DoubleSide}) ); 
			meshes[index]= new THREE.Mesh( geom, new THREE.MeshPhongMaterial({
				side: THREE.DoubleSide,
				color:0x777777,
				shading: THREE.FlatShading
			}) ); 

			//meshes[index]= new THREE.Mesh( geom, new THREE.MeshPhongMaterial({side: THREE.DoubleSide, color:0x777777}) ); 
			
			return meshes[index];
		}
		else{
			//console.log("hit");
			return meshes[index].clone();
		}
	}

};