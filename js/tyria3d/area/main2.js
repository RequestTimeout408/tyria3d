/// Globals variables
var scene, camera, controls, renderer,
geometry, material, mesh,
diffuseMap, glowMap, normalMap,
directionalLight, ambientLight;

/// Settings

var shamanPath = 'img/map.jpg';
var shamanLabelsPath = 'img/map_labels.jpg';
var originalPath = 'img/overlayed.jpg';
var modelPath = 'models/cube.obj';


__proto__: Object
var cameraOpts = {
	fov : 60,
	near : 10,
	far : 1000000,
	pos : {
		x: 6650.453486662317,
		y: -17031.559807482816,
		z: 46185.65446727842
	}
},
renderOpts = {
	antialias : true,
	alpha : true,
	clearClr : 0x000000,
	clearAlpha : .5
},
controlOpts = {
	damping : .2
},
lightOpts = {
	directionalClr : 0xaaaaaa,
	ambientClr : 0xffffff,
	directionalClrShaman : 0x888888,
	ambientClrShaman : 0x55555,
	directionalPos : {
		x : 200,
		y : 200,
		z : 200
	},
	glow : true,
	normals : true
};




var sc = 0;
function stringToVectors(s,offset){
	//		3 651 672
	//		3.6 miljoner
	s = s.substring(offset);
	//s = s.substring(16 + 2199936+240000+24000*(8 + 1));
	console.log(s.substring(0,32));
	console.log(s.substring(s.length-32,s.length));
	var len = 8;
	var res = [];
	var maxlen = 140000000;
//	var parse = hexToDouble;
	var parse = hexToFloat;
	var p1,p2,p0;
	for(var i=0;i<=s.length-len*3 && i/len<= maxlen;i+=len*3){
		
		p0 = s.substring(	i,			i+len);
		p1 = s.substring(	i+len,		i+len*2);
		p2 = s.substring(	i+len*2,	i+len*3);
		
		var patt = "AAAAAAAA"; 
		
		if(p0==patt || p1 ==patt || p2 == patt){
			console.log("A");			
			continue;
		}
		
		var v0 = parse(p0),
		v1 = parse(p1),
		v2 =-parse(p2);
		
		if(Math.abs(v0)<0.1 || Math.abs(v1)<0.1 || Math.abs(v2)<0.1){
			console.log(s.substring(	i,			i+len*3));
			continue;
		}
		
		console.log("OK");
		
		res.push( new THREE.Vector3(v0, v1, v2) );
	}
	
	console.log(p0+p1+p2)
	return res;
}


init();


function parseANetPfHeader(ds){
    var def = [
    	'identifier', 'cstring:2',
    	'unknownField1', 'uint16',
    	'unknownField2', 'uint16',
    	'pkFileVersion', 'uint16',
    	'type', [
    		//'name', 'cstring:8',
    		'id', 'uint32'
		],
    ];
    return ds.readStruct(def);
}

function parseANetPfChunkHeader(ds){
    var def = [
    	'type', [
    		'name', 'cstring:4',
    		'id', 'uint32'
		],
    	'chunkDataSize', 'uint32',
    	'chunkVersion', 'uint16',
    	'chunkHeaderSize', 'uint16',
    	'offsetTableOffset', 'uint32',
    ];
    
    //union {
     //   byte chunkType[4];          /**< Identifies the chunk type. */
      //  uint32 chunkTypeInteger;    /**< Identifies the chunk type, as integer for easy comparison. */
    //};
    //uint32 chunkDataSize;           /**< Total size of this chunk, excluding this field and mChunkType, but \e including the remaining fields. */
    //uint16 chunkVersion;            /**< Version of this chunk. */
    //uint16 chunkHeaderSize;         /**< Size of the chunk header. */
    //uint32 offsetTableOffset;       /**< Offset to the offset table. */
   
    return ds.readStruct(def);
}

function init() {
	
	var oReq = new XMLHttpRequest();
	//oReq.open("GET", "models/dr.data", true);
	oReq.open("GET", "models/fly.data", true);
	oReq.responseType = "arraybuffer";
	
	oReq.onload = function (oEvent) {
	  var arrayBuffer = oReq.response; // Note: not oReq.responseText
	  if (arrayBuffer) {
	  	
	  	var ds = new DataStream(arrayBuffer);
	  	ds.endianness = ds.SMALL_ENDIAN;
	    
	    /// Read file header
	    var fileHeader = parseANetPfHeader(ds);
	    console.log("file header", fileHeader);
	    
	    /// Read first chunk header
	    var chunkHeader = parseANetPfChunkHeader(ds);
	    console.log("chunk header", chunkHeader);
	    
	  }
	};
	
	oReq.send(null);

	
	
	
}


/// HEADER FÖR MAP sök efter

/// B9040133F7607F11BAC807DF2C0000000000803FE8D40846


