var Utils = require('../parsers/Utils.js');

module.exports = [
	//helpers::RefList<ModelMeshDataV66> meshes;
	"meshes", Utils.getRefArrayReader([
			//qword visBone;
			"visBone_pt1", "uint32",
			"visBone_pt2", "uint32",

		    //helpers::Array<ModelMeshMorphTargetV66> morphTargets;
		    "num_morphTargets","uint32",
		    "ptr_morphTargets","uint32",

		    //dword flags;
		    "flags", "uint32",

		    //helpers::Array<dword> seamVertIndices;
		    "seamVertIndices", Utils.getArrayReader("uint32"),
		    
		    //qword meshName;
		    "meshName_pt1","uint32",
		    "meshName_pt2","uint32",

		    //float3 minBound;
		    "minBound", ["x","float32","z","float32","y","float32"],
		    
		    //float3 maxBound;
		    "maxBound", ["x","float32","z","float32","y","float32"],

		    //helpers::Array<GrBoundData> bounds;
		    "bounds", Utils.getArrayReader([
			    	//float3 center;
			    	"center", ["x","float32","z","float32","y","float32"],
				    //float3 boxExtent;
				    "boxExtent", ["x","float32","z","float32","y","float32"],
				    //float sphereRadius;
				    "sphereRadius", "float32",
		    	]),

		    //dword materialIndex;
		    "materialIndex","uint32",

		    //helpers::String materialName;
		    //"materialName","uint32",
		    "materialName",Utils.getStringReader(),

		    //helpers::Array<qword> boneBindings;
		    "boneBindings", Utils.getArrayReader([
		    		"pt1","uint32",
		    		"pt2","uint32",
		    	]),

		    //helpers::Ptr<ModelMeshGeometryV1> geometry;
		    "geometry", Utils.getPointerReader([
		    		//ModelMeshVertexDataV1 verts;
		    		//"verts",[
		    			//dword vertexCount;
		    			"vertexCount", "uint32",
						//PackVertexType mesh;
					//	"mesh", [
							//dword fvf;
							"fvf", "uint32",
							//helpers::Array<byte> vertices;
							"vertices", Utils.getArrayReader("uint8"),
					//	]
		    		//],

				    //ModelMeshIndexDataV1 indices;
				    //"indices",[
				    	//helpers::Array<word> indices;
				    	"indices", Utils.getArrayReader("uint16"),
				    //],
				    	

				    //helpers::Array<ModelMeshIndexDataV1> lods;
				    "numLods","uint32","ptrLods","uint32",
				    //helpers::Array<dword> transforms;
				    "numTransforms","uint32","ptrtransforms","uint32"
		    	])
		]),
	
]