var File = require('./File');

function PagedImageFile(ds, addr){
	File.call(this, ds, addr);
};
PagedImageFile.prototype = Object.create(File.prototype);
PagedImageFile.prototype.constructor = PagedImageFile;

PagedImageFile.prototype.getChunkStructs = function(){
	return {
		"pgtb":require('./PagedImageTableDataDefinition')
	};
};

module.exports = PagedImageFile;