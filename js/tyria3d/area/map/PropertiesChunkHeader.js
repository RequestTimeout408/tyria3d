var PropertiesChunkHeaderParser = require("./../parsers/PropertiesChunkHeaderParser.js");


var PropertiesChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

PropertiesChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = PropertiesChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = PropertiesChunkHeader;