var HavokModel = require("./../parsers/HavokModelParser");

var HavokModel = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

HavokModel.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = HavokModelParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};

module.exports = HavokModel;