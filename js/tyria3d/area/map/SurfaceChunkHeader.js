var SurfaceChunkHeaderParser = require("./../parsers/SurfaceChunkHeaderParser.js");


var SurfaceChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

SurfaceChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = SurfaceChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = SurfaceChunkHeader;