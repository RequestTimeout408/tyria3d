var File = require('./File');

function MapFile(ds, addr){
	File.call(this, ds, addr);
};
MapFile.prototype = Object.create(File.prototype);
MapFile.prototype.constructor = MapFile;

MapFile.prototype.getChunkStructs = function(){
	return {
		"havk":require('./HavokDefinition'),
		"trn":require('./TerrainDefinition'),
		"parm":require('./ParameterDefinition'),
		"shor":require('./ShoreDefinition'),
		"zon2":require('./ZoneDefinition'),
		"prp2":require('./PropertiesDefinition'),
		"cube":require('./CubeMapDefinition'),
		"env":require('./EnvironmentDefinition'),
	};
};

module.exports = MapFile;