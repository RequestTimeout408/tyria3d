var HavokChunkHeaderParser = require("./../parsers/HavokChunkHeaderParser.js");
var HavokMeshInfo = require("./HavokMeshInfo.js");

var HavokChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

HavokChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = HavokChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};

HavokChunkHeader.prototype.getCollisionsForAnimation = function(animation) {
	var ret = [];
	
	for (var i = 0; i < animation.collisionIndices.length; i++) {
		var index = animation.collisionIndices[i];
		var collision = this.data.collisions[ index ];
		collision.index = index;
		ret.push( collision );
	}
	
	return ret;
}; 


module.exports = HavokChunkHeader;