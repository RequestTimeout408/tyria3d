var ZoneChunkHeaderParser = require("./../parsers/ZoneChunkHeaderParser.js");


var ZoneChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

ZoneChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = ZoneChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = ZoneChunkHeader;