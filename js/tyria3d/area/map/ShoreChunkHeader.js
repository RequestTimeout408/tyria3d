var ShoreChunkHeaderParser = require("./../parsers/ShoreChunkHeaderParser.js");


var ShoreChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

ShoreChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = ShoreChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = ShoreChunkHeader;