var TerrainChunkHeaderParser = require("./../parsers/TerrainChunkHeaderParser.js");


var TerrainChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

TerrainChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = TerrainChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = TerrainChunkHeader;