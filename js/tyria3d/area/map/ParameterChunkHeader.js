var ParameterChunkHeaderParser = require("./../parsers/ParameterChunkHeaderParser.js");


var ParameterChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

ParameterChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = ParameterChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = ParameterChunkHeader;