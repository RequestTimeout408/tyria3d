var Utils = require('../parsers/Utils.js');

module.exports = [
	//float4 rect;
	"rect", [
		"x1", "float32",
		"y1", "float32",
		"x2", "float32",
		"y2", "float32"
		],

	//dword flags;
	"flags", "uint32",

	//byte16 guid;
	"guid", "uint16",
];