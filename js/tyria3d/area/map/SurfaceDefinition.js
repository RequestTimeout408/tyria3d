var Utils = require('../parsers/Utils.js');

module.exports = [
	//helpers::Array<MapSurfaceAttributeV0> attributeData;
	"atributeData",Utils.getArrayReader([
		//qword id;
		"id_pt1","uint32",
		"id_pt2","uint32",
	    //qword sound;
	    "sound_pt1","uint32",
		"sound_pt2","uint32",

	    //dword flags;
	    "flags", "uint32"
	]),

    //helpers::Array<MapSurfaceAttributeToolV0> toolData;
    "_numToolData","uint32",
	"p2","uint32",

    //helpers::Array<MapSurfaceTerrainOverrideV0> terrainArray;
    "_numTerrainArray","uint32",
	"p3","uint32",

    //helpers::Array<MapSurfacePropOverrideV0> propArray;
    "_numPropArray","uint32",
	"p4","uint32",
];