var ChunkHeaderParser = require("./../parsers/ChunkHeaderParser");

var ChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

ChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = ChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};

ChunkHeader.prototype.next = function(){
	try{
		// Calculate actual data size, as mChunkDataSize
		// does not count the size of some header variables
		return new ChunkHeader(this.ds,this.addr + 8 + this.data.chunkDataSize);
	}
	catch(e){
		/// Out of bounds probably
		return null;
	}
};

module.exports = ChunkHeader;