var FileHeaderParser = require("./../parsers/FileHeaderParser");

var FileHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

FileHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = FileHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};

module.exports = FileHeader;