var Utils = require('../parsers/Utils.js');

var skycardAttributesDef = [
	//float azimuth;
	"azimuth", "float32",
    
    //float density;
    "density", "float32",
    
    //float hazeDensity;
    "hazeDensity", "float32",
    
    //float latitude;
    "latitude", "float32",

    //float lightIntensity;
    "lightIntensity", "float32",

    //float minHaze;
    "minHaze", "float32",

    //float2 scale;
    "scale", ["[]","float32",2],

    //float speed;
    "speed", "float32",

    //helpers::FileName texture;
    "texture", Utils.getFileNameReader(),

    //float4 textureUV;
    "textureUV", ["[]", "float32", 4],

    //float brightness;
    "brightness", "float32"
];

module.exports = [

	//helpers::Array<PackMapEnvDataLocalV74> dataLocalArray;
	"dataLocalArray", ["[]","uint32", 2]/*Utils.getArrayReader([
			//helpers::Array<PackMapEnvDataLightingV74> lighting;
			"lighting",["[]","uint32",2],

		    //helpers::Array<PackMapEnvDataLightingCharGroupV45> lightingCharGroups;
		    "lightingCharGroups",["[]","uint32",2],

		    //helpers::Ptr<PackMapEnvDataCloudsV74> clouds;
		    "clouds", "uint32",

		    //helpers::RefList<PackMapEnvDataColoredLightRingsV45> coloredLightRings;
		    "coloredLightRings",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataEffectV74> effect;
		    "effect",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataHazeV74> haze;
		    "haze",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataPFieldV74> particleFields;
		    "particleFields",["[]","uint32",2],

		    //helpers::Array<PackMapEnvDataPFieldCutoutV45> particleFieldCutouts;
		    "particleFieldCutouts",["[]","uint32",2],

		    //helpers::Ptr<PackMapEnvDataSkyV74> sky;
		    "sky", "uint32",
		    //helpers::Ptr<PackMapEnvDataSkyCardsV74> skyCards;
		    "skyCards", "uint32",
		    //helpers::Ptr<PackMapEnvDataSpawnGroupsV45> spawns;
		    "spawns", "uint32",

		    //helpers::RefList<PackMapEnvDataWaterV74> water;
		    "water",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataWindV74> wind;
		    "wind",["[]","uint32",2],

		    //helpers::RefList<PackMapEnvDataAudioV45> audio;
		    "audio",["[]","uint32",2],

		    //helpers::WString name;
		    "name", "uint32",

		    //helpers::Array<byte> nightMods;
		    "nightMods",["[]","uint32",2],

		    //qword bindTarget;
		    "bindTarget",["[]","uint32",2],

		    //helpers::WString reserved;
		    "reserved", "uint32",

		    //byte type;
		    "type", "uint8",

		    //qword guid;
		    "guid",["[]","uint32",2],

		    //helpers::Array<PackMapEnvDataShapeV45> shapeArray;
		    "shapeArray",["[]","uint32",2],
		])*/,

    //helpers::Ptr<PackMapEnvDataGlobalV74> dataGlobal;
    "dataGlobal", Utils.getPointerReader([

			//helpers::Array<PackMapEnvDataLightingV74> lighting;
			"lighting",Utils.getArrayReader([
					//helpers::RefList<PackMapEnvDataLightV74> lights;
					"lights", Utils.getRefArrayReader([
						//byte3 color;
						"color", ["[]", "uint8", 3],

					    //float intensity;
					    "intensity", "float32",

					    //float3 direction;
					    "direction", ["[]", "float32", 3],
					]),
				    
				    //float shadowInfluence;
				    "shadowInfluence", "float32",
				    
				    //byte3 backlightColor;
				    "backlightColor", ["[]", "uint8", 3],

				    //float backlightIntensity;
				    "backlightIntensity", "float32",
				]),
		    
		    //helpers::Array<PackMapEnvDataLightingCharGroupV45> lightingCharGroups;
		    "lightingCharGroups",["[]","uint32",2],
		    
		    //helpers::Ptr<PackMapEnvDataCloudsV74> clouds;
		    "clouds", "uint32",

		    //helpers::RefList<PackMapEnvDataColoredLightRingsV45> coloredLightRings;
		    "coloredLightRings",["[]","uint32",2],
		    //helpers::RefList<PackMapEnvDataEffectV74> effect;
		    "effect",["[]","uint32",2],
		    
		    //helpers::RefList<PackMapEnvDataHazeV74> haze;
		    "haze",Utils.getRefArrayReader([
		    		//byte4 nearColor;
		    		"nearColor", ["[]", "uint8", 4],/// B G R A

				    //byte4 farColor;
				    "farColor", ["[]", "uint8", 4],/// B G R A

				    //float2 distRange;
				    "distRange", ["[]", "float32", 2],

				    //byte4 heightColor;
				    "heightColor", ["[]", "uint8", 4],

				    //float2 heightRange;
				    "heightRange", ["[]", "float32", 2],

				    //float depthCue;
				    "depthCue", "float32",

				    //float2 sunDirRange;
				    "sunDirRange", ["[]", "float32", 2],

		    	]),


		    //helpers::RefList<PackMapEnvDataPFieldV74> particleFields;
		    "particleFields",["[]","uint32",2],

		    //helpers::Array<PackMapEnvDataPFieldCutoutV45> particleFieldCutouts;
		    "particleFieldCutouts",["[]","uint32",2],

		    //helpers::Ptr<PackMapEnvDataSkyV74> sky;
		    "sky", Utils.getPointerReader([
			    	//byte flags;
			    	"flags", "uint8",

				    //float dayBrightness;
				    "dayBrightness", "float32",

				    //float dayHazeBottom;
				    "dayHazeBottom", "float32",

				    //float dayHazeDensity;
				    "dayHazeDensity", "float32",

				    //float dayHazeFalloff;
				    "dayHazeFalloff", "float32",

				    //float dayLightIntensity;
				    "dayLightIntensity", "float32",

				    //float dayStarDensity;
				    "dayStarDensity", "float32",

				    //float nightBrightness;
				    "nightBrightness", "float32",

				    //float nightHazeBottom;
				    "nightHazeBottom", "float32",

				    //float nightHazeDensity;
				    "nightHazeDensity", "float32",

				    //float nightHazeFalloff;
				    "nightHazeFalloff", "float32",

				    //float nightLightIntensity;
				    "nightLightIntensity", "float32",

				    //float nightStarDensity;
				    "nightStarDensity", "float32",

				    //float verticalOffset;
				    "verticalOffset", "float32",
		    	]),
		    
		    //helpers::Ptr<PackMapEnvDataSkyCardsV74> skyCards;
		    "skyCards", Utils.getPointerReader([
		    	 //helpers::Array<PackMapEnvDataSkyCardV74> cards;
		    	 "cards", Utils.getArrayReader([
		    	 		// PackMapEnvDataSkyCardAttributesV74 day;
		    	 		"day", skycardAttributesDef,
					    // PackMapEnvDataSkyCardAttributesV74 night;
					    "night", skycardAttributesDef,
					    // dword flags;
					    "flags", "uint32",
					    // helpers::WString name;
					    "name", "uint32"
		    	 	])
		    ]),

		    //helpers::Ptr<PackMapEnvDataSpawnGroupsV45> spawns;
		    "spawns", "uint32",

		    //helpers::RefList<PackMapEnvDataWaterV74> water;
		    "water",["[]","uint32",2],
		    //helpers::RefList<PackMapEnvDataWindV74> wind;
		    "wind",["[]","uint32",2],
		    //helpers::RefList<PackMapEnvDataAudioV45> audio;
		    "audio",["[]","uint32",2],

		    //helpers::WString name;
		    "name", "uint32",
		    
		    //helpers::Array<byte> nightMods;
		    "nightMods",["[]","uint32",2],
		    
		    //qword bindTarget;
		    "bindTarget",["[]","uint32",2],

		    //helpers::WString reserved;
		    "reserved", "uint32",

		    /// NOT PART OF GW2 FORMATS, BUT DATA IN SKY MODE TEX
		    /// DOES NOT MAKE SENSE WIHTOUT SHIFTING...
		    "reserved2", "uint32",

		    //helpers::Array<PackMapEnvDataSkyModeTexV74> skyModeTex;
		    //
		    //
		    //KHYLO gets something that could NOT be length, pointer here!.. .like
		    // 2868 , 4
		    "skyModeTex", /*["[]","uint32",2]*/Utils.getArrayReader([
		    		//helpers::FileName texPathNE;
		    		"texPathNE", Utils.getFileNameReader(),
				    //helpers::FileName texPathSW;
				    "texPathSW", Utils.getFileNameReader(),
				    //helpers::FileName texPathT;
				    "texPathT", Utils.getFileNameReader(),
		    	])/**/,

		    //helpers::FileName starFile;
		    "starFile", Utils.getFileNameReader()
    	])
];