var MapFile = require("./file/MapFile.js");
var SceneUtils = require("../util/SceneUtils.js");

function unzipBlob(blob, callback, onProgress) {
  // use a zip.BlobReader object to read zipped data stored into blob variable
  zip.createReader(new zip.BlobReader(blob), function(zipReader) {
    
    // get entries from the zip file
    zipReader.getEntries(function(entries) {
      
      // get data from the first file
      entries[0].getData(
        new zip.BlobWriter("text/plain"),
        function(data) {
          
          // close the reader and calls callback function with uncompressed data as parameter
          zipReader.close();
          callback(data);
          
        },
        onProgress);
    });
  }, onerror);
}

function zipBlob(filename, blob, callback) {
  // use a zip.BlobWriter object to write zipped data into a Blob object
  zip.createWriter(new zip.BlobWriter("application/zip"), function(zipWriter) {
    // use a BlobReader object to read the data stored into blob variable
    zipWriter.add(filename, new zip.BlobReader(blob), function() {
      // close the writer and calls callback function
      zipWriter.close(callback);
    });
  }, onerror);
}


var saveData = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    return function (blob, fileName) {        
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
    };
}());

var Map = function(fileName, callback){
  this.chunkHeaders = [];
  this.ds = null; /// Set data stream to null before loading.
  this.fileHeader = null;
  this.fileName = fileName;
  
  
  var self=this;
  this.load(fileName,function(ds){
    self.file = new MapFile(ds, 0);   
    callback();
  });
};



Map.prototype.load = function(fileName, callback){
  
  
  function setUpDatastream(arrayBuffer){
    /// Initiate data stream object
    var ds =  new DataStream(arrayBuffer);
      ds.endianness = DataStream.LITTLE_ENDIAN;
      
      callback(ds);
  }

  /// If the fileName is an array buffer just read it!
  if ( fileName instanceof ArrayBuffer) {
    window.setTimeout(setUpDatastream, 1, fileName);
    return;
  }

  var oReq = new XMLHttpRequest();
  oReq.open("GET", fileName, true);
  oReq.responseType = "arraybuffer";
  
  /// Bind onload to handle data
  var that = this;

  var lastP = -1;
  var output = $("#progressPanel");

  SceneUtils.showProgressPanel();




  oReq.onprogress = function updateProgress (oEvent) {
      if (oEvent.lengthComputable) {
      var p = Math.ceil(100 * oEvent.loaded / oEvent.total);
      if(p != lastP){
        var size = Math.ceil(10*oEvent.total / (1024*1024))/10 + "MB";
        output.find('.title').html("Downloading map "+size);
        output.find('.progress').html(p+"%");
          lastP = p;
        }
        // ...
      } else {
        // Unable to compute progress information since the total size is unknown
      }
  }

  oReq.onload = function (oEvent) {

    /// Let's hope we got an array buffer
    var arrayBuffer = oReq.response;
    if (arrayBuffer) {
      
        /// Check file needs to be unzipped
        var nameParts = fileName.split(".");

        /// Current buffer needs to be unzipped
        if(nameParts.length>1 && nameParts[nameParts.length-1] == "zip"){
          
          var lastP = -1;
          unzipBlob(new Blob([arrayBuffer]),
            function(unzippedBlob){

              /// Blob is unzipped, connect resulting buffer to the maps DataStream

              /// Cast Blob to bufffer:
              var fileReader = new FileReader();
            fileReader.onload = function() {

              setUpDatastream(this.result)

            };
            fileReader.readAsArrayBuffer(unzippedBlob);

            } /// End unzip handler
            , function(current, total){
              var p = Math.round(current*100/total);
              if( p != lastP){
                output.find('.title').html("Unzipping");
              output.find('.progress').html(p+"%");
                lastP = p;
              }
                
            } /// End unzip progress handler
          );
        }

        /// Current buffer can be connected directly
        else{

          setUpDatastream(arrayBuffer)
        }

        
    };
  };
  
  /// Send the request
  oReq.send(null);
};

Map.prototype.download = function(){
  var buffers = [];


  /// Push file header
  buffers.push( this.file.ds.buffer.slice(0,  this.file.headerLength) );

  var self = this;

  function appendChunk(name){
    var chunk = self.file.getChunk(name);
    if(chunk){

        var begin = chunk.addr;

        /// For some reason chunks are separated by 8 bytes...
        /// This is part of my chunk header implementation, perhaps I'm paring them wrong...
        var end = begin + chunk.header.chunkDataSize + 8;

        var buffer = chunk.ds.buffer.slice(begin, end);

        /// Concat buffers
        buffers.push(buffer);
    }
  }

  /// Append buffers
  ["havk","zon2","trn","parm"].forEach(appendChunk);

  /// Build blob
  var blob = new Blob(buffers, {type: "octet/stream"});

  /// Zip blob
  zipBlob("gw2map", blob, function(zippedBlob){

    /// Save zip
    var fileName = self.fileName.split("/");
    fileName = fileName[fileName.length-1];
    saveData(zippedBlob, fileName+".zip");
  });
}

module.exports = Map;