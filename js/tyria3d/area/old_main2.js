/// Globals variables
var scene, camera, controls, renderer,
geometry, material, mesh,
diffuseMap, glowMap, normalMap,
directionalLight, ambientLight;

/// Settings

var shamanPath = 'img/map.jpg';
var shamanLabelsPath = 'img/map_labels.jpg';
var originalPath = 'img/overlayed.jpg';
var modelPath = 'models/cube.obj';


__proto__: Object
var cameraOpts = {
	fov : 60,
	near : 10,
	far : 1000000,
	pos : {
		x: 6650.453486662317,
		y: -17031.559807482816,
		z: 46185.65446727842
	}
},
renderOpts = {
	antialias : true,
	alpha : true,
	clearClr : 0x000000,
	clearAlpha : .5
},
controlOpts = {
	damping : .2
},
lightOpts = {
	directionalClr : 0xaaaaaa,
	ambientClr : 0xffffff,
	directionalClrShaman : 0x888888,
	ambientClrShaman : 0x55555,
	directionalPos : {
		x : 200,
		y : 200,
		z : 200
	},
	glow : true,
	normals : true
};

var p = new BinaryParser(false, false);


function uint32ToHex(v){
	var arrayBuffer = new Uint32Array(1);
	var ds = new DataStream(arrayBuffer);
  	
  	ds.endianness = ds.SMALL_ENDIAN;
  	ds.writeUint32(v);
  	ds.seek(0);
  	
  	console.log(
  		ds.readUint8().toString(16),
		ds.readUint8().toString(16),
  		ds.readUint8().toString(16),
  		ds.readUint8().toString(16) );
  	
}

function float32ToHex(v){
	var ds = new DataStream();
  	
  	ds.endianness = ds.SMALL_ENDIAN;
  	ds.writeFloat32(v/32);
  	ds.seek(0);
  	
  	
  	var s1 = ds.readUint8().toString(16),
  	s2 = ds.readUint8().toString(16),
  	s3 = ds.readUint8().toString(16),
  	s4 = ds.readUint8().toString(16);
  	
  	console.log(s4, s3,	s2, s1 );
}



function hexToFloat(v){
	return p.toFloat(hex2bin(v));
}
function hexToInt(v){
	return p.toInt(hex2bin(v));
}
function hexToWord(v){
	return p.toWord(hex2bin(v));
}
function hexToDWord(v){
	return p.toDWord(hex2bin(v));
}
function hexToDouble(v){
	return p.toDouble(hex2bin(v));
}

function hex2bin(s){
	for(var i = 0, l = s.length, r = ""; i < l; r += String.fromCharCode(parseInt(s.substr(i, 2), 16)), i += 2);
	return r;
}

function stringToFloat(s){
	for(var i=0;i<=s.length-8;i+=8){
		console.log(hexToFloat(s.substring(i,i+8)));
	}
}
function stringToDouble(s){
	for(var i=0;i<=s.length-16;i+=16){
		console.log(hexToDouble(s.substring(i,i+16)));
	}
}

var sc = 0;
var lastOK = false;
function stringToVectors(s,offset){
	//		3 651 672
	//		3.6 miljoner
	s = s.substring(offset);
	//s = s.substring(16 + 2199936+240000+24000*(8 + 1));
	console.log(s.substring(0,32));
	console.log(s.substring(s.length-32,s.length));
	var len = 8;
	var res = [];
	var maxlen = 140000000;
	var parse = hexToFloat;
	var p1,p2,p0;
	var i=0;
	for(i=0;i<=s.length-len*3 && i/len<= maxlen;i+=len*3){
		
		p0 = s.substring(	i,			i+len);
		p1 = s.substring(	i+len,		i+len*2);
		p2 = s.substring(	i+len*2,	i+len*3);
		
		var v0 = parse(p0),
		v1 = parse(p1),
		v2 =-parse(p2);
		
		if(Math.abs(v0)<0.1 || Math.abs(v1)<0.1 || Math.abs(v2)<0.1){
			if(lastOK)
				return res;			
			console.log(s.substring(	i,			i+len*3));
			continue;
		}
		
		console.log("OK");
		lastOK=true;
		i+=len*3*3;
		
		
		res.push( new THREE.Vector3(v0, v1, v2) );
	}
	
	console.log(p0+p1+p2)
	return res;
}


init();
var parse = new BinaryParser(false,false);
function init() {

	/// Create scene instance
	scene = new THREE.Scene();

	/// Create a camera, aim and place it.
	camera = new THREE.PerspectiveCamera(cameraOpts.fov,
		window.innerWidth / window.innerHeight,
		cameraOpts.near, cameraOpts.far);
		
	camera.position.set(cameraOpts.pos.x, cameraOpts.pos.y, cameraOpts.pos.z);

	/// Camera Orbit control
	controls = new THREE.OrbitControls(camera);
	controls.damping = controlOpts.damping;
	controls.addEventListener('change', render);

	// Add ambient lighting
	ambientLight = new THREE.AmbientLight(lightOpts.ambientClr);
	scene.add(ambientLight);
	
	
	particles = new THREE.Geometry();
	
	
	$.ajax( "models/rawdata.js",{
	//$.ajax( "models/divinity4.js",{
	//$.ajax( "models/dr.data",{
		cache:false,
		dataType:"text",
		success :
			function(  response,  textStatus ){
				console.log( textStatus);
				console.log( response.length);
				
				var data = response.replace(/(\r\n|\n|\r)/gm,"");
				arr = stringToVectors(data,2520616);
				//arr = stringToVectors(data,24);
				
				for( var i = 0; i < arr.length; i++ ){
					particles.vertices.push( arr[i] );
				}
				
				
				pMaterial = new THREE.ParticleBasicMaterial({
			    	color: 0xFF0000,
		    		size: 2
			    });
			    
			    console.log("PArticle count: "+particles.vertices.length);
				
				// create the particle system
				var particleSystem = new THREE.ParticleSystem(
			    	particles,
			    	pMaterial);
			
				// add it to the scene
				scene.add(particleSystem);
				render();
			}
		}
	);
	
	
	
	
	
	/*var vertices = arr;
	var holes = [];
	var triangles, mesh;
	var geometry = new THREE.Geometry();
	var material = new THREE.MeshBasicMaterial();
	material.side = THREE.DoubleSide;

	geometry.vertices = vertices;

	triangles = THREE.Shape.Utils.triangulateShape ( vertices, holes );

	for( var i = 0; i < triangles.length; i++ ){
	
	    geometry.faces.push( new THREE.Face3( triangles[i][0], triangles[i][1], triangles[i][2] ));
	
	}

	mesh = new THREE.Mesh( geometry, material );
	
	scene.add(mesh);*/
	
	
	/// Create canvas rendering output
	renderer = new THREE.WebGLRenderer({
	});
	renderer.setSize(window.innerWidth, window.innerHeight);
	//renderer.setClearColor(renderOpts.clearClr, renderOpts.clearAlpha);

	///Add renderer to html DOM
	document.body.appendChild(renderer.domElement);
	
	
}

function render() {
	renderer.render(scene, camera);
}




