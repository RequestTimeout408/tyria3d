var Utils = require('../util/ParserUtils.js');
var File = require('../format/file/File.js');
var MapFileList =  require('../MapFileList');
var SceneUtils = require('../util/SceneUtils');


/* TODO: move! */
var base32Max = Math.pow(2,32);
function arr32To64(arr){
	/// Re-read as uint64 (still little endian)
	/// Warn: this will not work for ~50+ bit longs cus all JS numbers are 64 bit floats...
	return base32Max*arr[1] + arr[0];
};


function sort_unique(arr_in, comparator) {
	var arr = Array.prototype.sort.call(arr_in, comparator);
   
    var u = {}, a = [];
	for(var i = 0, l = arr.length; i < l; ++i){
		if(u.hasOwnProperty(arr[i])) {
			continue;
		}
		a.push(arr[i]);
		u[arr[i]] = 1;
	}

	return a;
}

var LocalReader = function(datFile, callback){

	/// HTML pNaCl emed elements
	this.NaClInflater = document.getElementById('t3dgwtools');

	var self = this;

    /// Set up a listener for any messages passed by the pNaCl component
    document.getElementById('pNaClListener').addEventListener(
    	'message',
    	function(message_event){
    		self.NaClListener.call(self, message_event);
    	},
    	true
	);


    this.fileListeners = [];
	this.dat = datFile;
	this.onFullyLoaded = function(){

		var mapList = self.loadMapList(datFile);

		if(mapList){
			self.applyMapList(mapList);
		}
		else{
			self.readMapList(false);	
		}

		callback();
	};

	///Read dat file header, 40 bytes should always be the length
	this.loadFilePart(this.dat, 0, 40, this.readANDatHeader);
};

LocalReader.prototype.NaClListener = function(message_event){
	if( typeof message_event.data === 'string' ) {
		console.warn("NaCl threw an error",message_event.data);
		return;
	}

	
	//console.log("Got back a DS from NaCl RAW", message_event.data);
	//console.log("Got back a DS from NaCl Uint32Array", new Uint32Array(message_event.data));
	var handle = message_event.data[0];

	if(this.fileListeners[handle]){
		this.fileListeners[handle].forEach(function(callback){
			var data = message_event.data;
			/// Array buffer, dxtType, imageWidth, imageHeigh
			
			callback(data[1], data[2], data[3], data[4]);	
		});

		// Remove triggered listeners
		this.fileListeners[handle] = null;
	}
	
}


LocalReader.prototype.readANDatHeader = function(ds){


	/// Read file header data struct
	this.fileHeader = ds.readStruct(_ANDatDef);

	console.log("Loaded Main .dat header", this.fileHeader);

	/// Get pointer to MFT chunk header
	this.fileHeader.mftOffset =  arr32To64(this.fileHeader.mftOffset);

	/// Load MFT
	this.loadFilePart(
		this.dat,
		this.fileHeader.mftOffset,
		this.fileHeader.mftSize,
		this.readMFTHeader );

};

LocalReader.prototype.readMFTHeader = function(ds){

	/// Read MFT header data struct
	/// Global variable mft
	this.mft = ds.readStruct(_MFTDef);

	var entryStartPtr = ds.position;
	var numEntires = this.mft.nbOfEntries;

	/// MFT has entries with offset, size and compression flag
	/// for all files in the .dat

	/// Read all entry offsets and sizes into uint32 arrays
	this.mft.entryDict = {
		offset_0:new Uint32Array(numEntires),
		offset_1:new Uint32Array(numEntires),
		offset:new Float64Array(numEntires),
		size:new Uint32Array(numEntires),
		compressed:new Uint16Array(numEntires),
	}


	/// Read offset, size and compressed flag of the entries.
	//console.log("reading MFT entries ");	
	for(var i=0; i<numEntires-1; i++){
		
		/// Read first 14 bytes
		this.mft.entryDict.offset_0[i] = ds.readUint32();
		this.mft.entryDict.offset_1[i] = ds.readUint32();
		this.mft.entryDict.size[i] = ds.readUint32();
		this.mft.entryDict.compressed[i] = ds.readUint16();


		this.mft.entryDict.offset[i] =  arr32To64(
			[ this.mft.entryDict.offset_0[i],
			  this.mft.entryDict.offset_1[i] ]
		);

		/// Skip 10
		ds.seek(ds.position + 10);

	}
	//console.log(".done reading MFT entries!");	
	console.log( "Finished indexing MFT ", this.mft );


	/// Read data pointed to by 2nd mft entry
	/// This entry maps file ID to MFT index	
	var offset = arr32To64(
		[ this.mft.entryDict.offset_0[1],
		  this.mft.entryDict.offset_1[1] ]
	);
	var size = this.mft.entryDict.size[1];

	this.loadFilePart(this.dat, offset, size, this.readMFTIndexFile);

};


LocalReader.prototype.readMFTIndexFile = function(ds, size){
	
	//console.log("mapping MFT index to file ID");

	var length = size / 8;

	/// fileIdTable
	var ids = new Uint32Array(length);
	var mftIndices = new Uint32Array(length);

	/// m_entryToId
	var m_entryToId_baseId = new Uint32Array(length);
	var m_entryToId_fileFileId = new Uint32Array(length);

	
	for(var i=0; i<length; i++){
		ids[i] = ds.readUint32();
		mftIndices[i] = ds.readUint32();
	}
	
	/// Raw map of "ID" to mft index
	this.mft.id2index = {
		ids:ids,
		mftIndices:mftIndices
	}

	/// m_entryToId has both base and filed id
	for(var i=0; i<length; i++){


		if (ids[i] == 0 && mftIndices[i] == 0) {
            continue;
        }
 		
 		var entryIndex = mftIndices[i];
        var entry     = {
        	fileId : m_entryToId_fileFileId[entryIndex],
        	baseId : m_entryToId_baseId[entryIndex]
        }

        if (entry.baseId == 0) {
            entry.baseId = ids[i];            
        } else if (entry.fileId == 0) {
            entry.fileId = ids[i];
        }

        if (entry.baseId > 0 && entry.fileId > 0) {
            if (entry.baseId > entry.fileId) {
            	//std::swap(entry.baseId, entry.fileId);
            	var temp = entry.baseId;
            	entry.baseId = entry.fileId;
            	entry.fileId = temp;                
            }
        }

        //Write back
        m_entryToId_fileFileId[entryIndex] = entry.fileId;
        m_entryToId_baseId[entryIndex] = entry.baseId;
	}

	this.mft.m_entryToId = {
		baseId:m_entryToId_baseId,
		fileId:m_entryToId_fileFileId
	}

	//console.log("...done mapping MFT index to file/base ID!");

	if(this.onFullyLoaded)
		this.onFullyLoaded();
};

LocalReader.prototype.applyMapList = function(mapList){

	
	// Fuck up UI
	var picker = $("#mapPicker");
	picker.empty();
	picker.append($("<option selected='true' disabled='disabled'>Pick Map</option>")); 
	

	var compareName = function(a, b) {
		if (a.name < b.name)
		    return -1;
		if (a.name > b.name)
		    return 1;
		return 0;
	};

	mapList.maps.sort(compareName);
	
	mapList.maps.forEach(function(g){
		var group = $("<optgroup label='"+g.name+"' />");
		picker.append(group);

		g.maps.sort(compareName);
		g.maps.forEach(function(m){
			group.append("<option value='"+m.fileName+"'>"+m.name+"</option>");
		});
		
	});

	
	SceneUtils.showMapPanel();
	
}

LocalReader.prototype.loadMapList = function(datFile){
	var mapName = "mapList_1.0.2." + datFile.name + "_" + datFile.lastModified + "_" + datFile.size;
	var str = localStorage.getItem(mapName);
	if(!str)
		return null;
	try{
		return JSON.parse(str);	
	}
	catch(e){
		
	}
	return null;
	
}

LocalReader.prototype.storeMapList = function(datFile, mapList){
	var mapName = "mapList_1.0.2." + datFile.name + "_" + datFile.lastModified + "_" + datFile.size;
	localStorage.setItem(mapName, JSON.stringify(mapList) );
}

LocalReader.prototype.readMapList = function(searchAll){
	var self = this;
	SceneUtils.showProgressPanel(function(){
		
		var output = $("#progressPanel");
		output.find(".title").html("Finding maps (first visit only)");
		output.find(".progress").html("initializing");

		setTimeout(function(){
			self.doReadMapList(searchAll)
		},10);
	});
}

LocalReader.prototype.doReadMapList = function(searchAll){

	var self = this;
	var datFile = this.dat;

	var comparator = function(a, b) {
		return self.mft.entryDict.offset[a] - self.mft.entryDict.offset[b];
	};

	var mftIndices = [];
	

	/// Only look for known maps
	if(!searchAll){

		MapFileList.maps.forEach(function(mapCol){
			mapCol.maps.forEach(function(mapEntry){
				var entryBaseId = mapEntry.fileName.split(".")[0];
				var mftIndex = self.getFileIndex(entryBaseId)
				mftIndices.push(mftIndex);
			});
		});

		
	}
	else{
		mftIndices = this.mft.id2index.mftIndices;
	}

	var uniqueIdxs = sort_unique(mftIndices, comparator);
	
	//console.log("All indecies", this.mft.id2index.mftIndices);
	//console.log("Unique indecies", uniqueIdxs);


	/// N = 1 => 17s
	/// N = 2 => 11s
	/// N = 4 => 101s
	/// N = 8 => 83s
	var N = 8; 

	var t = new Date().getTime();
	var cb = function(result){
		var dt = new Date().getTime() - t;
		console.log("Time elapsed ", Math.round(0.001*dt)," seconds");	


		/// Clear listeners used during indexing
		self.fileListeners = [];


		var localMapList = {maps:[]};



		result.forEach(function(mftIndex){

			/// Base Id is used by gw2browser and is therefore the de facto identifier
			/// in the community. Let's use it here too!
			var baseId = self.mft.m_entryToId.baseId[mftIndex+1];

			/// Hack to avoid releasing maps newer than VB
			if(baseId > /*969663*/ 11542000){
				return;
			}

			var name = "";
			var group = "";

			MapFileList.maps.forEach(function(mapCol){
				mapCol.maps.forEach(function(mapEntry){
					if(name.length == 0 && mapEntry.fileName.indexOf(baseId+".")>=0){
						name = mapEntry.name;
						group = mapCol.name;
					}
				});
			});

			if(name.length == 0){
				name = "Unknown map "+baseId;
			}

			if(group.length == 0){
				group = "Unknown maps";
			}

			var localGroup = null;
			localMapList.maps.forEach(function(mapCol){
				if(mapCol.name == group){
					localGroup = mapCol;
				}
			});

			if(!localGroup){
				localGroup = {name:group,maps:[]};
				localMapList.maps.push(localGroup);
			}

			localGroup.maps.push({fileName:baseId, name:name});
			
		});
		
		
		/// Store 
		self.storeMapList(datFile, localMapList);

		self.applyMapList(localMapList);

	}

	var maxLength = uniqueIdxs.length; //100000
	console.log("Scanning ",maxLength, " files for maps...");
	

	this.findType(uniqueIdxs, "mapc", 0, maxLength, N, cb);

	//this.findType(uniqueIdxs, "mapc", Math.round(maxLength*0.95), maxLength, N, cb);
	//this.findType(uniqueIdxs, "mapc", 1, 10000, N, cb);

	



}

LocalReader.prototype.findType = function(uniqueIdxs, type, start, length, N ,callback){

	var self = this;
	
	var maxlen = start+length;

	/// 6 maps first 10 000
	
	var lastPct = 0;

	var pctM = 100.0 / Math.min(length, uniqueIdxs.length - start);
	var threadsDone = 0;

	var result = [];

	var peekCallback=function(inflatedData, i, mftIndex){
		if(!inflatedData){
			console.warn("No infalted data for entry");
			readUniqueId(i+N);
			return;
		}

		var ds = new DataStream(inflatedData);
		var file = new File(ds, 0);

		/// We're looking for mapc files
		if(file.header.type == type){
			//console.log(file.header.type);
			result.push(mftIndex);
		}

		readUniqueId(i+N);
	}

	var output = $("#progressPanel");
	var readUniqueId = function(i){

		if(i%N==0){
			var pct = Math.min(100, Math.floor( (i-start) * pctM) );
			if(lastPct != pct){
				output.find(".title").html("Finding maps (first visit only)");
				output.find(".progress").html(pct+"%");
				lastPct = pct;
			}
		}

		if( i>=uniqueIdxs.length || i>=maxlen ){
			console.log("Thread ",i%N+" done");
			threadsDone++;
			if(threadsDone == N){
				callback(result);
			}
			return;
		}

		var mftIndex = uniqueIdxs[i];

		var compressed = self.mft.entryDict.compressed[mftIndex];
		if(!compressed){
			//console.log("File is NOT compressed");
			readUniqueId(i+N);
			return;
		}

		var handle = mftIndex;
		var offset = self.mft.entryDict.offset[mftIndex];
		var size = self.mft.entryDict.size[mftIndex];

		if(size<0x20){
			//console.log("File is too small");
			readUniqueId(i+N);
			return;
		}

		/// Read .dat file part
		self.loadFilePart(self.dat, offset, size,
			function(ds, _size){
				
				///Infalte
				self.inflate(
					ds,
					_size,
					handle,
					///Infaltion  callback
					function(inflatedData){
						peekCallback(inflatedData, i, mftIndex);
					}, 
					false, 0x20
				);


		});

	}

	for(var n=0; n<N; n++){
		readUniqueId(start + n);	
	}
};

LocalReader.prototype.getFileIndex = function(baseOrFileId){
	var index = -1;

	/// Get by base id
	
	for(var i = 0; i<this.mft.m_entryToId.baseId.length; i++){
		if( this.mft.m_entryToId.baseId[i+1] == baseOrFileId ){	
			index = i;
			//console.log("Found BASE ID "+baseId +" at INDEX "+i);
		}
	}

	/// Get by file id
	if(index==-1){
		for(var i = 0; i<this.mft.m_entryToId.fileId.length; i++){
			if( this.mft.m_entryToId.fileId[i+1] == baseOrFileId ){	
				index = i;
				//console.log("Found FILE ID "+baseId +" at INDEX "+i);
			}
		}	
	}

	return index;
}

LocalReader.prototype.loadTextureFile = function(baseId, callback){
	this.loadFile(baseId, callback, true);
}
LocalReader.prototype.loadFile = function(baseId, callback, isImage, raw){
	
	var self = this;

	var index = this.getFileIndex(baseId);
	var mftIndex = index;//this.mft.id2index.mftIndices[index];

	//If the file doesn't exist still make sure to fire the callback
	if(mftIndex <= 0 ){
		console.log("Could not find file with baseId ",baseId);
		callback(null);
		return;
	}

	//console.log("fetchign  baseId ",baseId);
	
	var offset = arr32To64(
		[ this.mft.entryDict.offset_0[mftIndex],
		  this.mft.entryDict.offset_1[mftIndex] ]
	);

	var size = this.mft.entryDict.size[mftIndex];
	//console.log("File at found index is "+ size +" bytes");
	
	var compressed = this.mft.entryDict.compressed[mftIndex];
	if(!compressed){
		console.log("File is NOT compressed");
		
		callback(null);
		return;
	}

	/// Read map and pass the ds to our pNaCL inflate function
	//TODO: will this work?? Shared base id's and all that...
	var handle = index;

	this.loadFilePart(this.dat, offset, size, function(ds, size){
		
		/// If the raw param was passed return the un-inflated data
		if(raw){
			callback(ds);
		}

		/// If raw parameter was not passed or false, inflate first
		else{
			self.inflate(ds, size, handle, callback, isImage);	
		}
		
	});
};


LocalReader.prototype.inflate = function(ds, size, handle, callback, isImage, capLength){
	
	
	var arrayBuffer = ds.buffer;	

	if(!capLength){
		//capLength = arrayBuffer.byteLength;
		capLength = 1;
	}


	//console.log("Uncompressing file size ",size, "handle", handle, "cap", capLength, "isImage", isImage);
		

	

	if(arrayBuffer.byteLength < 12){
		console.log("not inflating, length is too short ("+arrayBuffer.byteLength+")", handle);
		callback(null);
		return;
	}
	if(handle == 123350 || handle == 123409 || handle == 123408 ){
		callback(null);
		return;
	}

	/// Register listener for file load callback
	if(this.fileListeners[handle]){
		this.fileListeners[handle].push(callback);

		///No need to make another call, just wait for callback event to fire.
		return;
	}
	else{
		this.fileListeners[handle] = [callback];	
	}
	
    
    /// Call pNaCl component with the handle and arrayBuffer as an arguments

    //console.log("posting",[handle,arrayBuffer,isImage===true])
	this.NaClInflater.postMessage([handle,arrayBuffer,isImage===true, capLength]);
};

/// Reads bytes from a big file. Uses offset and length (in bytes) in order to only load
/// parts of the file that are used.
LocalReader.prototype.loadFilePart = function(file, offset, length, callback){
	var self = this;

	var reader = new FileReader();
		
	reader.onerror = function(fileEvent){
		debugger;
	}
	reader.onload  = function(fileEvent){

		var buffer = fileEvent.target.result;
		var ds = new DataStream(buffer);
	  	ds.endianness = DataStream.LITTLE_ENDIAN;

	  	/// Pass data stream and data length to callback function, keeping "this" scope
	  	callback.call(self, ds, length);
	}
	
  	var start = offset;
	var end = start + length;
	reader.readAsArrayBuffer(file.slice(start, end));
};


/* Structure definitions*/

// AN file header
var _ANDatDef = [
	//uint8_t  version;
	"version","uint8",						

    //uint8_t  magic[3];
    "magic", "string:3",					

    //uint32_t headerSize;
    "headerSize","uint32",					

    //uint32_t unknown1;
    "unknown1","uint32",					

    //uint32_t chunkSize;
    "chunkSize","uint32",					
    
    //uint32_t crc;
    "crc","uint32",							
    
    //uint32_t unknown2;
    "unknown2","uint32",					
    
    //uint64_t mftOffset;
    "mftOffset",[ "[]","uint32", 2 ],
    
    //uint32_t mftSize;
    "mftSize","uint32",
    
    //uint32_t flags;
    "flags","uint32",
];

var _MFTDef = [
	//uint8_t  magic[4];
	"magic","string:4",
		
	//uint64_t unknown1;
	"unknown1",[ "[]","uint32", 2 ],

	//uint32_t nbOfEntries;
	"nbOfEntries","uint32",

	//uint32_t unknown2;
	"unknown2","uint32",
	
	//uint32_t unknown3;
	"unknown3","uint32",
];


module.exports = LocalReader;