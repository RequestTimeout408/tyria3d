var PagedImageTableDataChunkHeaderParser = require("./../parsers/pimg/PagedImageTableDataChunkHeaderParser.js");


var PagedImageTableDataChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

PagedImageTableDataChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = PagedImageTableDataChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = PagedImageTableDataChunkHeader;