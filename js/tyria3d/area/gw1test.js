var LocalReader =  require("./API/LocalReader/LocalReader.js");
var SceneUtils = require("./SceneUtils.js");

module.exports = {
	chunks:[],

	readAllChunks : function(ds, firstChunkAddr){

		var nextChunkAddr = firstChunkAddr;

		/// Read until end of file
		while(ds.byteLength>nextChunkAddr){

			/// Move data cursor
			ds.seek(nextChunkAddr);

			/// Read chunk header at cursor
			var chunk = this.readNextChunk(ds);
		
			/// Add chunk to collection
			this.chunks.push(chunk);

			/// Calculate position of next chunk
			nextChunkAddr =  chunk.addr + chunk.headerSize + chunk.dataSize;
		}

		console.log("All chunks", this.chunks);

	},

	readNextChunk : function(ds){
		
		var origP = ds.position;

		/// 4 byte tag or smthn
		var headerTag = ds.readUint32();

		/// 4 byte pointer to next
		var chunkSize = ds.readUint32();

		/// 4 chunk type ID (cmp'd in exe)
		var chunkType = ds.readUint32();
		
		return {
			addr:origP,
			tag:headerTag,
			id:chunkType,
			headerSize: 12,
			dataSize:chunkSize-4
		};

	},

	getChunkById: function(id){
		for(var i=0;i<this.chunks.length; i++){
			if(this.chunks[i].id == id){
				return this.chunks[i];
			}
		}
		return null;
	},

	renderParams: function(ds){
		if(headerTag==536870924){ /// This is map params? perhaps? Yeah...!?
			/// Total size is 37!!!
			/// Flags? Guid?
			console.log("param int8", ds.readUint8() ); // Byte 1

			/// Map size
			console.log("param floats?", ds.readFloat32() ); // Byte 2 3 4 5
			console.log("param floats?", ds.readFloat32() ); // Byte 6 7 8 9
			console.log("param floats?", ds.readFloat32() ); // Byte 10 11 12 13
			console.log("param floats?", ds.readFloat32() ); // Byte 14 15 16 17

			///20 more!

			console.log("param int16", ds.readUint16() ); 
			console.log("param int16", ds.readUint16() ); 

			console.log("param int16", ds.readUint16() ); 
			console.log("param int16", ds.readUint16() ); 

			console.log("param int16", ds.readUint16() ); 
			console.log("param int16", ds.readUint16() ); 

			console.log("param int16", ds.readUint16() ); 
			console.log("param int16", ds.readUint16() ); 

			console.log("param int16", ds.readUint16() ); 
			console.log("param int16", ds.readUint16() ); 

			console.log("POS ",ds.position);
		}
	},



	renderPropPos: function(ds){

		var propChunk = this.getChunkById();


		var geometry1 = new THREE.Geometry();
		
		
		console.log("Pre data", ds.readUint32());
		console.log("Pre data", ds.readUint32());
		console.log("Pre data", ds.readUint32());
		console.log("Pre data", ds.readUint32());
		console.log("Pre data", ds.readUint32());
		

		var nextChunkOfs = ds.readUint32();
		var nextChunkAdr = ds.position + nextChunkOfs;
		console.log("nextArrayOfs", nextChunkOfs);
		console.log("nextArrayAdr", nextChunkAdr);

		var numPoints = ds.readUint32();
		console.log("nextArrayNumElems", numPoints);


		console.log("Pushing",numPoints, "verts from adr",ds.position);

		
		for(var i = 0; i<numPoints; i++){
			geometry1.vertices.push( readVertex(ds) );
			
			/// Skip 12 bytes of flags
			ds.seek(ds.position+4*3);
		}

		console.log("Finished pushing verts at adr", ds.position);
		console.log("----------------------");


		material1 = new THREE.PointCloudMaterial( { size: 105 } );
		material1.color.setHSL( 1.0, 0.3, 0.7 );

		

		var particles1 = new THREE.PointCloud( geometry1, material1 );
		SceneUtils.getScene().add(particles1);	

	},

	renderCUBE: function(ds){
			
		///Find terrain chunk
		var cubeChunk = this.getChunkById(1161975107);
		
		if(!cubeChunk){
			console.error("No CUBE chunk found.");
			return;
		}

		console.log("CUBE starts at",cubeChunk.addr, cubeChunk);

		/// Skip header
		ds.seek(cubeChunk.addr + cubeChunk.headerSize);

		/// Print 2 ints
		console.log("CUBE",ds.readUint32());
		console.log("CUBE",ds.readUint32());
	},

	renderTerrain: function(ds){

		var self = this;
			
		///Find terrain chunk
		var terrainChunk = this.getChunkById(2273448244);
		
		if(!terrainChunk){
			console.error("No terrain chunk found.");
			return;
		}

		console.log("Terrain starts at",terrainChunk.addr);

		/// Skip header
		ds.seek(terrainChunk.addr + terrainChunk.headerSize);

		/// Skip first 8 bytes
		ds.seek(ds.position + 8); ///is this always 11 00 00 00 ?	


		/// DIMs, for garden DIM1 / DIM2 are equal to 14/18
		/// 14 * 18 is the byte size divided by 4 * 32^2
		var dim1RAW = ds.readUint32();
		var dim2RAW = ds.readUint32();
		var dim1= dim1RAW / (32*32*8);
		var dim2= dim2RAW / (32*32*8);
		console.log("terrain DIM1", dim1);
		console.log("terrain DIM2", dim2);

		///Unknown terrain data
		///5 bytes always equal?
		console.log("terrain data", ds.readUint8());   // Always 0
		console.log("terrain data", ds.readFloat32()); // Always 24576

		console.log("terrain data", ds.readFloat32()); // Def float
		
		console.log("terrain data", ds.readUint16());	/// 2245 6405 10466 8421

		console.log("terrain data", ds.readFloat32());	/// Def float
		console.log("terrain data", ds.readFloat32());	/// Def float

		
		console.log("terrain data", ds.readUint8());	/// Always 1? Same as "Scrap byte" LOD level??


		/// Height map array starts with byte length, then elements
		var numBytes = ds.readUint32();

		console.log("array byte size", numBytes);
		//console.log("POS pre elevation",ds.position);

		/// Read elevations data
		var numPoints = numBytes / 4;
		var size = 5000;
		for(var j = 0; j<dim2; j++){
			for(var i = 0; i<dim1; i++){			
				var chunk = this.buildChunk(ds, size);
				chunk.position.x = size*i;
				chunk.position.z = size*j;

				SceneUtils.getScene().add(chunk);
			}
		}		

		/// There are 6 more arrays like this, could be images or elevation data again I guess...

		console.log("--------------- MOAR ARRAYS FOR TERRAIN ----------------------");
		console.log("--------------- MOAR ARRAYS FOR TERRAIN ----------------------");


		/*
			1 	Original height array of 32bit floats
			2	1/4 th of first
			3	1/16 th of first
			4	58 g,57 i, 54 d, 41(random dungeon)  Just 00 to X where x is length of array
			5	58 g,57 i, 54 d, 41(random dungeon)
			7	Slightly more than 1/4 th of first sometimes more (g) sometimes even less than 1/4 th
				Has sub arrays with their own length uint32. Possibly 0 padded?
			9 	1/4 th of first
		 */
		
		var stepArrays = function(show){
			//console.log("Stepping from ", ds.position);
			var  scrap = ds.readUint8();
			console.log("Scrap byte", scrap);
			var arrLen = ds.readUint32()
			console.log("Array length", arrLen, "start", ds.position);


			if(scrap == 4){
				/// Theory
				/// 1 byte scrap
				/// 4 byte total size
				/// 1 byte array length
				/// array uint8
			}

			if(scrap == 5){
				/// Theory
				/// 1 byte scrap
				/// 4 byte total size
				
				/// 1 byte array length
				/// array uint8
				
				/// 1 byte array length
				/// array uint8
				
				/// 4 byte unknown float32
			}

			if(scrap==7){
				console.log("Stepping from ", ds.position);

				var start = ds.position;
				var end = start + arrLen;

				var numArrays = 0;
				var arrayFreq = [];


				var all128s = new Uint8Array(128*dim1*dim2);
				var allIdx = 0;

				self.texCB=[];
				while(ds.position < end){

					var subArrayLen = ds.readUint32();



					self.texCB[numArrays] = function(dsCB, idx){
						var x = Math.floor(idx / dim1);
						var y = idx % dim1;
						var size = 64;
						renderAsImage(dsCB,size,size, x*32,y*32);
					}

					if(true){

						console.log("image data pos",ds.position, "len",subArrayLen);


						document.getElementById('t3dgwtools').postMessage([
							numArrays/* AKA IDX ABOVE */ ,
							ds.buffer.slice(ds.position,ds.position+subArrayLen+128),
							//ds.buffer.slice(ds.position+subArrayLen,ds.position+subArrayLen+128),
							false,
							-1,
							true,
							subArrayLen+128
							//512
						]);	
					}

					numArrays++;
					//var subArrayLen = ds.readUint32();
					if(!arrayFreq[subArrayLen]){
						arrayFreq[subArrayLen] = 1;
					}
					else{
						arrayFreq[subArrayLen]++;
					}

					var subArrayData = ds.readUint8Array(subArrayLen);
					var next128 = ds.readUint8Array(128);

					all128s.set(next128,allIdx*128);
					allIdx++;
					
					

					
					
				}

				//var dsImg = new DataStream(all128s);
				//renderAsImage(dsImg,32,dim1*dim2);


				console.log("FINAL POSITION 7 AFTER SUB ARRAYS",ds.position);
				
				console.log("Total arrays",numArrays, arrayFreq);
				ds.seek(start);

			}

			ds.seek(ds.position+arrLen);

			
			
		};

		stepArrays();
		stepArrays();
		stepArrays();
		stepArrays();
		stepArrays();
		stepArrays();

		console.log("FINAL POSITION",ds.position);
		
	},


	buildChunk: function(ds, size){


		/// width, height, widthSegments, heightSegments
		var chunkGeo =  new THREE.PlaneBufferGeometry ( size, size, 31, 31);

		for(var i = 0; i <32*32; i++){
			var val =  ds.readFloat32();		
			chunkGeo.attributes.position.array[i*3 + 2] = val;
		}


		/// Flip the plane to fit wonky THREE js world axes
		var mS = (new THREE.Matrix4()).identity();
		mS.elements[5] = -1;
		chunkGeo.applyMatrix(mS);

		chunkGeo.computeVertexNormals();

		chunk = new THREE.Mesh(	chunkGeo , new THREE.MeshNormalMaterial({side: THREE.DoubleSide}) );	
		//chunk = new THREE.Mesh(	chunkGeo , new THREE.MeshBasicMaterial({side: THREE.DoubleSide,wireframe:true}) );	
		chunk.rotation.set(Math.PI/2,0,0);

		return chunk;

	},

	render: function(file){

		var self = this;



		/// HTML pNaCl emed elements
		this.NaClInflater = document.getElementById('t3dgwtools');

	    /// Set up a listener for any messages passed by the pNaCl component
	    document.getElementById('pNaClListener').addEventListener(
	    	'message',
	    	function(message_event){
	    		/*console.error(message_event);
	    		console.error(message_event.data);*/

	    		var dsCB = new DataStream(message_event.data[0]);

	    		self.texCB[message_event.data[1]].call(self,dsCB, message_event.data[1]);
	    		
	    	},
	    	true
		);


		loadFilePart(file, 0, file.size, function(aBuffer){



			var ds = new DataStream(aBuffer);

			console.log("Data stream", ds);
			console.log("FILE Identifier", ds.readCString(4));
			console.log("FILE Type", ds.readUint8());

			/// Read chunks, starting at byte 5
			self.readAllChunks(ds, 5);

			//// RENDER POINT CLOUD OF PROP POSITIONS
			//self.renderPropPos(ds);

			//// RENDER HEIGHT MAP
			self.renderTerrain(ds);	

			//// Render sky box
			self.renderCUBE(ds);
			

		});		

	}
};



function readVertex(ds){
	var vertex = new THREE.Vector3();
	var x = ds.readFloat32();
	var y = ds.readFloat32();
	var z = ds.readFloat32();
	vertex.x = x;
	vertex.y = -z;
	vertex.z = -y;
	return vertex;
}


function readVertex2(ds){
	var vertex = new THREE.Vector3();
	var x = ds.readFloat32();

	var y = ds.readFloat32();

	var z = ds.readFloat32();
	vertex.x = x;
	vertex.y = 0;
	vertex.z = z;
	return vertex;
}

function loadFilePart(file, offset, length, callback){
	var self = this;

	var reader = new FileReader();
		
	reader.onerror = function(fileEvent){
		debugger;
	}
	reader.onload  = function(fileEvent){

		var buffer = fileEvent.target.result;
		var ds = new DataStream(buffer);
	  	ds.endianness = DataStream.LITTLE_ENDIAN;

	  	/// Pass data stream and data length to callback function, keeping "this" scope
	  	callback.call(self, ds, length);
	}
	
  	var start = offset;
	var end = start + length;
	reader.readAsArrayBuffer(file.slice(start, end));
};












function renderAsImage(ds, w, h, ofsX, ofsY){
	var img =  $("<img  widht='32' height='32' />");
	img.css({
		position:"absolute",
		top:ofsX, left:ofsY
	});
	$("body").append( img );


	function newEl(tag){return document.createElement(tag);}
	function createImageFromRGBdata(rgbData, width, height)
	{
		var mCanvas = newEl('canvas');
		mCanvas.width = width;
		mCanvas.height = height;
		
		var mContext = mCanvas.getContext('2d');
		var mImgData = mContext.createImageData(width, height);
		
		var srcIndex=0, dstIndex=0, curPixelNum=0;
		
		for (curPixelNum=0; curPixelNum<width*height;  curPixelNum++)
		{
			mImgData.data[dstIndex] = rgbData[srcIndex+0];		// r
			mImgData.data[dstIndex+1] = rgbData[srcIndex+1];//1	// g
			mImgData.data[dstIndex+2] = rgbData[srcIndex+2];//2	// b
			mImgData.data[dstIndex+3] = rgbData[srcIndex+3];//3	// b255; // 255 = 0xFF - constant alpha, 100% opaque
			srcIndex += 4;
			dstIndex += 4;
		}
		mContext.putImageData(mImgData, 0, 0);
		return mCanvas;
	}

	// 1. - append data as a canvas element
	var mCanvas = createImageFromRGBdata(ds.readUint8Array(4*w*h), w, h);
	mCanvas.setAttribute('style', "width:64px; height:64px; border:solid 1px black"); // make it 

	// 2 - append data as a (saveable) image
	var imgDataUrl = mCanvas.toDataURL();	// make a base64 string of the image data (the array above)
	img.attr("src",imgDataUrl);


	ds.seek(ds.position - 4*w*h);
}


readFilePtr = function(ds){
	try{
		/*var ptr = ds.position + ds.readUint32();
		var pos = ds.position;
	
	
    	/// Go to pointer
    	ds.seek( ptr );*/

    	var ret = ds.readStruct([
    		"m_lowPart", "uint16", //uint16 m_lowPart;
		    "m_highPart", "uint16", //uint16 m_highPart;
		    "m_terminator", "uint16",//uint16 m_terminator;
		]);


		/// Getting the file name...
		/// Both need to be >= than 256 (terminator is 0)
		ret = (ret.m_highPart - 0x100) * 0xff00 + (ret.m_lowPart - 0xff);


    	/// Go back to where we were
    	//ds.seek( pos );

    	return ret;
	}
	catch(e){
		/// Go back to where we were
    	//ds.seek( pos );

    	return -1;
	}	    	
}