var ModelDataChunkHeaderParser = require("./../parsers/model/ModelDataChunkHeaderParser.js");


var ModelDataChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

ModelDataChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = ModelDataChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = ModelDataChunkHeader;