var GeometryChunkHeaderParser = require("./../parsers/model/GeometryChunkHeaderParser.js");


var GeometryChunkHeader = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.length  = NaN;
	
	this.load();
};

GeometryChunkHeader.prototype.load=function(){
	this.ds.seek(this.addr);
	this.data = GeometryChunkHeaderParser.parse(this.ds);
	this.length = this.ds.position - this.addr;
};


module.exports = GeometryChunkHeader;