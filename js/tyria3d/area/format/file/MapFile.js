var File = require('./File');

function MapFile(ds, addr){
	File.call(this, ds, addr);
};
MapFile.prototype = Object.create(File.prototype);
MapFile.prototype.constructor = MapFile;

MapFile.prototype.getChunkStructs = function(){
	return {
		"havk":require(	'../definition/mapc/HavokDefinition'),
		"trn":require(	'../definition/mapc/TerrainDefinition'),
		"parm":require(	'../definition/mapc/ParameterDefinition'),
		"shor":require(	'../definition/mapc/ShoreDefinition'),
		"zon2":require(	'../definition/mapc/ZoneDefinition'),
		"prp2":require(	'../definition/mapc/PropertiesDefinition'),
		"cube":require(	'../definition/mapc/CubeMapDefinition'),
		"env":require(	'../definition/mapc/EnvironmentDefinition'),
	};
};

module.exports = MapFile;