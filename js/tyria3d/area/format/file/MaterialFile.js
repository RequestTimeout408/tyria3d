var File = require('./File');

function MaterialFile(ds, addr){
	File.call(this, ds, addr);
};
MaterialFile.prototype = Object.create(File.prototype);
MaterialFile.prototype.constructor = MaterialFile;

MaterialFile.prototype.getChunkStructs = function(){
	return {
		"dx9s":require('../definition/amat/Dx9MaterialDefinition'),
		"grmt":require('../definition/amat/AmatGrDefinition'),
	};
};

module.exports = MaterialFile;