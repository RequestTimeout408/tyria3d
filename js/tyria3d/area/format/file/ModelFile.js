var File = require('./File');

function ModelFile(ds, addr){
	File.call(this, ds, addr);
};
ModelFile.prototype = Object.create(File.prototype);
ModelFile.prototype.constructor = ModelFile;

ModelFile.prototype.getChunkStructs = function(){
	return {
		"modl":require('../definition/modl/ModelDataDefinition'),
		"geom":require('../definition/modl/GeometryDefinition')
	};
};

module.exports = ModelFile;