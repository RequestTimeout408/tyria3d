var Chunk = require('./Chunk');

var HEAD_STRUCT = [
	'identifier', 'cstring:2',
	'unknownField1', 'uint16',
	'unknownField2', 'uint16',
	'pkFileVersion', 'uint16',
	'type', 'cstring:4'
];

var File = function(ds, addr){
	this.ds = ds;
	this.addr = addr;
	this.data = null;
	this.headerLength  = NaN;
	
	this.readHead();
	this.readChunks();
};

/**
 * Read the file header data, populating this.header.
 */
File.prototype.readHead = function(){
	this.ds.seek(this.addr);
	this.header = this.ds.readStruct(HEAD_STRUCT);
	this.headerLength = this.ds.position - this.addr;
};

/**
 * Read all chunks within this file and store them in this.chunks.
 */
File.prototype.readChunks = function(){

	/// Reset chunks
	this.chunks = [];

	var structs = this.getChunkStructs && this.getChunkStructs();

	/// Load basic Chunk in order to read the chunk header.
	var ch = new Chunk(this.ds, this.headerLength + this.addr);	



    while(structs && ch!=null && ch.header.type){
    	

    	/// Load the chunk data if a structure for this chunk type is defined.
		if( structs.hasOwnProperty(ch.header.type.toLowerCase()) ){

			var chunkStruct = structs[ch.header.type.toLowerCase()];
			if(chunkStruct){
				ch.loadData(chunkStruct);
			}
			

			/// Save chunk reference in this file.
	    	this.chunks.push(ch);
    	}

    	/// Load next basic Chunk in order to read the chunk header.
    	ch = ch.next();
    }
};

/**
 * Get a Chunk from this file
 * @param  {String} type - The name, or type of the desired chunk.
 * @return {?Chunk} The first Chunk in this file matching the type name, or null if no matching Chunk was found.
 */
File.prototype.getChunk = function (type){
	for(var i=0; i<this.chunks.length; i++){
		if( this.chunks[i].header.type.toLowerCase() == type.toLowerCase() )
			return this.chunks[i]; 
	}
	return null;
};

module.exports = File;