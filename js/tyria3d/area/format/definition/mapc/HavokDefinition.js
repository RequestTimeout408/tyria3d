var Utils = require('../../../util/ParserUtils');

module.exports = [
	//float3 boundsMin;
	//float3 boundsMax;   
	"boundsMin",["x","float32","z","float32","y","float32"],		
	"boundsMax",["x","float32","z","float32","y","float32"],
	
	//helpers::Array<PackMapCollideCollisionV14> collisions;
	"collisions", Utils.getArrayReader([
			// helpers::Array<word> indices;
	    	"indices", Utils.getArrayReader("uint16"),

	    	//helpers::Array<float3> vertices;
	    	"vertices", Utils.getArrayReader(["x","float32","z","float32","y","float32"]),
	    	
	    	//helpers::Array<word> surfaces;
	    	"surfaces", Utils.getArrayReader("uint16"),

	    	//PackMoppType moppCodeData; (byte array)
	    	// compressed bounding volume data
	    	"moppCodeDataCount", 'uint32',
	    	"moppCodeDataOffset", 'uint32'
		]),
	
	//helpers::Array<PackMapCollideBlockerV14> blockers;
	"blockersCount" , "uint32", 									
    "blockersOffset" , "uint32",									
    
    //helpers::Array<PackMapCollideNavMeshV14> navMeshes;
    "navMeshesCount" , "uint32", 									
    "navMeshesOffset" , "uint32", 									
    
    //helpers::Array<PackMapCollideAnimationV14> animations;
    "animations", Utils.getArrayReader([
    		//qword sequence;
			"sequencePT1","uint32",
			"sequencePT2","uint32",

			//helpers::Array<dword> collisionIndices; (dword = uint32)
			"collisionIndices",Utils.getArrayReader("uint32"),

			//helpers::Array<dword> blockerIndices;
			"blockerIndices",Utils.getArrayReader("uint32"),
		]),
    
    //helpers::Array<PackMapCollideGeometryV14> geometries;
    "geometries", Utils.getArrayReader([

		//byte quantizedExtents;
		"quantizedExtents", "uint8",
	   
	    //helpers::Array<dword> animations;
	    "animations", Utils.getArrayReader("uint32"),
		
		//word navMeshIndex;
		"navMeshIndex", "uint16"
		
    	]),
     
    //helpers::Array<PackMapCollideModelObsV14> obsModels;
    "obsModels", Utils.getArrayReader([
	    	// float3 translate
    		"translate", ["x","float32","z","float32","y","float32"],
	    	
	    	// dword geometryIndex  ( dword = uint32 )
    		"geometryIndex", "uint32", 
    	]),
    
    // helpers::Array<PackMapCollideModelPropV14> propModels;
    "propModels", Utils.getArrayReader([
    		// qword token
    		"token_pt1", "uint32",
    		"token_pt2", "uint32",	    		
    		
    		// qword sequence;
    		"sequence_pt1", "uint32",
    		"sequence_pt2", "uint32",

    		// float scale    		
    		"scale", "float32",	
    		
    		// float3 translate
    		"translate", ["x","float32","z","float32","y","float32"],

    		// float3 rotate
    		"rotation", ["x","float32","z","float32","y","float32"], 
	    	
	    	// dword geometryIndex  ( dword = uint32 )
    		"geometryIndex", "uint32", 
    	]),
    
    //helpers::Array<PackMapCollideModelZoneV14> zoneModels;
    "zoneModels", Utils.getArrayReader([
	    	// float scale    		
    		"scale", "float32",	
    		
    		// float3 translate
    		"translate", ["x","float32","z","float32","y","float32"],

    		// float3 rotate
    		"rotation", ["x","float32","z","float32","y","float32"], 
	    	
	    	// dword geometryIndex  ( dword = uint32 )
    		"geometryIndex", "uint32"
    	])
];