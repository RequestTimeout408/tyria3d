var Utils = require('../../../util/ParserUtils');

function getBasePropDef(){
	return [
		//helpers::FileName filename;
		"filename", Utils.getFileNameReader(),

	    //helpers::Array<helpers::FileName> blitTextures;
	    "blitTextures", Utils.getArrayReader(Utils.getFileNameReader()),

	    //helpers::Array<PackMapPropConstantV18> constants;
	    "constants", Utils.getArrayReader([
	    		// dword token;
	    		"token", "uint32",

			    //float4 constant;
			    "constant", ["[]","float32",4],

			    //dword submodel;
			    "submodel", "uint32"
	    	]),

	    //qword guid;
	    "guid",Utils.getQWordReader(), //// Crashes Dredgehaunt!!
	    //"guid_pt1", "uint32", "guid_pt2", "uint32",

	    //qword permutation;
	    "permutation_pt1", "uint32", "permutation_pt2", "uint32",

	    //float4 bounds;
	    "bounds", ["[]","float32",4],

	    //float3 position;
	    "position", ["x","float32","z","float32","y","float32"],

	    //float3 rotation;
	    "rotation", ["x","float32","z","float32","y","float32"],

	    //byte4 color;
	    "color", ["[]","uint8",4],

	    //float scale;
	    "scale", "float32",

	    //float lod1;
	    "lod1", "float32",

	    //float lod2;
	    "lod2", "float32",

	    //dword flags;
	    "flags", "uint32",

	    //dword reserved;
	    "reserved", "uint32",

	    //word broadId;
	    "broadId", "uint16",

	    //word bucketId;
	    "bucketId", "uint16",

	    //byte byteData;
	    "byteData", "uint8",

	    //byte sortLayer;
	    "sortLayer", "uint8"
	];
}

//PackMapPropObjV21
var propDef = getBasePropDef();

//PackMapPropObjAnimSeqV21
var animArrayDef = getBasePropDef();
animArrayDef.push(
	// qword animSequence;
	"animSequence_pt1","uint32",
	"animSequence_pt2","uint32"
);

var instanceArrayDef = getBasePropDef();
instanceArrayDef.push(
	//helpers::Array<PackMapPropTransformV21> transforms;
    "transforms", Utils.getArrayReader([
		    //float3 position;
		    "position", ["x","float32","z","float32","y","float32"],
		    //float3 rotation;
		    "rotation", ["x","float32","z","float32","y","float32"],
		    //float scale;
		    "scale", "float32",
    	]),

    //helpers::Array<qword> origGuidArray;
	"origGuidArray_len","uint32",
	"origGuidArray_ptr","uint32"
);


var metaArrayDef = getBasePropDef();
metaArrayDef.push(
	//dword layerMask;
	"layerMask","uint32",
    
    //byte glomType;
    "glomType","uint8",
    
    //qword parent;
    "parent_pt1","uint32",
	"parent_pt2","uint32",

    //float3 glomOrigin;
    "glomOrigin", ["x","float32","z","float32","y","float32"]
);

module.exports = [
	//helpers::Array<PackMapPropObjV21> propArray;
	"propArray", Utils.getArrayReader(propDef), 

    //helpers::Array<PackMapPropObjAnimSeqV21> propAnimArray;
    "propAnimArray", Utils.getArrayReader(animArrayDef),

    //helpers::Array<PackMapPropObjInstanceV21> propInstanceArray;
    "propInstanceArray", Utils.getArrayReader(instanceArrayDef),

    //helpers::Array<PackMapPropObjToolV21> propToolArray;
    "propToolArray_len","uint32",
    "propToolArray_ptr","uint32",
    
    //helpers::Array<PackMapPropObjMetaV21> propMetaArray;
    "propMetaArray", Utils.getArrayReader(metaArrayDef),

    //helpers::Array<PackMapPropObjVolumeV21> propVolumeArray;
    "propVolumeArray_len","uint32",
    "propVolumeArray_ptr","uint32",


    //helpers::WString reserved;
    //PackBroadphaseType broadPhase;
    //dword nextBroadId;
]