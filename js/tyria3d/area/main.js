/*
	Includes
*/

/// ----- JQuery ----- 
var $ = require('jquery');
require("../../vendor/jQuery/jquery-ui.min.js");

/// ----- Three.js ----- 
var THREE = require('THREE');
var Stats= require("../../vendor/three/stats.min.js");
require("THREE-Raycaster");

/// ----- DataStream.js ----- 
require("DataStream");

///  ----- T3D API  ----- 
var T3D = require("./API/T3D-API-1.0.3.js"); 

///  ----- Internal  ----- 
var UI =  require("./UI.js");
var SceneUtils = require("./SceneUtils.js");
var FlyControls =  require("./FlyControls.js");
var GW1 = require("./gw1test.js");

/*
	END Includes
*/


/// Global references
var map;
var stats;
var controller;
var hasPointerLock;
var ui;

var lastTs = -1;
var animating = false;
var localReader = null;

var _mapRect = null;


/// Starting point
$(document).ready(function(){
	console.log("Tyria 3D API version "+T3D.version);
	
	/// Detect Chrome and WebGL
	var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	var hasWebGL = webgl_detect();

	/// Hide blinking loader
	$("#frontpageLoader").addClass("hidden");

	/// Display error message for missing WebGL
	if (!window.WebGLRenderingContext || !hasWebGL) {
    	$("#errorGL").removeClass("hidden");
  	}

  	/// Display error message for non-Chrome browsers
  	else if(!is_chrome){
  		$("#errorChrome").removeClass("hidden");
  	}

  	/// If everyting is ok, enable the button that shows the file picker.
  	else{
		$("#ILoveYouDiddi").removeClass("hidden").one("click",initAnim);
  		
  	}
	
});

function initAnim(){
	var t1 = 350 * 0;
	$(".content  h1").first().animate({"margin-top":20},t1);
	$("nav").slideUp(t1,function(){
		$("#intro").delay(t1).fadeOut(t1,init);
	});

}

function init(){

	// Hide intro div
	$("#intro").hide();	

	/// Add stats (toggled by pressing "i")
	stats = new Stats();
	$("body").append( stats.domElement );

	/// Create fly controls, initare after UI DOM has been added.
	controller = new FlyControls();

	/// Set up UI
	ui = new UI( $("body"), controller, loadMap );
	ui.init();

	/// Create file picker, used to pick DAT file
	var filePicker = $("<input type='file' class='hidden' />");
	var fileIcon = $("<button id='fileInputIcon' tabindex='-1'>Select a Guild Wars 2 .dat file</button>");
	fileIcon.click(function () {
	    filePicker.trigger('click');
	});
	
	/// .dat file received from the file input, entry point!
	var onReceiveFile = function(evt){
		
		/// Get loaded file reference from event
		///TODO: Check file length etc.
		var files = evt.target.files;
		var file = files[0];

		filePicker.val('');

		/// GW1 TEST
		//GW1.render(file);
		//onMapLoaded();
		//return;
		// END GW1 TEST
		
		/// USE API!
		var lr = T3D.getLocalReader(file, $("#progressPanel"), function(){
			
			/// Global variable
			localReader = lr;

			/// Get the maps in the dat and put them in the UI.
			/// Show progress panel during load.
			SceneUtils.showProgressPanel(function(){		
		
				$("#output").find(".title").html("Finding maps (first visit only)");
				$("#output").find(".progress").html("initializing");

				setTimeout(function(){
						T3D.getMapListAsync(lr, applyMapList);
				},10);
			});

			

			/// TESTING 
			/*
			localReader.loadFile(
				641501,
				function(inflatedData){
					saveMap = new Map(inflatedData, testCollectIds);
				}
			);*/
		});

		// SAVE MAP CODE WAS HERE

	};

	/// Add listener to file input
	filePicker.change(onReceiveFile);

	/// Append file input elements to the DOM
	$("#filePanel").append(filePicker);
	$("#filePanel").append(fileIcon);

	/// Build deep map search interface
	var deepSearchButton = $("<span class='link'>Scan all .dat entries for maps</span>");
	deepSearchButton.click( function(evt){
		if ( confirm(
				"Searching the full .dat file will take roughly 10 minutes depending on file size.\n\n"+
				"Once complete the results will be stored locally and available every time you load this .dat.\n\n"+
				"Note that the vast majority of maps are probably already loaded."+
				"Are you sure you want to search the .dat for maps?"
			) ) {
		    localReader.readMapList(true);
		}
		
	} );

	/// Append deep search elements to the DOM
	var searchParagraph = $("<p class='instruction'>Missing maps? </p>");
	searchParagraph.append(deepSearchButton);
	$("#mapPanel").append(searchParagraph);

	/// Key listeners connected to UI (toggle UI panel and stats)
	document.addEventListener( 'keydown', keyDownListener, false );
	document.addEventListener('mousewheel',mouseWheelListener, false); 

	/// Set up scene holding visible objects, lights, camera and renderer.
	SceneUtils.setupScene();

	/// Keep rendered hidden until map is ready to render.
	SceneUtils.setRenderVisible(false);
	
	/// Initiate controlls, connection them to the scene.
	hasPointerLock = controller.init();

};

function applyMapList(mapList){

	// Fuck up UI
	var picker = $("#mapPicker");
	picker.empty();
	picker.append($("<option selected='true' disabled='disabled'>Pick Map</option>")); 
	

	var compareName = function(a, b) {
		if (a.name < b.name)
		    return -1;
		if (a.name > b.name)
		    return 1;
		return 0;
	};

	mapList.maps.sort(compareName);
	
	mapList.maps.forEach(function(g){
		var group = $("<optgroup label='"+g.name+"' />");
		picker.append(group);

		g.maps.sort(compareName);
		g.maps.forEach(function(m){
			group.append("<option value='"+m.fileName+"'>"+m.name+"</option>");
		});
		
	});

	
	SceneUtils.showMapPanel();
	
}


function mouseWheelListener(evt){
	var dir = Math.sign(evt.wheelDelta);
	var s = $("#moveSpeedSlider");
	var min =  s.slider("option","min")
	var max = s.slider("option","max") 
	var range =max - min;
	
	var val = range*dir*0.05 + s.slider( "option", "value" );
	val = Math.min(max,val);
	val = Math.max(min,val);

	s.slider("option", "value", val);
}


/**
 * Settings and debugging key listener
 * @param  {[type]} evt [description]
 * @return {[type]}     [description]
 */
function keyDownListener(evt){
	if(evt.keyCode == 85){ // U
		$("#UI").toggle();
		SceneUtils.resize();
	}
	if(evt.keyCode == 73){ // I
		$("#stats").toggle();
	}
	if(evt.keyCode == 80){ // P
		SceneUtils.setPerspective();
	}
	if(evt.keyCode == 79){ // O

		var r = _mapRect;
		if(r){
			var xMin = r.x1;
			var xMax = r.x2;
			var yMin = r.y1;
			var yMax = r.y2;
			SceneUtils.setOrthographic(xMin, xMax, yMin, yMax, 100000, -100000);
		}
	}
	if(evt.keyCode == 70){ // F
		$("#flyInput").trigger('click');
	}
}

/**
 * Called when a map is specified via the drop down
 * @param  {[type]} fileName [description]
 * @param  {[type]} absolute [description]
 * @return {[type]}          [description]
 */
function loadMap(fileName, absolute){

	SceneUtils.showProgressPanel(function(){

		/// Disable controller and hide rendered during load
		controller.setMapReady(false);
		SceneUtils.setRenderVisible(false);

		/// Make sure we got an actuall ID number		
		if(parseInt(fileName)){

			/// File name is baseId, load using local reader.
			localReader.loadFile(
				fileName,
				function(arrayBuffer){

					/// Set up datastream
					var ds = new DataStream(arrayBuffer, 0, DataStream.LITTLE_ENDIAN);

					/// Initiate Map file object. Connect callback
					window.map = map = new T3D.MapFile(ds, 0);
					onMapLoaded();
				}
			);
		}

		/// Primitive error message...
		else{
			console.error("Map id must be an integer!, was:",fileName);
		}

	});
	
}

/**
 * TODO
 * @return {[type]} [description]
 */
function onMapLoaded(){

	/// Clear scene
	SceneUtils.clear();
	
	/// Render data as visible objects
	/*
		var mapData = {
	    	hasLight : 		false,
	    	boundingBox : 	null,
	    	skyBox : 		null,
	    	ambientLight : 	null,
	    	
	    	visibles : [],
	    	terrainTiles : [],	    	
	    	collisions : 	[],
	    	nonCollisions : [],
	    	lights : 		[],
	    };
	 */
	T3D.MapRenderer.renderAsync(map, localReader, function(mapData){

		/// Add elements to the scene handler
		
		/// Add all visibles to the 3d scene.
		
		mapData.visibles.forEach(function(elem){
			SceneUtils.getScene().add(elem);
		});

		mapData.skyElements.forEach(function(elem){
			SceneUtils.getSkyScene().add(elem);
			SceneUtils.getSkyObjects().push(elem);
		});		
		
		/// Add terrain tiles to a special list
		/// ( these need their fog updated in a specific way ).
		mapData.terrainTiles.forEach(function(elem){
			SceneUtils.getTerrainChunks().push(elem);
		});	

		/// Add all collisions to a special list
		mapData.collisions.forEach(function(elem){
			SceneUtils.getCollisions().push(elem);
		});

		/// Add all non-collisions to a special list
		mapData.nonCollisions.forEach(function(elem){
			SceneUtils.getNonCollisions().push(elem);
		});

		/// Add lights
		mapData.lights.forEach(function(elem){
			SceneUtils.getScene().add(elem);
			SceneUtils.getLights().push(elem);
		});

		/// Set haze color 
		var hazeColor = mapData.hazeColor;
		var color = new THREE.Color(hazeColor[2]/255.0, hazeColor[1]/255.0, hazeColor[0]/255.0);
		SceneUtils.getRenderer().setClearColor( color, 1.0 );
		SceneUtils.getScene().fog.color.copy(color);
	

		_mapRect = mapData.bounds;

		controller.setMapReady(true);

		
		$("#UI").removeClass("hidden");

		/// Set ambient light slider to 50% if there were no parsed lights in the map data.
		$("#ambientSlider").slider("value",mapData.hasLight ? 0 : 0.5);


		/// Data Renderer is done start animating!
		if(hasPointerLock){
			SceneUtils.showPanel($("#suspendedPanel"));
		}
		else{
			SceneUtils.showPanel($("#errorPanel"));
		}
		

		// Set camera position
		controller.getControls().getObject().position.set(0, mapData.bounds ? mapData.bounds.y2 : 0, 0);
		controller.getControls().getPitchObject().rotation.x = -Math.PI/2;		

		// Initial render, indep. of controller being active
		SceneUtils.render();

		// Show canvas
		SceneUtils.setRenderVisible(true);

		/// Animate scene
		if(!animating){
			animating = true;
			animate(0);	
		}
	});


}//End onload callback


/**
 * Main "game" loop, called trough requestAnimationFrame
 * @param  {[type]} timestamp [description]
 * @return {[type]}           [description]
 */
function animate(timestamp) {
	
	/// Get time step
	var delta = timestamp - lastTs;
	if(delta > 0){
		
		lastTs = timestamp;

		/// Update current controller
		if( controller.update(delta*0.001) ){

			/// If controller returns true, render
			SceneUtils.render();
		}

		/// Update stats (FPS etc)
		stats.update();

	}

	window.requestAnimationFrame( animate );
}

/**
 * TODO
 * @param  {[type]} return_context [description]
 * @return {[type]}                [description]
 */
function webgl_detect(return_context)
{
    if (!!window.WebGLRenderingContext) {
        var canvas = document.createElement("canvas"),
             names = ["webgl", "experimental-webgl", "moz-webgl", "webkit-3d"],
           context = false;
 
        for(var i=0;i<4;i++) {
            try {
                context = canvas.getContext(names[i]);
                if (context && typeof context.getParameter == "function") {
                    // WebGL is enabled
                    if (return_context) {
                        // return WebGL object if the function's argument is present
                        return {name:names[i], gl:context};
                    }
                    // else, return just true
                    return true;
                }
            } catch(e) {}
        }
 
        // WebGL is supported, but disabled
        return false;
    }
 
    // WebGL not supported
    return false;
}