/*

module.exports = {
	maps : [
		{
			"name":"Sample Maps","maps":
			[
				{"fileName":"Maps_2013_01_28\/191000.parm","name":"Lion's Arch Pre Scarlet"},
				{ fileName :"197402.data", name:"The Battle of Khylo" },
				{ fileName :"193081.data", name:"North of Divinity's Reach" },
				{ fileName :"969663.data", name:"Verdant Brink" }
			]
		}
	]
};

Oh wow, there's even part of Verdant Brink, SAB worlds 1 and 2, the old LS1 dungeon instances, tower of nightmares.. and even more.
This is amazing, great job!
EDIT- If you're looking to be a bit more accurate with the map names:
Dry Top -> Dry Top (Past)
Dry Top Again -> Dry Top (Present)
Silver wastes -> The Silverwastes
Stronghold -> Champion's Dusk
Gendarran Fields and Timberline Falls are similar to Dry Top, the second version has the added mordrem vines.

Both Lion's Arch maps appear to be the same or otherwise very similar, I can't spot the differences.

As for the Newly Added map, it looks like the original design of the Stronghold/Champion's Dusk map that was
datamined ages ago... I don't think it was given a name.
*/


/*
Also, noticed you're missing original Southsun Cove, one of Lion's Arch instances (pre-Battle for Lion's Arch),
and two of Kessex Hills instances (pre-Tower of Nightmares, and during Tower of Nightmares). I think these four are important as well!
*/




/*
Shamans old uns
*/

module.exports = {

	maps : [

/*	{"name":"BWE2","maps":
[
{"fileName":"BWE2\/184799.parm","name":"Plane"}, 
{"fileName":"BWE2\/186397.parm","name":"Snowden Drifts"},
{"fileName":"BWE2\/187611.parm","name":"Wayfarer Foothills"},
{"fileName":"BWE2\/188591.parm","name":"Plains of Ashford"},
{"fileName":"BWE2\/189364.parm","name":"Ascalonian Catacombs"},
{"fileName":"BWE2\/190490.parm","name":"Diessa Plateau"},
{"fileName":"BWE2\/191000.parm","name":"Lion's Arch Pre Scarlet"},
{"fileName":"BWE2\/191265.parm","name":"Divinity's Reach"},
{"fileName":"BWE2\/192711.parm","name":"Queensdale"},
{"fileName":"BWE2\/193081.parm","name":"North of Divinity's Reach"},
{"fileName":"BWE2\/194288.parm","name":"Kessex Hills"},
{"fileName":"BWE2\/195149.parm","name":"Caledon Forest"},
{"fileName":"BWE2\/195493.parm","name":"Metrica Province"},
{"fileName":"BWE2\/195806.parm","name":"Eternal Battlegrounds"},
{"fileName":"BWE2\/196212.parm","name":"Borderlands"},
{"fileName":"BWE2\/196585.parm","name":"Black Citadel"},
{"fileName":"BWE2\/197122.parm","name":"Hoelbrak"},
{"fileName":"BWE2\/197249.parm","name":"Heart of the Mists"},
{"fileName":"BWE2\/197402.parm","name":"The Battle of Khylo"},
{"fileName":"BWE2\/197545.parm","name":"Forest of Niflhel"},
{"fileName":"BWE2\/197562.parm","name":"Empty Box"},
{"fileName":"BWE2\/198076.parm","name":"The Grove"},
{"fileName":"BWE2\/198272.parm","name":"Rata Sum"},
{"fileName":"BWE2\/275155.parm","name":"Dredgehaunt Cliffs"},
{"fileName":"BWE2\/275474.parm","name":"Sorrow's Embrace"},
{"fileName":"BWE2\/276252.parm","name":"Frostgorge Sound"},
{"fileName":"BWE2\/276520.parm","name":"Honor of the Waves"},
{"fileName":"BWE2\/277587.parm","name":"Lornar's Pass"},
{"fileName":"BWE2\/278717.parm","name":"Timberline Falls"},
{"fileName":"BWE2\/280025.parm","name":"Blazeridge Steppes"},
{"fileName":"BWE2\/281313.parm","name":"Fireheart Rise"},
{"fileName":"BWE2\/282668.parm","name":"Iron Marches"},
{"fileName":"BWE2\/283574.parm","name":"Fields of Ruin"},///LEss detailed than release
{"fileName":"BWE2\/284039.parm","name":"Citadel of Flame"},
{"fileName":"BWE2\/284829.parm","name":"Straits of Devastation"},
{"fileName":"BWE2\/285089.parm","name":"Malchor's Leap"},
{"fileName":"BWE2\/285634.parm","name":"Cursed Shore"},
{"fileName":"BWE2\/286945.parm","name":"Bloodtide Coast"},
{"fileName":"BWE2\/287214.parm","name":"Caudecus's Manor"},
{"fileName":"BWE2\/287870.parm","name":"Harathi Hinterlands"},
{"fileName":"BWE2\/289176.parm","name":"Gendarran Fields"}, /// LA entrance original
{"fileName":"BWE2\/291064.parm","name":"Mount Maelstrom"},
{"fileName":"BWE2\/291284.parm","name":"Twilight Arbor"}, /// Less detail
{"fileName":"BWE2\/292254.parm","name":"Sparkfly Fen"}, /// Teq's bone wall is there since BWE2 ???
{"fileName":"BWE2\/293307.parm","name":"Brisban Wildlands"},
{"fileName":"BWE2\/293606.parm","name":"Crucible of Eternity"},
{"fileName":"BWE2\/294938.parm","name":"Claw Island"},
{"fileName":"BWE2\/295005.parm","name":"Chantry of Secrets"},
{"fileName":"BWE2\/295179.parm","name":"Cathedral of Hidden Depths"},
{"fileName":"BWE2\/295282.parm","name":"Eye of the North"},
{"fileName":"BWE2\/295962.parm","name":"A Light in the Darkness"}
]}
,
{"name":"BWE3","maps":
[
{"fileName":"BWE3\/184799.parm","name":"Plane"}, 
{"fileName":"BWE3\/186397.parm","name":"Snowden Drifts"},
{"fileName":"BWE3\/187611.parm","name":"Wayfarer Foothills"},
{"fileName":"BWE3\/188591.parm","name":"Plains of Ashford"},
{"fileName":"BWE3\/189364.parm","name":"Ascalonian Catacombs"},
{"fileName":"BWE3\/190490.parm","name":"Diessa Plateau"},
{"fileName":"BWE3\/191000.parm","name":"Lion's Arch Pre Scarlet"},
{"fileName":"BWE3\/191265.parm","name":"Divinity's Reach"},
{"fileName":"BWE3\/192711.parm","name":"Queensdale"},
{"fileName":"BWE3\/193081.parm","name":"North of Divinity's Reach"},
{"fileName":"BWE3\/194288.parm","name":"Kessex Hills"},
{"fileName":"BWE3\/195149.parm","name":"Caledon Forest"},
{"fileName":"BWE3\/195493.parm","name":"Metrica Province"},
{"fileName":"BWE3\/195806.parm","name":"Eternal Battlegrounds"},
{"fileName":"BWE3\/196212.parm","name":"Borderlands"},
{"fileName":"BWE3\/196585.parm","name":"Black Citadel"},
{"fileName":"BWE3\/197122.parm","name":"Hoelbrak"},
{"fileName":"BWE3\/197249.parm","name":"Heart of the Mists"},
{"fileName":"BWE3\/197402.parm","name":"The Battle of Khylo"},
{"fileName":"BWE3\/197545.parm","name":"Forest of Niflhel"},
{"fileName":"BWE3\/197562.parm","name":"Empty Box"},
{"fileName":"BWE3\/198076.parm","name":"The Grove"},
{"fileName":"BWE3\/198272.parm","name":"Rata Sum"},
{"fileName":"BWE3\/275155.parm","name":"Dredgehaunt Cliffs"},
{"fileName":"BWE3\/275474.parm","name":"Sorrow's Embrace"},
{"fileName":"BWE3\/276252.parm","name":"Frostgorge Sound"},
{"fileName":"BWE3\/276520.parm","name":"Honor of the Waves"},
{"fileName":"BWE3\/277587.parm","name":"Lornar's Pass"},
{"fileName":"BWE3\/278717.parm","name":"Timberline Falls"},
{"fileName":"BWE3\/280025.parm","name":"Blazeridge Steppes"},
{"fileName":"BWE3\/281313.parm","name":"Fireheart Rise"},
{"fileName":"BWE3\/282668.parm","name":"Iron Marches"},
{"fileName":"BWE3\/283574.parm","name":"Fields of Ruin"},///LEss detailed than release
{"fileName":"BWE3\/284039.parm","name":"Citadel of Flame"},
{"fileName":"BWE3\/284829.parm","name":"Straits of Devastation"},
{"fileName":"BWE3\/285089.parm","name":"Malchor's Leap"},
{"fileName":"BWE3\/285634.parm","name":"Cursed Shore"},
{"fileName":"BWE3\/286945.parm","name":"Bloodtide Coast"},
{"fileName":"BWE3\/287214.parm","name":"Caudecus's Manor"},
{"fileName":"BWE3\/287870.parm","name":"Harathi Hinterlands"},
{"fileName":"BWE3\/289176.parm","name":"Gendarran Fields"}, /// LA entrance original
{"fileName":"BWE3\/291064.parm","name":"Mount Maelstrom"},
{"fileName":"BWE3\/291284.parm","name":"Twilight Arbor"}, /// Less detail
{"fileName":"BWE3\/292254.parm","name":"Sparkfly Fen"}, /// Teq's bone wall is there since BWE2 ???
{"fileName":"BWE3\/293307.parm","name":"Brisban Wildlands"},
{"fileName":"BWE3\/293606.parm","name":"Crucible of Eternity"},
{"fileName":"BWE3\/294938.parm","name":"Claw Island"},
{"fileName":"BWE3\/295005.parm","name":"Chantry of Secrets"},
{"fileName":"BWE3\/295179.parm","name":"Cathedral of Hidden Depths"},
{"fileName":"BWE3\/295282.parm","name":"Eye of the North"},
{"fileName":"BWE3\/295962.parm","name":"A Light in the Darkness"},
{"fileName":"BWE3\/376916.parm","name":"Legacy of the Foefire"}
]}
,

*/
/*
{"name":"Historical","maps":
[
{"fileName":"191000.parm","name":"Lion's Arch (Older)"}
{"fileName":"Maps_2013_01_28\/184799.parm","name":"Plane"}, 
{"fileName":"Maps_2013_01_28\/186397.parm","name":"Snowden Drifts"},
{"fileName":"Maps_2013_01_28\/187611.parm","name":"Wayfarer Foothills"},
{"fileName":"Maps_2013_01_28\/188591.parm","name":"Plains of Ashford"},
{"fileName":"Maps_2013_01_28\/189364.parm","name":"Ascalonian Catacombs"},
{"fileName":"Maps_2013_01_28\/190490.parm","name":"Diessa Plateau"},
{"fileName":"Maps_2013_01_28\/191000.parm","name":"Lion's Arch Pre Scarlet"},  *//*
{"fileName":"Maps_2013_01_28\/191265.parm","name":"Divinity's Reach"},
{"fileName":"Maps_2013_01_28\/192711.parm","name":"Queensdale"},
{"fileName":"Maps_2013_01_28\/193081.parm","name":"North of Divinity's Reach"},
{"fileName":"Maps_2013_01_28\/194288.parm","name":"Kessex Hills"},
{"fileName":"Maps_2013_01_28\/195149.parm","name":"Caledon Forest"},
{"fileName":"Maps_2013_01_28\/195493.parm","name":"Metrica Province"},
{"fileName":"Maps_2013_01_28\/195806.parm","name":"Eternal Battlegrounds"},
{"fileName":"Maps_2013_01_28\/196212.parm","name":"Borderlands"},
{"fileName":"Maps_2013_01_28\/196585.parm","name":"Black Citadel"},
{"fileName":"Maps_2013_01_28\/197122.parm","name":"Hoelbrak"},
{"fileName":"Maps_2013_01_28\/197249.parm","name":"Heart of the Mists"},
{"fileName":"Maps_2013_01_28\/197402.parm","name":"The Battle of Khylo"},
{"fileName":"Maps_2013_01_28\/197545.parm","name":"Forest of Niflhel"},
{"fileName":"Maps_2013_01_28\/197562.parm","name":"Empty Box"},
{"fileName":"Maps_2013_01_28\/198076.parm","name":"The Grove"},
{"fileName":"Maps_2013_01_28\/198272.parm","name":"Rata Sum"},
{"fileName":"Maps_2013_01_28\/275155.parm","name":"Dredgehaunt Cliffs"},
{"fileName":"Maps_2013_01_28\/275474.parm","name":"Sorrow's Embrace"},
{"fileName":"Maps_2013_01_28\/276252.parm","name":"Frostgorge Sound"},
{"fileName":"Maps_2013_01_28\/276520.parm","name":"Honor of the Waves"},
{"fileName":"Maps_2013_01_28\/277587.parm","name":"Lornar's Pass"},
{"fileName":"Maps_2013_01_28\/278717.parm","name":"Timberline Falls"},
{"fileName":"Maps_2013_01_28\/280025.parm","name":"Blazeridge Steppes"},
{"fileName":"Maps_2013_01_28\/281313.parm","name":"Fireheart Rise"},
{"fileName":"Maps_2013_01_28\/282668.parm","name":"Iron Marches"},
{"fileName":"Maps_2013_01_28\/283574.parm","name":"Fields of Ruin"},///LEss detailed than release
{"fileName":"Maps_2013_01_28\/284039.parm","name":"Citadel of Flame"},
{"fileName":"Maps_2013_01_28\/284829.parm","name":"Straits of Devastation"},
{"fileName":"Maps_2013_01_28\/285089.parm","name":"Malchor's Leap"},
{"fileName":"Maps_2013_01_28\/285634.parm","name":"Cursed Shore"},
{"fileName":"Maps_2013_01_28\/286945.parm","name":"Bloodtide Coast"},
{"fileName":"Maps_2013_01_28\/287214.parm","name":"Caudecus's Manor"},
{"fileName":"Maps_2013_01_28\/287870.parm","name":"Harathi Hinterlands"},
{"fileName":"Maps_2013_01_28\/289176.parm","name":"Gendarran Fields"}, /// LA entrance original
{"fileName":"Maps_2013_01_28\/291064.parm","name":"Mount Maelstrom"},
{"fileName":"Maps_2013_01_28\/291284.parm","name":"Twilight Arbor"}, /// Less detail
{"fileName":"Maps_2013_01_28\/292254.parm","name":"Sparkfly Fen"}, /// Teq's bone wall is there since Maps_2013_01_28 ???
{"fileName":"Maps_2013_01_28\/293307.parm","name":"Brisban Wildlands"},
{"fileName":"Maps_2013_01_28\/293606.parm","name":"Crucible of Eternity"},
{"fileName":"Maps_2013_01_28\/294938.parm","name":"Claw Island"},
{"fileName":"Maps_2013_01_28\/295005.parm","name":"Chantry of Secrets"},
{"fileName":"Maps_2013_01_28\/295179.parm","name":"Cathedral of Hidden Depths"},
{"fileName":"Maps_2013_01_28\/295282.parm","name":"Eye of the North"},
{"fileName":"Maps_2013_01_28\/295962.parm","name":"A Light in the Darkness"},

{"fileName":"Maps_2013_01_28\/467374.parm","name":"Raid on the Capricorn"},
{"fileName":"Maps_2013_01_28\/473765.parm","name":"Arah - Story"},
{"fileName":"Maps_2013_01_28\/473930.parm","name":"The Ruined City of Arah"},
{"fileName":"Maps_2013_01_28\/506539.parm","name":"Reaper's Rumble"},
{"fileName":"Maps_2013_01_28\/506592.parm","name":"Ascent to Madness"},
{"fileName":"Maps_2013_01_28\/506670.parm","name":"Mad King's Labyrinth"},
{"fileName":"Maps_2013_01_28\/506739.parm","name":"Mad King's Clock Tower"},
{"fileName":"Maps_2013_01_28\/519839.parm","name":"Fractals of the Mists"},
{"fileName":"Maps_2013_01_28\/520479.parm","name":"Southsun Cove"},
{"fileName":"Maps_2013_01_28\/520609.parm","name":"Temple of the Silent Storm"},
{"fileName":"Maps_2013_01_28\/529718.parm","name":"Snowball Mayhem"},
{"fileName":"Maps_2013_01_28\/529896.parm","name":"Tixx's Infinirarium"},
{"fileName":"Maps_2013_01_28\/529945.parm","name":"Winter Wonderland"},
]}
,
*/

		{
			name:"Heart of Maguuma",
		 	maps:[
		 		{ fileName :"1151420.data", name:"HoT BWE3 Raid" },
				{ fileName :"969663.data", name:"Verdant Brink" },				
			]
		},

		{
			name:"Maguuma Wastes",
		 	maps:[

		 		{ fileName :"836211.data", name:"Dry top (Past)" },
		 		{ fileName :"861770.data", name:"Dry top (Present)" },

		 		{ fileName :"909361.data", name:"The Silverwastes v1" },
		 		{ fileName :"996202.data", name:"The Silverwastes v2" },
		 		
			]
		},

		{
			name:"Other",
		 	maps:[

		 		{ fileName :"184799.data", name:"Empty Plane v1" },
		 		{ fileName :"197562.data", name:"Empty Plane v2" },
		 		
		 		{ fileName :"875614.data", name:"Unknown Mists Platforms" },
		 		{ fileName :"908730.data", name:"Glint's Lair" },		 		
		 		{ fileName :"969964.data", name:"Unknown Airship in tree" },

		 		{ fileName :"wizard.data.zip", name:"Wizard's tower ZIP" },
		 		{ fileName :"WizardsTowerSanctumCayLAKrytaMap.data", name:"Wizard's tower PARM" },
		 		
		 		{ fileName :"vale.data.zip", name:"Fortunes Vale ZIP" },
		 		{ fileName :"FortunesVale.data", name:"Fortunes Vale PARM" },
			]
		},
		

		{
			name:"PvP",
		 	maps:[
		 		{ fileName :"871093.data", name:"Original Stronghold" },
		 		{ fileName :"870987.data", name:"Champion's Dusk" },
		 		{ fileName :"197249.data", name:"Heart of the Mists" },
				{ fileName :"197402.data", name:"The Battle of Khylo" },
				{ fileName :"197545.data", name:"Forest of Niflhel" },
				{ fileName :"376916.data", name:"Legacy of the Foefire" },
				{ fileName :"467374.data", name:"Raid on the Capricorn" },
				{ fileName :"520609.data", name:"Temple of the Silent Storm" },
				{ fileName :"677968.data", name:"Skyhammer" },
				{ fileName :"791564.data", name:"Courtyard" },
				{ fileName :"556199.data", name:"Spirit Watch" },


				{ fileName :"506539.data", name:"Reaper's Rumble" },
				{ fileName :"529718.data", name:"Snowball Mayhem" },
				{ fileName :"595582.data", name:"Dragon Ball Arena" },
				{ fileName :"617120.data", name:"Aspect Arena" },

			]
		},

		{
			name:"WvW",
		 	maps:[
		 		{ fileName :"195806.data", name:"Eternal Battlegrounds" },
		 		{ fileName :"641501.data", name:"Borderlands" },
		 		{ fileName :"736241.data", name:"Edge of the Mists" },
			]
		},

		{
			name:"Dungeons",
		 	maps:[
	 			{ fileName :"189364.data", name:"Ascalonian Catacombs" },
				{ fileName :"275474.data", name:"Sorrow's Embrace" },
				{ fileName :"276520.data", name:"Honor of the Waves" },
				{ fileName :"284039.data", name:"Citadel of Flame" },
				{ fileName :"287214.data", name:"Caudecus's Manor" },

				{ fileName :"291284.data", name:"Twilight Arbor (Past)" },
				{ fileName :"645968.data", name:"Twilight Arbor (Present)" },

				{ fileName :"293606.data", name:"Crucible of Eternity" },
				{ fileName :"473930.data", name:"The Ruined City of Arah" },
				{ fileName :"473765.data", name:"Arah - Story" },

				{ fileName :"580061.data", name:"Molten Facility" },
				{ fileName :"595722.data", name:"Aetherblade Retreat" },
			]

		},

		{
			name:"The Mists (PvE)",
		 	maps:[
	 			{ fileName :"519839.data", name:"Fractals of the Mists" },
	 			{ fileName :"697450.data", name:"Thaumanova Reactor" },
	 			{ fileName :"506592.data", name:"Ascent to Madness" },
				{ fileName :"506670.data", name:"Mad King's Labyrinth (Past)" },
				{ fileName :"662436.data", name:"Mad King's Labyrinth (Present)" },
				{ fileName :"506739.data", name:"Mad King's Clock Tower" },
			]

		},


		{
			name:"Shiverpeak Mountains",
		 	maps:[
	 			{ fileName :"187611.data", name:"Wayfarer Foothills" },
	 			{ fileName :"568778.data", name:"Cragstead" },
	 			{ fileName :"197122.data", name:"Hoelbrak" },
	 			{ fileName :"186397.data", name:"Snowden Drifts" },
	 			{ fileName :"275155.data", name:"Dredgehaunt Cliffs" },
	 			{ fileName :"276252.data", name:"Frostgorge Sound" },
	 			{ fileName :"277587.data", name:"Lornar's Pass" },

				{ fileName :"278717.data", name:"Timberline Falls (Past)"},
				{ fileName :"846866.data", name:"Timberline Falls (Present)" },
			]

		},

		{
			name:"Far Shiverpeaks",
		 	maps:[
				{ fileName :"295282.data", name:"Eye of the North" },
			]

		},

		{
			name:"Ascalon",
		 	maps:[
	 			{ fileName :"188591.data", name:"Plains of Ashford" },
	 			{ fileName :"190490.data", name:"Diessa Plateau" },
	 			{ fileName :"196585.data", name:"Black Citadel" },
	 			{ fileName :"280025.data", name:"Blazeridge Steppes" },
				{ fileName :"281313.data", name:"Fireheart Rise" },
				{ fileName :"282668.data", name:"Iron Marches" },
				{ fileName :"283574.data", name:"Fields of Ruin" },
			]

		},

		{
			name:"Kryta",
		 	maps:[
	 			{ fileName :"191000.data", name:"Lion's Arch"},
	 			
	 			{ fileName :"191265.data", name:"Divinity's Reach" },
	 			{ fileName :"705746.data", name:"Divinity's Reach" },
	 			{ fileName :"193081.data", name:"North of Divinity's Reach" },

	 			{ fileName :"192711.data", name:"Queensdale" },
	 			
	 			{ fileName :"194288.data", name:"Kessex Hills v1" },
	 			{ fileName :"672138.data", name:"Kessex Hills v2" },
	 			{ fileName :"861815.data", name:"Kessex Hills v3" },


	 			{ fileName :"286945.data", name:"Bloodtide Coast" },
	 			{ fileName :"287870.data", name:"Harathi Hinterlands" },
				{ fileName :"289176.data", name:"Gendarran Fields" },
				{ fileName :"295005.data", name:"Chantry of Secrets" },
				{ fileName :"294938.data", name:"Claw Island" },
				{ fileName :"520479.data", name:"Southsun Cove" },
				{ fileName :"622681.data", name:"The Crown Pavilion" },
				

				{ fileName :"679089.data", name:"Tower of Nightmares" },
			]

		},

		{
			name:"Maguuma Jungle",
		 	maps:[
	 			{ fileName :"195149.data", name:"Caledon Forest" },
				{ fileName :"195493.data", name:"Metrica Province" },
				{ fileName :"922320.data", name:"Metrica Province Instance" },
				
				

				{ fileName :"198076.data", name:"The Grove" },
				{ fileName :"198272.data", name:"Rata Sum" },
				{ fileName :"291064.data", name:"Mount Maelstrom" },
				{ fileName :"292254.data", name:"Sparkfly Fen" },
				{ fileName :"293307.data", name:"Brisban Wildlands" },

				{ fileName :"636133.data", name:"SAB Hub" },
				{ fileName :"635555.data", name:"SAB World 1" },
				{ fileName :"635960.data", name:"SAB World 2" },

				{ fileName :"606255.data", name:"Zephyr Sanctum" },
		 		{ fileName :"605983.data", name:"Sanctum Sprint" },
				{ fileName :"606030.data", name:"Basket Brawl" },
			]

		},

		{
			name:"Ruins of Orr",
		 	maps:[
		 		{ fileName :"284829.data", name:"Straits of Devastation" },
		 		{ fileName :"285089.data", name:"Malchor's Leap" },
				{ fileName :"285634.data", name:"Cursed Shore" },
				{ fileName :"295179.data", name:"Cathedral of Hidden Depths" },
				{ fileName :"295962.data", name:"A Light in the Darkness" },
			]

		},

		{
			name:"Enchanted Snow Globe",
		 	maps:[
		 		{ fileName :"529896.data", name:"Tixx's Infinirarium" },
				{ fileName :"529945.data", name:"Winter Wonderland" },
			]

		},

		{
			name:"Historical Maps",
		 	maps:[

		 		{ fileName :"814803.data", name:"Lion's Arch" },
		 		
				{ fileName :"579383.data", name:"Skyhammer" },		///		
				{ fileName :"569756.data", name:"SAB" },		///	

				{ fileName :"122695.data", name:"Empty Plane" },		///	
				{ fileName :"127888.data", name:"Diessa Plateau" },		///	

				{ fileName :"128151.data", name:"Divinity's Reach"  },		///	 DR with collapse!

				{ fileName :"129524.data", name:"Queensdale" },		///	
				{ fileName :"129834.data", name:"North of Divinity's Reach" },		///	

				{ fileName :"130970.data", name:"Kessex Hills" },		///	Pre ToN!
				{ fileName :"131235.data", name:"Eternal Battlegrounds" },		///	No JP?
				{ fileName :"131574.data", name:"Borderlands" },		///	Pre ruins

				{ fileName :"131944.data", name:"Black Citadel" },		///	
				{ fileName :"132434.data", name:"Hoelbrak" },		///	
				{ fileName :"132570.data", name:"Heart of the Mists" },		///	

				{ fileName :"132710.data", name:"The Battle of Khylo" },		///	
				{ fileName :"132837.data", name:"Forest of Niflhel" },		///	
				{ fileName :"132853.data", name:"Empty Box" },		///	
				{ fileName :"124093.data", name:"Snowden Drifts" },		///	

				{ fileName :"125199.data", name:"Wayfarer Foothills" },		///	
				{ fileName :"126118.data", name:"Plains of Ashford" },		///	
				{ fileName :"126840.data", name:"Ascalonian Catacombs" },		///	
				
			]
		},
		
	]
}